package cn.access.group.android_all_banks_pos.applicationContextHelper;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import cn.access.group.android_all_banks_pos.Utilities.Utility;

import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.viewfragments.HomeFragment;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cn.access.group.android_all_banks_pos.R;

import static android.content.ContentValues.TAG;

/**
 * 9/25/2019
 */
public class MainApplication extends Application{

    private static SweetAlertDialog pDialog;
    private static SweetAlertDialog dialog;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }


    public static void showProgressDialog(Context context,String title)
    {   if(context!=null) {
        //hideProgressDialog();
        SweetAlertDialog.DARK_STYLE = true;
        pDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(context.getResources().getColor(R.color.green));
        Window window = pDialog.getWindow();
        WindowManager.LayoutParams wlp = window != null ? window.getAttributes() : null;
        if (wlp != null) {
            wlp.gravity = Gravity.BOTTOM;
            wlp.y = 250;
        }
        if (window != null) {
            window.setAttributes(wlp);
        }
        pDialog.setTitleText(title);
        pDialog.setCancelable(false);
        pDialog.show();
        //
    }
    else{
        Utility.DEBUG_LOG("MainApplication","ShowProgress");
    }

    }

    public static void hideProgressDialog()
    {
        if (pDialog != null)
            pDialog.dismissWithAnimation();
    }
    public static void successPrintingShow(Context context, String Content)
    {
        if (dialog != null) {
            Utility.DEBUG_LOG(TAG,"hello world");
            SweetAlertDialog.DARK_STYLE = true;
            dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                    .setTitleText("Success!")
                    .setContentText("Printing Finsihed")
                    .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.transparent))
                    .setConfirmText("");
            dialog.show();
        }

    }
    public static void successPrintingHide()
    {
        if (dialog != null) {
           dialog.hide();
            DashboardContainer.backStack();;
        }
    }


}
