package cn.access.group.android_all_banks_pos.Utilities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.util.Log;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.CardRanges;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalPassword;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUrl;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUserName;

public class ComplainClosure {


    public void makeJsonObjReq(Context context, String complainId, String resolution, String officerId, String officerName, String officerUserId, String officerUserPassword) {
        RequestQueue queue = Volley.newRequestQueue(context);
//        String currentTime = new SimpleDateFormat("HHmmss", Locale.getDefault()).format(new Date());
//        String currentDate = new SimpleDateFormat("MMddyy", Locale.getDefault()).format(new Date());

        String url = PortalUrl + "/baflservicecomplaintclosure";



        Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("UserName",PortalUserName);
        postParam.put("PASSWORD",PortalPassword);
        postParam.put("mid", Constants.MERCHANT_ID);
        postParam.put("tid", Constants.TERMINAL_ID);
        postParam.put("complaintId", complainId);
        postParam.put("Resolution", resolution);
        postParam.put("fieldOfficeUid",officerUserId );
        postParam.put("fieldOfficePass", officerUserPassword);
//        postParam.put("TxnDate", td.getTxnDate());
//        postParam.put("TxnTime", td.getTxnTime());
        postParam.put("fieldOfficeId", "0");
        postParam.put("fieldOfficeName", "pos");
        Utility.DEBUG_LOG("Userparams", String.valueOf(postParam));


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParam),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        MainApplication.hideProgressDialog();
                        try {
                            DialogUtil.successDialogComplaint(context,"Response",response.getString("Reponsemessage"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Utility.DEBUG_LOG("Volley", response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Utility.DEBUG_LOG("Volley", "Error: " + error.getMessage());
                MainApplication.hideProgressDialog();
                DialogUtil.errorDialog(context,"Error!", "Connection Failed!");
                error.printStackTrace();
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        new NukeSSLCerts().nuke();

        jsonObjReq.setTag("Volley");
        // Adding request to request queue
        queue.add(jsonObjReq);

        // Cancelling request
//     if (queue!= null) {
//    queue.cancelAll(TAG);
    }
    public static class NukeSSLCerts {
        protected static final String TAG = "NukeSSLCerts";

        public static void nuke() {
            try {
                TrustManager[] trustAllCerts = new TrustManager[] {
                        new X509TrustManager() {
                            public X509Certificate[] getAcceptedIssuers() {
                                X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                                return myTrustedAnchors;
                            }

                            @Override
                            public void checkClientTrusted(X509Certificate[] certs, String authType) {}

                            @Override
                            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                        }
                };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String arg0, SSLSession arg1) {
                        return true;
                    }
                });
            } catch (Exception e) {
            }
        }
    }

}

