package cn.access.group.android_all_banks_pos.repository.model;

public class CountTxnTypeAndAmount {
    public String status;
    public Double totalAmount;
    public Integer count;

    public Double getTotalAmount() {
        return totalAmount;
    }

    public Integer getCount() {
        return count;
    }

    @Override
    public String toString() {
        return status;
    }
}
