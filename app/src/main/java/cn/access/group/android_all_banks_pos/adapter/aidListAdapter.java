package cn.access.group.android_all_banks_pos.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import java.util.List;

import cn.access.group.android_all_banks_pos.R;

public class aidListAdapter extends ArrayAdapter<String> {

    private static final String TAG = "AIDLISTADAPTER";
    private LayoutInflater inflater;
   private Context myContext;
   private List<String> newList;

   public aidListAdapter(Context context, int resource, List<String> list) {
       super(context, resource, list);
       myContext = context;
       newList = list;
       inflater = LayoutInflater.from(context);
   }

   @NonNull
   @Override
   public View getView(final int position, View view, @NonNull ViewGroup parent) {
       final ViewHolder holder;

       if (view == null) {
           holder = new ViewHolder();
           view = inflater.inflate(R.layout.row_item, null);
           holder.tvSname = view.findViewById(R.id.aid_list_item_title);
//           holder.ivIcon = view.findViewById(R.id.aid_list_item_icon);
//           holder.ivIcon.setVisibility(View.GONE);
           view.setTag(holder);
           Utility.DEBUG_LOG(TAG,"masood in aid list adapter"+" view : "+view);

       } else {
           Utility.DEBUG_LOG(TAG,"masood in aid list adapter"+" view : "+view);
           holder = (ViewHolder) view.getTag();
       }
       holder.tvSname.setText(newList.get(position));
       return view;
   }

   private class ViewHolder {
       TextView tvSname;
      // ImageView ivIcon;
   }
}