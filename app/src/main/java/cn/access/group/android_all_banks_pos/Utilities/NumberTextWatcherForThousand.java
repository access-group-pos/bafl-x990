package cn.access.group.android_all_banks_pos.Utilities;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.NumberFormat;
import java.util.StringTokenizer;

public class NumberTextWatcherForThousand implements TextWatcher {

    EditText editText;
    private int digits = 2;
    public String current = "";


    public NumberTextWatcherForThousand(EditText editText) {
        this.editText = editText;

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (!s.toString().equals(current)) {
            editText.removeTextChangedListener(this);

            String cleanString = s.toString().replaceAll("[$,.]", "");

            double parsed = Double.parseDouble(cleanString);
            String formatted = NumberFormat.getCurrencyInstance().format((parsed / 100)).replaceAll("[$]", "");

            current = formatted;
            editText.setText(formatted);
            editText.setSelection(formatted.length());

            editText.addTextChangedListener(this);
        }
    }



    @Override
    public void afterTextChanged(Editable s) {
    }
}
