package cn.access.group.android_all_banks_pos.repository.model;
@Deprecated
public class HostConfigParameters {

    private String headerType;
    private String hostIP;
    private String hostPort;
    private String tID;
    private String mID;
    private String nII;

    public String getHeaderType() {
        return headerType;
    }

    public void setHeaderType(String headerType) {
        this.headerType = headerType;
    }

    public String getHostIP() {
        return hostIP;
    }

    public void setHostIP(String hostIP) {
        this.hostIP = hostIP;
    }

    public String getHostPort() {
        return hostPort;
    }

    public void setHostPort(String hostPort) {
        this.hostPort = hostPort;
    }

    public String getTID() {
        return tID;
    }

    public void setTID(String tID) {
        this.tID = tID;
    }

    public String getMID() {
        return mID;
    }

    public void setMID(String mID) {
        this.mID = mID;
    }

    public String getNII() {
        return nII;
    }

    public void setNII(String nII) {
        this.nII = nII;
    }
}
