package cn.access.group.android_all_banks_pos.viewfragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

import static android.content.ContentValues.TAG;


public class DialogPaymentSuccessFragment extends DialogFragment {

    @BindView(R.id.transDate)
    TextView transDate;
    @BindView(R.id.transTime)
    TextView transTime;
    @BindView(R.id.merchantName)
    TextView merchantName;
    @BindView(R.id.merchantDetail)
    TextView merchantDetail;
    @BindView(R.id.transAmount)
    TextView transAmount;
    @BindView(R.id.cardNo)
    TextView cardNo;
    Unbinder unbinder;
    @BindView(R.id.card_logo)
    ImageView cardLogo;


    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.dialog_payment_success, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        Bundle bundle = getArguments();
        if (bundle != null) {
            TransactionDetail transactionDetail= (TransactionDetail) bundle.getSerializable("transactionDetail");
            merchantName.setText(Constants.MERCHANT_NAME);
            if(transactionDetail!=null) {
                if(transactionDetail.getTxnTime()!=null){
                    transTime.setText(DataFormatterUtil.formattedTime(transactionDetail.getTxnTime()));}
                if(transactionDetail.getTxnDate()!=null){
                    transDate.setText(DataFormatterUtil.formattedDate(transactionDetail.getTxnDate()));}
                Utility.DEBUG_LOG(TAG,"txntype success"+transactionDetail.getTxnType());
                if(transactionDetail.getTxnType().equals("ORBIT INQUIRY")) {
                    transAmount.setText("-");
                }
                else{
                    transAmount.setText("PKR: " + transactionDetail.getAmount());
                }
                merchantDetail.setText("RRN: " + transactionDetail.getRrn());
                cardNo.setText(" ending with ***" + transactionDetail.getCardNo().charAt(transactionDetail.getCardNo().length() - 1));
                if (transactionDetail.getCardNo().startsWith("4")) {
                    cardLogo.setImageDrawable(getResources().getDrawable(R.drawable.visa));
                    cardNo.setText("VISA CARD" + " ending with ***" + transactionDetail.getCardNo().charAt(transactionDetail.getCardNo().length() - 1));
                } else if (transactionDetail.getCardNo().startsWith("5")) {
                    cardLogo.setImageDrawable(getResources().getDrawable(R.drawable.ic_mastercard_new));
                    cardNo.setText("MASTER CARD" + " ending with ***" + transactionDetail.getCardNo().charAt(transactionDetail.getCardNo().length() - 1));
                }
                else if (transactionDetail.getCardNo().startsWith("2")) {
                    cardLogo.setImageDrawable(getResources().getDrawable(R.drawable.paypak_logo));
                    cardNo.setText("PayPak CARD" + " ending with ***" + transactionDetail.getCardNo().charAt(transactionDetail.getCardNo().length() - 1));
                }
                else if (transactionDetail.getCardNo().startsWith("6")||transactionDetail.getCardNo().startsWith("8")) {
                    cardLogo.setImageDrawable(getResources().getDrawable(R.drawable.unionpay_logo));
                    cardNo.setText("UnionPay CARD" + " ending with ***" + transactionDetail.getCardNo().charAt(transactionDetail.getCardNo().length() - 1));
                }

            }
        }
       // initCountDownTimer();
        return root_view;
    }
    // masood count down timer on payment success
//    public void initCountDownTimer() {
//        new CountDownTimer(6000, 1000) {
//            public void onTick(long millisUntilFinished) {
//            }
//
//            public void onFinish() {
//                DashboardContainer.backStack();
//                Utility.DEBUG_LOG("timer", "working");
//            }
//
//        }.start();
//    }



    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        DashboardContainer.backStack();;
    }
}
