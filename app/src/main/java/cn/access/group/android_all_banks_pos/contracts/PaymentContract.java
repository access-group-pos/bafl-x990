package cn.access.group.android_all_banks_pos.contracts;

import java.util.List;

import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

/**
 * on 10/8/2019.
 */
public interface PaymentContract {
    void showPaymentSuccessDialog(TransactionDetail transactionDetail);
    void showMultipleAid(List<String> aidList);
}
