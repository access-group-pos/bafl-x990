package cn.access.group.android_all_banks_pos.basePackets.transactionPacketGenerator;

import android.util.SparseArray;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.basePackets.hostPacketSpecs.UPIPacketSpec;
import cn.access.group.android_all_banks_pos.basePackets.hostPacketSpecs.UblPacketSpec;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import android.util.Log;

/**
 * on 11/23/2019.
 */
public class VoidPacketGenerator {
    private SparseArray<String> data8583_u_void = new SparseArray<>();
    private static final String TAG = "EMVDemo";

    public VoidPacketGenerator() {

    }


    public SparseArray<String> getVoidPacket( TransactionDetail transactionDetail) {
        SparseArray<String> data8583;
        Utility.DEBUG_LOG(TAG,"+ getVoidPacket() +");
        switch (Constants.HOST_INDEX_FROM_DB) {
            case 0:
                UblPacketSpec ublPacketSpec = new UblPacketSpec();
                Utility.DEBUG_LOG(TAG,"before ublPacketSpec.generateVoidPacket");
                data8583 = ublPacketSpec.generateVoidPacket(transactionDetail);
                data8583_u_void=data8583;
                break;
            case 1:
                UPIPacketSpec upiPacketSpec = new UPIPacketSpec();
                Utility.DEBUG_LOG(TAG,"before upiPacketSpec.generateVoidPacket");
                data8583 = upiPacketSpec.generateUpiVoidPacket(transactionDetail);
                data8583_u_void=data8583;
        }
        data8583_u_void.put(ISO8583u.F_MessageType_00, Constants.SALE_MESSAGE_TYPE);
        return data8583_u_void;
    }

}
