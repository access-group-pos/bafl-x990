package cn.access.group.android_all_banks_pos.Utilities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.dao.CardRangesDao;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.CardRanges;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.HOST_INDEX_FROM_DB;

public class Validation {

    // variable to hold context
    Context context;
//    AppDatabase appDatabase;
//    CardRangesDao cardRangesDao;
    String pan;
    boolean bl = false;
    List<List<String>> cashoutRanges = new ArrayList<List <String>>(){{

        add(Arrays.asList("62710080", "62710999"));
        add(Arrays.asList("6233810001", "6233810003"));
//
//        add(Arrays.asList("222100", "272099"));
//        add(Arrays.asList("510000", "559999"));

        add(Arrays.asList("6234020000", "6234020599"));
        add(Arrays.asList("6234051400", "6234051499"));
        add(Arrays.asList("6234021200", "6234021299"));
        add(Arrays.asList("6234050000", "6234050099"));
        add(Arrays.asList("6233811600", "6233811790"));
        add(Arrays.asList("6233811600", "6233811900"));
        add(Arrays.asList("6234021100", "6234021199"));
    }};



    public Validation() {
       // appDatabase = AppDatabase.getAppDatabase(context);
    }

    public boolean validateCardRange(String pan) {
        this.pan = pan;
        Utility.DEBUG_LOG("Validation", "In validateCardRange ");
      //  List<CardRanges> cardRangesList = null;
//        try {
//            cardRangesList = getAllCardsList();
//        } catch (ExecutionException | InterruptedException e) {
//            e.printStackTrace();
//        }
//        for(int i=0; i<cardRangesList.size(); i++) {
//            CardRanges cardRanges = cardRangesList.get(i);
//                if (cardRanges != null) {
//                    if (pan >= cardRanges.getLowerRange() && pan < cardRanges.getHigherRange()) {
//                        return true;
//                    }
//                }
//        }
        if(SharedPref.read("button_fragment","").equals("CASH OUT")){
            Utility.DEBUG_LOG("Validation", "In if condtion if it is CASHOUT ");
            for(int i = 0; i<cashoutRanges.size() ; i++){
                String pan1;
                int n=cashoutRanges.get(i).get(0).length();
                int n1=cashoutRanges.get(i).get(1).length();
                pan1 = pan.substring(0,n1);


                if(  Long.parseLong(pan1) >= Long.parseLong(cashoutRanges.get(i).get(0)) &&  Long.parseLong(pan1) <= Long.parseLong(cashoutRanges.get(i).get(1)) )
                {
                    Utility.DEBUG_LOG("Validation lower", String.valueOf(Long.parseLong(cashoutRanges.get(i).get(0))));
                    Utility.DEBUG_LOG("Validation higher", String.valueOf(Long.parseLong(cashoutRanges.get(i).get(1))));
                    bl =  true;
                }
                else {
                   // bl =  false;
                }

            }
        }
        else{
            Utility.DEBUG_LOG("Validation", "In else condtion if not CASHOUT ");
            bl =  true;
        }



       // Utility.DEBUG_LOG("Validation", String.valueOf(cardRangesList.size()));

//        if (cardRangesList.size() > 0)
//            return true;
        Utility.DEBUG_LOG("Validation", "if bl is true or false: "+bl);
        return bl;
    }

//    @SuppressLint("StaticFieldLeak")
//    private class GetCardRangesAsyncTaskq extends AsyncTask<Void, Void, List<CardRanges>> {
//        @Override
//        protected List<CardRanges> doInBackground(Void... voids) {
//
//            return appDatabase.cardRangesDao().cardRangeCheck(Integer.parseInt(pan));
//        }
//    }
//
//    public List<CardRanges> getAllCardsList() throws ExecutionException, InterruptedException {
//        return new GetCardRangesAsyncTaskq().execute().get();
//    }

}
