package cn.access.group.android_all_banks_pos.Utilities;

import android.annotation.SuppressLint;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;

import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.PrinterConfig;
import com.vfi.smartpos.deviceservice.aidl.PrinterListener;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrinterDefine;
import cn.access.group.android_all_banks_pos.repository.model.CountedTransactionItem;
import cn.access.group.android_all_banks_pos.repository.model.EmvTagValuePair;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;


/**
 * on 8/23/2019.
 */
public class ReceiptPrinter {

    private Handler handler;

    public ReceiptPrinter(Handler handler) {
        this.handler = handler;
    }

    Bundle format = new Bundle();
    Bundle formatRight = new Bundle();
    Bundle fmtAddTextInLine = new Bundle();
    Bundle formatBold = new Bundle();

    private String TAG = "ReceiptPrinter";


    //header
    public void printHeader(IPrinter printer, AssetManager assetManager, TransactionDetail transactionDetail) throws RemoteException {

        printer.setGray(7);

        byte[] buffer;
//        printer.feedLine(2);
        try {
            //
            InputStream is = assetManager.open("logo4.jpg");
            // get the size
            int size = is.available();
            // crete the array of byte
            buffer = new byte[size];
            is.read(buffer);
            // close the stream
            is.close();
        } catch (IOException e) {
            // Should never happen!
            throw new RuntimeException(e);
        }
        Bundle fmtImage = new Bundle();
        fmtImage.putInt("offset", 50);
        fmtImage.putInt("width", 400);  // bigger then actual, will print the actual
        fmtImage.putInt("height", 100); // bigger then actual, will print the actual
        printer.setGray(7);
        printer.addImage(fmtImage, buffer);
        fmtAddTextInLine.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterDefine.Font_default);
        formatBold.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterDefine.Font_Bold);
        formatRight.putString(PrinterConfig.addText.GlobalFont.BundleName, PrinterDefine.Font_default);
        format.putString(PrinterConfig.addText.GlobalFont.BundleName, PrinterDefine.Font_default);

        Utility.DEBUG_LOG("Font", PrinterDefine.Font_default);

        formatRight.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.Alignment.RIGHT);
        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
        format.putBoolean(PrinterConfig.addText.StyleBold.BundleName, PrinterConfig.addText.StyleBold.BOLD);
        format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
        //fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.SMALL_16_16 );
        printer.addText(format, Constants.MERCHANT_NAME);
        format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
        format.putBoolean(PrinterConfig.addText.StyleBold.BundleName, PrinterConfig.addText.StyleBold.NotBOLD);
        format.putFloat("scale_w", (float) 1.1);
        format.putFloat("scale_h", (float) 1.1);
        printer.addText(format, Constants.HEADER_LINE_1);
        printer.addText(format, Constants.HEADER_LINE_2);
        printer.addText(format, Constants.HEADER_LINE_3);
        format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
        format.putBoolean(PrinterConfig.addText.StyleBold.BundleName, PrinterConfig.addText.StyleBold.BOLD);
        format.putFloat("scale_w", (float) 1.3);
        format.putFloat("scale_h", (float) 1.3);
        printer.addText(format, Constants.HEADER_LINE_4);
        // left
        //printer.feedLine();

        fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.SMALL_16_16);
        format.putBoolean(PrinterConfig.addTextInLine.StyleBold.BundleName, PrinterConfig.addTextInLine.StyleBold.NotBOLD);
        formatBold.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.SMALL_16_16);
        printer.addTextInLine(formatBold, "MERCHANT ID: ", "", Constants.MERCHANT_ID, 0);
        printer.addTextInLine(formatBold, "TERMINAL ID: ", "", Constants.TERMINAL_ID, 0);
        printer.addTextInLine(formatBold, "DATE: " + DataFormatterUtil.formattedDateECR(transactionDetail.getTxnDate()), "", "TIME: " + DataFormatterUtil.formattedTimeWithSec(transactionDetail.getTxnTime()), 0);
        printer.addTextInLine(formatBold, "BATCH:" + transactionDetail.getBatchNo(), "", "INVOICE:" + transactionDetail.getInvoiceNo(), 0);
        printer.addTextInLine(formatBold, "RRN:", "", transactionDetail.getRrn(), 0);
    }

    //footer
    void printFooter(IPrinter printer, String copy, String duplicate, String txnType,TransactionDetail transactionDetail) throws RemoteException {
        fmtAddTextInLine.putBoolean(PrinterConfig.addTextInLine.StyleBold.BundleName, PrinterConfig.addTextInLine.StyleBold.NotBOLD);
        fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.SMALL_16_16);
        printer.feedLine(2);
        fmtAddTextInLine.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);

        /*
         *     |---0 NO_CVM<br>
         *     |---1 CVM_PIN<br>
         *     |---2 CVM_SIGN<br>
         *     |---3 CVM_CDCVM<br>
         */
        fmtAddTextInLine.putFloat("scale_w", (float) 1.2);
        fmtAddTextInLine.putFloat("scale_h", (float) 1.2);
        if (transactionDetail.getCVM().equals("SIGNATURE")) {
            //printer.addText(format, "SIGN __________________________");
            printer.addText(fmtAddTextInLine, "SIGN ___________________");
            printer.feedLine(2);
            printer.addText(fmtAddTextInLine, "APPROVED WITH SIGNATURE");
        } else if (transactionDetail.getCVM().equals("ONLINE_PIN")) {
            printer.addText(fmtAddTextInLine, "APPROVED WITH PIN");
        } else if (transactionDetail.getCVM().equals("OFFLINE_PIN")) {
            printer.addText(fmtAddTextInLine, "APPROVED WITH OFFLINE PIN");
        } else if (transactionDetail.getCVM().equals("NO_CVM")) {
            printer.addText(fmtAddTextInLine, "APPROVED");
        }

        fmtAddTextInLine.putBoolean(PrinterConfig.addTextInLine.StyleBold.BundleName, PrinterConfig.addTextInLine.StyleBold.BOLD);
        fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.SMALL_16_16);

//        printer.feedLine(2);
        printer.addText(format, "  ");
        if (txnType.equals("SALEIPP")) {
            //printer.addTextInLine(fmtAddTextInLine, "", "I agree to pay the above final amount according to the card/merchant issuer agreement and SBS installment payments plan's terms and condition. Please note 5% of remaining loan amount or PKR 1,000 (whichever is high) is applicable on premature termination of SBS plan", "", 0);
            format.putFloat("scale_w", (float) 1.2);
            format.putFloat("scale_h", (float) 1.2);
            printer.addText(format, "I agree to pay the above final amount");
            printer.addText(format, "according to the card/merchants issuer");
            printer.addText(format, "agreement and SBS installment payments");
            printer.addText(format, "Plan's term and conditions");
            printer.addText(format, "Please note 5% of remaining loan amount");
            printer.addText(format, "or PKR 1,000 (whichever is high) is");
            printer.addText(format, "applicable on premature termination of");
            printer.addText(format, "SBS plan.");
        }
        else if((txnType.equals("PRE AUTH")))
        {
            format.putFloat("scale_w", (float) 1.2);
            format.putFloat("scale_h", (float) 1.2);
            printer.addText(format, "Funds related to this transaction will");
            printer.addText(format, "not be debited from the cardholder");
            printer.addText(format, "account until completion of sale");
        }

        else {
            if (txnType.equals("CASH ADV")) {
                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                format.putBoolean(PrinterConfig.addText.StyleBold.BundleName, PrinterConfig.addText.StyleBold.NotBOLD);
                format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
                printer.feedLine(2);
                printer.addText(format, "ID/Passport: ___________________");
                printer.feedLine(2);
                printer.addText(format, "4 Preprinted digits: _________________");
                printer.feedLine(2);
                printer.addText(format, "Bank sign: ___________________");
                printer.feedLine(2);
            }
            format.putFloat("scale_w", (float) 1.2);
            format.putFloat("scale_h", (float) 1.2);
            printer.addText(format, "I agree to pay the above final");
            printer.addText(format, "amount according to the card/");
            printer.addText(format, "merchant issuer agreement");
            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
            format.putBoolean(PrinterConfig.addText.StyleBold.BundleName, PrinterConfig.addText.StyleBold.NotBOLD);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
        }
        // printer.feedLine(2);

        printer.addText(format, "  ");
        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);

        format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
        format.putBoolean(PrinterConfig.addText.StyleBold.BundleName, PrinterConfig.addText.StyleBold.BOLD);
        printer.addText(format, copy);
        // printer.feedLine(2);
        printer.addText(format, "  ");

            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
            format.putBoolean(PrinterConfig.addText.StyleBold.BundleName, PrinterConfig.addText.StyleBold.NotBOLD);
            printer.addText(format, "Terminal Serial # " + Constants.TERMINAL_SERIAL);

        if (duplicate.equals("")) {
//            printer.feedLine(2);
            printer.addText(format, "__________________________");
            printer.feedLine(4);
        } else {
//            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
//            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
//            format.putBoolean(PrinterConfig.addText.StyleBold.BundleName, PrinterConfig.addText.StyleBold.NotBOLD);
//            printer.addText(format, "TERMINAL SERIAL # " + Constants.TERMINAL_SERIAL);
////            printer.feedLine(2);
            printer.addText(format, duplicate);
//            printer.feedLine(2);
            printer.addText(format, "__________________________");
            printer.feedLine(4);
        }

    }

    public void printEmvLogs(IPrinter printer, AssetManager assetManager, boolean printInEmv) {
        try {
            //Header
            printer.setGray(3);
            byte[] buffer;

            printer.feedLine(2);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.LARGE_32_32);
            printer.addTextInLine(fmtAddTextInLine, "", " ****EMV LOGS**** ", "", 0);
            printer.feedLine(2);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "TVR", "", TagConstants.TVR_Cons, 1);
            printer.addTextInLine(fmtAddTextInLine, "TSI", "", TagConstants.TSI, 1);
            printer.addTextInLine(fmtAddTextInLine, "CRYPTOGRAM", "", TagConstants.Cryptogram, 1);
            printer.addTextInLine(fmtAddTextInLine, "AIP", "", TagConstants.AIP, 1);
            printer.addTextInLine(fmtAddTextInLine, "CID", "", TagConstants.CID, 1);
            printer.addTextInLine(fmtAddTextInLine, "CVM RESULT", "", TagConstants.CVM_Result, 1);
            printer.addTextInLine(fmtAddTextInLine, "TERM CAP", "", TagConstants.TERM_CAP, 1);
            printer.addTextInLine(fmtAddTextInLine, "TERM ADD CAPS", "", TagConstants.TERM_ADD, 1);
//        printer.addTextInLine(fmtAddTextInLine, "CAP", "",TagConstants.CAP, 1);
            printer.addTextInLine(fmtAddTextInLine, "CVM RESULT", "", TagConstants.CVM_Result, 1);
//        printer.addTextInLine(fmtAddTextInLine, "TAC DENIAL", "",TagConstants.TAC_Denial, 1);
//        printer.addTextInLine(fmtAddTextInLine, "TAC ONLINE", "",TagConstants.TAC_Online, 1);
//        printer.addTextInLine(fmtAddTextInLine, "TAC DEFAULT", "",TagConstants.TAC_Default, 1);
            printer.addTextInLine(fmtAddTextInLine, "" +
                    "IAC DENIAL", "", TagConstants.IAC_Denial, 1);
            printer.addTextInLine(fmtAddTextInLine, "IAC ONLINE", "", TagConstants.IAC_Online, 1);
            printer.addTextInLine(fmtAddTextInLine, "IAC DEFAULT", "", TagConstants.IAC_Default, 1);
//        if ( transactionDetail.getCardType().equals("ctls") )
            {
                printer.addTextInLine(fmtAddTextInLine, "TTQ", "", TagConstants.TTQ, 1);
                printer.addTextInLine(fmtAddTextInLine, "CTQ", "", TagConstants.CTQ, 1);
            }
            printer.feedLine(3);

            Utility.DEBUG_LOG("startPrint1", "before startPrint");
            if (printInEmv) {
                printer.startPrint(new MyListener());
            } else {
                printer.startPrint(new MyListener());
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    public void printIsoLogs(String data, IPrinter printer, AssetManager assetManager, boolean printInEmv) {
        try {
            //Header
            printer.setGray(3);
            byte[] buffer;

            printer.feedLine(2);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.SMALL_16_16);
            printer.addTextInLine(fmtAddTextInLine, "", " **** ISO **** ", "", 0);
            printer.feedLine(2);
            printer.addTextInLine(fmtAddTextInLine, "", data, "", 0);

            Utility.DEBUG_LOG("startPrint1", "before startPrint");
            if (printInEmv) {
                printer.startPrint(new MyListener());
            } else {
                printer.startPrint(new MyListener());
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    //main content method card info
    void printCardInfo(IPrinter printer, AssetManager assetManager, TransactionDetail transactionDetail, String aid, String tnxType, String balInquiry, String copy) throws RemoteException {

        printer.addTextInLine(formatBold, "AUTH ID : ", "", transactionDetail.getAuthorizationIdentificationResponseCode(), 1);
        //printer.feedLine(2);
        printer.addText(format, "  ");
        fmtAddTextInLine.putBoolean(PrinterConfig.addTextInLine.StyleBold.BundleName, PrinterConfig.addTextInLine.StyleBold.BOLD);
        fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
        printer.addTextInLine(fmtAddTextInLine, transactionDetail.getCardScheme(), "", transactionDetail.getTxnType(), 1);
        printer.addTextInLine(fmtAddTextInLine, DataFormatterUtil.maskCardNo(transactionDetail.getCardNo()), "", transactionDetail.getCardType(), 1);
        fmtAddTextInLine.putBoolean(PrinterConfig.addTextInLine.StyleBold.BundleName, PrinterConfig.addTextInLine.StyleBold.NotBOLD);
        fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.SMALL_16_16);
        printer.addTextInLine(formatBold, "Expiry : " + transactionDetail.getCardexpiry(), "", "", 1);
        if (!(transactionDetail.getCardType().equals("Magstripe")) && !(transactionDetail.getCardType().equals("Fallback"))) {
            printer.addTextInLine(formatBold, "AID : ", "", transactionDetail.getAidCode(), 0);
            Utility.DEBUG_LOG(TAG, "AID = " + transactionDetail.getAidCode());
            printer.addTextInLine(formatBold, "TVR : ", "", transactionDetail.getTvrCode(), 0);
        }
//        printer.feedLine(2);

        if (tnxType.equals("ORBIT INQUIRY")) {
            fmtAddTextInLine.putBoolean(PrinterConfig.addTextInLine.StyleBold.BundleName, PrinterConfig.addTextInLine.StyleBold.BOLD);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "Bal. Points:", "", "" + balInquiry, 0);

            printer.addText(format, "  ");
        } else {
            fmtAddTextInLine.putBoolean(PrinterConfig.addTextInLine.StyleBold.BundleName, PrinterConfig.addTextInLine.StyleBold.BOLD);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
            if (tnxType.equals("VOID") || tnxType.equals("VOID_REDEEM")) {
                if (Constants.TIP_ENABLED.equalsIgnoreCase("Y") && !tnxType.equals("REDEEM"))
                    printer.addTextInLine(fmtAddTextInLine, "Amount:", "PKR", "-" + transactionDetail.getTransactionAmount(), 0);
                else
                    printer.addTextInLine(fmtAddTextInLine, "Amount:", "PKR", "-" + transactionDetail.getAmount(), 0);

                Utility.DEBUG_LOG("transactionAmount", transactionDetail.getAmount());
                if (!tnxType.equals("REDEEM") && !tnxType.equals("VOID_REDEEM"))
                    if (Constants.TIP_ENABLED.equalsIgnoreCase("Y") && !tnxType.equals("REDEEM")) {
                        printer.addTextInLine(fmtAddTextInLine, "TIP:", "PKR", "-" + transactionDetail.getTipAmount(), 0);
                        Utility.DEBUG_LOG("transactionAmount", transactionDetail.getTipAmount());
                    }
            } else {
                if (tnxType.equals("SALEIPP")) {
                    printer.addTextInLine(fmtAddTextInLine, "Tenure:", "", transactionDetail.getSaleippMonth() + " Months", 0);
                    printer.addTextInLine(fmtAddTextInLine, "Installment:", "PKR", new DecimalFormat("0.00").format(Double.parseDouble(transactionDetail.getSaleippInstallments())), 0);
                }
                if (Constants.TIP_ENABLED.equalsIgnoreCase("Y") && !tnxType.equals("REDEEM"))
                    printer.addTextInLine(fmtAddTextInLine, "Amount:", "PKR", transactionDetail.getTransactionAmount(), 0);
                else
                    printer.addTextInLine(fmtAddTextInLine, "Amount:", "PKR", transactionDetail.getAmount(), 0);
                if (!tnxType.equals("REDEEM") && !tnxType.equals("VOID_REDEEM"))
                    if (Constants.TIP_ENABLED.equalsIgnoreCase("Y") && !tnxType.equals("REDEEM")) {
                        printer.addTextInLine(fmtAddTextInLine, "TIP : ", "PKR", transactionDetail.getTipAmount(), 0);
                    }
            }
            printer.feedLine(2);
            if (!tnxType.equals("REDEEM") && !tnxType.equals("VOID_REDEEM")) {
//                if (tnxType.equals("VOID")) {
//                    printer.addTextInLine(fmtAddTextInLine, "Total Amount:", "PKR", "-" + transactionDetail.getAmount(), 0);
////                    if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")) {
////                        printer.addTextInLine(fmtAddTextInLine, "Total Amount:", "PKR", "-" + transactionDetail.getAmount() + transactionDetail.getTipAmount(), 0);
////                    }
//                } else {
                if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")) {
                    if (tnxType.equals("VOID") || tnxType.equals("VOID_REDEEM"))
                        printer.addTextInLine(fmtAddTextInLine, "Total Amount:", "PKR", "-" + transactionDetail.getAmount(), 0);
                    else
                        printer.addTextInLine(fmtAddTextInLine, "Total Amount:", "PKR", "" + transactionDetail.getAmount(), 0);
                }
                //printer.addTextInLine(fmtAddTextInLine, "Total Amount:", "PKR", transactionDetail.getAmount(), 0);

            } else {

                if (copy.equals("CUSTOMER COPY")) {
                    formatBold.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.SMALL_16_16);

//                    printer.addTextInLine(fmtAddTextInLine, "Orbit Redeemed:", "", transactionDetail.getRedeemAmount(), 0);
                    printer.addTextInLine(formatBold, "Orbit Redeemed:", "", transactionDetail.getAmount(), 0);
                    printer.addTextInLine(formatBold, "Orbit Available:", "", transactionDetail.getRedeemBalance(), 0);
                    printer.addTextInLine(formatBold, "Reference Key:", "", transactionDetail.getRedeemKey(), 0);

                    printer.addText(format, "  ");
                }
            }
        }
        printer.addTextInLine(fmtAddTextInLine, transactionDetail.getCardHolderName(), "", "", 1);
//        if (transactionDetail.getCardType().equals("Magstripe") || transactionDetail.getCardType().equals("Fallback")) {
//            printer.feedLine(2);
//            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
//            //printer.addText(format, "SIGN :__________________________");
//        }
    }

    //SALE METHOD
    public void printSaleReceipt(IPrinter printer, AssetManager assetManager, TransactionDetail transactionDetail, String aid, String copy, String tnxType, String duplicate, String balInquiry, boolean redeemCheck) {
        Utility.DEBUG_LOG("ReceiptPrinter", "+ printSaleReceipt +");
        try {
            //Header
            printHeader(printer, assetManager, transactionDetail);
            //body
            printCardInfo(printer, assetManager, transactionDetail, aid, tnxType, balInquiry, copy);
            //footer
            printFooter(printer, copy, duplicate, tnxType,transactionDetail);
            printer.startPrint(new MyListener());
            if (Constants.isEcrEnable.equals("Y") && copy.equals("MERCHANT COPY"))
          ECR.ecrWrite(tnxType,DataFormatterUtil.formattedDateECR(transactionDetail.getTxnDate()),DataFormatterUtil.formattedTimeWithSec(transactionDetail.getTxnTime()),DataFormatterUtil.maskCardNo(transactionDetail.getCardNo()),transactionDetail.getCardType(),transactionDetail.getCardHolderName(),transactionDetail.getAmount(),transactionDetail.getRrn(),transactionDetail.getAuthorizationIdentificationResponseCode(),"00","Approved",transactionDetail.getTipAmount(),transactionDetail.getAidCode(),transactionDetail.getTvrCode(),transactionDetail.getCardScheme());
                Utility.DEBUG_LOG("startPrint1", "before startPrint");
//        printer.startPrintInEmv(new MyListener());

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

//    void printCardInfoInvoice(IPrinter printer, AssetManager assetManager, TransactionDetail transactionDetail, String aid) throws RemoteException {
//        printer.addTextInLine(fmtAddTextInLine, "AUTH ID:", "", transactionDetail.getAuthorizationIdentificationResponseCode(), 1);
//        printer.feedLine(2);
//
//        printer.addTextInLine(fmtAddTextInLine, transactionDetail.getCardScheme(), "", transactionDetail.getTxnType(), 1);
//        printer.addTextInLine(fmtAddTextInLine, DataFormatterUtil.maskCardNo(transactionDetail.getCardNo()), "", transactionDetail.getCardType(), 1);
//        printer.addTextInLine(fmtAddTextInLine, "EXP:" + transactionDetail.getCardexpiry(), "", "", 1);
//
//
//        if (transactionDetail.getTxnType().equals("Void")) {
//            printer.addTextInLine(fmtAddTextInLine, "Amount:", "PKR", "-" + transactionDetail.getAmount(), 0);
//            if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")) {
//                printer.addTextInLine(fmtAddTextInLine, "TIP:", "PKR", "-" + transactionDetail.getTipAmount(), 0);
//            }
//        } else {
//            printer.addTextInLine(fmtAddTextInLine, "Amount:", "PKR", transactionDetail.getAmount(), 0);
//            if (Constants.TIP_ENABLED.equalsIgnoreCase("Y") && (transactionDetail.getTxnType().equals("Sale") || transactionDetail.getTxnType().equals("Adjust"))) {
//                printer.addTextInLine(fmtAddTextInLine, "TIP:", "PKR", transactionDetail.getTipAmount(), 0);
//            }
//        }
//
//        printer.feedLine(3);
//        if (transactionDetail.getTxnType().equals("Void")) {
//            printer.addTextInLine(fmtAddTextInLine, "Total Amount:", "PKR", "-" + transactionDetail.getAmount(), 0);
//            if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")) {
//                printer.addTextInLine(fmtAddTextInLine, "Total Amount:", "PKR", "-" + transactionDetail.getAmount() + transactionDetail.getTipAmount(), 0);
//            }
//        } else {
//            if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")) {
//                printer.addTextInLine(fmtAddTextInLine, "Total Amount:", "PKR", "-" + transactionDetail.getAmount() + transactionDetail.getTipAmount(), 0);
//            }
//            printer.addTextInLine(fmtAddTextInLine, "Total Amount:", "PKR", transactionDetail.getAmount(), 0);
//
//        }
//
//        printer.addTextInLine(fmtAddTextInLine, "CardHolder Name", "", "", 1);
//        if (transactionDetail.getCardType().equals("Magstripe") || transactionDetail.getCardType().equals("Fallback")) {
//            printer.feedLine(2);
//
//            /*
//             *     |---0 NO_CVM<br>
//             *     |---1 CVM_PIN<br>
//             *     |---2 CVM_SIGN<br>
//             *     |---3 CVM_CDCVM<br>
//             */
//            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
//            if (Constants.CARD_AUTH_METHOD.equals("SIGNATURE")) {
//                //printer.addText(format, "SIGN _____________________");
//                printer.addTextInLine(fmtAddTextInLine, "", "SIGN ___________________", "", 0);
//                printer.feedLine(2);
//                printer.addTextInLine(fmtAddTextInLine, "", "APPROVED WITH SIGNATURE", "", 0);
//            } else if (Constants.CARD_AUTH_METHOD.equals("ONLINE_PIN")) {
//
//            } else if (Constants.CARD_AUTH_METHOD.equals("OFFLINE_PIN")) {
//                printer.addTextInLine(fmtAddTextInLine, "", "APPROVED WITH OFFLINE PIN", "", 0);
//            } else if (Constants.CARD_AUTH_METHOD.equals("NO_CVM")) {
//                printer.addTextInLine(fmtAddTextInLine, "", "APPROVED", "", 0);
//            }
////            printer.feedLine(2);
//        }
//    }

//    public void printSaleInvoiceReceipt(IPrinter printer, AssetManager assetManager, TransactionDetail transactionDetail, String aid, String copy, String duplicate) {
//        try {
//            //Header
//            printHeader(printer, assetManager, transactionDetail);
//            //body
//            printCardInfoInvoice(printer, assetManager, transactionDetail, aid);
//            //footer
//            printFooter(printer, copy, duplicate,ts);
//            //printer.startSaveCachePrint(new MyListener());
//            printer.startPrint(new MyListener());
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }
//    }

   /* public void printDetailReportReceipt(IPrinter printer, AssetManager assetManager, List<TransactionDetail> transactionDetailList , List<CountedTransactionItem> countedTransactionItemList) throws RemoteException {
        try {
            ///MainApplication.showProgressDialog(context,"printng");
            String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
            String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
            printer.setGray(3);
            byte[] buffer;
            try {

                InputStream is = assetManager.open("logo4.jpg");

                // get the size
                int size = is.available();

                // crete the array of byte
                buffer = new byte[size];
                is.read(buffer);
                // close the stream
                is.close();
            } catch (IOException e) {
                // Should never happen!
                throw new RuntimeException(e);
            }
            try
            {
            Bundle fmtImage = new Bundle();
            fmtImage.putInt("offset", 0);
            fmtImage.putInt("width",400);  // bigger then actual, will print the actual
            fmtImage.putInt("height",100); // bigger then actual, will print the actual
            printer.setGray(7);
            printer.addImage( fmtImage, buffer );
            formatRight.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.Alignment.RIGHT);
            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
            printer.addText(format, Constants.MERCHANT_NAME);
            printer.addText(format, Constants.HEADER_LINE_1);
            printer.addText(format, Constants.HEADER_LINE_2);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
            printer.addText(format,Constants.HEADER_LINE_3);
            printer.addText(format, Constants.HEADER_LINE_4);
            // left
            printer.feedLine(1);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "MERCHANT ID:", "", Constants.MERCHANT_ID, 1);
            printer.addTextInLine(fmtAddTextInLine, "TERMINAL ID:", "", Constants.TERMINAL_ID, 1);
            printer.addTextInLine(fmtAddTextInLine, "DATE:" + currentDate, "","TIME:"+currentTime,1);
            printer.addTextInLine(fmtAddTextInLine, "BATCH NO:"+ Constants.BATCH_NO, "", "", 1);
            printer.feedLine(3);

            printer.addTextInLine(fmtAddTextInLine, "","DETAIL REPORT","",0);
            printer.feedLine(2);
//        //body
            for (int i = 0; i < transactionDetailList.size(); i++) {

                    Utility.DEBUG_LOG("printDate", transactionDetailList.get(i).getTxnDate());
                    printer.addTextInLine(fmtAddTextInLine, DataFormatterUtil.formattedDate(transactionDetailList.get(i).getTxnDate()), "", DataFormatterUtil.formattedTime(transactionDetailList.get(i).getTxnTime()), 1);
                    printer.addTextInLine(fmtAddTextInLine, transactionDetailList.get(i).getBatchNo(), transactionDetailList.get(i).getTxnType(), transactionDetailList.get(i).getAuthorizationIdentificationResponseCode(), 0);
                    printer.addTextInLine(fmtAddTextInLine, DataFormatterUtil.maskCardNo(transactionDetailList.get(i).getCardNo()), transactionDetailList.get(i).getCardType(), transactionDetailList.get(i).getCardScheme(), 0);
                    printer.addTextInLine(fmtAddTextInLine, "Amount", "PKR", transactionDetailList.get(i).getAmount(), 0);
                    printer.addText(format, "________________________________");

            }
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "","FINAL SUMMARY","",0);
            printer.feedLine(2);
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "TYPE","COUNT","AMOUNT",0);
            printer.addText(format, "________________________________");
            // TRANS TYPE LOOP IT N
            int sumfinalcount=0,sumfinalamount=0;
            String finalCount,finalSum,finalstatus = "";
            String saleType="SALE";
            String saleVoid="VOID";
            String cashOut ="CASHOUT";
            String orbitRedeem = "REDEEM";
            int saleCount=0, voidCount=0, cashOutCount=0, orbitRedeemCount=0;
            double saleAmount = 0.00, voidAmount=0.00, cashOutAmount=0.00, orbitRedeemAmount=0.00;

            for(int i =0;i<countedTransactionItemList.size();i++){
                if(countedTransactionItemList.get(i).getTxnType().equals("SALE")) {
                    saleType = "SALE";
                    saleCount += countedTransactionItemList.get(i).getCount();
                    saleAmount = saleAmount + countedTransactionItemList.get(i).getTotalAmount();
//                    sumfinalamount+=Double.parseDouble(countedTransactionItemList.get(i).getTotalAmount().toString());
                }
                if(countedTransactionItemList.get(i).getTxnType().equals("VOID")) {
                    saleVoid = "VOID";
                    voidCount += countedTransactionItemList.get(i).getCount();
                    voidAmount = voidAmount + countedTransactionItemList.get(i).getTotalAmount();
//                    sumfinalamount+=Double.parseDouble(countedTransactionItemList.get(i).getTotalAmount().toString());
                }
                if(countedTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                    cashOut = "CASH OUT";
                    cashOutCount += countedTransactionItemList.get(i).getCount();
                    cashOutAmount = cashOutAmount + countedTransactionItemList.get(i).getTotalAmount();
//                    sumfinalamount+=Double.parseDouble(countedTransactionItemList.get(i).getTotalAmount().toString());
                }
                if(countedTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                    orbitRedeem = "REDEEM";
                    orbitRedeemCount += countedTransactionItemList.get(i).getCount();
                    orbitRedeemAmount = orbitRedeemAmount + countedTransactionItemList.get(i).getTotalAmount();
//                    sumfinalamount+=Double.parseDouble(countedTransactionItemList.get(i).getTotalAmount().toString());
                }
                //sumfinalcount+=countedTransactionItemList.get(i).getCount();
            }
                sumfinalcount = saleCount + cashOutCount + orbitRedeemCount;

            printer.addTextInLine(fmtAddTextInLine, saleType, String.valueOf(saleCount),String.valueOf(saleAmount),0);
            printer.addTextInLine(fmtAddTextInLine, saleVoid, String.valueOf(voidCount),String.valueOf(voidAmount),0);
            printer.addTextInLine(fmtAddTextInLine, cashOut, String.valueOf(cashOutCount),String.valueOf(cashOutAmount),0);
            printer.addTextInLine(fmtAddTextInLine, orbitRedeem, String.valueOf(orbitRedeemCount),String.valueOf(orbitRedeemAmount),0);

            finalCount= String.valueOf(sumfinalcount);
            finalSum=String.valueOf(sumfinalamount);
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "", "","",0);

            printer.addText(format, "________________________________");
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "TOTAL Txn",finalCount,"PKR: "+saleAmount,0);
            printer.feedLine(2);
            //footer
            printer.addTextInLine(fmtAddTextInLine, "","***END***","",0);
            printer.feedLine(4);
            }
            catch (Exception e){
                Utility.DEBUG_LOG("printer catch exception", String.valueOf(e));
            }

            //printer.startSaveCachePrint(new MyListener());
            printer.startPrint(new MyListener());
            //MainApplication.hideProgressDialog();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
*/ /// Shivam's code updated on Jan 15 2021 for Detail report print

    @SuppressLint("LongLogTag")
    public void printDetailReportReceipt(IPrinter printer, AssetManager assetManager, List<TransactionDetail> transactionDetailList, List<CountedTransactionItem> countedTransactionItemList) throws RemoteException {
        try {
            ///MainApplication.showProgressDialog(context,"printng");
            String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
            String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
            printer.setGray(3);
            byte[] buffer;
            try {

                InputStream is = assetManager.open("logo4.jpg");

                // get the size
                int size = is.available();

                // crete the array of byte
                buffer = new byte[size];
                is.read(buffer);
                // close the stream
                is.close();
            } catch (IOException e) {
                // Should never happen!
                throw new RuntimeException(e);
            }
            try {
                Bundle fmtImage = new Bundle();
                fmtImage.putInt("offset", 0);
                fmtImage.putInt("width", 400);  // bigger then actual, will print the actual
                fmtImage.putInt("height", 100); // bigger then actual, will print the actual
                printer.setGray(7);
                printer.addImage(fmtImage, buffer);
                formatRight.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
                fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
                printer.addText(format, Constants.MERCHANT_NAME);
                printer.addText(format, Constants.HEADER_LINE_1);
                printer.addText(format, Constants.HEADER_LINE_2);
                format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
                printer.addText(format, Constants.HEADER_LINE_3);
                printer.addText(format, Constants.HEADER_LINE_4);
                // left
                printer.feedLine(1);
                fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
                printer.addTextInLine(fmtAddTextInLine, "MERCHANT ID:", "", Constants.MERCHANT_ID, 1);
                printer.addTextInLine(fmtAddTextInLine, "TERMINAL ID:", "", Constants.TERMINAL_ID, 1);
                printer.addTextInLine(fmtAddTextInLine, "DATE:" + currentDate, "", "TIME:" + currentTime, 1);
                printer.addTextInLine(fmtAddTextInLine, "BATCH NO:" + Constants.BATCH_NO, "", "", 1);
                printer.feedLine(3);

                printer.addTextInLine(fmtAddTextInLine, "", "DETAIL REPORT", "", 0);
                printer.feedLine(2);
//        //body
                for (int i = 0; i < transactionDetailList.size(); i++) {

                    Utility.DEBUG_LOG("printDate", transactionDetailList.get(i).getTxnDate());
                    printer.addTextInLine(fmtAddTextInLine, DataFormatterUtil.formattedDate(transactionDetailList.get(i).getTxnDate()), "", DataFormatterUtil.formattedTime(transactionDetailList.get(i).getTxnTime()), 1);
                    printer.addTextInLine(fmtAddTextInLine, transactionDetailList.get(i).getBatchNo(), transactionDetailList.get(i).getTxnType(), transactionDetailList.get(i).getAuthorizationIdentificationResponseCode(), 0);
                    printer.addTextInLine(fmtAddTextInLine, DataFormatterUtil.maskCardNo(transactionDetailList.get(i).getCardNo()), transactionDetailList.get(i).getCardType(), transactionDetailList.get(i).getCardScheme(), 0);

                    if (transactionDetailList.get(i).getTipAmount() != null) {
                        printer.addTextInLine(fmtAddTextInLine, "Amount", "PKR", transactionDetailList.get(i).getTransactionAmount(), 0);
                        printer.addTextInLine(fmtAddTextInLine, "TIP", "PKR", transactionDetailList.get(i).getTipAmount(), 0);
                    } else {
                        printer.addTextInLine(fmtAddTextInLine, "Amount", "PKR", transactionDetailList.get(i).getAmount(), 0);
                    }
                    printer.addText(format, "________________________________");

                }
                fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
                printer.addTextInLine(fmtAddTextInLine, "", "FINAL SUMMARY", "", 0);
                printer.feedLine(2);
                fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
                printer.addTextInLine(fmtAddTextInLine, "TYPE", "COUNT", "AMOUNT", 0);
                printer.addText(format, "________________________________");
                // TRANS TYPE LOOP IT N
                int sumfinalcount = 0;
                double sumfinalamount = 0.00;
                String finalCount, finalSum, finalstatus = "";
                String saleType = "SALE";
                String saleVoid = "VOID";
                String cashOut = "CASHOUT";
                String orbitRedeem = "REDEEM";
                int saleCount = 0, voidCount = 0, cashOutCount = 0, orbitRedeemCount = 0;
                double saleAmountStr = 0.00;
                double voidAmountStr = 0.00;
                double cashOutAmountStr = 0.00;
                double orbitRedeemAmountStr = 0.00;

                for (int i = 0; i < countedTransactionItemList.size(); i++) {
                    if (countedTransactionItemList.get(i).getTxnType().equals("SALE") || countedTransactionItemList.get(i).getTxnType().equals("SALEIPP") || countedTransactionItemList.get(i).getTxnType().equals("ADJUST")) {
                        saleType = "SALE";
                        saleCount += countedTransactionItemList.get(i).getCount();
                        //  saleAmount = saleAmount + countedTransactionItemList.get(i).getTotalAmount();
                        // Utility.DEBUG_LOG("SL saleAmount",(new DecimalFormat("##.##").format(countedTransactionItemList.get(i).getTotalAmount())));
                        saleAmountStr = saleAmountStr + countedTransactionItemList.get(i).getTotalAmount();
                        Utility.DEBUG_LOG("Detail report", String.valueOf(saleAmountStr));
                    }
                    if (countedTransactionItemList.get(i).getTxnType().equals("VOID") || countedTransactionItemList.get(i).getTxnType().equals("VOID_REDEEM")) {
                        saleVoid = "VOID";
                        voidCount += countedTransactionItemList.get(i).getCount();
                        //voidAmount = voidAmount + countedTransactionItemList.get(i).getTotalAmount();
                        voidAmountStr = voidAmountStr + (countedTransactionItemList.get(i).getTotalAmount());
                    }
                    if (countedTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        cashOut = "CASH OUT";
                        cashOutCount += countedTransactionItemList.get(i).getCount();
                        // cashOutAmount = cashOutAmount + countedTransactionItemList.get(i).getTotalAmount();
                        cashOutAmountStr = cashOutAmountStr + countedTransactionItemList.get(i).getTotalAmount();
                    }
                    if (countedTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        orbitRedeem = "REDEEM";
                        orbitRedeemCount += countedTransactionItemList.get(i).getCount();
                        //  orbitRedeemAmount = orbitRedeemAmount + countedTransactionItemList.get(i).getTotalAmount();
                        orbitRedeemAmountStr = orbitRedeemAmountStr + countedTransactionItemList.get(i).getTotalAmount();
                    }


                    if (!(countedTransactionItemList.get(i).getTxnType().equals("VOID") || countedTransactionItemList.get(i).getTxnType().equals("VOID_REDEEM"))) {
                        sumfinalcount += countedTransactionItemList.get(i).getCount();
                        sumfinalamount += countedTransactionItemList.get(i).getTotalAmount();
                    }
                }
                Utility.DEBUG_LOG("summaryTotalAmount", String.valueOf((new DecimalFormat("0.00").format(sumfinalamount))));


                printer.addTextInLine(fmtAddTextInLine, saleType, String.valueOf(saleCount), (new DecimalFormat("0.00").format(saleAmountStr)), 0);
                printer.addTextInLine(fmtAddTextInLine, saleVoid, String.valueOf(voidCount), (new DecimalFormat("0.00").format(voidAmountStr)), 0);
                printer.addTextInLine(fmtAddTextInLine, cashOut, String.valueOf(cashOutCount), (new DecimalFormat("0.00").format(cashOutAmountStr)), 0);
                printer.addTextInLine(fmtAddTextInLine, orbitRedeem, String.valueOf(orbitRedeemCount), (new DecimalFormat("0.00").format(orbitRedeemAmountStr)), 0);

                finalCount = String.valueOf(sumfinalcount);
                finalSum = String.valueOf(new DecimalFormat("0.00").format((sumfinalamount)));
                fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
                printer.addTextInLine(fmtAddTextInLine, "", "", "", 0);

                printer.addText(format, "________________________________");
                format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
                printer.addTextInLine(fmtAddTextInLine, "TOTAL Txn", finalCount, "PKR: " + finalSum, 0);
                printer.feedLine(3);
                printer.addText(format, "TERMINAL SERIAL # " + Constants.TERMINAL_SERIAL);
                printer.feedLine(2);
                //footer
                printer.addTextInLine(fmtAddTextInLine, "", "***END***", "", 0);
                printer.feedLine(4);
            } catch (Exception e) {
                Utility.DEBUG_LOG("printer catch exception", String.valueOf(e));
            }

            //printer.startSaveCachePrint(new MyListener());
            Utility.DEBUG_LOG("startPrint3", "before startPrint");
            printer.startPrint(new MyListener());
            //MainApplication.hideProgressDialog();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void printSummaryReport(IPrinter printer, AssetManager assetManager, List<CountedTransactionItem> cardTransactionItemList) throws RemoteException {
        try {
            String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
            String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
            printer.setGray(3);
            byte[] buffer;
            try {

                InputStream is = assetManager.open("logo4.jpg");

                // get the size
                int size = is.available();

                // crete the array of byte
                buffer = new byte[size];
                is.read(buffer);
                // close the stream
                is.close();
            } catch (IOException e) {
                // Should never happen!
                throw new RuntimeException(e);
            }
            Bundle fmtImage = new Bundle();
            fmtImage.putInt("offset", 0);
            fmtImage.putInt("width", 400);  // bigger then actual, will print the actual
            fmtImage.putInt("height", 100); // bigger then actual, will print the actual
            printer.setGray(7);
            printer.addImage(fmtImage, buffer);
            formatRight.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.Alignment.RIGHT);
            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
            printer.addText(format, Constants.MERCHANT_NAME);
            printer.addText(format, Constants.HEADER_LINE_1);
            printer.addText(format, Constants.HEADER_LINE_2);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
            printer.addText(format, Constants.HEADER_LINE_3);
            printer.addText(format, Constants.HEADER_LINE_4);
            // left
            printer.feedLine(1);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "MERCHANT ID:", "", Constants.MERCHANT_ID, 1);
            printer.addTextInLine(fmtAddTextInLine, "TERMINAL ID:", "", Constants.TERMINAL_ID, 1);
            printer.addTextInLine(fmtAddTextInLine, "DATE:" + currentDate, "", "TIME:" + currentTime, 1);
            printer.addTextInLine(fmtAddTextInLine, "BATCH NO:" + Constants.BATCH_NO, "", "", 1);
            printer.feedLine(3);
//        //body
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "", "SUMMARY REPORT", "", 0);
            printer.feedLine(2);
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "CARD", "COUNT", "AMOUNT", 0);
            printer.addText(format, "________________________________");
            // TRANS TYPE LOOP IT N
            String visaSaleType = "VISA";
            String visaCashOut = "VISA CASH";
            String visaOrbitRedeem = "VISA REDEEM";
            int visaSaleCount = 0, visaCashOutCount = 0, visaOrbitRedeemCount = 0;
            double visaSaleAmount = 0.00, visaCashOutAmount = 0.00, visaOrbitRedeemAmount = 0.00;
            String upiSaleType = "UPI";
            String upiCashOut = "UPI CASH";
            String upiOrbitRedeem = "UPI REDEEM";
            int upiSaleCount = 0, upiCashOutCount = 0, upiOrbitRedeemCount = 0;
            double upiSaleAmount = 0.00, upiCashOutAmount = 0.00, upiOrbitRedeemAmount = 0.00;
            String mcSaleType = "MASTER CARD";
            String mcCashOut = "MASTER CASH";
            String mcOrbitRedeem = "MASTER REDEEM";
            int mcSaleCount = 0, mcCashOutCount = 0, mcOrbitRedeemCount = 0, netSaleCount = 0;
            double mcSaleAmount = 0.00, mcCashOutAmount = 0.00, mcOrbitRedeemAmount = 0.00, netSale = 0.00;
            String payPakSaleType = "PAYPAK CARD";
            String payPakCashOut = "PAYPAK CASH";
            String payPakOrbitRedeem = "PAYPAK REDEEM";
            int payPakSaleCount = 0, payPakCashOutCount = 0, payPakOrbitRedeemCount = 0;
            double payPakSaleAmount = 0.00, payPakCashOutAmount = 0.00, payPakOrbitRedeemAmount = 0.00;
            String jcbSaleType = "JCB CARD";
            String jcbCashOut = "JCB CASH";
            String jcbOrbitRedeem = "JCB REDEEM";
            int jcbSaleCount = 0, jcbCashOutCount = 0, jcbOrbitRedeemCount = 0;
            double jcbSaleAmount = 0.00, jcbCashOutAmount = 0.00, jcbOrbitRedeemAmount = 0.00;
            String amexSaleType = "AMEX CARD";
            String amexCashOut = "AMEX CASH";
            String amexOrbitRedeem = "AMEX REDEEM";
            int amexSaleCount = 0, amexCashOutCount = 0, amexOrbitRedeemCount = 0;
            double amexSaleAmount = 0.00, amexCashOutAmount = 0.00, amexOrbitRedeemAmount = 0.00;
            DecimalFormat df2 = new DecimalFormat("0.00");

            int sumcardcount = 0;
            float sumcardamount = 0;
            String cardCount, cardSum;
            Boolean visa = false, upi = false, master = false, paypak = false, jcb = false, amex = false;

            for (int i = 0; i < cardTransactionItemList.size(); i++) {
                if (cardTransactionItemList.get(i).toString().equals("VISA")) {
                    visa = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        visaSaleCount += cardTransactionItemList.get(i).getCount();
                        visaSaleAmount = visaSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        visaCashOutCount += cardTransactionItemList.get(i).getCount();
                        visaCashOutAmount = visaCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        visaOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        visaOrbitRedeemAmount = visaOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                } else if (cardTransactionItemList.get(i).toString().equals("UNIONPAY")) {
                    upi = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        upiSaleCount += cardTransactionItemList.get(i).getCount();
                        upiSaleAmount = upiSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        upiCashOutCount += cardTransactionItemList.get(i).getCount();
                        upiCashOutAmount = upiCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        upiOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        upiOrbitRedeemAmount = upiOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }

                } else if (cardTransactionItemList.get(i).toString().equals("MASTER")) {
                    master = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        mcSaleCount += cardTransactionItemList.get(i).getCount();
                        mcSaleAmount = mcSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        mcCashOutCount += cardTransactionItemList.get(i).getCount();
                        mcCashOutAmount = mcCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        mcOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        mcOrbitRedeemAmount = mcOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }

                } else if (cardTransactionItemList.get(i).toString().equals("PAYPAK")) {
                    paypak = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        payPakSaleCount += cardTransactionItemList.get(i).getCount();
                        payPakSaleAmount = payPakSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        payPakCashOutCount += cardTransactionItemList.get(i).getCount();
                        payPakCashOutAmount = payPakCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        payPakOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        payPakOrbitRedeemAmount = payPakOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                } else if (cardTransactionItemList.get(i).toString().equals("JCB")) {
                    jcb = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        jcbSaleCount += cardTransactionItemList.get(i).getCount();
                        jcbSaleAmount = jcbSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        jcbCashOutCount += cardTransactionItemList.get(i).getCount();
                        jcbCashOutAmount = jcbCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        jcbOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        jcbOrbitRedeemAmount = jcbOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                } else if (cardTransactionItemList.get(i).toString().equals("AMEX")) {
                    amex = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        amexSaleCount += cardTransactionItemList.get(i).getCount();
                        amexSaleAmount = amexSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        amexCashOutCount += cardTransactionItemList.get(i).getCount();
                        amexCashOutAmount = amexCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        amexOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        amexOrbitRedeemAmount = amexOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                }


                if (!(cardTransactionItemList.get(i).getTxnType().equals("VOID")) && !(cardTransactionItemList.get(i).getTxnType().equals("VOID_REDEEM"))) {
                    sumcardcount += cardTransactionItemList.get(i).getCount();
                    sumcardamount += Double.parseDouble(cardTransactionItemList.get(i).getTotalAmount().toString());
                }
            }
            Utility.DEBUG_LOG("summaryTotalAmount", String.valueOf(sumcardcount));
            if (visa) {
                printer.addTextInLine(fmtAddTextInLine, visaSaleType, String.valueOf(visaSaleCount), String.valueOf(df2.format(visaSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, visaCashOut, String.valueOf(visaCashOutCount), String.valueOf(df2.format(visaCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, visaOrbitRedeem, String.valueOf(visaOrbitRedeemCount), String.valueOf(df2.format(visaOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            if (upi) {
                printer.addTextInLine(fmtAddTextInLine, upiSaleType, String.valueOf(upiSaleCount), String.valueOf(df2.format(upiSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, upiCashOut, String.valueOf(upiCashOutCount), String.valueOf(df2.format(upiCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, upiOrbitRedeem, String.valueOf(upiOrbitRedeemCount), String.valueOf(df2.format(upiOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            if (master) {
                printer.addTextInLine(fmtAddTextInLine, mcSaleType, String.valueOf(mcSaleCount), String.valueOf(df2.format(mcSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, mcCashOut, String.valueOf(mcCashOutCount), String.valueOf(df2.format(mcCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, mcOrbitRedeem, String.valueOf(mcOrbitRedeemCount), String.valueOf(df2.format(mcOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            if (paypak) {
                printer.addTextInLine(fmtAddTextInLine, payPakSaleType, String.valueOf(payPakSaleCount), String.valueOf(df2.format(payPakSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, payPakCashOut, String.valueOf(payPakCashOutCount), String.valueOf(df2.format(payPakCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, payPakOrbitRedeem, String.valueOf(payPakOrbitRedeemCount), String.valueOf(df2.format(payPakOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            if (jcb) {
                printer.addTextInLine(fmtAddTextInLine, jcbSaleType, String.valueOf(jcbSaleCount), String.valueOf(df2.format(jcbSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, jcbCashOut, String.valueOf(jcbCashOutCount), String.valueOf(df2.format(jcbCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, jcbOrbitRedeem, String.valueOf(jcbOrbitRedeemCount), String.valueOf(df2.format(jcbOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            if (amex) {
                printer.addTextInLine(fmtAddTextInLine, amexSaleType, String.valueOf(amexSaleCount), String.valueOf(df2.format(amexSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, amexCashOut, String.valueOf(amexCashOutCount), String.valueOf(df2.format(amexCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, amexOrbitRedeem, String.valueOf(amexOrbitRedeemCount), String.valueOf(df2.format(amexOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            netSale = mcSaleAmount + visaSaleAmount + upiSaleAmount + payPakSaleAmount + jcbSaleAmount + amexSaleAmount;
            netSaleCount = mcSaleCount + visaSaleCount + upiSaleCount + payPakSaleCount + jcbSaleCount + amexSaleCount;
            printer.feedLine(2);
            cardCount = String.valueOf(sumcardcount);
            cardSum = String.valueOf(sumcardamount);
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "", "", "", 0);
            printer.feedLine(1);

            /////////////

            //TOTAL NET SALES
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
            printer.addTextInLine(fmtAddTextInLine, "TOTAL SALES", String.valueOf(sumcardcount), String.valueOf(df2.format(sumcardamount)), 0);
            printer.feedLine(1);
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);

            printer.addText(format, "________________________________");
            printer.feedLine(3);
            printer.addText(format, "TERMINAL SERIAL # " + Constants.TERMINAL_SERIAL);
            printer.feedLine(2);
            //footer
            printer.addTextInLine(fmtAddTextInLine, "", "***END***", "", 0);
            printer.feedLine(4);

            //printer.startSaveCachePrint(new MyListener());
            Utility.DEBUG_LOG("startPrint4", "before startPrint");
            printer.startPrint(new MyListener());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }






   /* public void printSettlementReceipt(IPrinter printer, AssetManager assetManager,  String transType,
                                       List<TransactionDetail> transactionDetailList,
                                       List<CountedTransactionItem> cardTransactionItemList, boolean isAudit)
    {
        try {

            String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
            String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
            printer.setGray(3);
            byte[] buffer;
            try {

                InputStream is = assetManager.open("logo4.jpg");

                // get the size
                int size = is.available();

                // crete the array of byte
                buffer = new byte[size];
                is.read(buffer);
                // close the stream
                is.close();
            } catch (IOException e) {
                // Should never happen!
                throw new RuntimeException(e);
            }
            Bundle fmtImage = new Bundle();
            fmtImage.putInt("offset", 0);
            fmtImage.putInt("width",400);  // bigger then actual, will print the actual
            fmtImage.putInt("height",100); // bigger then actual, will print the actual
            printer.setGray(7);
            printer.addImage( fmtImage, buffer );
            formatRight.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.Alignment.RIGHT);
            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
            printer.addText(format, Constants.MERCHANT_NAME);
            printer.addText(format, Constants.HEADER_LINE_1);
            printer.addText(format, Constants.HEADER_LINE_2);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
            printer.addText(format,Constants.HEADER_LINE_3);
            printer.addText(format, Constants.HEADER_LINE_4);
            // left
            printer.feedLine(1);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "MERCHANT ID:", "", Constants.MERCHANT_ID, 1);
            printer.addTextInLine(fmtAddTextInLine, "TERMINAL ID:", "", Constants.TERMINAL_ID, 1);
            printer.addTextInLine(fmtAddTextInLine, "DATE:" + currentDate, "","TIME:"+currentTime,1);
            printer.addTextInLine(fmtAddTextInLine, "BATCH NO:"+ Constants.BATCH_NO, "", "", 1);
            printer.feedLine(3);
//
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "","SETTLEMENT REPORT","",0);
            printer.addTextInLine(fmtAddTextInLine, "","SUCCESSFULL","",0);
            printer.feedLine(2);
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "CARD","COUNT","AMOUNT",0);
            printer.addText(format, "________________________________");
            // TRANS TYPE LOOP IT N
            String visaSaleType="VISA";
            String visaCashOut ="VISA CASH";
            String visaOrbitRedeem = "VISA REDEEM";
            int visaSaleCount=0, visaCashOutCount=0, visaOrbitRedeemCount=0;
            double visaSaleAmount = 0.00, visaCashOutAmount=0.00, visaOrbitRedeemAmount=0.00;
            String upiSaleType="UPI";
            String upiCashOut ="UPI CASH";
            String upiOrbitRedeem = "UPI REDEEM";
            int upiSaleCount=0, upiCashOutCount=0, upiOrbitRedeemCount=0;
            double upiSaleAmount = 0.00, upiCashOutAmount=0.00, upiOrbitRedeemAmount=0.00;
            String mcSaleType="MASTER CARD";
            String mcCashOut ="MASTER CASH";
            String mcOrbitRedeem = "MASTER REDEEM";
            int mcSaleCount=0, mcCashOutCount=0, mcOrbitRedeemCount=0, netSaleCount=0;
            double mcSaleAmount = 0.00, mcCashOutAmount=0.00, mcOrbitRedeemAmount=0.00, netSale=0.00;
            DecimalFormat df2 = new DecimalFormat("#.##");

            float sumcardcount=0,sumcardamount=0;
            String cardCount,cardSum;
            Boolean visa= false, upi = false, master=false;

            for(int i =0;i<cardTransactionItemList.size();i++){
                if(cardTransactionItemList.get(i).toString().equals("VISA")) {
                    visa = true;
                    if(cardTransactionItemList.get(i).getTxnType().equals("SALE")) {
                        visaSaleCount += cardTransactionItemList.get(i).getCount();
                        visaSaleAmount = visaSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        visaCashOutCount += cardTransactionItemList.get(i).getCount();
                        visaCashOutAmount = visaCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        visaOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        visaOrbitRedeemAmount = visaOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                }
                else if(cardTransactionItemList.get(i).toString().equals("UNIONPAY")) {
                    upi = true;
                    if(cardTransactionItemList.get(i).getTxnType().equals("SALE")) {
                        upiSaleCount += cardTransactionItemList.get(i).getCount();
                        upiSaleAmount = upiSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        upiCashOutCount += cardTransactionItemList.get(i).getCount();
                        upiCashOutAmount = upiCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        upiOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        upiOrbitRedeemAmount = upiOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }

                }
                else if(cardTransactionItemList.get(i).toString().equals("MASTER")) {
                    master = true;
                    if(cardTransactionItemList.get(i).getTxnType().equals("SALE")) {
                        mcSaleCount += cardTransactionItemList.get(i).getCount();
                        mcSaleAmount = mcSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        mcCashOutCount += cardTransactionItemList.get(i).getCount();
                        mcCashOutAmount = mcCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        mcOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        mcOrbitRedeemAmount = mcOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }

                }

                sumcardcount += cardTransactionItemList.get(i).getCount();
                sumcardamount += Double.parseDouble(cardTransactionItemList.get(i).getTotalAmount().toString());
            }
            if(visa) {
                printer.addTextInLine(fmtAddTextInLine, visaSaleType, String.valueOf(visaSaleCount), String.valueOf(df2.format(visaSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, visaCashOut, String.valueOf(visaCashOutCount), String.valueOf(df2.format(visaCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, visaOrbitRedeem, String.valueOf(visaOrbitRedeemCount), String.valueOf(df2.format(visaOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            if(upi) {
                printer.addTextInLine(fmtAddTextInLine, upiSaleType, String.valueOf(upiSaleCount), String.valueOf(df2.format(upiSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, upiCashOut, String.valueOf(upiCashOutCount), String.valueOf(df2.format(upiCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, upiOrbitRedeem, String.valueOf(upiOrbitRedeemCount), String.valueOf(df2.format(upiOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            if(master) {
                printer.addTextInLine(fmtAddTextInLine, mcSaleType, String.valueOf(mcSaleCount), String.valueOf(df2.format(mcSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, mcCashOut, String.valueOf(mcCashOutCount), String.valueOf(df2.format(mcCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, mcOrbitRedeem, String.valueOf(mcOrbitRedeemCount), String.valueOf(df2.format(mcOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            netSale = mcSaleAmount + visaSaleAmount+ upiSaleAmount;
            netSaleCount = mcSaleCount + visaSaleCount + upiSaleCount;
            printer.feedLine(2);
            cardCount= String.valueOf(sumcardcount);
            cardSum=String.valueOf(sumcardamount);
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "", "","",0);
            printer.feedLine(1);

            /////////////

            //TOTAL NET SALES
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
            printer.addTextInLine(fmtAddTextInLine, "TOTAL SALES", String.valueOf(netSaleCount), String.valueOf(df2.format(netSale)),0);
            printer.feedLine(3);
            printer.addTextInLine(fmtAddTextInLine, "","DETAIL REPORT IN CHRONOLOGICAL ORDER","",0);
            printer.feedLine(2);
//        //body
            for (int i = 0; i < transactionDetailList.size(); i++) {
                Utility.DEBUG_LOG("printDate",transactionDetailList.get(i).getTxnDate());
                printer.addTextInLine(fmtAddTextInLine, DataFormatterUtil.formattedDate(transactionDetailList.get(i).getTxnDate()), "", DataFormatterUtil.formattedTime(transactionDetailList.get(i).getTxnTime()), 1);
                printer.addTextInLine(fmtAddTextInLine, transactionDetailList.get(i).getBatchNo(), transactionDetailList.get(i).getTxnType(), transactionDetailList.get(i).getAuthorizationIdentificationResponseCode(), 0);
                printer.addTextInLine(fmtAddTextInLine, DataFormatterUtil.maskCardNo(transactionDetailList.get(i).getCardNo()), transactionDetailList.get(i).getCardType(), transactionDetailList.get(i).getCardScheme(), 0);
                printer.addTextInLine(fmtAddTextInLine, "Amount", "PKR", transactionDetailList.get(i).getAmount(), 0);
                printer.addText(format, "________________________________");
            }
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "","FINAL SUMMARY","",0);
            printer.feedLine(2);
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "TYPE","COUNT","AMOUNT",0);
            printer.addText(format, "________________________________");
            // TRANS TYPE LOOP IT N
            int sumfinalcount=0,sumfinalamount=0;
            String finalCount,finalSum,finalstatus = "";
            String saleType="SALE";
            String saleVoid="VOID";
            String cashOut ="CASHOUT";
            String orbitRedeem = "REDEEM";
            int saleCount=0, voidCount=0, cashOutCount=0, orbitRedeemCount=0;
            double saleAmount = 0.00, voidAmount=0.00, cashOutAmount=0.00, orbitRedeemAmount=0.00;

            for(int i =0;i<cardTransactionItemList.size();i++){
                if(cardTransactionItemList.get(i).getTxnType().equals("SALE")) {
                    saleType = "SALE";
                    saleCount += cardTransactionItemList.get(i).getCount();
                    saleAmount = saleAmount + cardTransactionItemList.get(i).getTotalAmount();
//                    sumfinalamount+=Double.parseDouble(countedTransactionItemList.get(i).getTotalAmount().toString());
                }
                if(cardTransactionItemList.get(i).getTxnType().equals("VOID")) {
                    saleVoid = "VOID";
                    voidCount += cardTransactionItemList.get(i).getCount();
                    voidAmount = voidAmount + cardTransactionItemList.get(i).getTotalAmount();
//                    sumfinalamount+=Double.parseDouble(countedTransactionItemList.get(i).getTotalAmount().toString());
                }
                if(cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                    cashOut = "CASH OUT";
                    cashOutCount += cardTransactionItemList.get(i).getCount();
                    cashOutAmount = cashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
//                    sumfinalamount+=Double.parseDouble(countedTransactionItemList.get(i).getTotalAmount().toString());
                }
                if(cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                    orbitRedeem = "REDEEM";
                    orbitRedeemCount += cardTransactionItemList.get(i).getCount();
                    orbitRedeemAmount = orbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
//                    sumfinalamount+=Double.parseDouble(countedTransactionItemList.get(i).getTotalAmount().toString());
                }

                sumfinalcount = saleCount + cashOutCount + orbitRedeemCount;


            }
            printer.addTextInLine(fmtAddTextInLine, saleType, String.valueOf(saleCount),String.valueOf(saleAmount),0);
            printer.addTextInLine(fmtAddTextInLine, saleVoid, String.valueOf(voidCount),String.valueOf(voidAmount),0);
            printer.addTextInLine(fmtAddTextInLine, cashOut, String.valueOf(cashOutCount),String.valueOf(cashOutAmount),0);
            printer.addTextInLine(fmtAddTextInLine, orbitRedeem, String.valueOf(orbitRedeemCount),String.valueOf(orbitRedeemAmount),0);

            finalCount= String.valueOf(sumfinalcount);
            finalSum=String.valueOf(sumfinalamount);
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "", "","",0);

            printer.addText(format, "________________________________");
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "TOTAL Txn",finalCount,"PKR: "+saleAmount,0);
            printer.feedLine(2);
            //footer
            printer.addTextInLine(fmtAddTextInLine, "","***END***","",0);


            printer.feedLine(4);

        printer.startPrint(new MyListener());
    } catch (RemoteException e) {
        e.printStackTrace();
    }
    }
*/  /// Shivam's code update on 15 Jan 2021 for Settlement report print

    public void printSettlementReceipt(IPrinter printer, AssetManager assetManager, String transType,
                                       List<TransactionDetail> transactionDetailList,
                                       List<CountedTransactionItem> cardTransactionItemList, boolean isAudit) {
        try {

            String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
            String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
            printer.setGray(3);
            byte[] buffer;
            try {

                InputStream is = assetManager.open("logo4.jpg");

                // get the size
                int size = is.available();

                // crete the array of byte
                buffer = new byte[size];
                is.read(buffer);
                // close the stream
                is.close();
            } catch (IOException e) {
                // Should never happen!
                throw new RuntimeException(e);
            }
            Bundle fmtImage = new Bundle();
            fmtImage.putInt("offset", 0);
            fmtImage.putInt("width", 400);  // bigger then actual, will print the actual
            fmtImage.putInt("height", 100); // bigger then actual, will print the actual
            printer.setGray(7);
            printer.addImage(fmtImage, buffer);
            formatRight.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.Alignment.RIGHT);
            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
            printer.addText(format, Constants.MERCHANT_NAME);
            printer.addText(format, Constants.HEADER_LINE_1);
            printer.addText(format, Constants.HEADER_LINE_2);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
            printer.addText(format, Constants.HEADER_LINE_3);
            printer.addText(format, Constants.HEADER_LINE_4);
            // left
            printer.feedLine(1);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "MERCHANT ID:", "", Constants.MERCHANT_ID, 1);
            printer.addTextInLine(fmtAddTextInLine, "TERMINAL ID:", "", Constants.TERMINAL_ID, 1);
            printer.addTextInLine(fmtAddTextInLine, "DATE:" + currentDate, "", "TIME:" + currentTime, 1);
            printer.addTextInLine(fmtAddTextInLine, "BATCH NO:" + Constants.BATCH_NO, "", "", 1);
            printer.feedLine(3);

            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "", "SETTLEMENT REPORT", "", 0);
            printer.addTextInLine(fmtAddTextInLine, "", "SUCCESSFUL", "", 0);
            printer.feedLine(2);
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "CARD", "COUNT", "AMOUNT", 0);
            printer.addText(format, "________________________________");
            // TRANS TYPE LOOP IT N
            String visaSaleType = "VISA";
            String visaCashOut = "VISA CASH";
            String visaOrbitRedeem = "VISA REDEEM";
            int visaSaleCount = 0, visaCashOutCount = 0, visaOrbitRedeemCount = 0;
            double visaSaleAmount = 0.00, visaCashOutAmount = 0.00, visaOrbitRedeemAmount = 0.00;
            String upiSaleType = "UPI";
            String upiCashOut = "UPI CASH";
            String upiOrbitRedeem = "UPI REDEEM";
            int upiSaleCount = 0, upiCashOutCount = 0, upiOrbitRedeemCount = 0;
            double upiSaleAmount = 0.00, upiCashOutAmount = 0.00, upiOrbitRedeemAmount = 0.00;
            String mcSaleType = "MASTER CARD";
            String mcCashOut = "MASTER CASH";
            String mcOrbitRedeem = "MASTER REDEEM";
            int mcSaleCount = 0, mcCashOutCount = 0, mcOrbitRedeemCount = 0, netSaleCount = 0;
            double mcSaleAmount = 0.00, mcCashOutAmount = 0.00, mcOrbitRedeemAmount = 0.00, netSale = 0.00;
            String payPakSaleType = "PAYPAK CARD";
            String payPakCashOut = "PAYPAK CASH";
            String payPakOrbitRedeem = "PAYPAK REDEEM";
            int payPakSaleCount = 0, payPakCashOutCount = 0, payPakOrbitRedeemCount = 0;
            double payPakSaleAmount = 0.00, payPakCashOutAmount = 0.00, payPakOrbitRedeemAmount = 0.00;
            String jcbSaleType = "JCB CARD";
            String jcbCashOut = "JCB CASH";
            String jcbOrbitRedeem = "JCB REDEEM";
            int jcbSaleCount = 0, jcbCashOutCount = 0, jcbOrbitRedeemCount = 0;
            double jcbSaleAmount = 0.00, jcbCashOutAmount = 0.00, jcbOrbitRedeemAmount = 0.00;
            String amexSaleType = "AMEX CARD";
            String amexCashOut = "AMEX CASH";
            String amexOrbitRedeem = "AMEX REDEEM";
            int amexSaleCount = 0, amexCashOutCount = 0, amexOrbitRedeemCount = 0;
            double amexSaleAmount = 0.00, amexCashOutAmount = 0.00, amexOrbitRedeemAmount = 0.00;
            DecimalFormat df2 = new DecimalFormat("0.00");

            int sumcardcount = 0;
            double sumcardamount = 0.00;
            String cardCount, cardSum;
            Boolean visa = false, upi = false, master = false, paypak = false, jcb = false, amex = false;

            for (int i = 0; i < cardTransactionItemList.size(); i++) {
                if (cardTransactionItemList.get(i).toString().equals("VISA")) {
                    visa = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        visaSaleCount += cardTransactionItemList.get(i).getCount();
                        visaSaleAmount = visaSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        visaCashOutCount += cardTransactionItemList.get(i).getCount();
                        visaCashOutAmount = visaCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        visaOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        visaOrbitRedeemAmount = visaOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                } else if (cardTransactionItemList.get(i).toString().equals("UNIONPAY")) {
                    upi = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        upiSaleCount += cardTransactionItemList.get(i).getCount();
                        upiSaleAmount = upiSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        upiCashOutCount += cardTransactionItemList.get(i).getCount();
                        upiCashOutAmount = upiCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        upiOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        upiOrbitRedeemAmount = upiOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }

                } else if (cardTransactionItemList.get(i).toString().equals("MASTER")) {
                    master = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        mcSaleCount += cardTransactionItemList.get(i).getCount();
                        mcSaleAmount = mcSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        mcCashOutCount += cardTransactionItemList.get(i).getCount();
                        mcCashOutAmount = mcCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        mcOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        mcOrbitRedeemAmount = mcOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    } else if (cardTransactionItemList.get(i).toString().equals("PAYPAK")) {
                        paypak = true;
                        if (cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                            payPakSaleCount += cardTransactionItemList.get(i).getCount();
                            payPakSaleAmount = payPakSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                        }
                        if (cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                            payPakCashOutCount += cardTransactionItemList.get(i).getCount();
                            payPakCashOutAmount = payPakCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                        }
                        if (cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                            payPakOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                            payPakOrbitRedeemAmount = payPakOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                        }
                    } else if (cardTransactionItemList.get(i).toString().equals("JCB")) {
                        jcb = true;
                        if (cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                            jcbSaleCount += cardTransactionItemList.get(i).getCount();
                            jcbSaleAmount = jcbSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                        }
                        if (cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                            jcbCashOutCount += cardTransactionItemList.get(i).getCount();
                            jcbCashOutAmount = jcbCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                        }
                        if (cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                            jcbOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                            jcbOrbitRedeemAmount = jcbOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                        }
                    } else if (cardTransactionItemList.get(i).toString().equals("AMEX")) {
                        amex = true;
                        if (cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                            amexSaleCount += cardTransactionItemList.get(i).getCount();
                            amexSaleAmount = amexSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                        }
                        if (cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                            amexCashOutCount += cardTransactionItemList.get(i).getCount();
                            amexCashOutAmount = amexCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                        }
                        if (cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                            amexOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                            amexOrbitRedeemAmount = amexOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                        }
                    }

                }

                if (!(cardTransactionItemList.get(i).getTxnType().equals("VOID")) && !(cardTransactionItemList.get(i).getTxnType().equals("VOID_REDEEM"))) {
                    sumcardcount += cardTransactionItemList.get(i).getCount();
                    sumcardamount += cardTransactionItemList.get(i).getTotalAmount();
                }

            }
            if (visa) {
                printer.addTextInLine(fmtAddTextInLine, visaSaleType, String.valueOf(visaSaleCount), String.valueOf(df2.format(visaSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, visaCashOut, String.valueOf(visaCashOutCount), String.valueOf(df2.format(visaCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, visaOrbitRedeem, String.valueOf(visaOrbitRedeemCount), String.valueOf(df2.format(visaOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            if (upi) {
                printer.addTextInLine(fmtAddTextInLine, upiSaleType, String.valueOf(upiSaleCount), String.valueOf(df2.format(upiSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, upiCashOut, String.valueOf(upiCashOutCount), String.valueOf(df2.format(upiCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, upiOrbitRedeem, String.valueOf(upiOrbitRedeemCount), String.valueOf(df2.format(upiOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            if (master) {
                printer.addTextInLine(fmtAddTextInLine, mcSaleType, String.valueOf(mcSaleCount), String.valueOf(df2.format(mcSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, mcCashOut, String.valueOf(mcCashOutCount), String.valueOf(df2.format(mcCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, mcOrbitRedeem, String.valueOf(mcOrbitRedeemCount), String.valueOf(df2.format(mcOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            if (paypak) {
                printer.addTextInLine(fmtAddTextInLine, mcSaleType, String.valueOf(payPakSaleCount), String.valueOf(df2.format(payPakSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, mcCashOut, String.valueOf(payPakCashOutCount), String.valueOf(df2.format(payPakCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, mcOrbitRedeem, String.valueOf(payPakOrbitRedeemCount), String.valueOf(df2.format(payPakOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            if (jcb) {
                printer.addTextInLine(fmtAddTextInLine, mcSaleType, String.valueOf(jcbSaleCount), String.valueOf(df2.format(jcbSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, mcCashOut, String.valueOf(jcbCashOutCount), String.valueOf(df2.format(jcbCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, mcOrbitRedeem, String.valueOf(jcbOrbitRedeemCount), String.valueOf(df2.format(jcbOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            if (amex) {
                printer.addTextInLine(fmtAddTextInLine, mcSaleType, String.valueOf(amexSaleCount), String.valueOf(df2.format(amexSaleAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, mcCashOut, String.valueOf(amexCashOutCount), String.valueOf(df2.format(amexCashOutAmount)), 0);
                printer.addTextInLine(fmtAddTextInLine, mcOrbitRedeem, String.valueOf(amexOrbitRedeemCount), String.valueOf(df2.format(amexOrbitRedeemAmount)), 0);
                printer.feedLine(2);
            }
            netSale = mcSaleAmount + visaSaleAmount + upiSaleAmount + payPakSaleAmount + jcbSaleAmount + amexSaleAmount;
            netSaleCount = mcSaleCount + visaSaleCount + upiSaleCount + payPakSaleCount + jcbSaleCount + amexSaleCount;
            printer.feedLine(2);
            cardCount = String.valueOf(sumcardcount);
            cardSum = String.valueOf(df2.format(sumcardamount));
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "", "", "", 0);
            printer.feedLine(1);

            /////////////

            //TOTAL NET SALES
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
            printer.addTextInLine(fmtAddTextInLine, "TOTAL SALES", cardCount, cardSum, 0);
            printer.feedLine(3);
            printer.addTextInLine(fmtAddTextInLine, "", "DETAIL REPORT IN CHRONOLOGICAL ORDER", "", 0);
            printer.feedLine(2);
//        //body
            for (int i = 0; i < transactionDetailList.size(); i++) {
                if (!transactionDetailList.get(i).getTxnType().equals("PRE AUTH")) {
                    Utility.DEBUG_LOG("printDate", transactionDetailList.get(i).getTxnDate());
                    printer.addTextInLine(fmtAddTextInLine, DataFormatterUtil.formattedDate(transactionDetailList.get(i).getTxnDate()), "", DataFormatterUtil.formattedTime(transactionDetailList.get(i).getTxnTime()), 1);
                    printer.addTextInLine(fmtAddTextInLine, transactionDetailList.get(i).getBatchNo(), transactionDetailList.get(i).getTxnType(), transactionDetailList.get(i).getAuthorizationIdentificationResponseCode(), 0);
                    printer.addTextInLine(fmtAddTextInLine, DataFormatterUtil.maskCardNo(transactionDetailList.get(i).getCardNo()), transactionDetailList.get(i).getCardType(), transactionDetailList.get(i).getCardScheme(), 0);
                    if (transactionDetailList.get(i).getTipAmount() != null) {
                        printer.addTextInLine(fmtAddTextInLine, "Amount", "PKR", transactionDetailList.get(i).getTransactionAmount(), 0);
                        printer.addTextInLine(fmtAddTextInLine, "TIP", "PKR", transactionDetailList.get(i).getTipAmount(), 0);
                    } else {
                        printer.addTextInLine(fmtAddTextInLine, "Amount", "PKR", transactionDetailList.get(i).getAmount(), 0);
                    }
                    printer.addText(format, "________________________________");
                }
            }
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "", "FINAL SUMMARY", "", 0);
            printer.feedLine(2);
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "TYPE", "COUNT", "AMOUNT", 0);
            printer.addText(format, "________________________________");
            // TRANS TYPE LOOP IT N
            int sumfinalcount = 0;
            double sumfinalamount = 0.00;
            String finalCount, finalSum, finalstatus = "";
            String saleType = "SALE";
            String saleVoid = "VOID";
            String cashOut = "CASHOUT";
            String orbitRedeem = "REDEEM";
            int saleCount = 0, voidCount = 0, cashOutCount = 0, orbitRedeemCount = 0;
            double saleAmountStr = 0.00;
            double voidAmountStr = 0.00;
            double cashOutAmountStr = 0.00;
            double orbitRedeemAmountStr = 0.00;

            for (int i = 0; i < cardTransactionItemList.size(); i++) {
                if (cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                    saleType = "SALE";
                    saleCount += cardTransactionItemList.get(i).getCount();
                    //  saleAmount = saleAmount + countedTransactionItemList.get(i).getTotalAmount();
                    Utility.DEBUG_LOG("SL saleAmount", (new DecimalFormat("##.##").format(cardTransactionItemList.get(i).getTotalAmount())));
                    saleAmountStr = saleAmountStr + cardTransactionItemList.get(i).getTotalAmount();
                }
                if (cardTransactionItemList.get(i).getTxnType().equals("VOID") || cardTransactionItemList.get(i).getTxnType().equals("VOID_REDEEM")) {
                    saleVoid = "VOID";
                    voidCount += cardTransactionItemList.get(i).getCount();
                    //voidAmount = voidAmount + countedTransactionItemList.get(i).getTotalAmount();
                    voidAmountStr = voidAmountStr + cardTransactionItemList.get(i).getTotalAmount();
                }
                if (cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                    cashOut = "CASH OUT";
                    cashOutCount += cardTransactionItemList.get(i).getCount();
                    // cashOutAmount = cashOutAmount + countedTransactionItemList.get(i).getTotalAmount();
                    cashOutAmountStr = cashOutAmountStr + cardTransactionItemList.get(i).getTotalAmount();

                }
                if (cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                    orbitRedeem = "REDEEM";
                    orbitRedeemCount += cardTransactionItemList.get(i).getCount();
                    //  orbitRedeemAmount = orbitRedeemAmount + countedTransactionItemList.get(i).getTotalAmount();
                    orbitRedeemAmountStr = orbitRedeemAmountStr + cardTransactionItemList.get(i).getTotalAmount();
                }

                if (!(cardTransactionItemList.get(i).getTxnType().equals("VOID")) && !(cardTransactionItemList.get(i).getTxnType().equals("VOID_REDEEM"))) {
                    sumfinalcount += cardTransactionItemList.get(i).getCount();
                    sumfinalamount += cardTransactionItemList.get(i).getTotalAmount();
                }


            }
            Utility.DEBUG_LOG("totalAmountSum", String.valueOf(sumfinalamount));
            Utility.DEBUG_LOG("totalcount", String.valueOf(sumfinalcount));

            printer.addTextInLine(fmtAddTextInLine, saleType, String.valueOf(saleCount), String.valueOf(new DecimalFormat("0.00").format(saleAmountStr)), 0);
            printer.addTextInLine(fmtAddTextInLine, saleVoid, String.valueOf(voidCount), String.valueOf(new DecimalFormat("0.00").format(voidAmountStr)), 0);
            printer.addTextInLine(fmtAddTextInLine, cashOut, String.valueOf(cashOutCount), String.valueOf(new DecimalFormat("0.00").format(cashOutAmountStr)), 0);
            printer.addTextInLine(fmtAddTextInLine, orbitRedeem, String.valueOf(orbitRedeemCount), String.valueOf(new DecimalFormat("0.00").format(orbitRedeemAmountStr)), 0);

            finalCount = String.valueOf(sumfinalcount);
            finalSum = String.valueOf((new DecimalFormat("0.00").format(sumfinalamount)));
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "", "", "", 0);

            printer.addText(format, "________________________________");
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "TOTAL Txn", finalCount, "PKR: " + finalSum, 0);
            Utility.DEBUG_LOG("totaoAmount", finalSum);
            printer.feedLine(3);
            printer.addText(format, "TERMINAL SERIAL # " + Constants.TERMINAL_SERIAL);
            printer.feedLine(2);
            //footer
            printer.addTextInLine(fmtAddTextInLine, "", "***END***", "", 0);
            printer.feedLine(4);
            Utility.DEBUG_LOG("startPrint5", "before startPrint");
            printer.startPrint(new MyListener());
            if (Constants.isEcrEnable.equals("Y"))
                ECR.ecrWriteSettlement(currentDate, currentTime,
                        String.valueOf(new DecimalFormat("0.00").format(saleAmountStr)), saleCount,
                        String.valueOf(new DecimalFormat("0.00").format(voidAmountStr)), voidCount,
                        String.valueOf(new DecimalFormat("0.00").format(cashOutAmountStr)), cashOutCount,
                        String.valueOf(new DecimalFormat("0.00").format(orbitRedeemAmountStr)), orbitRedeemCount,
                        finalSum,finalCount);



        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    /**
     * @param printer
     * @param type
     * @param length
     * @param packet
     */
    public void printPacket(IPrinter printer, String type, int length, String packet) {
        // bundle formate for AddTextInLine
        Bundle format = new Bundle();
        try {
            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
            printer.setGray(7);
            printer.addText(format, type + "  " + length);
            printer.addText(format, packet);
            printer.feedLine(3);
            //printer.startPrint(new MyListener());
            //printer.startPrint(null);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void printEmvTags(IPrinter printer, /*String tagName, String tagValue,*/ List<EmvTagValuePair> emvTagsToPrint) {
        // bundle formate for AddTextInLine
        Bundle format = new Bundle();
        Bundle fmtAddTextInLine = new Bundle();
        try {
            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.SMALL_16_16);
            printer.setGray(7);

            for (int j = 0; j < emvTagsToPrint.size(); j++) {


                printer.addTextInLine(fmtAddTextInLine, emvTagsToPrint.get(j).getKey() + " : " + emvTagsToPrint.get(j).getValue(), "", "", 0);
                printer.feedLine(1);
                printer.addText(format, "--------------------------------");
                printer.feedLine(1);
                printer.addText(format, "byte # | bits:  8 7 6 5 4 3 2 1");
                printer.feedLine(1);
                printer.addText(format, "--------------------------------");
                printer.feedLine(1);
                int counter = 1;
                for (int i = 0; i < emvTagsToPrint.get(j).getValue().length(); i = i + 2) {
                    String hexToBinary = DataFormatterUtil.hexCharToBin(emvTagsToPrint.get(j).getValue().charAt(i)) +
                            DataFormatterUtil.hexCharToBin(emvTagsToPrint.get(j).getValue().charAt(i + 1));
                    String exToBinary = DataFormatterUtil.insertCharacterForEveryNDistance(1, hexToBinary, ' ');
                    Utility.DEBUG_LOG("EMVPrint", "byte " + counter + " | bits: " + exToBinary);
                    printer.addTextInLine(fmtAddTextInLine, "byte " + counter + " | bits:", exToBinary, "", 0);
                    printer.feedLine(1);
                    counter = counter + 1;

                }
                printer.feedLine(2);

            }
            //printer.startPrint(new MyListener());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void printIndividualTagValue(IPrinter printer, List<EmvTagValuePair> emvTagsToPrint) {
        Bundle format = new Bundle();
        Bundle fmtAddTextInLine = new Bundle();
        try {
            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
            for (int j = 0; j < emvTagsToPrint.size(); j++) {
                printer.addTextInLine(fmtAddTextInLine, emvTagsToPrint.get(j).getKey() + " : " + emvTagsToPrint.get(j).getValue(), "", "", 0);
                printer.feedLine(1);
            }
            Utility.DEBUG_LOG("startPrint6", "before startPrint");
            printer.startPrint(new MyListener());
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

    class MyListener extends PrinterListener.Stub {
        @Override
        public void onError(int error) throws RemoteException {
            Utility.DEBUG_LOG(TAG, "+ onError +");
            Utility.DEBUG_LOG(TAG, "before getData");
            Message msg = new Message();
            msg.getData().putString("msg", "print error,errno:" + error);

            handler.sendMessage(msg);
        }

        @Override
        public void onFinish() throws RemoteException {
            Utility.DEBUG_LOG(TAG, "+ onFinish +");
            Utility.DEBUG_LOG(TAG, "before getData");
            Message msg = new Message();
            msg.getData().putString("msg", "print finished");
            handler.sendMessage(msg);
        }
    }

}
