package cn.access.group.android_all_banks_pos.usecase;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.SparseArray;

import java.io.IOException;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.Utilities.Comm;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.basic.ISO8583;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.support.constraint.Constraints.TAG;

/**
 * on 10/25/2019.
 * NOT USED CURRENTLY
 */
public class ThreadRunImplementation {
    private AppDatabase appDatabase;
    private TransactionContract transactionContract;
    private SparseArray<String> tagOfF55 = null;
    private String pinKey_WorkKey;
    private String macKey;
    Context context;

    public ThreadRunImplementation( AppDatabase appDatabase, TransactionContract transactionContract) {
        this.appDatabase = appDatabase;
        this.transactionContract = transactionContract;
    }

    public ISO8583u runImplementation(SparseArray<String> data8583, TransactionDetail transactionDetail){
        ISO8583u iso8583u;
        GenerateReversal generateReversal = new GenerateReversal(appDatabase);
        byte[] packet;
        if(generateReversal.isReversalPresentInDB()){
            iso8583u= new ISO8583u(16);
            SparseArray<String> reversal = generateReversal.generateReversalPacket();
            if( reversal.get(ISO8583u.F_55)!=null){
                SparseArray<String> tagOfF55Reversal = generateReversal.createTagOf55();
                if (tagOfF55Reversal != null) {
                    for (int i = 0; i < tagOfF55Reversal.size(); i++) {
                        int tag = tagOfF55Reversal.keyAt(i);
                        String value = tagOfF55Reversal.valueAt(i);
                        if (value.length() > 0) {
                            byte[] tmp = iso8583u.appendF55(tag, value);
                            if (tmp == null) {
                                Utility.DEBUG_LOG(TAG, "error of tag:" + Integer.toHexString(tag) + ", value:" + value);
                            } else {
                                Utility.DEBUG_LOG(TAG, "append F55 tag:" + Integer.toHexString(tag) + ", value:" + Utility.byte2HexStr(tmp));
                            }
                        }
                    }

                }

            }
            packet = iso8583u.makePacket(reversal, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);

        }
        else {
            iso8583u= new ISO8583u();
            if (tagOfF55 != null) {
                for (int i = 0; i < tagOfF55.size(); i++) {
                    int tag = tagOfF55.keyAt(i);
                    String value = tagOfF55.valueAt(i);
                    if (value.length() > 0) {
                        byte[] tmp = iso8583u.appendF55(tag, value);
                        if (tmp == null) {
                            Utility.DEBUG_LOG(TAG, "error of tag:" + Integer.toHexString(tag) + ", value:" + value);
                        } else {
                            Utility.DEBUG_LOG(TAG, "append F55 tag:" + Integer.toHexString(tag) + ", value:" + Utility.byte2HexStr(tmp));
                        }
                    }
                }
                tagOfF55 = null;

            }
            packet = iso8583u.makePacket(data8583, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);
          /*  transactionDetail.setMId(merchantID);
            transactionDetail.setTId(terminalID);
            transactionDetail.setStan(STAN);
            transactionDetail.setRrn(rnn);
            transactionDetail.setTxnDate(dateFromResponse);
            transactionDetail.setTxnTime(timeFromResponse);
            if(transactionDetail.getCardNo()==null)
            {transactionDetail.setCardNo(cardPan);}
            transactionDetail.setAmount(transAmount);
            transactionDetail.setCardScheme(cardType);
            transactionDetail.setInvoiceNo(INVOICE_NO);
            transactionDetail.setBatchNo(BATCH_NO);
            transactionDetail.setTrack2(track2);
            // transactionDetail.setStatus("approved");
            transactionDetail.setMessageType(SALE_MESSAGE_TYPE);
            transactionDetail.setProcessingCode(SALE_PROCESSING_CODE);
            transactionDetail.setAcquiringInstitutionIdentificationCode(ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
            transactionDetail.setForwardingInstitutionIdentificationCode(FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
           */

        }
        Comm comm = new Comm(Constants.hostIP, Constants.hostPort,context);
        if (!comm.connect()) {
            Utility.DEBUG_LOG(TAG, "Unable to establish connection with server.");
            transactionContract.hideProgress();
            transactionContract.transactionFailed("Error","Unable to establish connection with server");
            return null;
        }
        transactionContract.showProgress("processing");
        comm.send(packet);


        byte[] response = new byte[0];
        try {
            response = comm.receive(1024, 40);
        } catch (IOException e) {
            transactionContract.hideProgress();
            transactionContract.transactionFailed("Error",e.getMessage());
            transactionDetail.setStatus("reversal");
        }
        comm.disconnect();
        transactionContract.hideProgress();

        ISO8583u isoResponse = null;
        if (response == null ||response.length==0) {
            Utility.DEBUG_LOG(TAG, "Test fails");

            Utility.DEBUG_LOG(TAG, "receive error");
            if(transactionDetail.getStatus()!=null && transactionDetail.getStatus().equals("reversal")){
                insertTransaction(appDatabase,transactionDetail);
            }
            else {
                transactionDetail.setStatus("reversal");
                if (generateReversal.isReversalPresentInDB()) return null;
                else
                {insertTransaction(appDatabase, transactionDetail);}
            }
            transactionContract.transactionFailed("Failed","will be reversed");
        } else {
            Utility.DEBUG_LOG(TAG, "Test return length:" + response.length);
            Utility.DEBUG_LOG(TAG, Utility.byte2HexStr(response));
            isoResponse = new ISO8583u();
            if (isoResponse.unpack(response, 2)) {
                String message = "";
                String s;
                String type = "";

                s = isoResponse.getUnpack(0);
                if (null != s) {
                    type = s;
                    message += "Message Type:";
                    message += s;
                    message += "\n";
                }

                s = isoResponse.getUnpack(39);
                if (null != s) {
                    message += "Response(39):";
                    message += s;
                    message += "\n";
                }
                if (type.equals("0810")) {
                    s = isoResponse.getUnpack(62 + 200);
                    if (null != s) {
                        Utility.DEBUG_LOG(TAG, "Field62:" + s);
                        if (s.length() == 48) {
                            pinKey_WorkKey = s.substring(0, 32);
                            macKey = s.substring(32, 48);
                        } else if (s.length() == 80) {
                            pinKey_WorkKey = s.substring(0, 64);
                            macKey = s.substring(64, 80);
                        } else if (s.length() == 120) {
                            pinKey_WorkKey = s.substring(0, 64);
                            macKey = s.substring(64, 80);
                        }
                        message += "pinKey:";
                        message += pinKey_WorkKey;
                        message += "\n";
                        message += "macKey:";
                        message += macKey;
                        message += "\n";
                    }
                } else if (type.equals("0210")) {
                    s = isoResponse.getUnpack(54);
                    if (null != s) {
                        message += "Balance(54):";
                        message += s.substring(0, 2) + "," + s.substring(2, 4) + "," + s.substring(4, 7) + "," + s.substring(7, 8);
                        message += "\n" + Integer.valueOf(s.substring(8, s.length() - 1));
                        message += "\n";
                    }

                }

                transactionContract.hideProgress();

                transactionContract.toastShow(message);
            }
        }


        Message msg = new Message();
        Bundle data = new Bundle();
        data.putString("value", "receive finished");
        msg.setData(data);
        onlineResponse.sendMessage(msg);


        return isoResponse;
    }


    @SuppressLint("CheckResult")
    private static void insertTransaction(final AppDatabase db, TransactionDetail transactionDetail) {
        Completable.fromAction(() -> db.transactionDetailDao().insertTransaction(transactionDetail))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{ }// completed with success
                        ,throwable ->{}// there was an error
                );
    }

    @SuppressLint("HandlerLeak")
    private Handler onlineResponse = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Utility.DEBUG_LOG(TAG,"before getData");

            Bundle data = msg.getData();
            String val = data.getString("value");
            Utility.DEBUG_LOG(TAG, "handle message:" + val);
        }
    };
}
