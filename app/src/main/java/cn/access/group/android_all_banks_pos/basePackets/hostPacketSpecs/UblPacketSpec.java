package cn.access.group.android_all_banks_pos.basePackets.hostPacketSpecs;

import android.util.SparseArray;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import java.util.Locale;

import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.repository.model.Reversal;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import android.util.Log;

import static android.content.ContentValues.TAG;


/**
 * on 11/23/2019.
 */
public class UblPacketSpec {
    private SparseArray<String> data8583_Packet = new SparseArray<>();
    private String TAG  = "UblPacketSpec";

    public SparseArray<String> generateSalePacket(){
        Utility.DEBUG_LOG("UblPacketSpec","+ UblPacketSpec::generateSalePacket +");
      /*  000,004: ...........................................................................................0200
        003,006: .........................................................................................000000
        011,006: .........................................................................................000056
        022,004: ...........................................................................................0022
        024,004: ...........................................................................................0003
        025,002: .............................................................................................00
         tid 11111112
        mid 1100000001 20 20 20 20 20*/
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.SALE_PROCESSING_CODE);
        data8583_Packet.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        data8583_Packet.put( ISO8583u.F_ServiceCode_61 , Constants.FIELD_61 );
        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        return data8583_Packet;
    }
    public SparseArray<String> generateRefundPacket(){
        Utility.DEBUG_LOG("UblPacketSpec","+ UblPacketSpec::generateSalePacket +");
      /*  000,004: ...........................................................................................0200
        003,006: .........................................................................................000000
        011,006: .........................................................................................000056
        022,004: ...........................................................................................0022
        024,004: ...........................................................................................0003
        025,002: .............................................................................................00
         tid 11111112
        mid 1100000001 20 20 20 20 20*/
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, "200000");
        data8583_Packet.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        //data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        data8583_Packet.put( ISO8583u.F_ServiceCode_61 , Constants.FIELD_61 );
        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        return data8583_Packet;
    }

    public SparseArray<String> generateSaleIPPPacket() {
      /*000,004: ...........................................................................................0200
        003,006: .........................................................................................000000
        011,006: .........................................................................................000056
        022,004: ...........................................................................................0022
        024,004: ...........................................................................................0003
        025,002: .............................................................................................00
         tid 11111112
        mid 1100000001 20 20 20 20 20*/
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.SALEIPP_PROCESSING_CODE);
        data8583_Packet.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        data8583_Packet.put(ISO8583u.F_ServiceCode_61, Constants.FIELD_61);
        data8583_Packet.put(ISO8583u.F_KeyExchange_62, Constants.INVOICE_NO);
        return data8583_Packet;
    }

    public SparseArray<String> generateCompletionPacket(){
      /*  000,004: ...........................................................................................0200
        003,006: .........................................................................................000000
        011,006: .........................................................................................000056
        022,004: ...........................................................................................0022
        024,004: ...........................................................................................0003
        025,002: .............................................................................................00
         tid 11111112
        mid 1100000001 20 20 20 20 20*/
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.SALE_PROCESSING_CODE);
        data8583_Packet.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        data8583_Packet.put( ISO8583u.F_ServiceCode_61 , Constants.FIELD_61 );
        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        return data8583_Packet;
    }


    public SparseArray<String> generateCashOutPacket(){
      /*  000,004: ...........................................................................................0200
        003,006: .........................................................................................000000
        011,006: .........................................................................................000056
        022,004: ...........................................................................................0022
        024,004: ...........................................................................................0003
        025,002: .............................................................................................00
         tid 11111112
        mid 1100000001 20 20 20 20 20*/
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.CASH_OUT_PROCESSING_CODE);
        data8583_Packet.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        return data8583_Packet;
    }
    public SparseArray<String> generateCashAdvPacket(){
      /*  000,004: ...........................................................................................0200
        003,006: .........................................................................................000000
        011,006: .........................................................................................000056
        022,004: ...........................................................................................0022
        024,004: ...........................................................................................0003
        025,002: .............................................................................................00
         tid 11111112
        mid 1100000001 20 20 20 20 20*/
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.CASH_ADV_PROCESSING_CODE);
        data8583_Packet.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        data8583_Packet.put(ISO8583u.F_ServiceCode_61,Constants.FIELD_61);
        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        return data8583_Packet;
    }
    public SparseArray<String> generateOrbitRedeemPacket(){
      /*  000,004: ...........................................................................................0200
        003,006: .........................................................................................000000
        011,006: .........................................................................................000056
        022,004: ...........................................................................................0022
        024,004: ...........................................................................................0003
        025,002: .............................................................................................00
         tid 11111112
        mid 1100000001 20 20 20 20 20*/
      String procCode = "000070";
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, procCode);
        data8583_Packet.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        data8583_Packet.put(ISO8583u.F_ServiceCode_61,Constants.FIELD_61);
        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        return data8583_Packet;
    }
    public SparseArray<String> generateOrbitInquiryPacket(){
      /*  000,004: ...........................................................................................0200
        003,006: .........................................................................................000000
        011,006: .........................................................................................000056
        022,004: ...........................................................................................0022
        024,004: ...........................................................................................0003
        025,002: .............................................................................................00
         tid 11111112
        mid 1100000001 20 20 20 20 20*/
        String processCode = "310070";
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, processCode);
        data8583_Packet.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        data8583_Packet.put(ISO8583u.F_ServiceCode_61 ,Constants.FIELD_61 );
        return data8583_Packet;
    }

    public SparseArray<String> generatePreAuthPacket(){
      /*  000,004: ...........................................................................................0200
        003,006: .........................................................................................000000
        011,006: .........................................................................................000056
        022,004: ...........................................................................................0022
        024,004: ...........................................................................................0003
        025,002: .............................................................................................00
         tid 11111112
        mid 1100000001 20 20 20 20 20*/
        String ubl_PreAuth_Processing_Code = "300000";
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, ubl_PreAuth_Processing_Code);
        data8583_Packet.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        data8583_Packet.put(ISO8583u.F_ServiceCode_61 ,Constants.FIELD_61 );
        return data8583_Packet;
    }
    public SparseArray<String> generateAuthPacket(){
      /*  000,004: ...........................................................................................0200
        003,006: .........................................................................................000000
        011,006: .........................................................................................000056
        022,004: ...........................................................................................0022
        024,004: ...........................................................................................0003
        025,002: .............................................................................................00
         tid 11111112
        mid 1100000001 20 20 20 20 20*/
        //String ubl_PreAuth_Processing_Code = "300000";
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, "000000");
        data8583_Packet.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        data8583_Packet.put(ISO8583u.F_ServiceCode_61 ,Constants.FIELD_61 );
        return data8583_Packet;
    }

    public SparseArray<String> generateVoidPacket( TransactionDetail transactionDetail){
      /*000,004: ...........................................................................................0200
        003,006: .........................................................................................020000
        004,012: ...................................................................................000000000010
        011,006: .........................................................................................000055
        012,006: .........................................................................................113255
        013,004: ...........................................................................................1123
        022,004: ...........................................................................................0051
        024,004: ...........................................................................................0003
        025,002: .............................................................................................00
        035,037: ..........................................................4724390000115156D24032011042951300000
        037,012: ...................................................................................932706111213 rnn from response
        038,006: .........................................................................................050090 authorization resp from response
        041,008: .......................................................................................11111112
        042,015: ................................................................................1100000001
        062,006: .........................................................................................271321*/
//        String ubl_Void_Processing_Code = "020000";
        if ( transactionDetail.getTxnType().equals("REDEEM"))
        {
            Utility.DEBUG_LOG(TAG,"Redeem proc code for void:"+Constants.VOID_PROCESSING_CODE_REDEEM);
            data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.VOID_PROCESSING_CODE_REDEEM);
        }
        else
        {
            data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.VOID_PROCESSING_CODE);
        }

        data8583_Packet.put(ISO8583u.F_AmountOfTransactions_04, String.format(Locale.getDefault(),"%012d", (int)(Double.parseDouble(transactionDetail.getAmount())*100)));
        data8583_Packet.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        if ( transactionDetail.getTxnTime() != null)
            data8583_Packet.put(ISO8583u.F_TxTime_12, transactionDetail.getTxnTime());
        if ( transactionDetail.getTxnDate() != null)
            data8583_Packet.put(ISO8583u.F_TxDate_13, transactionDetail.getTxnDate());
        if ( transactionDetail.getCardexpiry() != null) {
            String exp = transactionDetail.getCardexpiry();
            String month = exp.substring(0,2);
            String year = exp.substring(3,5);
            Utility.DEBUG_LOG(TAG,"void exp month:"+ month);
            Utility.DEBUG_LOG(TAG,"void exp year:"+ year);
            Utility.DEBUG_LOG(TAG,"void exp:" + year + "" + month);
            String expiry = year + month;
            data8583_Packet.put(ISO8583u.F_DateOfExpired_14, expiry);
        }
        if ( transactionDetail.getPosEntryMode() != null)
            data8583_Packet.put(ISO8583u.F_POSEntryMode_22, transactionDetail.getPosEntryMode());
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII);
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        if ( transactionDetail.getTrack2() != null)
            data8583_Packet.put(ISO8583u.F_Track_2_Data_35, transactionDetail.getTrack2());
        if ( transactionDetail.getRrn() != null)
            data8583_Packet.put(ISO8583u.F_RRN_37, transactionDetail.getRrn());
        if ( transactionDetail.getAuthorizationIdentificationResponseCode() != null)
            data8583_Packet.put(ISO8583u.F_AuthorizationIdentificationResponseCode_38, transactionDetail.getAuthorizationIdentificationResponseCode());
        data8583_Packet.put(ISO8583u.F_TID_41,transactionDetail.getTId());
        data8583_Packet.put(ISO8583u.F_MerchantID_42,transactionDetail.getMId());
        if (Constants.TIP_ENABLED.equals("Y") && (transactionDetail.getTxnType().equals("SALE") ||transactionDetail.getTxnType().equals("ADJUST"))){
            data8583_Packet.put(ISO8583u.F_BalancAmount_54, String.format(Locale.getDefault(),"%012d", (int)(Double.parseDouble(transactionDetail.getTipAmount())*100)));
        }
        data8583_Packet.put( ISO8583u.F_ServiceCode_61 ,"01");
        if ( transactionDetail.getInvoiceNo() != null)
            data8583_Packet.put( ISO8583u.F_KeyExchange_62 ,transactionDetail.getInvoiceNo());
        return data8583_Packet;
    }

    public SparseArray<String> generateSettlementPacket(String processingCode,String field63){
      /* 000,004: ...........................................................................................0500
        003,006: .........................................................................................920000
        011,006: .........................................................................................000052
        024,004: ...........................................................................................0003
        041,008: .......................................................................................11111112
        042,015: ................................................................................1100000001
        060,006: .........................................................................................000002
        063,075: ....................001000000001000000000000000000000000000000000000000000000000000000000000000*/
        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.SETTLEMENT_MESSAGE_TYPE);
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, processingCode);
        data8583_Packet.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_TID_41, Constants.TERMINAL_ID);
        data8583_Packet.put(ISO8583u.F_MerchantID_42, Constants.MERCHANT_ID);
//        data8583_Packet.put( ISO8583u.F_ReservedNational_60 , Constants.RESERVED_NATIONAL );
        data8583_Packet.put( ISO8583u.F_ReservedNational_60 ,Constants.BATCH_NO );
        data8583_Packet.put( ISO8583u.F_SettlementData_63 , field63 );
        return data8583_Packet;
    }

    public SparseArray<String> generateReversalPacket(Reversal transactionToReverse){
        /* *
        000,004: ...........................................................................................0400
        003,006: .........................................................................................000000
        004,012: ...................................................................................000000002000
        011,006: .........................................................................................000053
        022,004: ...........................................................................................0052
        023,004: ...........................................................................................0001
        024,004: ...........................................................................................0003
        025,002: .............................................................................................00
        035,037: ..........................................................5240840067770139D21102011000020900000
        041,008: .......................................................................................11111112
        042,015: ................................................................................1100000001
        055,139: ...............................................................................................
        062,006: .........................................................................................271320
        * */
        //+ taha 20-01-2021 do not use constants use actual transaction data
//        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.REVERSAL_MESSAGE_TYPE);
//        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.SALE_PROCESSING_CODE);
//        data8583_Packet.put(ISO8583u.F_AmountOfTransactions_04,transactionToReverse.getAmount());
//        data8583_Packet.put(ISO8583u.F_STAN_11,DataFormatterUtil.getNewStan());
//        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
//        if(transactionToReverse.getCardType().equals("chip")){
//          data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE_CHIP);
//        }
//        data8583_Packet.put(ISO8583u.F_ApplicationPANSequenceNumber_23,"001");
//        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
//        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
//        data8583_Packet.put(ISO8583u.F_Track_2_Data_35,transactionToReverse.getTrack2());
//        data8583_Packet.put(ISO8583u.F_TID_41, Constants.TERMINAL_ID);
//        data8583_Packet.put(ISO8583u.F_MerchantID_42, Constants.MERCHANT_ID);
//        data8583_Packet.put(ISO8583u.F_55,"present");
//        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );

        Utility.DEBUG_LOG("TAG","+ generateReversalPacket +");
        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.REVERSAL_MESSAGE_TYPE);
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, transactionToReverse.getProcessingCode());
        if ( transactionToReverse.getAmount() != null )
        {
            String tempAmount = transactionToReverse.getAmount();
            if ( tempAmount.length() == 12 )
            {
                Utility.DEBUG_LOG("Reversal:","probably BCD encoded, pass as is");
                data8583_Packet.put(ISO8583u.F_AmountOfTransactions_04,transactionToReverse.getAmount());
            }
            else
            {
                Utility.DEBUG_LOG("Reversal:","actual amount, convert to BCD first");
                String   amount = String.format(Locale.getDefault(),"%012d", (int)(Double.parseDouble(transactionToReverse.getAmount())*100));
                data8583_Packet.put(ISO8583u.F_AmountOfTransactions_04,amount);
            }
        }
        else
        {
            Utility.DEBUG_LOG("Reversal:","Warning:amount not found");
        }
        data8583_Packet.put(ISO8583u.F_STAN_11,transactionToReverse.getStan());
        if ( transactionToReverse.getPosEntryMode() != null )
            data8583_Packet.put(ISO8583u.F_POSEntryMode_22, transactionToReverse.getPosEntryMode());   // 02 mag, 05 smart, 07 ctls; 1 pin
        if ( transactionToReverse.getPanSequenceNumber() != null )
            data8583_Packet.put(ISO8583u.F_ApplicationPANSequenceNumber_23,transactionToReverse.getPanSequenceNumber());
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        if ( transactionToReverse.getTrack2() != null )
        {
            Utility.DEBUG_LOG("TAG","before put F_Track_2_Data_35 reversal");
            data8583_Packet.put(ISO8583u.F_Track_2_Data_35,transactionToReverse.getTrack2());
        }
        data8583_Packet.put(ISO8583u.F_TID_41, transactionToReverse.getTId());
        data8583_Packet.put(ISO8583u.F_MerchantID_42, transactionToReverse.getMId());
        if ( transactionToReverse.getTag55() != null )
            data8583_Packet.put(ISO8583u.F_55,"present");
        data8583_Packet.put( ISO8583u.F_ServiceCode_61 , Constants.FIELD_61 );
        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , transactionToReverse.getInvoiceNo() );
        return data8583_Packet;
    }

}
