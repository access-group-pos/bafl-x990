package cn.access.group.android_all_banks_pos.Utilities;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.access.group.android_all_banks_pos.repository.model.CountedTransactionItem;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalPassword;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalTxnUpload;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUrl;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUserName;

public class BatchUploadRequest {


    private String TAG  = "BatchUploadRequest";
        public void makeJsonObjReq(Context context, List<TransactionDetail> td, List<CountedTransactionItem> cardTransactionItemList) throws JSONException {

            if (PortalTxnUpload.equals("Y")) {
                RequestQueue queue = Volley.newRequestQueue(context);


                String url = PortalUrl + "/baflservicesettlement";
                //String cardNo = td.getCardNo();
                Utility.DEBUG_LOG("BatchUploadRequest",url);
//            cardNo = cardNo.substring(0,6);
//            Utility.DEBUG_LOG("cardNo",cardNo);
                Map<String, String> postParam = new HashMap<String, String>();
                Utility.DEBUG_LOG("transactionDetailList", String.valueOf(td.size()));
                JSONObject obj1;
                JSONArray transactionArr = new JSONArray();
                JSONArray transactionArr2 = new JSONArray();

                int i;
                for (i = 0; i < td.size(); i++) {
                    obj1 = new JSONObject();
                    if (i == 0) {
                        obj1.put("UserName", PortalUserName);
                        obj1.put("PASSWORD", PortalPassword);
                    }
                    String cardNo = td.get(i).getCardNo();
                    cardNo = cardNo.substring(0, 6);
                    Utility.DEBUG_LOG("cardNo", cardNo);
                    obj1.put("BIN", cardNo);
                    obj1.put("FieldOne", DataFormatterUtil.maskCardNo(td.get(i).getCardNo()));
                    obj1.put("AuthID", td.get(i).getAuthorizationIdentificationResponseCode());
                    obj1.put("Amount", td.get(i).getAmount());
                    obj1.put("MID", td.get(i).getMId());
                    obj1.put("TID", td.get(i).getTId());
                    obj1.put("TxnDate", td.get(i).getTxnDate());
                    obj1.put("TxnTime", td.get(i).getTxnTime());
                    obj1.put("POSEntryMode", td.get(i).getPosEntryMode());
                    obj1.put("BatchNo", td.get(i).getBatchNo());
                    obj1.put("RRN", td.get(i).getRrn());
                    obj1.put("InvoiceNo", td.get(i).getInvoiceNo());
                    obj1.put("Stan", td.get(i).getStan());
                    obj1.put("TxnType", td.get(i).getTxnType());
                    Utility.DEBUG_LOG("TXN", td.get(i).getTxnType());
                    obj1.put("Acquirer", "BAF-Payment");
                    obj1.put("Model", "X990");
                    obj1.put("SerialNumber", Constants.TERMINAL_SERIAL);
                    transactionArr.put(obj1);
                }
                //Utility.DEBUG_LOG("settle obj1", String.valueOf(transactionArr));


                JSONObject obj2 = new JSONObject();
                for (int j = 0; j < cardTransactionItemList.size(); j++) {
                    if (cardTransactionItemList.get(j).getTxnType().equals("SALE")) {
                        Utility.DEBUG_LOG("txn", String.valueOf(cardTransactionItemList.get(j).getTxnType().equals("SALE")));
                        obj2.put("SaleCount", String.valueOf(cardTransactionItemList.get(j).getCount()));
                        obj2.put("SaleAmount", (new DecimalFormat("0.00").format(cardTransactionItemList.get(j).getTotalAmount())));
                    }

                    if (cardTransactionItemList.get(j).getTxnType().equals("REDEEM")) {
                        Utility.DEBUG_LOG("txn", String.valueOf(cardTransactionItemList.get(j).getTxnType().equals("REDEEM")));
                        obj2.put("RedeemCount", String.valueOf(cardTransactionItemList.get(j).getCount()));
                        obj2.put("RedeemAmount", (new DecimalFormat("0.00").format(cardTransactionItemList.get(j).getTotalAmount())));
                    }

                    if (cardTransactionItemList.get(j).getTxnType().equals("VOID")) {
                        Utility.DEBUG_LOG("txn", String.valueOf(cardTransactionItemList.get(j).getTxnType().equals("VOID")));
                        obj2.put("VoidCount", String.valueOf(cardTransactionItemList.get(j).getCount()));
                        obj2.put("VoidAmount", (new DecimalFormat("0.00").format(cardTransactionItemList.get(j).getTotalAmount())));
                    }

                    if (cardTransactionItemList.get(j).getTxnType().equals("CASH OUT")) {
                        Utility.DEBUG_LOG("txn", String.valueOf(cardTransactionItemList.get(j).getTxnType().equals("CASH OUT")));
                        obj2.put("CashOutCount", String.valueOf(cardTransactionItemList.get(j).getCount()));
                        obj2.put("CashOutAmount", (new DecimalFormat("0.00").format(cardTransactionItemList.get(j).getTotalAmount())));
                    }

                    if (cardTransactionItemList.get(j).getTxnType().equals("REFUND")) {
                        Utility.DEBUG_LOG("txn", String.valueOf(cardTransactionItemList.get(j).getTxnType().equals("REFUND")));
                        obj2.put("RefundCount", String.valueOf(cardTransactionItemList.get(j).getCount()));
                        obj2.put("RefundAmount", (new DecimalFormat("0.00").format(cardTransactionItemList.get(j).getTotalAmount())));
                    }
                }
                String currentTime = new SimpleDateFormat("HHmmss", Locale.getDefault()).format(new Date());
                String currentDate = new SimpleDateFormat("MMddyy", Locale.getDefault()).format(new Date());
                String POSDateTime = currentTime + currentDate;
                obj2.put("PosDateTime", POSDateTime);
                transactionArr2.put(obj2);
                //Utility.DEBUG_LOG("full obj", String.valueOf(transactionArr2));

                JSONObject obj3 = new JSONObject();
                JSONArray transactionArr3 = new JSONArray();
                obj3.put("imei", "123");
                obj3.put("telco", "123");
                obj3.put("latitude", "123");
                obj3.put("longitude", "123");
                obj3.put("fieldOne", "123");
                obj3.put("fieldTwo", "123");
                obj3.put("fieldThree", "123");
                obj3.put("fieldFour", "123");
                obj3.put("fieldFive", "123");
                obj3.put("fieldSix", "123");
                transactionArr3.put(obj3);


                JSONObject obj = new JSONObject();
                obj.put("Transaction", transactionArr);
                obj.put("Summary", transactionArr2);
                obj.put("SimData", transactionArr3);

                Utility.DEBUG_LOG("full obj", String.valueOf(obj));


                JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                        url, obj,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                try {
//                                //Toast.makeText(context,"Error! :" + response.getString("Reponsemessage"), Toast.LENGTH_LONG).show();
                                    Utility.DEBUG_LOG(TAG, "Response: " + response.getString("Reponsemessage"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Utility.DEBUG_LOG("Volley", response.toString());
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Utility.DEBUG_LOG("Volley", "Error: " + error.getMessage());
                        //Toast.makeText(context,"Error! :" + error.toString(), Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                    }
                }) {
                    /**
                     * Passing some request headers
                     */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }
                };
                new NukeSSLCerts().nuke();

                jsonObjReq.setTag("Volley");
                // Adding request to request queue
                queue.add(jsonObjReq);

                // Cancelling request
//     if (queue!= null) {
//    queue.cancelAll(TAG);
            }
            else
            {
                Utility.DEBUG_LOG("BatchUploadRequest","Not allowed");
            }

        }
        public class NukeSSLCerts {
            protected static final String TAG = "NukeSSLCerts";

            public void nuke() {
                try {
                    TrustManager[] trustAllCerts = new TrustManager[] {
                            new X509TrustManager() {
                                public X509Certificate[] getAcceptedIssuers() {
                                    X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                                    return myTrustedAnchors;
                                }

                                @Override
                                public void checkClientTrusted(X509Certificate[] certs, String authType) {}

                                @Override
                                public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                            }
                    };

                    SSLContext sc = SSLContext.getInstance("SSL");
                    sc.init(null, trustAllCerts, new SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String arg0, SSLSession arg1) {
                            return true;
                        }
                    });
                } catch (Exception e) {
                }
            }
        }

    }
