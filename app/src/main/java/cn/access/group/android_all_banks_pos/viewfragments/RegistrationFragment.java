package cn.access.group.android_all_banks_pos.viewfragments;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceInfo;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.aidl.IPinpad;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.IRFCardReader;
import com.vfi.smartpos.deviceservice.aidl.ISerialPort;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Delayed;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.DecimalDigitsInputFilter;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.Utilities.ECR;
import cn.access.group.android_all_banks_pos.Utilities.FourDigitCardFormatWatcher;
import cn.access.group.android_all_banks_pos.Utilities.MerchantRegistration;
import cn.access.group.android_all_banks_pos.Utilities.NumberTextWatcherForThousand;
import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.Utilities.Validation;
import cn.access.group.android_all_banks_pos.adapter.aidListAdapter;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.contracts.PaymentContract;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.dao.TransactionDetailDao;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author muhammad.humayun
 * on 8/20/2019.
 * this is a view call which is reponsible for starting the hardware service and invoking the presenter of this view
 */
@SuppressLint("ValidFragment")
public class RegistrationFragment extends Fragment {


    @BindView(R.id.editTextTextEmailAddress)
    EditText editTextTextEmailAddress;


    @BindView(R.id.editTextPhone)
    EditText editTextPhone;

    @BindView(R.id.buttonRegistration)
    Button buttonRegistration;

    IBeeper iBeeper;
    IPrinter printer;
    public static IDeviceService idevice;
    boolean isSucc;
    AssetManager assetManager;
    private static final String TAG = "EMVDemo";
    Intent intent = new Intent();
    Unbinder unbinder;
    Dialog alertDialog;
    String buttonClicked;
    public String current = "";
    public boolean bool;
    private WeakReference<Context> mContext;
    public boolean isFallback;

    MerchantRegistration merchantRegistration = new MerchantRegistration();
    String phone = "";
    String email = "";

    @SuppressLint("ValidFragment")
    public RegistrationFragment() {

    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Utility.DEBUG_LOG(TAG,"+ FragmentTipAdjust:onCreateView() +");
        //Repository repo = new Repository(AppDatabase.getAppDatabase(Context));
        View rootView = inflater.inflate(R.layout.fragment_registration, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        return rootView;
    }


    @OnClick({R.id.buttonRegistration})
    @Nullable

    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonRegistration:
                phone = editTextPhone.getText().toString();
                email = editTextTextEmailAddress.getText().toString();
                if(phone.equals("") && email.equals("")){
                    DialogUtil.errorDialog(mContext.get(),"Error","No field should be empty!");
                }
                else if(phone.equals("")){
                    DialogUtil.errorDialog(mContext.get(),"Error","Phone number field is empty!");
                }
                else if(email.equals("")){
                    DialogUtil.errorDialog(mContext.get(),"Error","Email field is empty!");
                }
                else{
                    MainApplication.showProgressDialog(mContext.get(),"Please wait!");
                    merchantRegistration.makeJsonObjReq(mContext.get(), email, phone);
                }
                break;
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        isSucc = context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        assetManager = getResources().getAssets();
        alertDialog=new Dialog(context);
        mContext = new WeakReference<>(context);
        Utility.DEBUG_LOG(TAG,"PaymentAmountFragment:onAttach()");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if ( getActivity() != null)
            getActivity().unbindService(conn);
    }


    // connect service -- start
    /**
     * \Brief connect the VFI-Service and set some devices
     */
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            new CountDownTimer(01, 01) {
                public void onFinish() {
                    idevice = IDeviceService.Stub.asInterface(service);
                    try {
                        Utility.DEBUG_LOG("onServiceConnected","onServiceConnected--Control here..");
                        Utility.DEBUG_LOG("terminal serial", Constants.TERMINAL_SERIAL );
                        iBeeper = idevice.getBeeper();
                        printer = idevice.getPrinter();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                }

                public void onTick(long millisUntilFinished) {
                    // millisUntilFinished    The amount of time until finished.
                }
            }.start();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    /**
     * this method animates the view, and executes the purchase function of the presenter which takes amount as parameter
     */

    // mmasood: countdown timer in sales screen


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        buttonClicked = "";
        unbinder.unbind();

    }



    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            Utility.DEBUG_LOG(TAG,"+ PaymentFragment:Handler:handleMessage()+ ");
            Utility.DEBUG_LOG(TAG,"before getData");

            String str = msg.getData().toString();
            Utility.DEBUG_LOG(TAG,"Message:" + str);
            super.handleMessage(msg);
//            Toast.makeText(getContext(), msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();
        }
    };






}


