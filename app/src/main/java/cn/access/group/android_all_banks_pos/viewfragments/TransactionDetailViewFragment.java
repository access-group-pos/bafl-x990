package cn.access.group.android_all_banks_pos.viewfragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetManager;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.RemoteException;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.PrinterListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;


import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.PrinterFonts;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.ProfileAck;
import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;

import cn.access.group.android_all_banks_pos.Utilities.TransPrinter;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.Utilities.Validation;
import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrintRecpDetail;
import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrintRecpSale;
import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrintRecpSettlement;
import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrintRecpSummary;
import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrinterCanvas;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.dao.TransactionDetailDao;
import cn.access.group.android_all_banks_pos.repository.model.CardRanges;
import cn.access.group.android_all_banks_pos.repository.model.CountedTransactionItem;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.adapter.MyVoidTransactionRecyclerViewAdapter;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.presenter.SettlementPresenter;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import io.reactivex.schedulers.Schedulers;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUrl;
import static cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication.hideProgressDialog;
import static cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication.showProgressDialog;
import static cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication.successPrintingHide;
import static cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication.successPrintingShow;
import static java.lang.Thread.sleep;


/**
 * A simple {@link Fragment} subclass.
 * @author muhammad.humayun
 * on 8/30/2019.
 */
public class TransactionDetailViewFragment extends Fragment {
    //    @BindView(R.id.transactionLists)
//    RecyclerView transactionList;
    Unbinder unbinder;
    IBeeper iBeeper;
    IPrinter printer;
    public static IDeviceService idevice;
    private static final String TAG = "EMVDemo";
    Intent intent = new Intent();
    AssetManager assetManager;
    private ReceiptPrinter receiptPrinter;
    private MyVoidTransactionRecyclerViewAdapter.OnListFragmentInteractionListener mListener;
    @BindView(R.id.detailReport)
    CardView detailReport;
    @BindView(R.id.summaryReport)
    CardView summaryReport;
    @BindView(R.id.anyReport)
    CardView anyReport;
    @BindView(R.id.lastReceipt)
    CardView lastReceipt;
    @BindView(R.id.lastSettleReport)
    CardView lastSettleReport;
//    TransactionDetail transaction;

    List<TransactionDetail> saveList ;
    List<CountedTransactionItem> saveCountedItemList;
    String printfinished = "";
    private WeakReference<Context> mContext;
    TransPrinter transPrinter;
    //PrinterCanvas pc;
    ProgressDialog bar;
    SweetAlertDialog dialog;


    private String headerType="";

    public TransactionDetailViewFragment() {
        receiptPrinter = new ReceiptPrinter(handler);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        assetManager = getResources().getAssets();
        // Pass your adapter interface in the constructor
        mContext = new WeakReference<>(context);
        Utility.DEBUG_LOG("reports", String.valueOf(context));
        sumAndCountTransactions();
        cardAndCountTransactions();
        //pc = new  PrinterCanvas(mContext);

        //findByInvoiceNumber(appDatabase, password);
    }


    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onPause() {
        super.onPause();
        PowerManager pm = (PowerManager) mContext.get().getSystemService(Context.POWER_SERVICE);
        if (!pm.isScreenOn()) {
            Utility.DEBUG_LOG(TAG, "wake up");
            PowerManager.WakeLock mWakelock;
            mWakelock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP |
                    PowerManager.SCREEN_DIM_WAKE_LOCK, "target");
            mWakelock.acquire();
            mWakelock.release();
        }
    }

    @SuppressLint({"CheckResult", "InvalidWakeLockTag"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_void_transaction, container, false);
        unbinder = ButterKnife.bind(this, view);
        SharedPref.write("lastSettle","N");
        SharedPref.write("ListReport","N");
        SharedPref.write("reportType","null");
        Utility.DEBUG_LOG(TAG,"TransactionDetailViewFragment onCreate(): List report "+SharedPref.read("ListReport",""));
//        try {
//            setRecycleViewData(getSaveSettlement());
//            Utility.DEBUG_LOG("Masood12", String.valueOf(getSaveSettlement().size()));
//        } catch (ExecutionException | InterruptedException | RemoteException e) {
//            e.printStackTrace();
//        }



        InitializeFontFiles();
        return view;
    }
    //
//    private void setRecycleViewData(List<SaveSettlement> saveSettlementList) throws RemoteException {
//        TransactionDetailAdapter adapter = new TransactionDetailAdapter(saveSettlementList);
//        transactionList.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
//
//    }
    // check assets fonts and copy to file system for Service -- start
    private void InitializeFontFiles() {
        PrinterFonts.initialize(assetManager);
    }
    //
    @SuppressLint({"CheckResult", "InvalidWakeLockTag"})
    @OnClick({R.id.detailReport,R.id.summaryReport,R.id.anyReport,R.id.lastReceipt,R.id.lastSettleReport})
    @Nullable
    public void onViewClicked(View view) {

        switch (view.getId()) {

            case R.id.detailReport:

                SharedPref.write("reportType","detailReport");
                if(mContext.get() != null) {
                  //
                    detailReport.setEnabled(false);
                    summaryReport.setEnabled(false);
                    lastReceipt.setEnabled(false);
                    lastSettleReport.setEnabled(false);
                    anyReport.setEnabled(false);

                        Utility.DEBUG_LOG(TAG,"Printing detail report");
                        try {
                            if (getAllTransactions().size() != 0 || sumTransactionItemList.size() != 0) {

                                List<TransactionDetail> allTransactions = getAllTransactions();
                                Utility.DEBUG_LOG(TAG,"allTransactions:"+allTransactions);
                                if ( allTransactions != null )
                                {
                                    SharedPref.write("ListReport","Y");

                                    new Thread(() -> {
                                        ((DashboardContainer) mContext.get()).runOnUiThread(new Runnable() {

                                            @Override
                                            public void run() {
                                                showProgressDialog(mContext.get(), "Printing..");
                                            }
                                        });



                                        Utility.DEBUG_LOG(TAG, "in detail thread");
                                        transPrinter = new PrintRecpDetail(mContext.get(),allTransactions, sumTransactionItemList);
                                        transPrinter.initializeData(allTransactions, sumTransactionItemList);
                                        transPrinter.print(allTransactions);
                                        Utility.DEBUG_LOG(TAG, "after print detail thread");
                                        //MainApplication.successPrintingShow(mContext.get(),"Print Finished..");
                                    }).start();


                                    Utility.DEBUG_LOG(TAG,"allTransactions:"+allTransactions.toString());

                                    Utility.DEBUG_LOG(TAG,"TransactionDetailViewFragment detailReport(): List report "+SharedPref.read("ListReport",""));
                                }

                            } else {
                                hideProgressDialog();
                               DashboardContainer.backStack();
                                DialogUtil.errorDialog(mContext.get(), "Error!", "Batch is empty!");
                            }
                        } catch (ExecutionException | InterruptedException e) {
                                hideProgressDialog();
                            e.printStackTrace();
                        }
//                        sweetAlertDialog1.dismissWithAnimation();
//
//                    });
                }else{
                    hideProgressDialog();
                    Utility.DEBUG_LOG("detailReport","Error");
                }

                break;

            case R.id.summaryReport:
                SharedPref.write("reportType","summaryReport");
                if(mContext.get() != null) {
                    detailReport.setEnabled(false);
                    summaryReport.setEnabled(false);
                    lastReceipt.setEnabled(false);
                    lastSettleReport.setEnabled(false);
                    anyReport.setEnabled(false);
                    SharedPref.write("ListReport","N");
                    showProgressDialog(mContext.get(), "printing");
                    //DialogUtil.confirmDialog(mContext.get(), "Print", sweetAlertDialog1 -> {
                        if (cardTransactionItemList.size() != 0) {

                            transPrinter = new PrintRecpSummary(mContext.get());
                            transPrinter.initializeData(cardTransactionItemList);
                            transPrinter.print();
                            DialogUtil.successDialog(mContext.get(),"Print Finished");
                            DashboardContainer.backStack();
                        } else {
                            hideProgressDialog();
                            DialogUtil.errorDialog(mContext.get(), "Error!", "Batch is empty!");
                            DashboardContainer.backStack();
                        }
                }else{
                    hideProgressDialog();
                    Utility.DEBUG_LOG("summaryReport","Error");
                }
                break;
            case R.id.lastReceipt:
                SharedPref.write("reportType","lastReceipt");
                if(mContext.get()!=null) {
                    detailReport.setEnabled(false);
                    summaryReport.setEnabled(false);
                    lastReceipt.setEnabled(false);
                    lastSettleReport.setEnabled(false);
                    anyReport.setEnabled(false);
                    SharedPref.write("ListReport","N");
                   showProgressDialog(mContext.get(), "printing");

                  //  Repository repository = new Repository(mContext.get());

                    TransactionDetail defaultTransactionDetail = new TransactionDetail();
                    defaultTransactionDetail.setInvoiceNo("-1");
                    AppDatabase.getAppDatabase(mContext.get()).transactionDetailDao().lastReceipt()
                            .subscribeOn(Schedulers.newThread())
                            .defaultIfEmpty(defaultTransactionDetail)
                            .subscribe(transactionDetail ->{

                                Utility.DEBUG_LOG(TAG,"Lasttransaction: "+ transactionDetail.getTxnType());
                            handler.postDelayed(() -> {
                            if (!transactionDetail.getInvoiceNo().equals("-1")) {
//                                transPrinter = new PrintRecpSale(mContext.get());
//                                transPrinter.initializeData(transactionDetail,"",false,"MERCHANT COPY");
//                                transPrinter.print();
                                receiptPrinter.printSaleReceipt(printer, assetManager, transactionDetail, transactionDetail.getAidCode(), "Merchant Copy", transactionDetail.getTxnType(), "DUPLICATE","",false);
                                    hideProgressDialog();
                                DialogUtil.confirmDialogCustomer(mContext.get(), "CUSTOMER COPY", "Print Customer Copy", "", sweetAlertDialog1 -> { // SHIVAM UPDATE HERE FOR TEXT PRINTER
//                                    transPrinter = new PrintRecpSale(mContext.get());
//                                        transPrinter.initializeData(transactionDetail, "", false, "CUSTOMER COPY");
//                                        transPrinter.print();
                                    receiptPrinter.printSaleReceipt(printer, assetManager, transactionDetail,transactionDetail.getAidCode() , "CUSTOMER COPY", transactionDetail.getTxnType(),"DUPLICATE","",true);
                                        sweetAlertDialog1.dismissWithAnimation();
                                        DashboardContainer.backStack();
                                        Constants.BANK_COPY = true;
                                    });

                            } else {
                                hideProgressDialog();
                                DashboardContainer.backStack();
                                DialogUtil.errorDialog(mContext.get(), "Error!", "Batch is empty!");
                            }
                            }, 3000);
                            });
                }else{
                    hideProgressDialog();
                    Utility.DEBUG_LOG("LastReceipt","Error");
                }


                break;
            case R.id.anyReport:
                SharedPref.write("reportType","anyReport");
                if(mContext.get()!=null) {
                    detailReport.setEnabled(false);
                    summaryReport.setEnabled(false);
                    lastReceipt.setEnabled(false);
                    lastSettleReport.setEnabled(false);
                    anyReport.setEnabled(false);
                    DialogUtil.showKey(getActivity());
                    SharedPref.write("ListReport","N");
                    // receiptPrinter.printSaleInvoiceReceipt(printer,assetManager, transaction, "000000");
                    DialogUtil.inputDialogAnyReport(mContext.get(), "Enter Invoice Number", "Enter Invoice Number", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            EditText text = sweetAlertDialog.getWindow().getDecorView().findViewById(R.id.my_edit_text_1);
                            //show confirmation
                            showProgressDialog(mContext.get(), "Searching..");
                            TransactionDetail defaultTransactionDetail = new TransactionDetail();
                            defaultTransactionDetail.setInvoiceNo("-1");
                            AppDatabase.getAppDatabase(mContext.get()).transactionDetailDao().findByInvoiceNo(text.getText().toString())
                                    .subscribeOn(Schedulers.newThread())
                                    .defaultIfEmpty(defaultTransactionDetail)
                                    .subscribe(transaction ->{
                                        DialogUtil.hideKeyboard(getActivity());
                                        handler.postDelayed(() -> {

                                            if (!transaction.getInvoiceNo().equals("-1")) {
                                                hideProgressDialog();
                                                showProgressDialog(mContext.get(), "Printing..");
//                                                transPrinter = new PrintRecpSale(mContext.get());
//                                                transPrinter.initializeData(transaction,"",false,"MERCHANT COPY");
//                                                transPrinter.print();
                                                receiptPrinter.printSaleReceipt(printer, assetManager, transaction, transaction.getAidCode(), "Merchant Copy", transaction.getTxnType(), "DUPLICATE","",false);
                                                hideProgressDialog();
                                                DialogUtil.confirmDialogCustomer(mContext.get(), "CUSTOMER COPY", "Print Customer Copy", "", sweetAlertDialog1 -> { // SHIVAM UPDATE HERE FOR TEXT PRINTER
//                                                    transPrinter = new PrintRecpSale(mContext.get());
//                                                    transPrinter.initializeData(transaction,"",false,"CUSTOMER COPY");
//                                                    transPrinter.print();
                                                    receiptPrinter.printSaleReceipt(printer, assetManager, transaction,transaction.getAidCode() , "CUSTOMER COPY", transaction.getTxnType(),"DUPLICATE","",true);
                                                    sweetAlertDialog1.dismissWithAnimation();
                                                    DashboardContainer.backStack();
                                                    hideProgressDialog();
                                                    //Constants.BANK_COPY = true;
                                                });



                                            } else {
                                                hideProgressDialog();
                                                DialogUtil.errorDialog(mContext.get(), "Error!", "Invoice number not found!");
                                                DashboardContainer.backStack();
                                                sweetAlertDialog.dismissWithAnimation();
                                            }

                                        }, 3000);
                                    });

                            sweetAlertDialog.dismissWithAnimation();

                        }
                    }, InputType.TYPE_CLASS_NUMBER, null);
                }
                else{
                    Utility.DEBUG_LOG("anyReport","Error");
                }

                break;
            case R.id.lastSettleReport:

                if(mContext.get()!=null) {
                    SharedPref.write("reportType","lastSettleReport");
                    detailReport.setEnabled(false);
                    summaryReport.setEnabled(false);
                    lastReceipt.setEnabled(false);
                    lastSettleReport.setEnabled(false);
                    anyReport.setEnabled(false);
                    try {
                        saveCountedItemList = SharedPref.countedTransactionRead(mContext.get());
                        saveList = SharedPref.getList(mContext.get());

                        if (saveCountedItemList != null || saveList != null) {
                            SharedPref.write("ListReport", "Y");
                            SharedPref.write("lastSettle", "Y");

                            Utility.DEBUG_LOG(TAG, "before last settle thread");
                            showProgressDialog(mContext.get(), "Printing..");
                            new Thread(new Runnable() {
                                public void run() {
                                    Utility.DEBUG_LOG(TAG, "in last settle thread");
                                    transPrinter = new PrintRecpSettlement(mContext.get(), saveList, saveCountedItemList);
                                    transPrinter.initializeData(saveList, saveCountedItemList);
                                    transPrinter.print(saveList);
                                    Utility.DEBUG_LOG(TAG, "after print last settle thread");
                                   // printingShow(mContext.get(),"Print Finished");
                                }

                            }).start();
                            Utility.DEBUG_LOG(TAG, "after last settle thread");
                        } else {
                            hideProgressDialog();
                            DashboardContainer.backStack();
                            DialogUtil.errorDialog(mContext.get(), "Error!", "Batch is empty!");
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        hideProgressDialog();
                        DashboardContainer.backStack();
                        DialogUtil.errorDialog(mContext.get(), "Error!", "Batch is empty!");
                    }
                }
                else{
                    hideProgressDialog();
                    Utility.DEBUG_LOG("lastSettlementReport","Error");
                }
                break;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        if (getActivity() != null ) {
            getActivity().unbindService(conn);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
       // hideProgressDialog();
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        hideProgressDialog();
    }

    public List<TransactionDetail> getAllTransactions() throws ExecutionException, InterruptedException {
        return ((DashboardContainer)mContext.get()).getAllBatchTransactionsNotPreAuth();
    }


    public void printingShow(Context context,String content){
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MainApplication.successPrintingShow(context, content);
                }
            });
        }
        else{
            Utility.DEBUG_LOG(TAG,"prinitng error");
        }
    }
    public void printingHide(){
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    MainApplication.successPrintingHide();
                }
            });
        }
        else{
            Utility.DEBUG_LOG(TAG,"prinitng error");
        }
    }




    // connect service -- start
    /**
     * \Brief connect the VFI-Service and set some devices
     */
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            idevice = IDeviceService.Stub.asInterface(service);
            try {
                iBeeper = idevice.getBeeper();
                printer = idevice.getPrinter();
                TransPrinter.initialize(printer);

            } catch (RemoteException e) {
                e.printStackTrace();
            }
          //  Toast.makeText(mContext.get(), "bind service success", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Utility.DEBUG_LOG(TAG,"+ handleMessage +");
            Utility.DEBUG_LOG(TAG,"msg:" + msg.toString());
            Utility.DEBUG_LOG(TAG,"before getData");

            String string = msg.getData().getString("string");
            super.handleMessage(msg);
            Utility.DEBUG_LOG(TAG, msg.getData().getString("msg"));
            printfinished = msg.getData().getString("msg");
            //Toast.makeText(mContext.get(), printfinished, Toast.LENGTH_SHORT).show();

        }
    };
    private List <CountedTransactionItem> sumTransactionItemList;
    @SuppressLint("CheckResult")
    private void sumAndCountTransactions(){
        Utility.DEBUG_LOG("sumAndCountTransactions","sumAndCountTransactions");
        AppDatabase.getAppDatabase(mContext.get()).transactionDetailDao().transactionSumCount()
                .subscribeOn(Schedulers.newThread())
                .subscribe(count ->
                        { sumTransactionItemList=count;
                            Utility.DEBUG_LOG("Shivam", String.valueOf(sumTransactionItemList));
                        }
                );
    }

    private List <CountedTransactionItem> cardTransactionItemList;
    @SuppressLint("CheckResult")
    private void cardAndCountTransactions(){
        AppDatabase.getAppDatabase(mContext.get()).transactionDetailDao().transactionCardSchemeCount()
                .subscribeOn(Schedulers.newThread())
                .subscribe(count ->
                        { cardTransactionItemList=count;
                        }
                );
    }






//    @SuppressLint("ObsoleteSdkInt")
//    public void executeAsyncTask() {
//        @SuppressLint("StaticFieldLeak")
//        AsyncTask<String, Integer, Boolean> task = new AsyncTask<String, Integer, Boolean>() {
//
//            //show the progress dialog
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                bar = new ProgressDialog(mContext.get());
//                bar.setCancelable(false);
//
//                bar.setMessage("Downloading...");
//                //bar.setIndeterminate(true);
//                bar.setCanceledOnTouchOutside(false);
//                bar.show();
//            }
//
//            // show progress update
//            protected void onProgressUpdate(Integer... progress) {
//                super.onProgressUpdate(progress);
//                // bar.setIndeterminate(false);
//                bar.setMax(100);
//                bar.setProgress(progress[0]);
//                String msg = "";
//                if (progress[0] > 99) {
//                    msg = "Finishing... ";
//                } else {
//                    msg = "Downloading... " + progress[0] + "%";
//                }
//                bar.setMessage(msg);
//            }
//
//            @Override
//            protected void onPostExecute(Boolean result) {
//                // TODO Auto-generated method stub
//                super.onPostExecute(result);
//                bar.dismiss();
//                if (result) {
//                    Toast.makeText(mContext.get(), "Done!!", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(mContext.get(), "Error: Try Again", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            protected Boolean doInBackground(String... arg0) {
//                Boolean flag = false;
//                try {
//
//                    if (mContext.get() != null) {
//
//                        Utility.DEBUG_LOG(TAG, "Printing detail report");
//                        if (getAllTransactions().size() != 0 || sumTransactionItemList.size() != 0) {
//
//                            List<TransactionDetail> allTransactions = getAllTransactions();
//                            Utility.DEBUG_LOG(TAG, "allTransactions:" + allTransactions);
//                            if (allTransactions != null) {
//
//                                SharedPref.write("ListReport", "Y");
//                                Utility.DEBUG_LOG(TAG, "allTransactions:" + allTransactions.toString());
//
//                                Utility.DEBUG_LOG(TAG, "TransactionDetailViewFragment detailReport(): List report " + SharedPref.read("ListReport", ""));
//                                flag = true;
//                            }
//
//                        } else {
//                            bar.hide();
//                            DashboardContainer.switchFragment(new HomeFragment(), "HomeFragment");
//                            DialogUtil.errorDialog(mContext.get(), "Error!", "Batch is empty!");
//                            flag = false;
//                        }
//
//                    } else {
//                        flag = false;
//                        bar.hide();
//                        Utility.DEBUG_LOG("detailReport", "Error");
//                    }
//
//                } catch (Exception e) {
//                    Utility.DEBUG_LOG("TAG", "Update Error: " + e.getMessage());
//
//                    bar.hide();
//                    DialogUtil.errorDialog(mContext.get(), "Update Error:", e.getMessage());
//                    e.printStackTrace();
//                    flag = false;
//                }
//                return flag;
//            }
//        };
//
//        if(Build.VERSION.SDK_INT >= 11/*HONEYCOMB*/) {
//            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//        } else {
//            task.execute();
//        }
//
//    }


//    @SuppressLint("ObsoleteSdkInt")
//    public void executeAsyncTask()
//    {
//        @SuppressLint("StaticFieldLeak")
//        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, Boolean>() {
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                Utility.DEBUG_LOG("AsyncTask", "onPreExecute");
//            }
//
//            @Override
//            protected Boolean doInBackground(Void... params) {
//                Utility.DEBUG_LOG("AsyncTask", "doInBackground");
//                Boolean msg = false;
//                // some calculation logic of msg variable
//                return msg;
//            }
//
//            @Override
//            public void onPostExecute(boolean msg) {
//                super.onPostExecute(msg);
//                Utility.DEBUG_LOG("AsyncTask", "onPostExecute");
//            }
//        };
//
//        if(Build.VERSION.SDK_INT >= 11/*HONEYCOMB*/) {
//            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//        } else {
//            task.execute();
//        }
//    }

}
