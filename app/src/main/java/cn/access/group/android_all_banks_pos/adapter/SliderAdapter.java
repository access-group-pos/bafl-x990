package cn.access.group.android_all_banks_pos.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import java.util.List;

import cn.access.group.android_all_banks_pos.R;

public class SliderAdapter extends PagerAdapter {

    private Context context;
    private List<Integer> adImage;

    public SliderAdapter(Context context, List<Integer> adImage) {
        this.context = context;
        this.adImage = adImage;
    }

    @Override
    public int getCount() {
        return adImage.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_slider, null);

        ImageView linearLayout = (ImageView) view.findViewById(R.id.linearLayout);


        linearLayout.setImageResource(adImage.get(position));

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}
