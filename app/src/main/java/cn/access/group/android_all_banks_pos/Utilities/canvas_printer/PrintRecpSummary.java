package cn.access.group.android_all_banks_pos.Utilities.canvas_printer;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import cn.access.group.android_all_banks_pos.Utilities.Utility;

import com.vfi.smartpos.deviceservice.aidl.PrinterConfig;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.TransPrinter;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.repository.model.CountedTransactionItem;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;


/**
 * Created by Simon on 2019/5/15.
 */

public class PrintRecpSummary extends TransPrinter {
    private static final String TAG = "PrintRecpSummary";
    public PrintRecpSummary(Context context) {
        super(context);

    }


    public String getCardTypeMode(int im_pan) {
        String cardTypeMode;
        switch (im_pan) {
            case 1:
                cardTypeMode = "M";
                break;
            case 2:
                cardTypeMode = "S";
                break;
            case 5:
                cardTypeMode = "C";
                break;
            case 7:
                cardTypeMode = "Q";
                break;
            default:
                cardTypeMode = "M";
                break;
        }
        return cardTypeMode;
    }

    // Here to "draw" your receipt with various PrinterItem
    @SuppressLint("LongLogTag")
    public void initializeData (List<CountedTransactionItem> cardTransactionItemList) {
        super.initializeData(cardTransactionItemList);
        Utility.DEBUG_LOG(TAG, "initializeData");
        try {
            String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
            String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

            //Get some extra infos about this receipt, like the index of copy and if it is reprint
            String tmp;
            int copyIndex = 2;
            //int copyIndex = extraItems.getInt("copyIndex");
            //Boolean isReprint = extraItems.getBoolean("reprint", false);

            // A List to put PrintItems, printCanvas will resolve this list to draw receipt
            printerItems = new ArrayList<>();
            Utility.DEBUG_LOG(TAG,"This Works");
            // The LOGO on the top of receipt, set stype to align center.
            PrinterItem.LOGO.title.sValue = "logo4.jpg";
            PrinterItem.LOGO.title.style = PrinterDefine.PStyle_align_center;
            printerItems.add(PrinterItem.LOGO);

            //Merchant name
            // PrinterItem.MERCHANT_NAME.title. = PrinterDefine.PStyle_align_center;
            printerItems.add(PrinterItem.MERCHANT_NAME);
            // HEADER LINES
            printerItems.add(PrinterItem.HEADER1);
            printerItems.add(PrinterItem.HEADER2);
            printerItems.add(PrinterItem.HEADER3);
            printerItems.add(PrinterItem.HEADER4);



            // Merchant id
            PrinterItem.MERCHANT_ID.value.sValue = Constants.MERCHANT_ID;
            printerItems.add(PrinterItem.MERCHANT_ID);
            PrinterItem.TERMINAL_ID.value.sValue = Constants.TERMINAL_ID;;
            printerItems.add(PrinterItem.TERMINAL_ID);
            // DATE AND TIME
            PrinterItem.DATE_TIME.title.sValue = "DATE: "+currentDate;
            PrinterItem.DATE_TIME.value.sValue = "TIME: "+ currentTime;
            //Utility.DEBUG_LOG(" PrinterItem.DATE_TIME.value.sValue", PrinterItem.DATE_TIME.value.sValue);
            printerItems.add(PrinterItem.DATE_TIME);
            // BATCH AND INVOICE
            PrinterItem.BATCH_NO_DETAIL.title.sValue = "BATCH: " + Constants.BATCH_NO;
            printerItems.add(PrinterItem.BATCH_NO_DETAIL);

            //SPACE
            printerItems.add(PrinterItem.FEED_LINE);

            //Heading summary report
            PrinterItem.SUMMARY_HEADING.title.sValue = "SUMMARY REPORT";
            printerItems.add(PrinterItem.SUMMARY_HEADING);
            //SPACE
            printerItems.add(PrinterItem.FEED_LINE);


            //SUMMARY HEADING 2
            printerItems.add(PrinterItem.SUMMARY_HEADING2);

            //SPACE
            printerItems.add(PrinterItem.FEED_LINE);

            // TRANS TYPE LOOP IT N
            String visaSaleType="VISA CARD";
            String visaCashOut ="VISA CASH";
            String visaOrbitRedeem = "VISA REDEEM";
            int visaSaleCount=0, visaCashOutCount=0, visaOrbitRedeemCount=0;
            double visaSaleAmount = 0.00, visaCashOutAmount=0.00, visaOrbitRedeemAmount=0.00;
            String upiSaleType="UPI CARD";
            String upiCashOut ="UPI CASH";
            String upiOrbitRedeem = "UPI REDEEM";
            int upiSaleCount=0, upiCashOutCount=0, upiOrbitRedeemCount=0;
            double upiSaleAmount = 0.00, upiCashOutAmount=0.00, upiOrbitRedeemAmount=0.00;
            String mcSaleType="MASTER CARD";
            String mcCashOut ="MASTER CASH";
            String mcOrbitRedeem = "MASTER REDEEM";
            int mcSaleCount=0, mcCashOutCount=0, mcOrbitRedeemCount=0, netSaleCount=0;
            double mcSaleAmount = 0.00, mcCashOutAmount=0.00, mcOrbitRedeemAmount=0.00, netSale=0.00;
            String payPakSaleType="PAYPAK CARD";
            String payPakCashOut ="PAYPAK CASH";
            String payPakOrbitRedeem = "PAYPAK REDEEM";
            int payPakSaleCount=0, payPakCashOutCount=0, payPakOrbitRedeemCount=0;
            double payPakSaleAmount = 0.00, payPakCashOutAmount=0.00, payPakOrbitRedeemAmount=0.00;
            String jcbSaleType="JCB CARD";
            String jcbCashOut ="JCB CASH";
            String jcbOrbitRedeem = "JCB REDEEM";
            int jcbSaleCount=0, jcbCashOutCount=0, jcbOrbitRedeemCount=0;
            double jcbSaleAmount = 0.00, jcbCashOutAmount=0.00, jcbOrbitRedeemAmount=0.00;
            String amexSaleType="AMEX CARD";
            String amexCashOut ="AMEX CASH";
            String amexOrbitRedeem = "AMEX REDEEM";
            int amexSaleCount=0, amexCashOutCount=0, amexOrbitRedeemCount=0;
            double amexSaleAmount = 0.00, amexCashOutAmount=0.00, amexOrbitRedeemAmount=0.00;
            DecimalFormat df2 = new DecimalFormat("0.00");
            double saleWithTipAmount = 0.00;
            String saleWithTipCount;

            int sumcardcount=0; float sumcardamount=0;
            String cardCount,cardSum;
            Boolean visa= false, upi = false, master=false, paypak = false, jcb = false, amex=false;

            for(int i =0;i<cardTransactionItemList.size();i++){
                if(cardTransactionItemList.get(i).toString().equals("VISA")) {
                    visa = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("COMPLETION") || cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        visaSaleCount += cardTransactionItemList.get(i).getCount();
                        visaSaleAmount = visaSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        visaCashOutCount += cardTransactionItemList.get(i).getCount();
                        visaCashOutAmount = visaCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        visaOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        visaOrbitRedeemAmount = visaOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                } else if (cardTransactionItemList.get(i).toString().equals("UPI") ) {
                    upi = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("COMPLETION") || cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        upiSaleCount += cardTransactionItemList.get(i).getCount();
                        upiSaleAmount = upiSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        upiCashOutCount += cardTransactionItemList.get(i).getCount();
                        upiCashOutAmount = upiCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        upiOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        upiOrbitRedeemAmount = upiOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                }
                else if(cardTransactionItemList.get(i).toString().equals("MASTERCARD")) {
                    master = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("COMPLETION") || cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        mcSaleCount += cardTransactionItemList.get(i).getCount();
                        mcSaleAmount = mcSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        mcCashOutCount += cardTransactionItemList.get(i).getCount();
                        mcCashOutAmount = mcCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        mcOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        mcOrbitRedeemAmount = mcOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                }
                else if(cardTransactionItemList.get(i).toString().equals("PAYPAK")) {
                    paypak = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("COMPLETION") || cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        payPakSaleCount += cardTransactionItemList.get(i).getCount();
                        payPakSaleAmount = payPakSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        payPakCashOutCount += cardTransactionItemList.get(i).getCount();
                        payPakCashOutAmount = payPakCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        payPakOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        payPakOrbitRedeemAmount = payPakOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                }
                else if(cardTransactionItemList.get(i).toString().equals("JCB")) {
                    jcb = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("COMPLETION") || cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        jcbSaleCount += cardTransactionItemList.get(i).getCount();
                        jcbSaleAmount = jcbSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if (cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        jcbCashOutCount += cardTransactionItemList.get(i).getCount();
                        jcbCashOutAmount = jcbCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        jcbOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        jcbOrbitRedeemAmount = jcbOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                }
                else if(cardTransactionItemList.get(i).toString().equals("AMEX")) {
                    amex = true;
                    if (cardTransactionItemList.get(i).getTxnType().equals("COMPLETION") || cardTransactionItemList.get(i).getTxnType().equals("SALE") || cardTransactionItemList.get(i).getTxnType().equals("ADJUST") || cardTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                        amexSaleCount += cardTransactionItemList.get(i).getCount();
                        amexSaleAmount = amexSaleAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                        amexCashOutCount += cardTransactionItemList.get(i).getCount();
                        amexCashOutAmount = amexCashOutAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                    if(cardTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                        amexOrbitRedeemCount += cardTransactionItemList.get(i).getCount();
                        amexOrbitRedeemAmount = amexOrbitRedeemAmount + cardTransactionItemList.get(i).getTotalAmount();
                    }
                }


                if(!(cardTransactionItemList.get(i).getTxnType().equals("VOID"))) {
                    sumcardcount += cardTransactionItemList.get(i).getCount();
                    sumcardamount += Double.parseDouble(cardTransactionItemList.get(i).getTotalAmount().toString());
                    if(Constants.TIP_ENABLED.equals("Y")) {
                        saleWithTipAmount += cardTransactionItemList.get(i).getTotaTiplAmount();
                    }
                }
            }
            Utility.DEBUG_LOG("summaryTotalAmount", String.valueOf(sumcardcount));
            if(visa) {
                PrinterItem.VISA_SALETYPE.title.sValue = visaSaleType;
                PrinterItem.VISA_SALETYPE.title.secValue = String.valueOf(visaSaleCount);
                PrinterItem.VISA_SALETYPE.value.sValue = String.valueOf(df2.format(visaSaleAmount));
                printerItems.add(PrinterItem.VISA_SALETYPE);

                PrinterItem.VISA_CASHOUT.title.sValue = visaCashOut;
                PrinterItem.VISA_CASHOUT.title.secValue = String.valueOf(visaCashOutCount);
                PrinterItem.VISA_CASHOUT.value.sValue = String.valueOf(df2.format(visaCashOutAmount));
                printerItems.add(PrinterItem.VISA_CASHOUT);

                PrinterItem.VISA_REDEEM.title.sValue = visaOrbitRedeem;
                PrinterItem.VISA_REDEEM.title.secValue = String.valueOf(visaOrbitRedeemCount);
                PrinterItem.VISA_REDEEM.value.sValue = String.valueOf(df2.format(visaOrbitRedeemAmount));
                printerItems.add(PrinterItem.VISA_REDEEM);
                printerItems.add(PrinterItem.FEED_LINE);

            }
            if(upi) {
                PrinterItem.UPI_SALETYPE.title.sValue = upiSaleType;
                PrinterItem.UPI_SALETYPE.title.secValue = String.valueOf(upiSaleCount);
                PrinterItem.UPI_SALETYPE.value.sValue = String.valueOf(df2.format(upiSaleAmount));
                printerItems.add(PrinterItem.UPI_SALETYPE);

                PrinterItem.UPI_CASHOUT.title.sValue = upiCashOut;
                PrinterItem.UPI_CASHOUT.title.secValue = String.valueOf(upiCashOutCount);
                PrinterItem.UPI_CASHOUT.value.sValue = String.valueOf(df2.format(upiCashOutAmount));
                printerItems.add(PrinterItem.UPI_CASHOUT);

                PrinterItem.UPI_REDEEM.title.sValue = upiOrbitRedeem;
                PrinterItem.UPI_REDEEM.title.secValue = String.valueOf(upiOrbitRedeemCount);
                PrinterItem.UPI_REDEEM.value.sValue = String.valueOf(df2.format(upiOrbitRedeemAmount));
                printerItems.add(PrinterItem.UPI_REDEEM);
                printerItems.add(PrinterItem.FEED_LINE);

            }
            if(master) {
                PrinterItem.MASTER_SALETYPE.title.sValue = mcSaleType;
                PrinterItem.MASTER_SALETYPE.title.secValue = String.valueOf(mcSaleCount);
                PrinterItem.MASTER_SALETYPE.value.sValue = String.valueOf(df2.format(mcSaleAmount));
                printerItems.add(PrinterItem.MASTER_SALETYPE);

                PrinterItem.MASTER_CASHOUT.title.sValue = mcCashOut;
                PrinterItem.MASTER_CASHOUT.title.secValue = String.valueOf(mcCashOutCount);
                PrinterItem.MASTER_CASHOUT.value.sValue = String.valueOf(df2.format(mcCashOutAmount));
                printerItems.add(PrinterItem.MASTER_CASHOUT);

                PrinterItem.MASTER_REDEEM.title.sValue = mcOrbitRedeem;
                PrinterItem.MASTER_REDEEM.title.secValue = String.valueOf(mcOrbitRedeemCount);
                PrinterItem.MASTER_REDEEM.value.sValue = String.valueOf(df2.format(mcOrbitRedeemAmount));
                printerItems.add(PrinterItem.MASTER_REDEEM);
                printerItems.add(PrinterItem.FEED_LINE);
            }
            if(paypak) {
                PrinterItem.PAYPAK_SALETYPE.title.sValue = payPakSaleType;
                PrinterItem.PAYPAK_SALETYPE.title.secValue = String.valueOf(payPakSaleCount);
                PrinterItem.PAYPAK_SALETYPE.value.sValue = String.valueOf(df2.format(payPakSaleAmount));
                printerItems.add(PrinterItem.PAYPAK_SALETYPE);

                PrinterItem.PAYPAK_CASHOUT.title.sValue = payPakCashOut;
                PrinterItem.PAYPAK_CASHOUT.title.secValue = String.valueOf(payPakCashOutCount);
                PrinterItem.PAYPAK_CASHOUT.value.sValue = String.valueOf(df2.format(payPakCashOutAmount));
                printerItems.add(PrinterItem.PAYPAK_CASHOUT);

                PrinterItem.PAYPAK_REDEEM.title.sValue = payPakOrbitRedeem;
                PrinterItem.PAYPAK_REDEEM.title.secValue = String.valueOf(payPakOrbitRedeemCount);
                PrinterItem.PAYPAK_REDEEM.value.sValue = String.valueOf(df2.format(payPakOrbitRedeemAmount));
                printerItems.add(PrinterItem.PAYPAK_REDEEM);
                printerItems.add(PrinterItem.FEED_LINE);

            }
            if(jcb) {
                PrinterItem.JCB_SALETYPE.title.sValue = jcbSaleType;
                PrinterItem.JCB_SALETYPE.title.secValue = String.valueOf(jcbSaleCount);
                PrinterItem.JCB_SALETYPE.value.sValue = String.valueOf(df2.format(jcbSaleAmount));
                printerItems.add(PrinterItem.JCB_SALETYPE);

                PrinterItem.JCB_CASHOUT.title.sValue = jcbCashOut;
                PrinterItem.JCB_CASHOUT.title.secValue = String.valueOf(jcbCashOutCount);
                PrinterItem.JCB_CASHOUT.value.sValue = String.valueOf(df2.format(jcbCashOutAmount));
                printerItems.add(PrinterItem.JCB_CASHOUT);

                PrinterItem.JCB_REDEEM.title.sValue = jcbOrbitRedeem;
                PrinterItem.JCB_REDEEM.title.secValue = String.valueOf(jcbOrbitRedeemCount);
                PrinterItem.JCB_REDEEM.value.sValue = String.valueOf(df2.format(jcbOrbitRedeemAmount));
                printerItems.add(PrinterItem.JCB_REDEEM);
                printerItems.add(PrinterItem.FEED_LINE);

            }
            if(amex) {
                PrinterItem.AMEX_SALETYPE.title.sValue = amexSaleType;
                PrinterItem.AMEX_SALETYPE.title.secValue = String.valueOf(amexSaleCount);
                PrinterItem.AMEX_SALETYPE.value.sValue = String.valueOf(df2.format(amexSaleAmount));
                printerItems.add(PrinterItem.AMEX_SALETYPE);

                PrinterItem.AMEX_CASHOUT.title.sValue = amexCashOut;
                PrinterItem.AMEX_CASHOUT.title.secValue = String.valueOf(amexCashOutCount);
                PrinterItem.AMEX_CASHOUT.value.sValue = String.valueOf(df2.format(amexCashOutAmount));
                printerItems.add(PrinterItem.AMEX_CASHOUT);

                PrinterItem.AMEX_REDEEM.title.sValue = amexOrbitRedeem;
                PrinterItem.AMEX_REDEEM.title.secValue = String.valueOf(amexOrbitRedeemCount);
                PrinterItem.AMEX_REDEEM.value.sValue = String.valueOf(df2.format(amexOrbitRedeemAmount));
                printerItems.add(PrinterItem.AMEX_REDEEM);
                printerItems.add(PrinterItem.FEED_LINE);
            }
            netSale = mcSaleAmount + visaSaleAmount + upiSaleAmount + payPakSaleAmount + jcbSaleAmount + amexSaleAmount;
            netSaleCount = mcSaleCount + visaSaleCount + upiSaleCount + payPakSaleCount + jcbSaleCount + amexSaleCount;
            printerItems.add(PrinterItem.FEED_LINE);
            cardCount = String.valueOf(sumcardcount);
            cardSum = String.valueOf(sumcardamount);


            /////////////

            //TOTAL NET SALES
            printerItems.add(PrinterItem.FEED_LINE);
            PrinterItem.SUMMARY_TOTAL_SALE.title.secValue = String.valueOf(sumcardcount);
            PrinterItem.SUMMARY_TOTAL_SALE.value.sValue = String.valueOf(df2.format(sumcardamount));
            printerItems.add(PrinterItem.SUMMARY_TOTAL_SALE);


            if(Constants.TIP_ENABLED.equals("Y")) {
                if(saleWithTipAmount != 0) {
                    saleWithTipCount = String.valueOf(new DecimalFormat("0.00").format(((saleWithTipAmount / sumcardamount) * 100)));
                    Utility.DEBUG_LOG("tip count", saleWithTipCount);
                    // printerItems.add(PrinterItem.FEED_LINE);

                    PrinterItem.SUMMARY_TOTAL_TIP_SALE.title.secValue = saleWithTipCount + "%";
                    Utility.DEBUG_LOG("tip count", saleWithTipCount);
                }
                else{
                    PrinterItem.SUMMARY_TOTAL_TIP_SALE.title.secValue = "0.00%";
                    Utility.DEBUG_LOG("tip count", "0.00%");
                }
                PrinterItem.SUMMARY_TOTAL_TIP_SALE.value.sValue = String.valueOf(new DecimalFormat("0.00").format((saleWithTipAmount)));
                Utility.DEBUG_LOG("tip count", String.valueOf(new DecimalFormat("0.00").format((saleWithTipAmount))));
                printerItems.add(PrinterItem.SUMMARY_TOTAL_TIP_SALE);
            }


            //printerItems.add(PrinterItem.LINE);
            printerItems.add(PrinterItem.FEED_LINE);

            //SERIAL
           // printerItems.add(PrinterItem.TERMINAL_SERIAL);












//            //CUT BREAK
//            printerItems.add(PrinterItem.CUT_BREAK);
//            //SPACE
//            printerItems.add(PrinterItem.FEED_LINE);
//            //SPACE
//            printerItems.add(PrinterItem.FEED_LINE);


            //TRANS TYPE
//            PrinterItem.TRANS_TYPE.title.sValue = "SALE";
//            printerItems.add(PrinterItem.TRANS_TYPE);



//            switch (copyIndex) {
//                case 1:
//                    tmp = getResources().getString(R.string.prn_merchantCopy); //"商户存根                           请妥善保管";
//                    break;
//                case 2:
//                    tmp = getResources().getString(R.string.prn_cardholderCopy); //"持卡人存根                         请妥善保管";
//                    break;
//                case 3:
//                default:
//                    tmp = getResources().getString(R.string.prn_bankCopy); //"银行存根                           请妥善保管";
//                    break;
//            }
//            PrinterItem.SUBTITLE.value.sValue = tmp;
//            printerItems.add(PrinterItem.SUBTITLE);
//            printerItems.add(PrinterItem.LINE);
//
//            // MERCHANT NAME
////            PrinterItem.MERCHANT_NAME.value.sValue = hostInformation.merchantName;
//            //printerItems.add(PrinterItem.MERCHANT_NAME);
//
//            // MERCHANT NO.
////            PrinterItem.MERCHANT_ID.value.sValue = hostInformation.merchantID;
//            printerItems.add(PrinterItem.MERCHANT_ID);
//
//            // TERMINAL NO
//            PrinterItem.TERMINAL_ID.value.sValue = "123456789";
////            PrinterItem.TERMINAL_ID.value.sValue = hostInformation.terminalID;
//            printerItems.add(PrinterItem.TERMINAL_ID);
//
//            // OPERATOR NO
////            PrinterItem.OPERATOR_ID.value.sValue = getAppParam(AppParam.System.oper_no);
////            printerItems.add(PrinterItem.OPERATOR_ID);
////            printerItems.add(PrinterItem.LINE);
//
//            // ISSUE
//            PrinterItem.CARD_ISSUE.value.sValue = "VISA"; // extraItems.getString(TXNREC.ISSBANKNAME).trim();
//            printerItems.add(PrinterItem.CARD_ISSUE);
//
//            // CARD NO.
////            String pansn = extraItems.getString(TXNREC.PANSN);
////            String pan = TransactionParams.getInstance().getPan();
////            PrinterItem.CARD_NO.title.sValue = getResources().getString(R.string.cardno);
////            PrinterItem.CARD_NO.value.sValue = pan;
//            String pan = TransactionParams.getInstance().getPan();
//            PrinterItem.CARD_NO.title.sValue = "getResources().getString(R.string.cardno)";
//            PrinterItem.CARD_NO.value.sValue = "pan";
//            // todo, add & print the card type
////            if (pan != null && pan.trim().length() > 4) {
////                if (pansn != null && !pansn.isEmpty()) {
////                    PrinterItem.CARD_NO.title.sValue = getResources().getString(R.string.cardno1) + pansn.substring(1);
////                } else {
////                }
///                PrinterItem.CARD_NO.value.sValue = fixCardNumWithMask(pan);
////            } else {
////                Utility.DEBUG_LOG(TAG, "No Card No. got!");
////            }
//            printerItems.add(PrinterItem.CARD_NO);
//
//            // EXP. DATE
//            String expiredDate = extraItems.getString(TransactionParams.getInstance().getExpiredDate());
//            if (expiredDate != null && !expiredDate.isEmpty()) {
//                PrinterItem.CARD_VALID.value.sValue = expiredDate.substring(0, 4) + "/" + expiredDate.substring(0, 2);
//            } else {
//                Utility.DEBUG_LOG(TAG, "no card expire date got");
//                PrinterItem.CARD_VALID.value.sValue = "";
//            }
//            printerItems.add(PrinterItem.CARD_VALID);
//
//            // TRANS TYPE
//            PrinterItem.TRANS_TYPE.value.sValue = TransactionParams.getInstance().getTransactionType();
//            printerItems.add(PrinterItem.TRANS_TYPE);
//
//
//            // BATCH NO. TODO
////            String batchNo =  getAppParam(AppParam.System.batch_num);
////            PrinterItem.BATCH_NO.value.sValue = getAppParam(AppParam.System.batch_num);
////            printerItems.add(PrinterItem.BATCH_NO);
//
//            // TRACE NO. TODO
////            String traceNo = extraItems.getString(TXNREC.TRACE);
////            if (traceNo != null && !traceNo.isEmpty()) {
////                PrinterItem.TRACK_NO.value.sValue = traceNo;
////                printerItems.add(PrinterItem.TRACK_NO);
////            }
//
//            // AUTH NO. TODO
////            tmp = extraItems.getString(TXNREC.AUTHID);
////            if (tmp != null && !tmp.isEmpty()) {
////                PrinterItem.AUTH_NO.value.sValue = tmp;
////                printerItems.add(PrinterItem.AUTH_NO);
////            }
//
//            // REF NO. TODO
////            String referenceNo = extraItems.getString(TXNREC.REFERNUM);
////            if (referenceNo != null && !referenceNo.isEmpty()) {
////                PrinterItem.REFER_NO.value.sValue = referenceNo;
////                printerItems.add(PrinterItem.REFER_NO);
////            }
//
//            // DATE/TIME
////            String dateString = extraItems.getString(TXNREC.DATE) + extraItems.getString(TXNREC.TIME);
////            String dateString = "1996-03-07";
////            if (dateString != null && !dateString.isEmpty()) {
////                dateString = "getSystemDatetime";
////                PrinterItem.DATE_TIME.value.sValue = dateString;
////            } else {
////                PrinterItem.DATE_TIME.value.sValue = "";
////            }
////            printerItems.add(PrinterItem.DATE_TIME);
//
//            // AMOUNT
//            String retamount = TransactionParams.getInstance().getTransactionAmount();
//            if (retamount != null && !retamount.isEmpty()) {
//
//                PrinterItem.AMOUNT.value.sValue = getResources().getString(R.string.prn_currency) + retamount;
//                printerItems.add(PrinterItem.AMOUNT);
//            }
//            printerItems.add(PrinterItem.LINE);
//
//            printerItems.add(PrinterItem.REFERENCE);
//            printerItems.add(PrinterItem.FEED_LINE);
//            printerItems.add(PrinterItem.FEED_LINE);
//
//            // TC TODO
////            String ac = extraItems.getString(TXNREC.AC);
////            int im_pan = 0;
////            try {
////                im_pan = Integer.parseInt(extraItems.getString(TXNREC.MODE).substring(0, 2));
////            } catch (Exception e) {
////            }
//
////            if (ac != null && "C".equals(getCardTypeMode(im_pan))) {
////                PrinterItem.TC.value.sValue = ac;
////            }
//
//            // REPRINT
//            if (isReprint) {
//                printerItems.add(PrinterItem.RE_PRINT_NOTE);
//            }
//            printerItems.add(PrinterItem.LINE);
//
//            // CARDHOLDER SIGNATURE
//
//            PrinterItem.E_SIGN.value.sValue = TransactionParams.getInstance().getEsignData();
//            printerItems.add(PrinterItem.E_SIGN);
//
////            if (!printEsign()) {
////                printerItems.add(PrinterItem.FEED_LINE);
////                printerItems.add(PrinterItem.FEED_LINE);
////            }
//
//          //  PrinterItem.QRCODE_1.value.sValue = getResources().getString(R.string.prn_qrcode2);
//
//           // PrinterItem.BARCODE_1.value.sValue = getResources().getString(R.string.prn_barcode);
//
//            printerItems.add(PrinterItem.FEED);
//           // printerItems.add(PrinterItem.BARCODE_1);
//            printerItems.add(PrinterItem.FEED);
//            printerItems.add(PrinterItem.FEED);
//            //printerItems.add(PrinterItem.QRCODE_1);
//            printerItems.add(PrinterItem.LINE);
//
//            printerItems.add(PrinterItem.COMMENT_1);
//            printerItems.add(PrinterItem.COMMENT_2);
//            printerItems.add(PrinterItem.COMMENT_3);


        } catch (Exception e) {
            Utility.DEBUG_LOG(TAG, "Exception :" + e.getMessage());
            for (StackTraceElement m : e.getStackTrace()
            ) {
                Utility.DEBUG_LOG(TAG, "Exception :" + m);

            }
        }
    }
}

