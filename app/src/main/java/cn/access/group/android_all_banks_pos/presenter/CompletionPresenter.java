package cn.access.group.android_all_banks_pos.presenter;


import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.SparseArray;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.vfi.smartpos.deviceservice.aidl.CheckCardListener;
import com.vfi.smartpos.deviceservice.aidl.EMVHandler;
import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceInfo;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.aidl.IPinpad;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.ISerialPort;
import com.vfi.smartpos.deviceservice.aidl.OnlineResultHandler;
import com.vfi.smartpos.deviceservice.aidl.PinInputListener;
import com.vfi.smartpos.deviceservice.aidl.PinpadKeyType;
import com.vfi.smartpos.deviceservice.constdefine.ConstCheckCardListener;
import com.vfi.smartpos.deviceservice.constdefine.ConstIPBOC;
import com.vfi.smartpos.deviceservice.constdefine.ConstIPinpad;
import com.vfi.smartpos.deviceservice.constdefine.ConstOnlineResultHandler;
import com.vfi.smartpos.deviceservice.constdefine.ConstPBOCHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.PrinterFonts;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Comm;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.Utilities.ECR;
import cn.access.group.android_all_banks_pos.Utilities.ErrorMap;
import cn.access.group.android_all_banks_pos.Utilities.PostRequest;
import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
import cn.access.group.android_all_banks_pos.Utilities.SSLComm;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.Utilities.Validation;
import cn.access.group.android_all_banks_pos.basePackets.transactionPacketGenerator.SalePackageGenerator;
import cn.access.group.android_all_banks_pos.basic.ISO8583;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;
import cn.access.group.android_all_banks_pos.contracts.PaymentContract;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.EmvTagValuePair;
import cn.access.group.android_all_banks_pos.repository.model.Reversal;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.usecase.EmvSetAidRid;
import cn.access.group.android_all_banks_pos.usecase.GenerateReversal;
import cn.access.group.android_all_banks_pos.basePackets.transactionPacketGenerator.SalePackageGenerator;
import cn.access.group.android_all_banks_pos.viewfragments.DialogPaymentSuccessFragment;
import cn.access.group.android_all_banks_pos.viewfragments.HomeFragment;
import cn.access.group.android_all_banks_pos.viewfragments.PaymentAmountFragment;
import cn.access.group.android_all_banks_pos.viewfragments.SettlementFragment;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.*;
import static com.vfi.smartpos.deviceservice.aidl.PinpadKeyType.DUKPTKEY;
import static java.lang.Thread.sleep;
import static java.security.AccessController.getContext;

enum ReaderTypeCompletion { RT_Chip, RT_Mag, RT_CTLS, RT_Chip_Mag, RT_All };
/**
 * @author muhammad.humayun
 * on 8/21/2019.
 */
public class CompletionPresenter {
    private static final String TAG = "CompletionPresenter";
    private IEMV iemv;
    private IPinpad ipinpad;
    private IBeeper iBeeper;
    private IPrinter printer;

    double cleanAmount; /// Shivam's code update on 15 Jan 2021 for Amount decimal points calculation.
    private WeakReference<Context> context;

    private EMVHandler emvHandler;
    private PinInputListener pinInputListener;
    private String headerType="";

    // keys
    private int mainKeyId = 97;
    private int workKeyId = 1;

    //ubl keys...11/22/2019
  /*  private String pinKey_WorkKey = "9A75CD73C53EAD2510C4FD6A3AD95DAF"; ///neww
 //  private String pinKey_WorkKey ="1A98C8528C83E9E5C891B5043E51B045";
    private String mainKey_MasterKey = "22222222222222222222222222222222";*/// Taha's code update on Jan21 for BAF Keys'

    //+ taha 15-01-2021 setting BAF keys
    private String mainKey_MasterKey = "D6DF20AEAEBC70E5";
    private String pinKey_WorkKey = "AB85765997CDDA0C"; ///neww

    private String macKey = "";




    private String savedPan = "";
    private byte[] savedPinBlock = null;
    String newText;
    SparseArray<String> data8583_i = null;
    /**
     * field-value map to save iso data
     */
    private SparseArray<String> data8583 = null;
    private SparseArray<String> tagOfF55 = null;

    private String  cardName=null, cardExpiry=null;

    private String totalAmountStr ,dateFromResponse,timeFromResponse,rnn;

    private String transAmount=null,transDate=null,transTime=null;
    String cardType = "UNKNOWN";
    private boolean doSecondTap=true;
    String cardRanges;
    String cardholdername;

    /**
     * \Brief the transaction type
     * <p>
     * Prefix
     * T_ means transaction
     * M_ means management
     */
    enum TransType {
        T_BANLANCE, T_PURCHASE,
        M_SIGNIN
    }
    private AssetManager assetManager;
    private TransactionContract transactionContract;
    TransactionDetail td;
    PostRequest postRequest;
    private ReceiptPrinter receiptPrinter;
    private ErrorMap errorMap;
    private PaymentContract paymentContract;
    // private static String STAN;
    private String track2,aid;
    private boolean CTLS=false;
    private boolean internet= true;
    private String from="";
    String tipAmount="0";
    String transactionAmount = "0";
    String txnTypePresenter;
    Validation vs;
    ISerialPort serialPort;
    String Bal = "";
    String field_63;
    String orbit_amount="";
    String orbit_bal="";
    String orbit_key="";
    IDeviceInfo iDeviceInfo;
    String UpdatedYear = "";
    String saleippMonths ="";
    Double installment = 0.00;
    // SSLComm sslComm = new SSLComm(context, "keystore/kclient.bks", "172.191.1.194", 5555);

    public CompletionPresenter(IEMV iemv){
        this.iemv=iemv;
    }

    public CompletionPresenter(TransactionContract transactionContract, PaymentContract paymentContract, Handler handler,
                                  boolean isSucc, AssetManager assetManager, IEMV iemv, IPinpad ipinpad, IBeeper iBeeper,
                                  IPrinter printer, String transactionAmount, String from, Context context, ISerialPort serialPort, TransactionDetail td){
        this.transactionContract = transactionContract;
        this.paymentContract = paymentContract;
        this.iemv = iemv; this.ipinpad = ipinpad;this.iBeeper= iBeeper;this.printer = printer;
        this.assetManager = assetManager;
        receiptPrinter = new ReceiptPrinter(handler);
        errorMap = new ErrorMap();
        this.from = from;
        this.transactionAmount = transactionAmount;
        this.vs = new Validation();
        this.context=new WeakReference<>(context);
        this.serialPort = serialPort;
        this.postRequest = new PostRequest();
        this.td = td;
        UpdatedYear = new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date());
        Utility.DEBUG_LOG("updateYear",UpdatedYear);



        // reset the aid and rid
        if(firstBootUp) {
            EmvSetAidRid emvSetAidRid = new EmvSetAidRid(iemv, context);
            try {
                emvSetAidRid.setAID(3);
                emvSetAidRid.setRID(3);
                emvSetAidRid.setAID(1);
                emvSetAidRid.setRID(1);
                firstBootUp = false;
            } catch (Exception e) {
                Utility.DEBUG_LOG(TAG, "it works" + e);
            }
        }


        //sending post request


        //sendGetRequest();

       /* emptytable(appDatabase);
        SharedPref.clear();*/
        //doSetKeys();
        // emptytable(appDatabase);

      /*  boolean bRet;
        try {
            bRet = ipinpad.isKeyExist(2, 1);
            transactionContract.toastShow("loadMainKey:" + bRet);
        } catch (RemoteException e) {
            e.printStackTrace();
        }*/
        doSetKeys();
        loadConfiguration();
        ISO8583u.SimulatorType st;
        switch (headerType)
        {
            case ("AuthAll"):
                st=ISO8583u.SimulatorType.AuthAll;
                break;
            case "SimulatorHost":
                st=ISO8583u.SimulatorType.SimulatorHost;
                break;
            case "ActualBank":
                st=ISO8583u.SimulatorType.ActualBank;
                break;
            default:
                st=ISO8583u.SimulatorType.AuthAll;
                break;
        }
        initialize8583data(st);
        //getSerialPort();

        // check assets fonts and copy to file system for Service
        InitializeFontFiles();
        if (!isSucc) {
            Utility.DEBUG_LOG("TAG", "deviceService connect fail!");
        } else {
            Utility.DEBUG_LOG("TAG", "deviceService connect success");
            initializeEMV();
            initializePinInputListener();
        }


//        try {
//            serialPort.open();
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }
    }

//    private boolean IsReversalNeeded(TransactionDetail txn)
//    {
//        boolean rv=false;
//        Utility.DEBUG_LOG(TAG,"+ IsReversalNeeded() +");
//
//        Utility.DEBUG_LOG(TAG,"MTI:"+txn.getMessageType());
//        if ( txn.getMessageType().equals("0200") )
//            rv = true;
//        Utility.DEBUG_LOG(TAG,"rv:"+rv);
//        return rv;
//    }
//

//    public ISerialPort getSerialPort() {
//        try {
//            serialPort.init(115200,0,8);
//            Utility.DEBUG_LOG("Port connected", String.valueOf(serialPort.init(115200,0,8)));
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }
//        return serialPort;
//    }

    /**
     * \brief initialize the pin pad listener
     * <p>
     * \code{.java}
     * \endcode
     */
    private void initializePinInputListener() {
        pinInputListener = new PinInputListener.Stub() {
            @Override
            public void onInput(int len, int key) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "PinPad onInput, len:" + len + ", key:" + key);
            }

            @Override
            public void onConfirm(byte[] data, boolean isNonePin) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "PinPad onConfirm");
                iemv.importPin(1, data);
                savedPinBlock = data;
            }

            @Override
            public void onCancel() throws RemoteException {
                Utility.DEBUG_LOG(TAG, "PinPad onCancel");
                DashboardContainer.backStack();;
            }

            @Override
            public void onError(int errorCode) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "PinPad onError, code:" + errorCode);
                DashboardContainer.backStack();;
            }
        };
    }

    private void loadConfiguration() {
        CARD_AUTH_METHOD = "SIGNATURE";
        this.rnn = DataFormatterUtil.systemYear() + DataFormatterUtil.systemDateAndTime();
        Constants.BATCH_NO = String.format(Locale.getDefault(), "%06d", SharedPref.read("batch_no"));
        Utility.DEBUG_LOG("payment presenter batch", BATCH_NO);
        Constants.INVOICE_NO = String.format(Locale.getDefault(), "%06d", SharedPref.read("invoice_no"));
        txnTypePresenter = String.valueOf(SharedPref.read("button_fragment", txnTypePresenter));
        Utility.DEBUG_LOG("Presenter type",txnTypePresenter);
        int inv = SharedPref.read("invoice_no");
        inv = inv + 1;
        SharedPref.write("invoice_no", inv);
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            this.headerType = obj.getString("Header Type");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = assetManager.open("config.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void initialize8583data(ISO8583u.SimulatorType st) {
        ISO8583u.simType = st;
    }

    // check assets fonts and copy to file system for Service -- start
    private void InitializeFontFiles() {
        PrinterFonts.initialize(assetManager);
    }

    /**
     * \brief show the pinpad
     * <p>
     * \code{.java}
     * \endcode
     */
    private void doPinPad(boolean isOnlinePin, int retryTimes) {
        Bundle param = new Bundle();
        Bundle globleparam = new Bundle();
        String panBlock = savedPan;

        byte[] pinLimit = {4, 5, 6, 8, 12}; //todo add 0 to bypass pin, if needed
        param.putString(ConstIPinpad.startPinInput.param.KEY_promptString_String, "ENTER YOUR CARD PIN");
        param.putString("promptsFont", "/system/fonts/DroidSans-Bold.ttf" );
        param.putByteArray("displayKeyValue", new byte[]{0,1,2,3,4,5,6,7,8,9} );
        param.putByteArray(ConstIPinpad.startPinInput.param.KEY_pinLimit_ByteArray, pinLimit);
        param.putInt(ConstIPinpad.startPinInput.param.KEY_timeout_int, 20);
        param.putBoolean(ConstIPinpad.startPinInput.param.KEY_isOnline_boolean, isOnlinePin);
        param.putString(ConstIPinpad.startPinInput.param.KEY_pan_String, panBlock);
        param.putInt(ConstIPinpad.startPinInput.param.KEY_desType_int, ConstIPinpad.startPinInput.param.Value_desType_3DES);
        if (!isOnlinePin) {
            param.putString(ConstIPinpad.startPinInput.param.KEY_promptString_String, "OFFLINE PIN, retry times:" + retryTimes);
        }
        //globleparam.putString( ConstIPinpad.startPinInput.globleParam.KEY_Display_One_String, "[1]");
        try {

            ipinpad.startPinInput(workKeyId, param, globleparam, pinInputListener);
        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }

    private void doPinPadSynchronous(boolean isOnlinePin, int retryTimes) {
        Utility.DEBUG_LOG(TAG,"Before doPinPad");
        doPinPad(isOnlinePin,retryTimes);
        Utility.DEBUG_LOG(TAG,"After doPinPad");
        while(true)
        {
            if ( savedPinBlock == null)
            {
                Utility.DEBUG_LOG(TAG,"in while");
                try
                {
                    sleep(1*1000);
                }
                catch (Exception e)
                {
                    Utility.DEBUG_LOG(TAG,"exception:"+e.getMessage().toString());
                }
            }
            else
            {
                Utility.DEBUG_LOG(TAG,"break pin input");
                break;
            }
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler onlineResponse = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Utility.DEBUG_LOG(TAG,"before getData");
            Bundle data = msg.getData();
            String val = data.getString("value");
            Utility.DEBUG_LOG(TAG, "handle message:" + val);
        }
    };

 /// Shivam's code update on 15 Jan 2021 for amount Decimal point calculation

    public void doPurchase(String amount, String tipAmount, PaymentAmountPresenter.FallbackOrignatorType... isFallback) {
        //

        Utility.DEBUG_LOG(TAG,"+ CompletionPresenter::doPurchase +");
//        SparseArray<String> data8583_u_purchase = new SparseArray<>();
//
//        SalePackageGenerator salePackageGenerator= new SalePackageGenerator();
//        data8583_u_purchase= salePackageGenerator.getSalePacket();
        Utility.DEBUG_LOG("SL Amount",amount);

        Utility.DEBUG_LOG(TAG,"Step1");
        String cleanamount = amount.replaceAll("[*,]", "");
        String cleanamount1 = tipAmount.replaceAll("[*,]", "");

        Utility.DEBUG_LOG(TAG,"Step2");
        double a = Double.parseDouble(cleanamount);
        Utility.DEBUG_LOG("SL Amount", String.valueOf(a));
        Utility.DEBUG_LOG(TAG,"Step3");
        if (a > 0 || txnTypePresenter.equals("ORBIT INQUIRY")) {
            Utility.DEBUG_LOG(TAG,"Step4");

            cleanAmount = Double.parseDouble(cleanamount);
            Double b = Double.parseDouble(cleanamount1);
            Double c = (a + b);
            Utility.DEBUG_LOG(TAG,"Step5");

            Utility.DEBUG_LOG("SL Amount C", String.valueOf(c));
            cleanamount = String.format(Locale.getDefault(), "%012.0f", (Double) (Double.parseDouble(cleanamount) * 100));
            tipAmount = String.format(Locale.getDefault(), "%012.0f", (Double) (Double.parseDouble(cleanamount1) * 100));
            transAmount = String.format(Locale.getDefault(), "%012.0f", (Double) (c * 100));
            //transAmount= amount+tipAmount;
            Utility.DEBUG_LOG("SL transAmount", transAmount);
            this.tipAmount = tipAmount;
            this.transactionAmount = cleanamount;
            Utility.DEBUG_LOG(TAG,"Step6");

            //data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, amount);
//            if (!(txnTypePresenter.equals("ORBIT INQUIRY"))) {
//
//                if ((Constants.TIP_ENABLED.equalsIgnoreCase("Y")))
//                    data8583_u_purchase.put(ISO8583u.F_BalancAmount_54, tipAmount);
//                data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, transAmount);
//            }
//            if(txnTypePresenter.equals("SALEIPP")){
//                double tAmount = Double.parseDouble(DataFormatterUtil.formattedAmount(transAmount));
//                double tenure = Double.parseDouble(saleippMonths);
//                Utility.DEBUG_LOG("SALEIPP amount", String.valueOf(tAmount));
//                Utility.DEBUG_LOG("SALEIPP tenure", String.valueOf(tenure));
//                installment = tAmount/tenure;
//                Utility.DEBUG_LOG("SALEIPP installment", String.valueOf(installment));
//                transactionDetail.setSaleippInstallments(String.valueOf(installment));
//                transactionDetail.setSaleippMonth(String.valueOf(saleippMonths));
//            }

            //data8583 = data8583_u_purchase;
            if ( isFallback.length > 0 ) {
//                if ( isFallback[0] == true )
//                {
//                    Utility.DEBUG_LOG(TAG,"before doTransaction fallback");
//                    doTransaction(ReaderType.RT_Mag);
//                }

                if ( isFallback[0] == PaymentAmountPresenter.FallbackOrignatorType.FT_Dip )
                {
                    Utility.DEBUG_LOG(TAG,"before doTransaction fallback to swipe");
                    doTransaction(ReaderType.RT_Mag);
                }
                else if ( isFallback[0] == PaymentAmountPresenter.FallbackOrignatorType.FT_Swipe )
                {
                    Utility.DEBUG_LOG(TAG,"before doTransaction fallback to Tap / Insert / Swipe");
                    doTransaction(ReaderType.RT_All);
                }
                else if ( isFallback[0] == PaymentAmountPresenter.FallbackOrignatorType.FT_Ctls )
                {
                    Utility.DEBUG_LOG(TAG,"before doTransaction fallback to Insert / Swipe");
                    doTransaction(ReaderType.RT_Chip_Mag);
                }
                else if ( isFallback[0] == PaymentAmountPresenter.FallbackOrignatorType.FT_SeeYourPhone )
                {
                    Utility.DEBUG_LOG(TAG,"before doTransaction fallback to Tap / Insert / Swipe");
                    doTransaction(ReaderType.RT_All);
                }
            }
            else
            {
                Utility.DEBUG_LOG(TAG,"before doTransaction");
                doTransaction(ReaderType.RT_All);
            }
        }
        else {
            transactionContract.transactionFailed("Error","amount should be greater than 0");
            DashboardContainer.backStack();
        }


    }

    public void doTransaction(ReaderType readerType) {


        // set some fields
//        data8583.put(ISO8583u.F_TID_41, TERMINAL_ID);//42017890  UPI //11111112 UBL
//        data8583.put(ISO8583u.F_MerchantID_42, MERCHANT_ID);//200000005000510 UPI // 1100000001     //// UBL

        // do search card and online request
        Utility.DEBUG_LOG(TAG,"before doSearchCard");
        doSearchCard(readerType);
    }
    public void doSearchCard(ReaderType readerType) {
        Utility.DEBUG_LOG(TAG,"doSearchCard()");
        transactionContract.toastShow("show");
        Bundle cardOption = new Bundle();
        td.setIsFallback(false);
//        cardOption.putBoolean( ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_unsupported);
        switch (readerType) {
            case RT_Chip:
                Utility.DEBUG_LOG(TAG,"enable dip only");
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean("supportSmartCard", true);
                break;
            case RT_Mag:
                Utility.DEBUG_LOG(TAG,"enable mag only");
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean("supportMagCard", true);
                td.setIsFallback(true);
                break;
            case RT_CTLS:
                Utility.DEBUG_LOG(TAG,"enable ctls only");
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean("supportCTLSCard", true);
                break;
            case RT_Chip_Mag:
                Utility.DEBUG_LOG(TAG,"enable dip and mag readers");
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean("supportMagCard", true);
                cardOption.putBoolean("supportSmartCard", true);
                break;
            case RT_All:
                Utility.DEBUG_LOG(TAG,"enable all readers");
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean("supportMagCard", true);
                cardOption.putBoolean("supportCTLSCard", true);
                cardOption.putBoolean("supportSmartCard", true);
                break;
            default:
                Utility.DEBUG_LOG(TAG,"default: enable all readers");
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
                cardOption.putBoolean("supportMagCard", true);
                cardOption.putBoolean("supportCTLSCard", true);
                cardOption.putBoolean("supportSmartCard", true);
                break;
        };
//        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//        cardOption.putBoolean("supportMagCard", true);
//        //+ masood 01-09-2020 commented below line
//        cardOption.putBoolean("supportCTLSCard", true);
////        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
//        cardOption.putBoolean("supportSmartCard", true);
        String transactionAmount = this.transactionAmount;


        try {
            iemv.checkCard(cardOption, 30, new CheckCardListener.Stub() {
                        @Override
                        public void onCardSwiped(Bundle track) throws RemoteException{
                            Utility.DEBUG_LOG(TAG, "onCardSwiped ...");

                            iBeeper.startBeep(200);

                            String pan = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_PAN_String);
                            String track1 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK1_String);
                            track2 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK2_String);
                            String track3 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK3_String);
                            String serviceCode = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_SERVICE_CODE_String);

                            Utility.DEBUG_LOG("cardNam", track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK1_String));
                            //+ taha 23-02-2021 regex logic is incorrect, remaking it
                            //cardholdername = track1.replaceAll(".+\\d\\^", "");
//                            cardholdername = cardholdername.replaceAll("\\^.+", "").trim();
                            Utility.DEBUG_LOG(TAG,"track1:"+track1);
                            cardholdername = track1.replaceAll("(.*\\^)(.*)(\\^.*)", "$2");
                            Utility.DEBUG_LOG(TAG, cardholdername);
                            savedPan = pan;
                            String pan1 = pan.substring(0, 6);
                            String Last4digits = track2;
                            String cardnumber = Last4digits.replaceAll("\\=.*", "");
                            int startNumber = cardnumber.length() - 4;
                            int endNumber = cardnumber.length();
                            String fourDigits = cardnumber.substring(startNumber, endNumber);
                            Utility.DEBUG_LOG("regex", String.valueOf(fourDigits));
                           // Utility.DEBUG_LOG("context", String.valueOf(context));
                            Utility.DEBUG_LOG("Track2 completion mag",td.getCardNo());
                            Utility.DEBUG_LOG("Track2 mag",savedPan);

                            if(!td.getCardNo().equals(savedPan)){
                                transactionContract.transactionFailed("error", "Card not matched");
                                transactionContract.hideProgress();

                                DashboardContainer.backStack();                            }
                            else{

                            boolean bl = vs.validateCardRange(pan);
                            if (!bl) {

                                transactionContract.hideProgress();
                                transactionContract.transactionFailed("error", "Issuer not found");
                                DashboardContainer.backStack();
                            } else {

                                String cardExpiryMag = track2.replaceAll(".+\\=", "");
                                Utility.DEBUG_LOG("cardExpiryMag", cardExpiryMag);
                                int cardexp = Integer.parseInt(cardExpiryMag.substring(0, 4));
                                String cardExpiryYear = cardExpiryMag.substring(0, 2);
                                String cardExpiryMonth = cardExpiryMag.substring(2, 4);
                                cardExpiry = cardExpiryMonth + "/" + cardExpiryYear;
                                String expiryCheck = new SimpleDateFormat("yyMM", Locale.getDefault()).format(new Date());
                                int currentDateExpiry = Integer.parseInt(expiryCheck);
                                if (null != track2) {
                                    Utility.DEBUG_LOG(TAG,"track2: "+track2);
//                                    transactionDetail.setTrack2(track2);
                                }
                                Utility.DEBUG_LOG(TAG,"getIsFallback" + td.getIsFallback());
                                if ( td.getIsFallback() == true )
                                {
                                    Utility.DEBUG_LOG(TAG,"In case of fallback ignore track2 service code first digit");
                                }
                                else if ( isChipCardFromTrack2() )
                                {
                                    Utility.DEBUG_LOG(TAG,"Chip card detected3");

                                    transactionContract.hideProgress();
                                    transactionContract.transactionFailed("ERROR", "Chip card detected");
                                    DashboardContainer.backStack();
                                    return;
                                }
                                if (currentDateExpiry > cardexp) {
                                    transactionContract.hideProgress();
                                    transactionContract.transactionFailed("error", "Card Expired");
                                    DashboardContainer.backStack();
                                } else {
                                    Utility.DEBUG_LOG(TAG, "onCardSwiped ...1");
                                    byte[] bytes = Utility.hexStr2Byte(track2);
                                    Utility.DEBUG_LOG(TAG, "Track2:" + track2 + " (" + Utility.byte2HexStr(bytes) + ")");
                                    Boolean bIsKeyExist = null;
                                    try {
                                        bIsKeyExist = ipinpad.isKeyExist(DUKPTKEY, 1);
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                    if (!bIsKeyExist) {
                                        Utility.DEBUG_LOG(TAG, "no key exist type: 12, @: 1");
                                    }
                                    byte[] enctypted = new byte[0];
                                    try {
                                        enctypted = ipinpad.dukptEncryptData(1, 1, 1, bytes, new byte[]{0, 0, 0, 0, 0, 0, 0, 0,});
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                    if (null == enctypted) {
                                        Utility.DEBUG_LOG(TAG, "NO DUKPT Encrypted got");
                                    } else {
                                        Utility.DEBUG_LOG(TAG, "DUKPT:" + Utility.byte2HexStr(enctypted));
                                    }
                                    try {
                                        bIsKeyExist = ipinpad.isKeyExist(DUKPTKEY, 1);
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                    if (!bIsKeyExist) {
                                        Utility.DEBUG_LOG(TAG, "no key exist type: 12, @: 1");
                                    }

                                    Utility.DEBUG_LOG(TAG, "onCardSwiped ...3");
                                    td.setCardType("Magstripe");
                                    // masood commented this on 12-02-21

//                                    if ( isPinRequiredUsingServiceCode() == true ) {
//                                        Utility.DEBUG_LOG(TAG,"before doPinPadSynchronous");
//                                        doPinPadSynchronous(false,3);
//                                        Utility.DEBUG_LOG(TAG,"after doPinPadSynchronous");
//                                    }
                                    if (savedPinBlock !=null) {
                                        Utility.DEBUG_LOG(TAG, "pin data present:" + Utility.byte2HexStr(savedPinBlock));

                                    }
                                    if ( td.getIsFallback() == true )
                                    {
                                        if (savedPinBlock==null){
                                            Utility.DEBUG_LOG(TAG,"fallback detected (no pin); setting 0802");

                                        }
                                        else
                                        {
                                            Utility.DEBUG_LOG(TAG,"fallback detected (with pin); setting 0801");

                                        }
                                    }
                                    else
                                    {
                                        if (savedPinBlock==null){
                                            Utility.DEBUG_LOG(TAG,"no pin entered 022");

                                        }
                                        else
                                        {
                                            Utility.DEBUG_LOG(TAG,"pin entered 021");

                                        }
                                    }

                                    Utility.DEBUG_LOG(TAG,"before onlineRequest.run");

                                    Utility.DEBUG_LOG("transactionDetail", String.valueOf(td));
                                    updateTransaction(td.getTxnType(),td.getIsAdvice(), td.getAmount() ,td.getAuthorizationIdentificationResponseCode());
                                    //updateTransaction(appDatabase,td.getAcquiringInstitutionIdentificationCode(), td.getTxnType(), td.getAmount(),td.getIsAdvice() );
                                    receiptPrinter.printSaleReceipt(printer, assetManager, td, aid, "MERCHANT COPY", txnTypePresenter, "", Bal,false); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                                    transactionContract.transactionSuccessCustomer("CUSTOMER COPY", td, "CUSTOMER COPY", txnTypePresenter,Bal,false); // SHIVAM UPDATE HERE FOR TEXT PRINTER
                                }
                            }
                            }

                        }
                        @Override
                        public void onCardPowerUp() throws RemoteException {
                            Utility.DEBUG_LOG(TAG,"+ onCardPowerUp() +");
                            iemv.stopCheckCard();
                            iemv.abortEMV();
                            iBeeper.startBeep(200);
                            Utility.DEBUG_LOG(TAG, "before dEMV-1");
                            doEMV(ConstIPBOC.startEMV.intent.VALUE_cardType_smart_card, TransType.T_PURCHASE, transAmount);
                        }
                        @Override
                        public void onCardActivate() throws RemoteException {
                            Utility.DEBUG_LOG(TAG,"+ onCardActivate() +");
                            iemv.stopCheckCard();
                            iemv.abortEMV();
                            iBeeper.startBeep(200);
                            CTLS = true;
                            Utility.DEBUG_LOG(TAG, "before dEMV-2");
                            doEMV(ConstIPBOC.startEMV.intent.VALUE_cardType_contactless, TransType.T_PURCHASE, transAmount);
                        }
                        @Override
                        public void onTimeout() throws RemoteException {
                            Utility.DEBUG_LOG(TAG,"+ onTimeout() +");
                            transactionContract.transactionFailed("Time Out", "Card listener Closed, Please Re-Attempt the Transaction");
                            DashboardContainer.backStack();
                        }

                        @Override
                        public void onError(int error, String message) throws RemoteException {
//                            transactionContract.toastShow("error:" + error + ", msg:" + message);
                            Utility.DEBUG_LOG(TAG,"+ onError() +");
                            Utility.DEBUG_LOG(TAG,"Error code 3: " + error + " Error message: " + message);
                            switch (error){
                                case 1:
                                    transactionContract.transactionFailed("ERROR","Magnetic Stripe Error");
                                    DashboardContainer.backStack();
                                    break;

                                case 2:
                                    transactionContract.transactionFailed("ERROR","Smart Card Insert Error");
                                    DashboardContainer.backStack();
                                    break;

                                case 3:
                                    transactionContract.transactionFailed("ERROR","Fallback");
                                    Utility.DEBUG_LOG(TAG,"ERROR: fallback from completion presenter");
                                    try
                                    {
                                        if((!Objects.equals(txnTypePresenter, "SALEIPP"))){
//                                            DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(true, transactionAmount), "payment fragment");
                                            DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(PaymentAmountPresenter.FallbackOrignatorType.FT_Dip, transactionAmount,tipAmount), "payment fragment");
                                        }
//                                       DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(true), "payment fragment");
//                                        DashboardContainer.backStack();
                                    }
                                    catch (Exception e)
                                    {
                                        Utility.DEBUG_LOG(TAG,"Exception:"+e.getMessage().toString());
                                    }
                                    break;

                                case 4:
                                    transactionContract.transactionFailed("ERROR","Contactless Card Read Error");
                                    DashboardContainer.backStack();
                                    break;

                                case 5:
                                    transactionContract.transactionFailed("ERROR","Contactless Card Active Error");
                                    DashboardContainer.backStack();
                                    break;
                                case 6:
                                    transactionContract.transactionFailed("ERROR","Found Multi-Cards");
                                    DashboardContainer.backStack();
                                    break;

                                case 99:
                                    transactionContract.transactionFailed("ERROR","Service Crash");
                                    DashboardContainer.backStack();
                                    break;

                                case 100:
                                    transactionContract.transactionFailed("ERROR","Request Case Exception");
                                    DashboardContainer.backStack();
                                    break;

                            }
                        }
                    }
            );
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

    /**
     * \brief sample of EMV
     * <p>
     * \code{.java}
     * \endcode
     *
     * @seeF
     */
    void doEMV(int type, TransType transType,String amount) {
        //

        Utility.DEBUG_LOG(TAG, "start EMV demo");
        transactionContract.showProgress("Processing card");

        Bundle emvIntent = new Bundle();
        emvIntent.putInt(ConstIPBOC.startEMV.intent.KEY_cardType_int, type);
        if (transType ==TransType.T_PURCHASE) {
            emvIntent.putLong(ConstIPBOC.startEMV.intent.KEY_authAmount_long, Long.valueOf(amount));
        }
        emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_merchantName_String, MERCHANT_NAME);//

        emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_merchantId_String,MERCHANT_ID);  // 010001020270123
        emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_terminalId_String, TERMINAL_ID);   // 00000001
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportQ_boolean, ConstIPBOC.startEMV.intent.VALUE_supported);
//        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportQ_boolean, ConstIPBOC.startEMV.intent.VALUE_unsupported);
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportSM_boolean, ConstIPBOC.startEMV.intent.VALUE_supported);
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isQPBOCForceOnline_boolean, ConstIPBOC.startEMV.intent.VALUE_unforced);
        if (type == ConstIPBOC.startEMV.intent.VALUE_cardType_contactless) {   // todo, check here
            emvIntent.putByte(ConstIPBOC.startEMV.intent.KEY_transProcessCode_byte, (byte) 0x00);
        }
        emvIntent.putBoolean("isSupportPBOCFirst", false);
        emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_transCurrCode_String, "0586");
        emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_otherAmount_String, "0");

        try {
            iemv.startEMV(ConstIPBOC.startEMV.processType.full_process, emvIntent, emvHandler);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    private void initializeEMV() {

        /**
         * \brief initialize the call back listener of EMV
         *
         *  \code{.java}
         * \endcode
         * @version
         * @see
         *
         */
        emvHandler = new EMVHandler.Stub() {
            @Override
            public void onRequestAmount() throws RemoteException {
            }

            @Override
            public void onSelectApplication(List<Bundle> appList) throws RemoteException {
                List<String> aidNamesList= new ArrayList<>();
                for (Bundle aidBundle : appList) {
                    String aidName = aidBundle.getString("aidName");
                    aid = aidBundle.getString("aid");
                    String aidLabel = aidBundle.getString("aidLabel");
                    Utility.DEBUG_LOG(TAG, "AID Name=" + aidName + " | AID Label=" + aidLabel + " | AID=" + aid);
                    aidNamesList.add(aidName);
                }
                paymentContract.showMultipleAid(aidNamesList);
            }

            /**
             * \brief confirm the card info
             * show the card info and import the confirm result
             * \code{.java}
             * \endcode
             *
             */
            @SuppressLint("LongLogTag")
            @Override
            public void onConfirmCardInfo(Bundle info) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "onConfirmCardInfo...");
                transactionContract.hideProgress();
                savedPan = info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_PAN_String);
                String result = "onConfirmCardInfo callback, " +
                        "\nPAN:" + savedPan +
                        "\nTRACK2:" + info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_TRACK2_String) +
                        "\nCARD_SN:" + info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_CARD_SN_String) +
                        "\nSERVICE_CODE:" + info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_SERVICE_CODE_String) +
                        "\nEXPIRED_DATE:" + info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_EXPIRED_DATE_String);

                String completionTrack2 = info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_TRACK2_String);

                if(!td.getCardNo().equals(savedPan)){
                    transactionContract.transactionFailed("Error!","Card not matched");
                    DashboardContainer.backStack();
                    Utility.DEBUG_LOG("Completion", String.valueOf(td));
                }
                else {


                    String pan_validate = savedPan.substring(0, 6);
                    boolean bl = vs.validateCardRange(savedPan);
                    if (!bl) {

                        transactionContract.hideProgress();
                        transactionContract.transactionFailed("error", "Issuer not found");
                        DashboardContainer.backStack();
                    } else {
                        track2 = info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_TRACK2_String);
                        track2 = track2.substring(0, track2.length() - 1);
                        String cardExpiryChip = info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_EXPIRED_DATE_String);
                        int cardexp = Integer.parseInt(cardExpiryChip);
                        String expiryCheck = new SimpleDateFormat("yyMM", Locale.getDefault()).format(new Date());
                        int currentDateExpiry = Integer.parseInt(expiryCheck);

                        if (currentDateExpiry > cardexp) {
                            try {

                                transactionContract.hideProgress();
                                transactionContract.transactionFailed("error", "Card Expired");
                                DashboardContainer.backStack();
                            } catch (Exception e) {
                                Utility.DEBUG_LOG(TAG, "Exception:" + e.getMessage().toString());
                            }
                        } else {
                            String yearOfExpiry = cardExpiryChip.substring(0, 2);
                            String monthOfExpiry = cardExpiryChip.substring(2, 4);
                            Utility.DEBUG_LOG("", monthOfExpiry + yearOfExpiry);
                            String dateOfExpiryReverse = monthOfExpiry + yearOfExpiry;
                            //data8583.put(ISO8583u.F_Track_2_Data_35, track2);
                            if (!(txnTypePresenter.equals("ORBIT INQUIRY"))) {
                                //data8583.put(ISO8583u.F_DateOfExpired_14, cardExpiryChip);
                            }
                            if (HOST_INDEX_FROM_DB != 0) {
                                // data8583.put(ISO8583u.F_AccountNumber_02, savedPan);
                            }
                            cardExpiry = monthOfExpiry + "/" + yearOfExpiry;
                            transactionContract.toastShow("onConfirmCardInfo:" + result);
                            iemv.importCardConfirmResult(ConstIPBOC.importCardConfirmResult.pass.allowed);
                            Utility.DEBUG_LOG("Saved pan", savedPan);
                            Utility.DEBUG_LOG("Saved pan 1", td.getCardNo());


                        }
                    }
                }
            }

            /**
             * \brief show the pin pad
             *
             *  \code{.java}
             * \endcode
             *
             */
            //masood completion
            @Override
            public void onRequestInputPIN(boolean isOnlinePin, int retryTimes) throws RemoteException {
                transactionContract.toastShow("onRequestInputPIN isOnlinePin:" + isOnlinePin);
                // show the pin pad, import the pin block
                doPinPad(isOnlinePin, retryTimes);
                if (isOnlinePin) {
                    CARD_AUTH_METHOD = "ONLINE_PIN";
                } else if (!isOnlinePin) {
                    CARD_AUTH_METHOD = "OFFLINE_PIN";
                }


            }

            @Override
            public void onConfirmCertInfo(String certType, String certInfo) throws RemoteException {
//                transactionContract.toastShow("onConfirmCertInfo, type:" + certType + ",info:" + certInfo);
//                iemv.importCertConfirmResult(ConstIPBOC.importCertConfirmResult.option.CONFIRM);
            }

            // masood completion
            @Override
            public void onRequestOnlineProcess(Bundle aaResult) throws RemoteException {

                if(CTLS){
                    Utility.DEBUG_LOG("POS scheme", "ctls");
                    //value of CTLS
                    td.setCardType("Contactless");
                }
                //add field 02,14,23,26,53
                else {
                    //FOR EMV PIN SET THESE PARAMs
                    td.setCardType("Chip");
                    Utility.DEBUG_LOG("POS scheme", "chip");
                }
                // set the pin block
                /*if (savedPinBlock!=null){

                    if(HOST_INDEX_FROM_DB!=0) {
                        data8583.put(ISO8583u.F_PINData_52, Utility.byte2HexStr(savedPinBlock));
                        data8583.put(ISO8583u.F_SecurityRelateControlInformation_53, SECURITY_RELATED_CONTROL_INFORMATION);
                        data8583.put(ISO8583u.F_POSEntryMode_22, "071");
                    }// 02 mag, 05 smart, 07 ctls; 1 pin
                }*/// Taha's code updated on 15-Jan-2021 for PINBlock'

                if (savedPinBlock!=null){
                   // Utility.DEBUG_LOG(TAG, "pin data present:" + Utility.byte2HexStr(savedPinBlock));
                   // data8583.put(ISO8583u.F_PINData_52, Utility.byte2HexStr(savedPinBlock));
//                    data8583.put(ISO8583u.F_SecurityRelateControlInformation_53, SECURITY_RELATED_CONTROL_INFORMATION);
                    if ( td.getCardType() == "Magstripe")
                    {
                        Utility.DEBUG_LOG(TAG, "setting 021" );
                        td.setPosEntryMode("021");
                       // data8583.put(ISO8583u.F_POSEntryMode_22, "021");
                    }
                    else if ( td.getCardType() == "Contactless")
                    {
                        Utility.DEBUG_LOG(TAG, "setting 071" );
                        td.setPosEntryMode("071");
                       // data8583.put(ISO8583u.F_POSEntryMode_22, "071");
                    }
                    else if ( td.getCardType() == "Chip")
                    {
                        Utility.DEBUG_LOG(TAG, "setting 051" );
                        td.setPosEntryMode("051");
                      //  data8583.put(ISO8583u.F_POSEntryMode_22, "051");
                    }
//                    data8583.put(ISO8583u.F_POSEntryMode_22, "051");
//                    if(HOST_INDEX_FROM_DB!=0) {
//                        data8583.put(ISO8583u.F_PINData_52, Utility.byte2HexStr(savedPinBlock));
//                        data8583.put(ISO8583u.F_SecurityRelateControlInformation_53, SECURITY_RELATED_CONTROL_INFORMATION);
//                        data8583.put(ISO8583u.F_POSEntryMode_22, "071");
//                    }// 02 mag, 05 smart, 07 ctls; 1 pin
                }
                else
                {
                    Utility.DEBUG_LOG(TAG, "pin data not present");
                    if ( td.getCardType() == "Magstripe")
                    {
                        Utility.DEBUG_LOG(TAG, "setting 022" );
                        td.setPosEntryMode("022");
                       // data8583.put(ISO8583u.F_POSEntryMode_22, "022");
                    }
                    else if ( td.getCardType() == "Contactless")
                    {
                        Utility.DEBUG_LOG(TAG, "setting 072" );
                        td.setPosEntryMode("072");
                       // data8583.put(ISO8583u.F_POSEntryMode_22, "072");
                    }
                    else if ( td.getCardType() == "Chip")
                    {
                        Utility.DEBUG_LOG(TAG, "setting 052" );
                        td.setPosEntryMode("052");
                        //data8583.put(ISO8583u.F_POSEntryMode_22, "052");
                    }
                }


//                transactionDetail.setCardNo(savedPan);
//                transactionDetail.setCardexpiry(cardExpiry);

//                Utility.DEBUG_LOG("expiry 2",  transactionDetail.getCardexpiry());
//                transactionDetail.setTag55(tagOfF55);
                Utility.DEBUG_LOG(TAG, "start online request");
                // masood completion

                    Utility.DEBUG_LOG(TAG, "online request not finished because completion");
                paymentContract.showPaymentSuccessDialog(td);
                updateTransaction(td.getTxnType(),td.getIsAdvice(), td.getAmount() ,td.getAuthorizationIdentificationResponseCode());
                receiptPrinter.printSaleReceipt(printer, assetManager, td, aid, "Merchant Copy", txnTypePresenter, "", Bal,false);
                    transactionContract.transactionSuccessCustomer("Customer Copy", td, "Customer Copy", txnTypePresenter,Bal,true);


                Utility.DEBUG_LOG(TAG, "online request finished");

            }

            @Override
            public void onTransactionResult(int result, Bundle data) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "onTransactionResult");
                String msg = data.getString("ERROR");
                Utility.DEBUG_LOG(TAG,"onTransactionResult result = " + result + ",msg = " + msg);
                transactionContract.hideProgress();
                if(internet) {
                    // transactionContract.transactionFailed(String.valueOf(result),msg);

                    switch (result) {
                        case ConstPBOCHandler.onTransactionResult.result.TRY_AGAIN:
                            // read card fail
                            transactionContract.transactionFailed("Failed", "Unable to Read Card, Please Try again!");
                            DashboardContainer.backStack();
                            break;

                        case ConstPBOCHandler.onTransactionResult.result.EMV_NO_APP:
                            // read card fail
                            transactionContract.transactionFailed("Failed", "EMV no Application");
                            DashboardContainer.backStack();
                            break;

                        case ConstPBOCHandler.onTransactionResult.result.EMV_BALANCE_EXCEED:
                            // read card fail
                            transactionContract.transactionFailed("Failed", "EMV BALANCE EXCEED");
                            DashboardContainer.backStack();
                            break;

                        case ConstPBOCHandler.onTransactionResult.result.EMV_CARD_BIN_CHECK_FAIL:
                            // read card fail
                            transactionContract.transactionFailed("Failed", "Unable to Read Card, Please Re-Attempt the Transaction");
                            DashboardContainer.backStack();
                            break;

                        case ConstPBOCHandler.onTransactionResult.result.GAC_ERROR:
                            // read card fail
                            transactionContract.transactionFailed("Failed", "GAC Error, Please Re-Attempt the Transaction");
                            DashboardContainer.backStack();
                            break;
                        case ConstPBOCHandler.onTransactionResult.result.EMV_MULTI_CARD_ERROR:
                            // multi-cards found
                            transactionContract.transactionFailed("Failed", "multi-cards found, Please Re-Attempt the Transaction");
                            transactionContract.toastShow(data.getString(ConstPBOCHandler.onTransactionResult.data.KEY_ERROR_String));
                            DashboardContainer.backStack();
                            break;
                        case ConstPBOCHandler.onTransactionResult.result.QPBOC_ABORT:
                            // Transaction abort
                            transactionContract.transactionFailed("Failed", "Transaction abort, Please Re-Attempt the Transaction");
                            DashboardContainer.backStack();
                            break;
                        case ConstPBOCHandler.onTransactionResult.result.AARESULT_AAC:
                            // refuse on action analysis
                            transactionContract.transactionFailed("Failed", "refuse on action analysis, Please Re-Attempt the Transaction");
                            DashboardContainer.backStack();
                            break;
                        case ConstPBOCHandler.onTransactionResult.result.NEED_CONTACT:
                            // need contact
                            transactionContract.transactionFailed("Failed", "Unable to read card, Contact Card Needed!");
                            DashboardContainer.backStack();
                            break;

                        case ConstPBOCHandler.onTransactionResult.result.EMV_CARD_BLOCK:
                            // need contact
                            transactionContract.transactionFailed("Failed", "Card Blocked");
                            DashboardContainer.backStack();
                            break;

                        case ConstPBOCHandler.onTransactionResult.result.EMV_FALLBACK:
                            // need contact
                            transactionContract.transactionFailed("Failed", "Insert Mag");
                            DashboardContainer.backStack();
                            break;
                        case ConstPBOCHandler.onTransactionResult.result.EMV_SEE_PHONE:
                            Utility.DEBUG_LOG("SL","See Your Phone");
//
//                            try {
//                                sleep(5000);
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }

                            transactionContract.transactionFailed("failed","See Your Phone");
                            doSearchCard(ReaderType.RT_All);

//                            DashboardContainer.backStack();
                            break;
                    }
                }

            }
        };
    }


    @SuppressLint("CheckResult")
    private void updateTransaction(String txnType, boolean isAdvice,String amount,String authId) {
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).transactionDetailDao().updateCompletion(txnType,isAdvice,amount,authId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{ Utility.DEBUG_LOG("update Completion","success");}// completed with success
                        ,throwable ->{}// there was an error
                );
    }



    /**
     * \brief set main key and work key
     * <p>
     * \code{.java}
     * \end code
     *
     * @see IPinpad
     */
    void doSetKeys() {
        boolean bRet;
        try {
            bRet = ipinpad.loadMainKey(mainKeyId, Utility.hexStr2Byte(mainKey_MasterKey), null);
            transactionContract.toastShow("loadMainKey:" + bRet);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        try {
            bRet = ipinpad.loadWorkKey(PinpadKeyType.PINKEY, mainKeyId, workKeyId, Utility.hexStr2Byte(pinKey_WorkKey), null);
            transactionContract.toastShow("loadWorkKey:" + bRet);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    private void set8A(String val) {
        List<String> tlvList = new ArrayList<>();
        Utility.DEBUG_LOG(TAG,"+ set8A +");
        tlvList.add("8A02"+val);
        try {
            iemv.setEMVData(tlvList);
            Utility.DEBUG_LOG(TAG,tlvList.get(0));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    public void selectedAidIndex(int i){
        try {
            iemv.importAppSelection(i);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public boolean isPinRequiredUsingServiceCode()
    {
        boolean rv = false;
        Utility.DEBUG_LOG(TAG,"transactionDetail:"+track2);

        if (track2 == null )
        {
            Utility.DEBUG_LOG(TAG,"trk2:"+track2);
            rv = false;
        }
        else
        {
            int separatorIndex = 0;
            char serviceCode3rdDigit=0;
            String trk2  = track2;
            if ( (separatorIndex = trk2.indexOf('=') ) > 0 )
            {
                serviceCode3rdDigit = trk2.charAt(separatorIndex+7);
            }
            else if ( (separatorIndex = trk2.indexOf('D') ) > 0 )
            {
                serviceCode3rdDigit = trk2.charAt(separatorIndex+7);
            }
            Utility.DEBUG_LOG(TAG,"Track2:"+trk2);
            Utility.DEBUG_LOG(TAG,"service code 3rd digit:"+ serviceCode3rdDigit);
            if (
                    serviceCode3rdDigit == '0'  ||
                            serviceCode3rdDigit == '5'  ||
                            serviceCode3rdDigit == '6'  ||
                            serviceCode3rdDigit == '7'
            )
            {
                rv = true;
            }
        }
        Utility.DEBUG_LOG(TAG,"return rv:"+rv);
        return rv;
    }

    public boolean isChipCardFromTrack2()
    {
        boolean rv = false;
        Utility.DEBUG_LOG(TAG,"transactionDetail:"+track2);

        if (track2 == null )
        {
            Utility.DEBUG_LOG(TAG,"trk2:"+track2);
            rv = false;
        }
        else
        {
            int separatorIndex = 0;
            char serviceCode1stDigit=0;
            String trk2  = track2;
            if ( (separatorIndex = trk2.indexOf('=') ) > 0 )
            {
                serviceCode1stDigit = trk2.charAt(separatorIndex+5);
            }
            else if ( (separatorIndex = trk2.indexOf('D') ) > 0 )
            {
                serviceCode1stDigit = trk2.charAt(separatorIndex+7);
            }
            Utility.DEBUG_LOG(TAG,"Track2:"+trk2);
            Utility.DEBUG_LOG(TAG,"service code 1st digit:"+ serviceCode1stDigit);
            if (
                    serviceCode1stDigit == '2'  ||
                            serviceCode1stDigit == '6'
            )
            {
                rv = true;
            }
        }
        Utility.DEBUG_LOG(TAG,"return rv:"+rv);
        return rv;
    }
}
