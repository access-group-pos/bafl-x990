package cn.access.group.android_all_banks_pos;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;

import com.vfi.smartpos.deviceservice.aidl.CheckCardListener;
import com.vfi.smartpos.deviceservice.aidl.EMVHandler;
import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.PrinterConfig;
import com.vfi.smartpos.deviceservice.aidl.PrinterListener;
import com.vfi.smartpos.deviceservice.constdefine.*;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IPinpad;
import com.vfi.smartpos.deviceservice.aidl.OnlineResultHandler;
import com.vfi.smartpos.deviceservice.aidl.PinInputListener;
import com.vfi.smartpos.deviceservice.aidl.PinpadKeyType;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cn.access.group.android_all_banks_pos.Utilities.Comm;
import cn.access.group.android_all_banks_pos.Utilities.DecimalDigitsInputFilter;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.basic.ISO8583;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.*;
import cn.access.group.android_all_banks_pos.usecase.EmvSetAidRid;


/**
 * \Brief this a EMV workflow demo
 * <p>
 * Here you can find how to start EMV, build the 8583 packet, transfer packet from server
 * start pinpad, download AID, RID
 * THIS IS A DEMO ACTIVITY  ...  NOT TO BE USED
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "EMVDemo";
    public static IDeviceService idevice;

    IEMV iemv;
    IPinpad ipinpad;
    IBeeper iBeeper;
    IPrinter printer;

    EMVHandler emvHandler;
    PinInputListener pinInputListener;

    Button btnCheckCard;
    Button btnPinPad;
    Button btnSetKeys;
    Button btnSetAID;
    Button btnSetRID;
    Button btnClearAID;
    Button btnClearRID;
    Button btnTest;
    Button btnTransSignIn;
    Button btnTransBalance;
    Button btnPurchase;
    Button btnCustomPin;
    Context context;

    EditText edIP;
    EditText edPort;
    EditText edAmount;

    String hostIP = "172.191.1.110";
    int hostPort=5556;

    // some client static
    String terminalID = "11111112";
    String merchantName = "X990 EMV Demo";
    String merchantID = "1100000001     ";
    String headerType="";

    // keys
    int mainKeyId = 97;
    int workKeyId = 1;

    String pinKey_WorkKey = "89B07B35A1B3F47E89B07B35A1B3F488";
    String macKey = "";
    String mainKey_MasterKey = "758F0CD0C866348099109BAF9EADFA6E";

    String savedPan = "8880197100005603384";
    byte[] savedPinblock = null;


    SparseArray<String> data8583_i = null;
    /**
     * field-value map to save iso data
     */
    SparseArray<String> data8583 = null;
    SparseArray<String> tagOfF55 = null;

    String cardPan=null;
    String cardName=null;
    String cardExpiry=null;
    String stan=null;


    /**
     * \Brief the transaction type
     * <p>
     * Prefix
     * T_ means transaction
     * M_ means management
     */
    enum TransType {
        T_BANLANCE, T_PURCHASE,
        M_SIGNIN
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadConfiguration();


        btnCheckCard = findViewById(R.id.btnCheckCard);
        btnCheckCard.setOnClickListener(onClickListener);

        btnPinPad = findViewById(R.id.btnPinPad);
        btnPinPad.setOnClickListener(onClickListener);

        btnSetKeys = findViewById(R.id.btnSetKeys);
        btnSetKeys.setOnClickListener(onClickListener);

        btnSetAID = findViewById(R.id.btnSetAID);
        btnSetAID.setOnClickListener(onClickListener);

        btnSetRID = findViewById(R.id.btnRID);
        btnSetRID.setOnClickListener(onClickListener);

        btnClearAID = findViewById(R.id.btnClearAID);
        btnClearAID.setOnClickListener(onClickListener);

        btnClearRID = findViewById(R.id.btnClearRID);
        btnClearRID.setOnClickListener(onClickListener);

        btnTest = findViewById(R.id.btnTest);
        btnTest.setOnClickListener(onClickListener);

        btnTransSignIn = findViewById(R.id.btnTransSignIn);
        btnTransSignIn.setOnClickListener(onClickListener);

        btnTransBalance = findViewById(R.id.btnBalance);
        btnTransBalance.setOnClickListener(onClickListener);

        btnPurchase = findViewById(R.id.btnPurchase);
        btnPurchase.setOnClickListener(onClickListener);

        btnCustomPin = findViewById(R.id.btnCustomPinpad);
        btnCustomPin.setOnClickListener(onClickListener);

        edIP = findViewById(R.id.edIP);
        if (null == edIP) {
            Utility.DEBUG_LOG(TAG, "cannot get the IP edit");
        }
        else
        {
            edIP.setText(hostIP);
        }
        edPort = findViewById(R.id.edPort);
        if (null == edPort) {
            Utility.DEBUG_LOG(TAG, "cannot get the Port edit");
        }
        else
        {
            edPort.setText(String.format("%d", hostPort));
        }
        edAmount = findViewById(R.id.edAmount);
        // Filter for max Length
        InputFilter maxLenFilter = new InputFilter.LengthFilter(13);
        edAmount.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(2), maxLenFilter});


        ISO8583u.SimulatorType st;
        switch (headerType)
        {
            case ("AuthAll"):
                st=ISO8583u.SimulatorType.AuthAll;
                break;
            case "SimulatorHost":
                st=ISO8583u.SimulatorType.SimulatorHost;
                break;
            case "ActualBank":
                st=ISO8583u.SimulatorType.ActualBank;
                break;
            default:
                st=ISO8583u.SimulatorType.AuthAll;
                break;
        }
        initialize8583data(st);

        // check assets fonts and copy to file system for Service
        InitializeFontFiles();
    }

    public void writeToFile(String str)
    {
        try
        {
            FileOutputStream fOut = this.openFileOutput("temp.json",Context.MODE_PRIVATE);
            fOut.write(str.getBytes());
            fOut.close();
        }
        catch (Exception ex)
        {

        }
    }

    public String loadFromFile()
    {
        try {

            FileInputStream fin = this.openFileInput("temp.json");
            int c;
            String temp="";
            while( (c = fin.read()) != -1){
                temp = temp + Character.toString((char)c);
            }
            fin.close();
            return temp;

        }
        catch (Exception ex)
        {

        }
        return null;
    }
    public void writeFileOnInternalStorage(String sFileName, String sBody){
        File file = new File(this.getFilesDir(),"UBL");
        if(!file.exists()){
            file.mkdir();
        }

        try{
            File gpxfile = new File(file, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();

        }catch (Exception e){
            e.printStackTrace();

        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("config.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    protected  void loadConfiguration()
    {
        Date currentTime = Calendar.getInstance().getTime();
        this.stan = String.format("%02d%02d%02d",
                Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
                Calendar.getInstance().get(Calendar.MINUTE),
                Calendar.getInstance().get(Calendar.SECOND)
        );
        try {

            JSONObject obj = new JSONObject(loadJSONFromAsset());
            this.terminalID = obj.getString("TID");
            this.merchantID = obj.getString("MID");
//            this.stan = obj.getString("stan");

            this.hostIP = obj.getString("Host IP");
            this.hostPort = obj.getInt("Host Port");
            this.headerType = obj.getString("Header Type");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent();
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        boolean isSucc = bindService(intent, conn, Context.BIND_AUTO_CREATE);
        if (!isSucc) {
            Utility.DEBUG_LOG("TAG", "deviceService connect fail!");
        } else {
            Utility.DEBUG_LOG("TAG", "deviceService connect success");
            initializeEMV();
            initializePinInputListener();
        }


    }

    // button -- start
    OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == btnCheckCard) {
                doBalance();
            } else if (view == btnPinPad) {
                doPinPad();
            } else if (view == btnSetKeys) {
                doSetKeys();
            } else if (view == btnSetAID) {
                doSetAID(1);
            } else if (view == btnClearAID) {
                doSetAID(3);
            } else if (view == btnSetRID) {
                doSetRID(1);
            } else if (view == btnClearRID) {
                doSetRID(3);
            } else if (view == btnTransSignIn) {
                if (null == edIP) {
                    Utility.DEBUG_LOG(TAG, "cannot get the IP edit");
                }
                if (null == edPort) {
                    Utility.DEBUG_LOG(TAG, "cannot get the Port edit");
                }

                hostIP = edIP.getText().toString();
                String port = edPort.getText().toString();
                if (port.length() == 0) {
                    Utility.DEBUG_LOG(TAG, "cannot read port");
                    hostPort = 5555;
                } else {
                    hostPort = Integer.valueOf(port);
                }
                Utility.DEBUG_LOG(TAG, "Host:" + hostIP + ":" + hostPort);

                doSignIn();
            } else if (view == btnTransBalance) {
                if (null == hostIP) {
                    toastShow("Sign In first!");
                    // return;
                }
                doBalance();

            } else if (view == btnPurchase) {
                hostIP = edIP.getText().toString();
                String port = edPort.getText().toString();
                if (port.length() == 0) {
                    Utility.DEBUG_LOG(TAG, "cannot read port");
                    hostPort = 5555;
                } else {
                    hostPort = Integer.valueOf(port);
                }
                Utility.DEBUG_LOG(TAG, "Host:" + hostIP + ":" + hostPort);

//                SplashScreen main2Activity = new SplashScreen();
                Utility.DEBUG_LOG(TAG,"before doPurchase 1");
                doPurchase(edAmount.getText().toString());
            } else if (view == btnTest) {
                // nothing test here
              //  UblSale();
                doPrintString();
            } else if (view == btnCustomPin) {
                doStartCustonViewPinpad();
            }
        }
    };

    public void UblSale() {
    }

    // connect service -- start
    /**
     * \Brief connect the VFI-Service and set some devices
     */
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            idevice = IDeviceService.Stub.asInterface(service);
            try {

                iemv = idevice.getEMV();
                ipinpad = idevice.getPinpad(1);
                iBeeper = idevice.getBeeper();
                printer = idevice.getPrinter();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
           // Toast.makeText(MainActivity.this, "bind service success", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };
    // connect service -- end

    // log & display
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String string = msg.getData().getString("string");
            Utility.DEBUG_LOG(TAG,"before getData");
            super.handleMessage(msg);
            Utility.DEBUG_LOG(TAG, msg.getData().getString("msg"));
            Toast.makeText(MainActivity.this, msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();

        }
    };

    void toastShow(String str) {
        Message msg = new Message();
        Utility.DEBUG_LOG(TAG,"before getData");
        msg.getData().putString("msg", str);
        handler.sendMessage(msg);
    }

    public void printSaleReceipt()
    {
        try {

            // bundle formate for AddTextInLine
            Bundle format = new Bundle();
            Bundle formatRight = new Bundle();

            // bundle formate for AddTextInLine
            Bundle fmtAddTextInLine = new Bundle();
            // image

            String start6digits=cardPan.substring(0,6);
            String cardType = "UNKNOWN";
            String tempString = "";
            if ( start6digits.equals("458539"))
                cardType = "VISA";


            byte[] buffer = null;
            try {
                //
                InputStream is = this.getAssets().open("access_logo.jpg");
                // get the size
                int size = is.available();
                // crete the array of byte
                buffer = new byte[size];
                is.read(buffer);
                // close the stream
                is.close();

            } catch (IOException e) {
                // Should never happen!
                throw new RuntimeException(e);
            }
            if( null != buffer) {
                Bundle fmtImage = new Bundle();
                fmtImage.putInt("offset", (384/2)-150/2);
                fmtImage.putInt("width", 150);  // bigger then actual, will print the actual
                fmtImage.putInt("height", 150); // bigger then actual, will print the actual
                printer.addImage( fmtImage, buffer );
            }

            formatRight.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.Alignment.RIGHT);

            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.SMALL_16_16);
//            format.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterFonts.path + PrinterFonts.FONT_AGENCYB );

            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24 );
//            fmtAddTextInLine.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterFonts.path + PrinterFonts.FONT_AGENCYB );

            printer.addText(format, "ACCESS GROUP TEST MERCHANT");
            printer.addText(format, "BAHRIA COMPLEX IV");
            printer.addText(format, "KARACHI");
            // left


            printer.addTextInLine(fmtAddTextInLine, "TID:", "", this.terminalID, 0);
            printer.addTextInLine(fmtAddTextInLine, "MID:", "", this.merchantID, 0);

            printer.feedLine(2);


//            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
//            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.Alignment.LEFT);
//            tempString = "CARD TYPE: " + cardType;

//            printer.addText(format, tempString );
            printer.addTextInLine(fmtAddTextInLine, "CARD TYPE: " + cardType, "", "", 0);
//            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
//            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.Alignment.LEFT);
//            printer.addText(format, cardPan);
            printer.addTextInLine(fmtAddTextInLine, cardPan, "", "", 0);
//            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
//            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.Alignment.LEFT);
//            printer.addText(format, cardName);
            printer.addTextInLine(fmtAddTextInLine, cardName,"", "", 0);

            printer.feedLine(2);

            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_DH_24_48_IN_BOLD);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.Alignment.CENTER);
            printer.feedLine(1);

            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_DH_24_48_IN_BOLD);
            printer.addTextInLine(fmtAddTextInLine, "","SALE","",0);

            printer.feedLine(2);

            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.Alignment.RIGHT);
            printer.feedLine(1);
            tempString = "EXP.:" + cardExpiry;
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "", "","EXP.:" + cardExpiry,0);
//            printer.addText(formatRight, tempString);

            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "BATCH: 000001", "","TRACE NO:"+this.stan,0);

            String dateFromResponse = isoResponse.getUnpack(ISO8583u.F_TxDate_13);
            String timeFromResponse = isoResponse.getUnpack(ISO8583u.F_TxTime_12);
            String hourStr = timeFromResponse.substring(0,2);
            String minuteStr = timeFromResponse.substring(2,4);
            String time = hourStr+":"+minuteStr;

            String monthStr = dateFromResponse.substring(0,2);
            String dayStr = dateFromResponse.substring(2,2+2);
            int iMonth = Integer.parseInt(monthStr);
            int iDay = Integer.parseInt(dayStr);
            String date = dateFromResponse;
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, iMonth );
            cal.add(Calendar.DAY_OF_MONTH, iDay );
            Date dt = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("MMM dd, yy");

            String formattedDate = df.format(dt);

            date=formattedDate;
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "DATE: "+date, "","TIME: "+time,0);
            String RRN = isoResponse.getUnpack(ISO8583u.F_RRN_37);
            printer.addTextInLine(fmtAddTextInLine, "RRN: " + RRN, "","APP.:88989",0);

            printer.feedLine(2);
            String totalAmountStr = isoResponse.getUnpack(ISO8583u.F_AmountOfTransactions_04);
            float totalAmountF = Integer.parseInt(totalAmountStr) / (100.0f);
            totalAmountStr =  String.format("%.2f", totalAmountF);
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            printer.addTextInLine(fmtAddTextInLine, "TOTAL:", "","RS " + totalAmountStr,0);

            printer.feedLine(2);
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.Alignment.LEFT);
            printer.addTextInLine(fmtAddTextInLine, "AID: 0980980980909", "","",0);
            printer.addTextInLine(fmtAddTextInLine, "APP.: DEBIT "+ cardType, "","",0);
//            printer.addTextInLine(fmtAddTextInLine, "ARQC: 0980980980909", "","",0);
//            printer.addText(format,"AID: 0980980980909");
//            printer.addText(format,"APP.: DEBIT "+ cardType);
//            printer.addText(format,"ARQC: 0980980980909");

            printer.feedLine(2);

            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);

            printer.addText(format, "SIGN X__________________________");
            printer.feedLine(2);

            format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.Alignment.CENTER);
            printer.addText(format, cardName);
            printer.addText(format, "I AGREE TO PAY ABOVE\nTOTAL AMOUNT ACCORDING\nTO CARD ISSUER AGREEMENT");

            printer.feedLine(4);
            printer.addText(format, "MERCHANT COPY");

//            printer.addText(format, "تمباکو نوشی صحت کیلئے");
//            printer.addText(format, "مضر ہے۔ وزارت صحت");
            fmtAddTextInLine.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.LARGE_32_32);
//            printer.addTextInLine(format, "","برائے مہربانی رسید","",0);
//            printer.addTextInLine(format, "","کو بغور دیکھ کر","",0);
//            printer.addTextInLine(format, "","دستخظ کریں۔ بعد میں","",0);
//            printer.addTextInLine(format, "","کمپنی زمہ دار نہ ہوگی","",0);
            printer.feedLine(4);

            printer.startPrint(new MyListener());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    public void doPrintString(View view, String msg) {
        try {
            // bundle formate for AddTextInLine
            Bundle fmtAddTextInLine = new Bundle();
            fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.LARGE_32_32);
            fmtAddTextInLine.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterFonts.path + PrinterFonts.FONT_HuaWenZhongSong);
            printer.addTextInLine(fmtAddTextInLine, msg, "", "", 0);

            //
            printer.feedLine(3);

            // start print here
            printer.startPrint(new MyListener());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    public void doPrintString(){
            try {
                // bundle format for addText
                Bundle format = new Bundle();

                // bundle formate for AddTextInLine
                Bundle fmtAddTextInLine = new Bundle();
                //
                format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_DH_24_48_IN_BOLD);
                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
//            printer.addText(format, "Hello!");

                format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.LARGE_DH_32_64_IN_BOLD);
                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
//            printer.addText(format, "Hello!");

                format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.HUGE_48);
                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                printer.addText(format, "Hello!");

                // image

                byte[] buffer = null;
                try {
                    //
                    InputStream is = this.getAssets().open("logo.jpg");
                    // get the size
                    int size = is.available();
                    // crete the array of byte
                    buffer = new byte[size];
                    is.read(buffer);
                    // close the stream
                    is.close();

                } catch (IOException e) {
                    // Should never happen!
                    throw new RuntimeException(e);
                }
                if (null != buffer) {
                    Bundle fmtImage = new Bundle();
                    fmtImage.putInt("offset", (384 - 200) / 2);
                    fmtImage.putInt("width", 250);  // bigger then actual, will print the actual
                    fmtImage.putInt("height", 128); // bigger then actual, will print the actual
                    printer.addImage(fmtImage, buffer);

                    fmtImage.putInt("offset", 50);
                    fmtImage.putInt("width", 100); // smaller then actual, will print the setting
                    fmtImage.putInt("height", 24); // smaller then actual, will print the setting
                    printer.addImage(fmtImage, buffer);
                }


                //
                fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.LARGE_32_32);
                fmtAddTextInLine.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterFonts.path + PrinterFonts.FONT_FORTE);
                printer.addTextInLine(fmtAddTextInLine, "humayun X9-Series", "", "", 0);
                //
                fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.NORMAL_24_24);
                fmtAddTextInLine.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterFonts.path + PrinterFonts.FONT_segoesc);
                printer.addTextInLine(fmtAddTextInLine, "", "", "This is the Print Demo", 0);


                format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
                printer.addText(format, "Hello humayun in font NORMAL_24_24!");
                // left
                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                printer.addText(format, "Left Alignment long string here: PrinterConfig.addText.Alignment.LEFT ");

                // right
                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                printer.addText(format, "Right Alignment  long  string with wrapper here");

                printer.addText(format, "--------------------------------");
                Bundle fmtAddBarCode = new Bundle();
                fmtAddBarCode.putInt(PrinterConfig.addBarCode.Alignment.BundleName, PrinterConfig.addBarCode.Alignment.RIGHT);
                fmtAddBarCode.putInt(PrinterConfig.addBarCode.Height.BundleName, 64);
                printer.addBarCode(fmtAddBarCode, "123456 humayun");

                fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.LARGE_32_32);
                fmtAddTextInLine.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterFonts.FONT_AGENCYB);
                printer.addTextInLine(fmtAddTextInLine, "", "123456 humayun", "", 0);
                fmtAddTextInLine.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterConfig.addTextInLine.GlobalFont.English);    // set to the default

                printer.addText(format, "--------------------------------");


                fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.LARGE_32_32);
                fmtAddTextInLine.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterFonts.path + PrinterFonts.FONT_ALGER);
                printer.addTextInLine(fmtAddTextInLine, "Left", "Center", "right", 0);
                fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.LARGE_32_32);
                fmtAddTextInLine.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterFonts.path + PrinterFonts.FONT_BROADW);
                printer.addTextInLine(fmtAddTextInLine, "L & R", "", "Divide Equally", 0);
                printer.addTextInLine(fmtAddTextInLine, "L & R", "", "Divide flexible", PrinterConfig.addTextInLine.mode.Devide_flexible);
                // left
                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                printer.addText(format, "--------------------------------");

                fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.LARGE_32_32);
                fmtAddTextInLine.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterConfig.addTextInLine.GlobalFont.English);
                fmtAddTextInLine.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterFonts.path + PrinterFonts.FONT_segoesc);
                printer.addTextInLine(fmtAddTextInLine,
                        "", "",
                        "Right long string here call addTextInLine ONLY give the right string",
                        0);

                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                format.putInt(PrinterConfig.addText.FontSize.BundleName, PrinterConfig.addText.FontSize.NORMAL_24_24);
                printer.addText(format, "--------------------------------");

                fmtAddTextInLine.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterConfig.addTextInLine.GlobalFont.English);  // this the default
                printer.addTextInLine(fmtAddTextInLine, "", "#",
                        "Right long string with the center string",
                        0);
                printer.addText(format, "--------------------------------");
                fmtAddTextInLine.putInt(PrinterConfig.addTextInLine.FontSize.BundleName, PrinterConfig.addTextInLine.FontSize.SMALL_16_16);
                fmtAddTextInLine.putString(PrinterConfig.addTextInLine.GlobalFont.BundleName, PrinterFonts.FONT_AGENCYB);
                printer.addTextInLine(fmtAddTextInLine, "Print the QR code far from the barcode to avoid scanner found both of them", "",
                        "",
                        PrinterConfig.addTextInLine.mode.Devide_flexible);

                Bundle fmtAddQRCode = new Bundle();
                fmtAddQRCode.putInt(PrinterConfig.addQrCode.Offset.BundleName, 128);
                fmtAddQRCode.putInt(PrinterConfig.addQrCode.Height.BundleName, 128);
                printer.addQrCode(fmtAddQRCode, "www.humayun.cn");

                printer.addTextInLine(fmtAddTextInLine, "", "try to scan it",
                        "",
                        0);


                printer.addText(format, "---------X-----------X----------");

                printer.feedLine(3);

                // start print here
                printer.startPrint(new MyListener());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

    class MyListener extends PrinterListener.Stub {
        @Override
        public void onError(int error) throws RemoteException {
            Message msg = new Message();
            Utility.DEBUG_LOG(TAG,"before getData");
            msg.getData().putString("msg", "print error,errno:" + error);
            handler.sendMessage(msg);
        }

        @Override
        public void onFinish() throws RemoteException {
            Message msg = new Message();
            Utility.DEBUG_LOG(TAG,"before getData");
            msg.getData().putString("msg", "print finished");
            handler.sendMessage(msg);
        }
    }
        void initializeEMV() {

        /**
         * \brief initialize the call back listener of EMV
         *
         *  \code{.java}
         * \endcode
         * @version
         * @see
         *
         */
        emvHandler = new EMVHandler.Stub() {
            @Override
            public void onRequestAmount() throws RemoteException {
                // this is an deprecated callback
//                toastShow("onRequestAmount...");
//                iemv.importAmount(234);
            }

            @Override
            public void onSelectApplication(List<Bundle> appList) throws RemoteException {
                for (Bundle aidBundle : appList) {
                    String aidName = aidBundle.getString("aidName");
                    String aid = aidBundle.getString("aid");
                    String aidLabel = aidBundle.getString("aidLabel");
                    Utility.DEBUG_LOG(TAG, "AID Name=" + aidName + " | AID Label=" + aidLabel + " | AID=" + aid);
                }
                toastShow("onSelectApplication..." + appList.get(0));
                iemv.importAppSelection(0);
            }

            /**
             * \brief confirm the card info
             *
             * show the card info and import the confirm result
             * \code{.java}
             * \endcode
             *
             */
            @Override
            public void onConfirmCardInfo(Bundle info) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "onConfirmCardInfo...");
                savedPan = info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_PAN_String);

                String result = "onConfirmCardInfo callback, " +
                        "\nPAN:" + savedPan +
                        "\nTRACK1:" + info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_TRACK1_String) +
                        "\nTRACK2:" + info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_TRACK2_String) +
                        "\nCARD_SN:" + info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_CARD_SN_String) +
                        "\nSERVICE_CODE:" + info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_SERVICE_CODE_String) +
                        "\nEXPIRED_DATE:" + info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_EXPIRED_DATE_String);

                byte[] tlv = iemv.getCardData("9F51");
                result += ("\n9F51:" + Utility.byte2HexStr(tlv));

                String track2 = info.getString(ConstPBOCHandler.onConfirmCardInfo.info.KEY_TRACK2_String);
                if (null != track2) {
                    int a = track2.indexOf('D');
                    if (a > 0) {
                        track2 = track2.substring(0, a);
                    }
                    data8583.put(ISO8583u.F_Track_2_Data_35, track2);
                }

                toastShow("onConfirmCardInfo:" + result);

                iemv.importCardConfirmResult(ConstIPBOC.importCardConfirmResult.pass.allowed);
            }

            /**
             * \brief show the pin pad
             *
             *  \code{.java}
             * \endcode
             *
             */
            @Override
            public void onRequestInputPIN(boolean isOnlinePin, int retryTimes) throws RemoteException {
                toastShow("onRequestInputPIN isOnlinePin:" + isOnlinePin);
                // show the pin pad, import the pin block
                doPinPad();
            }

            @Override
            public void onConfirmCertInfo(String certType, String certInfo) throws RemoteException {
                toastShow("onConfirmCertInfo, type:" + certType + ",info:" + certInfo);

                iemv.importCertConfirmResult(ConstIPBOC.importCertConfirmResult.option.CONFIRM);
            }

            @Override
            public void onRequestOnlineProcess(Bundle aaResult) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "onRequestOnlineProcess...");
                int result = aaResult.getInt(ConstPBOCHandler.onRequestOnlineProcess.aaResult.KEY_RESULT_int);
                boolean signature = aaResult.getBoolean(ConstPBOCHandler.onRequestOnlineProcess.aaResult.KEY_SIGNATURE_boolean);
                toastShow("onRequestOnlineProcess result=" + result + " signal=" + signature);
                switch (result) {
                    case ConstPBOCHandler.onRequestOnlineProcess.aaResult.VALUE_RESULT_AARESULT_ARQC:
                    case ConstPBOCHandler.onRequestOnlineProcess.aaResult.VALUE_RESULT_QPBOC_ARQC:
                        toastShow(aaResult.getString(ConstPBOCHandler.onRequestOnlineProcess.aaResult.KEY_ARQC_DATA_String));
                        break;
                    case ConstPBOCHandler.onRequestOnlineProcess.aaResult.VALUE_RESULT_PAYPASS_EMV_ARQC:
                        break;
                }

                byte[] tlv;
                tagOfF55 = new SparseArray<>();

                int[] tagList = {
                        0x9F26,
                        0x9F27,
                        0x9F10,
                        0x9F37,
                        0x9F36,
                        0x95,
                        0x9A,
                        0x9C,
                        0x9F02,
                        0x5F2A,
                        0x82,
                        0x9F1A,
                        0x9F03,
                        0x9F33,
                        0x9F74,
                        0x9F24,
                };

                for (int tag : tagList) {
                    tlv = iemv.getCardData(Integer.toHexString(tag).toUpperCase());
                    if (null != tlv && tlv.length > 0) {
                        Utility.DEBUG_LOG(TAG, Utility.byte2HexStr(tlv));
                        tagOfF55.put(tag, Utility.byte2HexStr(tlv));  // build up the field 55
                    } else {
                        Utility.DEBUG_LOG(TAG, "getCardData:" + Integer.toHexString(tag) + ", fails");
                    }
                }

                // set the pin block
                data8583.put(ISO8583u.F_PINData_52, Utility.byte2HexStr(savedPinblock));


                Utility.DEBUG_LOG(TAG, "start online request");
                onlineRequest.run();
                Utility.DEBUG_LOG(TAG, "online request finished");

                // import the online result
                Bundle onlineResult = new Bundle();
                onlineResult.putBoolean(ConstIPBOC.inputOnlineResult.onlineResult.KEY_isOnline_boolean, true);
                if (isoResponse.unpackValidField[ISO8583u.F_ResponseCode_39]) {
                    onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String, isoResponse.getUnpack(ISO8583u.F_ResponseCode_39));
                } else {
                    onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_respCode_String, "00");
                }

                if (isoResponse.unpackValidField[ISO8583u.F_AuthorizationIdentificationResponseCode_38]) {
                    //
                    onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_authCode_String, isoResponse.getUnpack(ISO8583u.F_AuthorizationIdentificationResponseCode_38));
                } else {
                    onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_authCode_String, "123456");
                }

                // onlineResult.putString( ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String, "910A1A1B1C1D1E1F2A2B30307211860F04DA9F790A0000000100001A1B1C1D");
                if (isoResponse.unpackValidField[55]) {
                    onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String, isoResponse.getUnpack(55));

                } else {
                   // onlineResult.putString(ConstIPBOC.inputOnlineResult.onlineResult.KEY_field55_String, "5F3401019F3303E0F9C8950500000000009F1A0201569A039707039F3704F965E43082027C009F3602041C9F260805142531F709C8669C01009F02060000000000125F2A0201569F101307010103A02000010A01000000000063213EC29F2701809F1E0831323334353637389F0306000000000000");
                }
//                onlineResult.putBoolean("getPBOCData", true);
//                onlineResult.putInt("importAppSelectResult", 1);
//                onlineResult.putInt("IsPinInput", 1);
//                onlineResult.putString("importPIN", "123456");
//                onlineResult.putInt("importAmount", 101);
//                onlineResult.putBoolean("cancelCardConfirmResult", false);


                iemv.inputOnlineResult(onlineResult, new OnlineResultHandler.Stub() {
                    @Override
                    public void onProccessResult(int result, Bundle data) throws RemoteException {
                        Utility.DEBUG_LOG(TAG, "onProccessResult callback:");
                        String str = "RESULT:" + result +
                                "\nTC_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_TC_DATA_String, "not defined") +
                                "\nSCRIPT_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_SCRIPT_DATA_String, "not defined") +
                                "\nREVERSAL_DATA:" + data.getString(ConstOnlineResultHandler.onProccessResult.data.KEY_REVERSAL_DATA_String, "not defined");
                        toastShow(str);

                        switch (result) {
                            case ConstOnlineResultHandler.onProccessResult.result.TC:
                                toastShow("TC");
                                break;
                            case ConstOnlineResultHandler.onProccessResult.result.Online_AAC:
                                toastShow("Online_AAC");
                                onlineRequest.run();
                                break;
                            default:
                                toastShow("error, code:" + result);
                                break;
                        }
                    }
                });
            }

            @Override
            public void onTransactionResult(int result, Bundle data) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "onTransactionResult");
                String msg = data.getString("ERROR");
                toastShow("onTransactionResult result = " + result + ",msg = " + msg);

                switch (result) {

                    case ConstPBOCHandler.onTransactionResult.result.EMV_CARD_BIN_CHECK_FAIL:
                        // read card fail
                        toastShow("read card fail");
                        return;
                    case ConstPBOCHandler.onTransactionResult.result.EMV_MULTI_CARD_ERROR:
                        // multi-cards found
                        toastShow(data.getString(ConstPBOCHandler.onTransactionResult.data.KEY_ERROR_String));
                        return;
                }

            }
        };
    }

    void doSearchCard(final TransType transType) {
        toastShow("Please Swipe \nMagnetic Card \nto continue...");
        Bundle cardOption = new Bundle();
//        cardOption.putBoolean( ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_unsupported);
        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_Contactless_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_SmartCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);
        cardOption.putBoolean(ConstIPBOC.checkCard.cardOption.KEY_MagneticCard_boolean, ConstIPBOC.checkCard.cardOption.VALUE_supported);


        try {
            iemv.checkCard(cardOption, 30, new CheckCardListener.Stub() {
                        @Override
                        public void onCardSwiped(Bundle track) throws RemoteException {
                            Utility.DEBUG_LOG(TAG, "onCardSwiped ...");
//                            iemv.stopCheckCard();
//                            iemv.abortPBOC();

                          //  iBeeper.startBeep(200);

                            String pan = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_PAN_String);
                            String track1 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK1_String);
                            String track2 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK2_String);
                            String track3 = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_TRACK3_String);
                            String serviceCode = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_SERVICE_CODE_String);

                            cardPan = pan;
//                            cardExpiry = track2.substring( 17, 17+4 );
                            String expiryMonth = track2.substring( 19, 19+2 );
                            String expiryYear = track2.substring( 17, 17+2 );
                            cardExpiry = expiryMonth + "/" + expiryYear;
                            cardName = track1.substring( 18, 33 );



                            Utility.DEBUG_LOG(TAG, "onCardSwiped ...1");
                            byte[] bytes = Utility.hexStr2Byte(track2);
                            Utility.DEBUG_LOG(TAG, "Track2:" + track2 + " (" + Utility.byte2HexStr(bytes) + ")");

                            Boolean bIsKeyExist = ipinpad.isKeyExist(12, 1);
                            if (!bIsKeyExist) {
                                Utility.DEBUG_LOG(TAG, "no key exist type: 12, @: 1");
                            }
                            byte[] enctypted = ipinpad.dukptEncryptData(1, 1, 1, bytes, new byte[]{0, 0, 0, 0, 0, 0, 0, 0,});
                            if (null == enctypted) {
                                Utility.DEBUG_LOG(TAG, "NO DUKPT Encrypted got");
                            } else {
                                Utility.DEBUG_LOG(TAG, "DUKPT:" + Utility.byte2HexStr(enctypted));
                            }
                            bIsKeyExist = ipinpad.isKeyExist(12, 1);
                            if (!bIsKeyExist) {
                                Utility.DEBUG_LOG(TAG, "no key exist type: 12, @: 1");
                            }


                            if ( null != track2 )
                            {
                                data8583.put(ISO8583u.F_Track_2_Data_35, track2); //refill this
                            }


                            if (null != track3) {
//                                data8583.put(ISO8583u.F_Track_3_Data_36, track3);

                            }
                            String validDate = track.getString(ConstCheckCardListener.onCardSwiped.track.KEY_EXPIRED_DATE_String);
                            if (null != validDate) {
//                                data8583.put(ISO8583u.F_DateOfExpired_14, validDate);
//                                cardExpiry = validDate;
                            }
//                            String amount = edAmount.getText().toString();
//                            data8583.put(ISO8583u.F_AmountOfTransactions_04, amount );
                            Utility.DEBUG_LOG(TAG, "onCardSwiped ...3");
                            onlineRequest.run();
                            if ( isoResponse == null )
                            {
                                toastShow("Transaction failed");
                            }
                            else {
                                stan = isoResponse.getUnpack(ISO8583u.F_STAN_11);
                                String responseCode = isoResponse.getUnpack(ISO8583u.F_ResponseCode_39);
                                //toastShow("response:" + responseCode);
                                if (responseCode.equals("00"))//success
                                {
                                    toastShow("SALE Transaction\nPerformed Successfully");
                                   // iBeeper.startBeep(400);
                                   // printSaleReceipt();
                                    int stanInt = Integer.parseInt(stan);
                                    stan = String.format("%06d", ++stanInt );
                                } else {
                                    toastShow("Transaction failed");
                                }
//                            toastShow("response:" + isoResponse.unpackValidField[ISO8583u.F_ResponseCode_39] );
// toastShow("response:" + isoResponse.getField(ISO8583u.F_ResponseCode_39));
                            }
                            isoResponse = null;
                        }


                        @Override
                        public void onCardPowerUp() throws RemoteException {
                            iemv.stopCheckCard();
                            iemv.abortEMV();
                            //iBeeper.startBeep(200);
                            doEMV(ConstIPBOC.startEMV.intent.VALUE_cardType_smart_card, transType);
                        }

                        @Override
                        public void onCardActivate() throws RemoteException {
                            iemv.stopCheckCard();
                            iemv.abortEMV();
                          //  iBeeper.startBeep(200);
                            doEMV(ConstIPBOC.startEMV.intent.VALUE_cardType_contactless, transType);

                        }

                        @Override
                        public void onTimeout() throws RemoteException {
                            toastShow("timeout");
                        }

                        @Override
                        public void onError(int error, String message) throws RemoteException {
                            toastShow("error:" + error + ", msg:" + message);
                        }
                    }
            );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    /**
     * \brief sample of EMV
     * <p>
     * \code{.java}
     * \endcode
     *
     * @see
     */
    void doEMV(int type, TransType transType) {
        //
        Utility.DEBUG_LOG(TAG, "start EMV demo");

        Bundle emvIntent = new Bundle();
        emvIntent.putInt(ConstIPBOC.startEMV.intent.KEY_cardType_int, type);
        if (transType == TransType.T_PURCHASE) {
            emvIntent.putLong(ConstIPBOC.startEMV.intent.KEY_authAmount_long, Long.valueOf(edAmount.getText().toString()));
        }
        emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_merchantName_String, merchantName);

        emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_merchantId_String, merchantID);  // 010001020270123
        emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_terminalId_String, terminalID);   // 00000001
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportQ_boolean, ConstIPBOC.startEMV.intent.VALUE_supported);
//        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportQ_boolean, ConstIPBOC.startEMV.intent.VALUE_unsupported);
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isSupportSM_boolean, ConstIPBOC.startEMV.intent.VALUE_supported);
        emvIntent.putBoolean(ConstIPBOC.startEMV.intent.KEY_isQPBOCForceOnline_boolean, ConstIPBOC.startEMV.intent.VALUE_unforced);
        if (type == ConstIPBOC.startEMV.intent.VALUE_cardType_contactless) {
            emvIntent.putByte(ConstIPBOC.startEMV.intent.KEY_transProcessCode_byte, (byte) 0x00);
        }
        emvIntent.putBoolean("isSupportPBOCFirst", false);
        //emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_transCurrCode_String, "0156");
        emvIntent.putString(ConstIPBOC.startEMV.intent.KEY_otherAmount_String, "0");

        try {
            iemv.startEMV(ConstIPBOC.startEMV.processType.full_process, emvIntent, emvHandler);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * \brief set main key and work key
     * <p>
     * \code{.java}
     * \endcode
     *
     * @see
     */
    void doSetKeys() {
        // Load Main key
        // 758F0CD0C866348099109BAF9EADFA6E
        boolean bRet;
        try {
            bRet = ipinpad.loadMainKey(mainKeyId, Utility.hexStr2Byte(mainKey_MasterKey), null);
            toastShow("loadMainKey:" + bRet);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        String DukptSN = "01020304050607080900";
        String dukptKey = "343434343434343434343434343434343434343434343434";


        // Load work key
        // 89B07B35A1B3F47E89B07B35A1B3F488
        try {
            bRet = ipinpad.loadWorkKey(PinpadKeyType.PINKEY, mainKeyId, workKeyId, Utility.hexStr2Byte(pinKey_WorkKey), null);
            toastShow("loadWorkKey:" + bRet);

            bRet = ipinpad.loadDukptKey(1, Utility.hexStr2Byte(DukptSN), Utility.hexStr2Byte(dukptKey), null);
            toastShow("loadDukptKey:" + bRet);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    /**
     * \brief show the pinpad
     * <p>
     * \code{.java}
     * \endcode
     */
    void doPinPad() {
        Bundle param = new Bundle();
        Bundle globleparam = new Bundle();
        String panBlock = savedPan;
        byte[] pinLimit = {4, 5, 6};
        param.putByteArray(ConstIPinpad.startPinInput.param.KEY_pinLimit_ByteArray, pinLimit);
        param.putInt(ConstIPinpad.startPinInput.param.KEY_timeout_int, 20);
        param.putBoolean(ConstIPinpad.startPinInput.param.KEY_isOnline_boolean, true);
        param.putString(ConstIPinpad.startPinInput.param.KEY_pan_String, panBlock);
        param.putInt(ConstIPinpad.startPinInput.param.KEY_desType_int, ConstIPinpad.startPinInput.param.Value_desType_3DES);
        try {
            ipinpad.startPinInput(workKeyId, param, globleparam, pinInputListener);
        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }

    /**
     * \brief initialize the pin pad listener
     * <p>
     * \code{.java}
     * \endcode
     */
    void initializePinInputListener() {
        pinInputListener = new PinInputListener.Stub() {
            @Override
            public void onInput(int len, int key) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "PinPad onInput, len:" + len + ", key:" + key);
            }

            @Override
            public void onConfirm(byte[] data, boolean isNonePin) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "PinPad onConfirm");
                iemv.importPin(1, data);
                savedPinblock = data;
            }

            @Override
            public void onCancel() throws RemoteException {
                Utility.DEBUG_LOG(TAG, "PinPad onCancel");
            }

            @Override
            public void onError(int errorCode) throws RemoteException {
                Utility.DEBUG_LOG(TAG, "PinPad onError, code:" + errorCode);
            }
        };
    }

    private void doSetAID(int type) {
        toastShow("Set AID start");
        EmvSetAidRid emvSetAidRid = new EmvSetAidRid(iemv, context);
        emvSetAidRid.setAID(type);
        try {
            iBeeper.startBeep(200);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        toastShow("Set AID DONE");
    }

    private void doSetRID(int type) {
        toastShow("Set RID start");
        EmvSetAidRid emvSetAidRid = new EmvSetAidRid(iemv, context);
        emvSetAidRid.setRID(type);
        try {
            iBeeper.startBeep(200);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        toastShow("Set RID DONE");
    }


    Handler onlineResponse = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Utility.DEBUG_LOG(TAG,"before getData");
            Bundle data = msg.getData();
            String val = data.getString("value");
            Utility.DEBUG_LOG(TAG, "handle message:" + val);
        }
    };

    public ISO8583u isoResponse = null;

    Runnable onlineRequest = new Runnable() {
        @Override
        public void run() {

            ISO8583u iso8583u = new ISO8583u();
            if (tagOfF55 != null) {
                for (int i = 0; i < tagOfF55.size(); i++) {
                    int tag = tagOfF55.keyAt(i);
                    String value = tagOfF55.valueAt(i);
                    if (value.length() > 0) {
                        byte[] tmp = iso8583u.appendF55(tag, value);
                        if (tmp == null) {
                            Utility.DEBUG_LOG(TAG, "error of tag:" + Integer.toHexString(tag) + ", value:" + value);
                        } else {
                            Utility.DEBUG_LOG(TAG, "append F55 tag:" + Integer.toHexString(tag) + ", value:" + Utility.byte2HexStr(tmp));
                        }
                    }
                }
                tagOfF55 = null;

            }
            byte[] packet = iso8583u.makePacket(data8583, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);

            Comm comm = new Comm(hostIP, hostPort,context);
            if (false == comm.connect()) {
                Utility.DEBUG_LOG(TAG, "connect server error");
                return;
            }

            comm.send(packet);
            byte[] response = new byte[0];
            try {
                response = comm.receive(1024, 40);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (null == response) {
                Utility.DEBUG_LOG(TAG, "receive error");
            }
            comm.disconnect();

            if (response == null) {
                Utility.DEBUG_LOG(TAG, "Test fails");
            } else {
                Utility.DEBUG_LOG(TAG, "Test return length:" + response.length);
                Utility.DEBUG_LOG(TAG, Utility.byte2HexStr(response));
                isoResponse = new ISO8583u();
                if (isoResponse.unpack(response, 2)) {
                    String message = "";
                    String s;
                    String type = "";

                    s = isoResponse.getUnpack(0);
                    if (null != s) {
                        type = s;
                        message += "Message Type:";
                        message += s;
                        message += "\n";
                    }

                    s = isoResponse.getUnpack(39);
                    if (null != s) {
                        message += "Response(39):";
                        message += s;
                        message += "\n";
                    }
                    if (type.equals("0810")) {
                        s = isoResponse.getUnpack(62 + 200);
                        if (null != s) {
                            Utility.DEBUG_LOG(TAG, "Field62:" + s);
                            if (s.length() == 48) {
                                pinKey_WorkKey = s.substring(0, 32);
                                macKey = s.substring(32, 48);
                            } else if (s.length() == 80) {
                                pinKey_WorkKey = s.substring(0, 64);
                                macKey = s.substring(64, 80);
                            } else if (s.length() == 120) {
                                pinKey_WorkKey = s.substring(0, 64);
                                macKey = s.substring(64, 80);
                            }
                            message += "pinKey:";
                            message += pinKey_WorkKey;
                            message += "\n";
                            message += "macKey:";
                            message += macKey;
                            message += "\n";
                        }
                    } else if (type.equals("0210")) {
                        s = isoResponse.getUnpack(54);
                        if (null != s) {
                            message += "Balance(54):";
                            message += s.substring(0, 2) + "," + s.substring(2, 4) + "," + s.substring(4, 7) + "," + s.substring(7, 8);
                            message += "\n" + Integer.valueOf(s.substring(8, s.length() - 1));
                            message += "\n";
                        }

                    }

                    toastShow(message);
                }
            }


            Message msg = new Message();
            Bundle data = new Bundle();
            data.putString("value", "receive finished");
            msg.setData(data);
            onlineResponse.sendMessage(msg);
        }
    };

    /**
     * \Brief make purchase fields
     */
    public void doPurchase(String amount) {
        //

//        float fAmount = Double.parseDouble(amount);
//        int iAmount = (int) (fAmount * 100);
        amount = String.format("%012d", (int)(Double.parseDouble(amount)*100));
        SparseArray<String> data8583_u_purchase = new SparseArray<>();
        data8583_u_purchase.put(ISO8583u.F_MessageType_00, "0200");
        data8583_u_purchase.put(ISO8583u.F_ProcessingCode_03, "000000");
        data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, amount);
        data8583_u_purchase.put(ISO8583u.F_STAN_11, this.stan);
        data8583_u_purchase.put(ISO8583u.F_POSEntryMode_22, "0022");   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_u_purchase.put(ISO8583u.F_NII_24, "0002");
        data8583_u_purchase.put(ISO8583u.F_POSConditionCode_25, "00");
        data8583_u_purchase.put(ISO8583u.F_Track_2_Data_35, "00"); //refill this
        data8583_u_purchase.put( ISO8583u.F_KeyExchange_62 , "360024" );

        data8583 = data8583_u_purchase;


        doTransaction(TransType.T_PURCHASE);


    }
//    void doPurchase() {
//        //
//        String amount = edAmount.getText().toString();
//        amount = String.format("%012d", Integer.parseInt(amount));
//        SparseArray<String> data8583_u_purchase = new SparseArray<>();
//        data8583_u_purchase.put(ISO8583u.F_MessageType_00, "0200");
//        data8583_u_purchase.put(ISO8583u.F_AccountNumber_02, "");
//        data8583_u_purchase.put(3, "00 00 00");
//        data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, amount);
//        data8583_u_purchase.put(11, "01 02 03");
//        data8583_u_purchase.put(22, "02 1");   // 02 mag, 05 smart, 07 ctls; 1 pin
//        data8583_u_purchase.put(25, "00");
////        data8583_u_purchase.put( 26, "" );  // pin
//        data8583_u_purchase.put(49, "156");
////        data8583_u_purchase.put( 52 , "" ); // pin
////        data8583_u_purchase.put( 55 , "" );
////        data8583_u_purchase.put( 59 , "" );
////        data8583_u_purchase.put( 60 , "" );
//
//        data8583 = data8583_u_purchase;
//
//
//        doTransaction(TransType.T_PURCHASE);
//
//
//    }

    /**
     * \Brief make balance fields
     */
    void doBalance() {
        SparseArray<String> data8583_u_balance = new SparseArray<>();
        data8583_u_balance.put(0, "0200");
        data8583_u_balance.put(2, savedPan);
        data8583_u_balance.put(3, "310000");
        data8583_u_balance.put(11, "010203");
        data8583_u_balance.put(14, "9912");
        data8583_u_balance.put(22, "020");  // mag
//        data8583_u_balance.put(22, "021");  // mag + pin
        data8583_u_balance.put(25, "00");
//        data8583_u_balance.put(26, ""); // pin
        data8583_u_balance.put(35, ""); // track 2

        data8583_u_balance.put(49, "156");  // RMB
//        data8583_u_balance.put(55, ""); // IC
        data8583_u_balance.put(60, "01654321");

        data8583 = data8583_u_balance;

        doTransaction(TransType.T_BANLANCE);

    }

    /**
     * \Brief make sign in fields
     */
    void doSignIn() {
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        Date dt = new Date();

        SparseArray<String> data8583_u_signin = new SparseArray<>();
        data8583_u_signin.put(0, "0800");
        // data8583_u_signin.put( 1, "");
        data8583_u_signin.put(11, "012345");
        sdf = new SimpleDateFormat("HHmmss");
        data8583_u_signin.put(12, sdf.format(dt));
        sdf = new SimpleDateFormat("MMdd");
        data8583_u_signin.put(13, sdf.format(dt));
        data8583_u_signin.put(32, "12345678");
        data8583_u_signin.put(37, "ABCDEF123456");
        data8583_u_signin.put(39, "00");
        data8583_u_signin.put(60, "000008673720");
        data8583_u_signin.put(62, "10O");
        data8583_u_signin.put(63, "001");

        data8583 = data8583_u_signin;

        doTransaction(TransType.M_SIGNIN);
    }

    void doTransaction(TransType transType) {

        if (transType == TransType.M_SIGNIN) {
            // management, no card need
            // start onlineRequest
            new Thread(onlineRequest).start();
        } else {
            // set some fields
            data8583.put(ISO8583u.F_TID_41, terminalID);
            data8583.put(ISO8583u.F_MerchantID_42, merchantID);

            // do search card and online request
            doSearchCard(transType);
        }
    }

     public void doStartCustonViewPinpad() {
        Intent intent = new Intent(this, CustomPinActivity.class);
        startActivity(intent);
    }

    void initialize8583data(ISO8583u.SimulatorType st) {
        // build up the iso data

        ISO8583u.simType = st;
        data8583_i = new SparseArray<>();
        data8583_i.put(0, "02 00");
//        data8583_i.put(1, "73 A4 0E 10 20 C0 90 51 ");
        data8583_i.put(2, "62 26 20 01 02 84 80 00");
        data8583_i.put(3, "00 00 00");
        data8583_i.put(4, "000000001234");
        data8583_i.put(7, "20 18 08 23 13 ");
        data8583_i.put(8, "0");
        data8583_i.put(9, "00 00 11 00");
        data8583_i.put(11, "00 01 16");
        data8583_i.put(14, "26 12");
        data8583_i.put(21, "0010001");
        data8583_i.put(22, "00 07");
        data8583_i.put(23, " 00 00");
        data8583_i.put(28, "00");
        data8583_i.put(35, "6226200102848000=26122200119442300000");
        data8583_i.put(41, "88888888       ");
        data8583_i.put(42, "123456789012");
        data8583_i.put(49, "001");
        data8583_i.put(52, "09 65 5D 76 DC C2 81 7D");
        data8583_i.put(58, "humayun-X-EMV-DEMO-0.0.1");
        data8583_i.put(60, "60 00 00 00 00 00 00 00");
        data8583_i.put(64, "12 34 56 78 90 12 34 56");

    }

    // check assets fonts and copy to file system for Service -- start
    protected void InitializeFontFiles () {
        PrinterFonts.initialize(this.getAssets());
    }

}
