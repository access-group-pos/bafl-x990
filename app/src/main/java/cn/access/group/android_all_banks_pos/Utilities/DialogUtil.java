package cn.access.group.android_all_banks_pos.Utilities;


import android.content.Context;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.RemoteException;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;





import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;

import android.view.Gravity;
import android.view.View;

import android.widget.EditText;
import android.widget.LinearLayout;


import android.widget.TextView;


import com.vfi.smartpos.system_service.aidl.ISystemManager;

import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.viewfragments.HomeFragment;
import cn.access.group.android_all_banks_pos.viewfragments.SettlementFragment;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cn.access.group.android_all_banks_pos.R;

import static java.lang.Thread.sleep;

/**
 * on 9/25/2019.
 */
public class DialogUtil {

    static String TAG="DialogUtil";
    public static void inputDialogForSystemSettingFragment(Context context, String title, String content, SweetAlertDialog.OnSweetClickListener clickListener, int inputType, TransformationMethod transformationMethod,int Length)
    {
        showKey((Activity) context);
        final EditText editText = new EditText(context);
        int maxLength = Length;

        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(maxLength);
        editText.setFilters(fArray);
        editText.setId(R.id.my_edit_text_1);
        editText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        editText.setInputType(inputType);
        if(transformationMethod!=null) {
            editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        final TextView textView = new TextView(context);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        textView.setGravity(Gravity.CENTER);
        textView.setText(content);
        textView.setTextColor(context.getResources().getColor(R.color.seagreen));
        editText.setTextColor(context.getResources().getColor(R.color.white));
        linearLayout.addView(textView);
        linearLayout.addView(editText);
        SweetAlertDialog.DARK_STYLE =true;

        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(title)
                .setCustomView(linearLayout)
                .setConfirmText("Confirm")
                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.seagreen))
                .setCancelText("Cancel");
        dialog.setConfirmClickListener(clickListener);
        dialog.setCancelClickListener(sweetAlertDialog -> {
            //hideKeyboard((Activity) context);
            dialog.dismissWithAnimation();
            //hideKeyboard((Activity) context);
        });
        dialog.setCancelable(false);
        dialog.show();
        //hideKeyboard((Activity) context);
    }
    public static void inputDialogAnyReport(Context context, String title, String content, SweetAlertDialog.OnSweetClickListener clickListener, int inputType, TransformationMethod transformationMethod)
    {

        final EditText editText = new EditText(context);




        editText.setId(R.id.my_edit_text_1);
        editText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        editText.setInputType(inputType);
        if(transformationMethod!=null) {
            editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        final TextView textView = new TextView(context);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        textView.setGravity(Gravity.CENTER);
        textView.setText(content);
        textView.setTextColor(context.getResources().getColor(R.color.seagreen));
        editText.setTextColor(context.getResources().getColor(R.color.white));
        linearLayout.addView(textView);
        linearLayout.addView(editText);
        SweetAlertDialog.DARK_STYLE =true;

        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(title)
                .setCustomView(linearLayout)
                .setConfirmText("Confirm")

                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.seagreen))
                .setCancelText("Cancel");
        dialog.setConfirmClickListener(clickListener);





        dialog.setCancelClickListener(sweetAlertDialog -> {
            dialog.dismissWithAnimation();
            DashboardContainer.backStack();
        });
        dialog.setCancelable(false);
        dialog.show();

    }
    public static void inputDialog(Context context, String title, String content, SweetAlertDialog.OnSweetClickListener clickListener, int inputType, TransformationMethod transformationMethod)
    {

        final EditText editText = new EditText(context);




        editText.setId(R.id.my_edit_text_1);
        editText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        editText.setInputType(inputType);
        if(transformationMethod!=null) {
            editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        final TextView textView = new TextView(context);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        textView.setGravity(Gravity.CENTER);
        textView.setText(content);
        textView.setTextColor(context.getResources().getColor(R.color.seagreen));
        editText.setTextColor(context.getResources().getColor(R.color.white));
        linearLayout.addView(textView);
        linearLayout.addView(editText);
        SweetAlertDialog.DARK_STYLE =true;

        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(title)
                .setCustomView(linearLayout)
                .setConfirmText("Confirm")
                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.seagreen))
                .setCancelText("Cancel");
        dialog.setConfirmClickListener(clickListener);



        dialog.setCancelClickListener(sweetAlertDialog -> {
            dialog.dismissWithAnimation();

        });
        dialog.setCancelable(false);
        dialog.show();

    }
    public static void settingListDialog(Context context, String title, String content, SweetAlertDialog.OnSweetClickListener clickListener, int inputType, TransformationMethod transformationMethod)
    {
        final EditText editText = new EditText(context);
        editText.setId(R.id.my_edit_text_1);
        editText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        editText.setInputType(inputType);
        if(transformationMethod!=null) {
            editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        final TextView textView = new TextView(context);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        textView.setGravity(Gravity.CENTER);
        textView.setText(content);
        textView.setTextColor(context.getResources().getColor(R.color.seagreen));
        editText.setTextColor(context.getResources().getColor(R.color.white));
        linearLayout.addView(textView);
        linearLayout.addView(editText);
        SweetAlertDialog.DARK_STYLE =true;

        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(title)
                .setCustomView(linearLayout)
                .setConfirmText("Confirm")
                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.seagreen))
                .setCancelText("Cancel");
        dialog.setConfirmClickListener(clickListener);
        dialog.setCancelClickListener(sweetAlertDialog -> {
            dialog.dismissWithAnimation();
        });
        dialog.setCancelable(false);
        dialog.show();

    }

    public static void infoDialog(Context context,String content){
        SweetAlertDialog sd = new SweetAlertDialog(context);
        sd.setCancelable(false);
        sd.setCanceledOnTouchOutside(false);
        sd.setContentText(content);
        sd.show();
    }
    public static void successDialogPrinting(Context context,String content){
        Handler mHandler = new Handler();

        SweetAlertDialog.DARK_STYLE =true;
        SweetAlertDialog dialog= new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("Success!")
                .setContentText(content)
                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.transparent))
                .setConfirmText("");


//                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.seagreen))
        //         .setContentText(content);
        //   dialog.setCancelable(false);
        dialog.show();
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                dialog.dismiss();
//            }
//        },1000L);
    }
    public static void successDialogprint(Context context,String content){
        Handler mHandler = new Handler();

        SweetAlertDialog.DARK_STYLE =true;
        SweetAlertDialog dialog= new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("Success!")
                .setContentText(content)
                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.transparent))
                .setConfirmText("");

//                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.seagreen))
        //         .setContentText(content);
        //   dialog.setCancelable(false);
        dialog.show();
        mHandler.postDelayed(() -> {
            dialog.dismiss();
            DashboardContainer.backStack();
        },1000L);
    }



    public static void successDialog(Context context,String content){
        Handler mHandler = new Handler();

        SweetAlertDialog.DARK_STYLE =true;
        SweetAlertDialog dialog= new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("Success!")
                .setContentText(content)
                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.transparent))
                .setConfirmText("");

//                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.seagreen))
        //         .setContentText(content);
        //   dialog.setCancelable(false);
        dialog.show();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        },1000L);
    }

    public static void successDialogComplaint(Context context,String title,String content){
        Handler mHandler = new Handler();

        SweetAlertDialog.DARK_STYLE =true;
        SweetAlertDialog dialog= new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(title)
                .setContentText(content)
                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.transparent))
                .setConfirmText("");

//                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.seagreen))
        //         .setContentText(content);
        //   dialog.setCancelable(false);
        dialog.show();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        },2000L);
    }



    public static void errorDialog(Context context,String title,String error){
        SweetAlertDialog.DARK_STYLE =true;
        SweetAlertDialog dialog= new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(error)
                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.red_600));
        dialog.setCancelable(false);

        dialog.show();
        MainApplication.hideProgressDialog();
    }

    public static void confirmReprintDialog(Context context,String confirmText,SweetAlertDialog.OnSweetClickListener sweetClickListener){
        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Paper out!")
                .setContentText("Please fill the roll")
                .setCancelText("No, cancel pls!")
                .setConfirmText(confirmText)
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        // reuse previous dialog instance, keep widget user state, reset them if you need
                        sDialog.dismissWithAnimation();
                        DashboardContainer.backStack();
                    }
                })
                .setConfirmClickListener(sweetClickListener);
        dialog.setCancelable(false);
        dialog.show();
    }


    public static void confirmDialog(Context context,String confirmText,SweetAlertDialog.OnSweetClickListener sweetClickListener){
        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("Won't be able to undo this!")
                .setCancelText("No, cancel pls!")
                .setConfirmText(confirmText)
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        // reuse previous dialog instance, keep widget user state, reset them if you need
                        sDialog.dismissWithAnimation();
                        DashboardContainer.backStack();
                    }
                })
                .setConfirmClickListener(sweetClickListener);
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void restartDialogForMaintenance_NoButton(Context context, ISystemManager systemManager, String confirmText, SweetAlertDialog.OnSweetClickListener sweetClickListener){
        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setContentText("Maintenance cycle; restart required")
                .setConfirmText(confirmText)
                .showCancelButton(false)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        Utility.DEBUG_LOG(TAG,"Trying to do nothing...");
//                        // reuse previous dialog instance, keep widget user state, reset them if you need
//                        sDialog.dismissWithAnimation();
//                        DashboardContainer.switchFragment(new HomeFragment(), "home");
                    }
                })
                .setConfirmClickListener(sweetClickListener);
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void confirmDialogSingleButton(Context context,String confirmText,SweetAlertDialog.OnSweetClickListener sweetClickListener){
        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
//                .setTitleText("Are you sure?")
                .setContentText("Won't be able to undo this!")
//                .setCancelText("No, cancel pls!")
                .setConfirmText(confirmText)
//                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        // reuse previous dialog instance, keep widget user state, reset them if you need
                        sDialog.dismissWithAnimation();
                        DashboardContainer.switchFragment(new HomeFragment(), "home");
                    }
                })
                .setConfirmClickListener(sweetClickListener);
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void confirmDialogTip(Context context,String Amount , String Tip, String Total,SweetAlertDialog.OnSweetClickListener sweetClickListener){
        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Total Amount")
                .setContentText("Amount:" + Amount + System.lineSeparator() +"Tip:"+Tip + System.lineSeparator() + "Total Amount:" +Total)
//                .setContentText("Tip: "+Tip)
//                .setContentText("Total Amount:" + Total)
                .setCancelText("No, cancel pls!")
                .setConfirmText("Confirm")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        // reuse previous dialog instance, keep widget user state, reset them if you need
                        sDialog.dismissWithAnimation();
                        DashboardContainer.backStack();
                    }
                })
                .setConfirmClickListener(sweetClickListener);
        dialog.setCancelable(false);
        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    public static void radioButton(String type ,Context context, String title, String content, SweetAlertDialog.OnSweetClickListener clickListener, int inputType, TransformationMethod transformationMethod, int Length)
    {
        final RadioGroup radioGrp = new RadioGroup(context);
        //radioGrp.setOutlineSpotShadowColor(context.getResources().getColor(R.color.white));
        //  radioGrp.setOutlineAmbientShadowColor(context.getResources().getColor(R.color.white));
        //get string array from source

        String[] websitesArray = context.getResources().getStringArray(R.array.network_array);

        //create radio buttons
        for (int i = 0; i < websitesArray.length; i++) {
            RadioButton radioButton = new RadioButton(context);
            radioButton.setText(websitesArray[i]);
            radioButton.setId(i);
            radioGrp.addView(radioButton);
            radioButton.setTextColor(context.getResources().getColor(R.color.white));
            radioButton.setChecked(false);
        }

        //  radioGrp.setOutlineSpotShadowColor(context.getResources().getColor(R.color.white));


        //set listener to radio button group
        radioGrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // Utility.DEBUG_LOG("radio button", String.valueOf(group)) ;
                Utility.DEBUG_LOG("radio button", String.valueOf(checkedId)) ;
                switch (type){
                    case "pm" :
                        if(checkedId == 0){
                            SharedPref.write("PRI_MEDIUM", "GPRS");
                            //  Utility.DEBUG_LOG("if = 0" , "if = 0") ;
                        }
                        else if(checkedId == 1){
                            SharedPref.write("PRI_MEDIUM", "WIFI");
                        }
                        else{
                            SharedPref.write("PRI_MEDIUM", "notSelected");
                        }
                        // break ;
                    case "sm" :
                        if(checkedId == 0){
                            SharedPref.write("SEC_MEDIUM", "GPRS");
                        }
                        else if(checkedId == 1){
                            SharedPref.write("SEC_MEDIUM", "WIFI");
                        }
                        else{
                            SharedPref.write("SEC_MEDIUM", "notSelected");
                        }
                        //  break;


                }


                int checkedRadioButtonId = radioGrp.getCheckedRadioButtonId();
                //  RadioButton radioBtn = context(checkedRadioButtonId);
//                RadioButton radioBtn = new  RadioButton(context);
//                Utility.DEBUG_LOG("radio button", String.valueOf(radioBtn.getText())) ;
                // Toast.makeText(context, (CharSequence) group, Toast.LENGTH_SHORT).show();
                //radioBtn.setTextColor(context.getResources().getColor(R.color.white));
            }
        });
//        final RadioButton[] rb = new RadioButton[2];
//        RadioGroup rg = new RadioGroup(context); //create the RadioGroup
//        rg.setBackgroundColor(context.getResources().getColor(R.color.white));
//        rg.setOrientation(RadioGroup.VERTICAL);//or RadioGroup.VERTICAL
//        for(int i=0; i<2; i++){
//            rb[i]  = new RadioButton(context);
//            rb[i].setText(" " + "Wifi"
//                    + "    " + "GPRS");
//            rb[i].setId(i + 100);
//            rg.addView(rb[i]);
//        }
        //you add the whole RadioGroup to the layout

//        final EditText editText = new EditText(context);
//        int maxLength = Length;
//        InputFilter[] fArray = new InputFilter[1];
//        fArray[0] = new InputFilter.LengthFilter(maxLength);
//        editText.setFilters(fArray);
//        editText.setId(R.id.my_edit_text_1);
//        editText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//        editText.setInputType(inputType);
//        if(transformationMethod!=null) {
//            editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
//        }
//        final TextView textView = new TextView(context);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
//        textView.setGravity(Gravity.CENTER);
//        textView.setText(content);
//        textView.setTextColor(context.getResources().getColor(R.color.seagreen));
//        editText.setTextColor(context.getResources().getColor(R.color.white));
//        linearLayout.addView(textView);
//        linearLayout.addView(editText);
        linearLayout.addView(radioGrp);
        SweetAlertDialog.DARK_STYLE =true;

        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(title)
                .setCustomView(linearLayout)
                .setConfirmText("Confirm")
                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.seagreen))
                .setCancelText("Cancel");
        dialog.setConfirmClickListener(clickListener);
        dialog.setCancelClickListener(sweetAlertDialog -> {
            dialog.dismissWithAnimation();

        });
        dialog.setCancelable(false);
        dialog.show();

    }

    public static void radioButtonForMarketSegAndMaintenanceThreshold(String type , Context context, String title, String content, SweetAlertDialog.OnSweetClickListener clickListener, int inputType, TransformationMethod transformationMethod, int Length)
    {
        Utility.DEBUG_LOG(TAG,"+ radioButtonForMarketSeg +");
        final RadioGroup radioGrp = new RadioGroup(context);
        //radioGrp.setOutlineSpotShadowColor(context.getResources().getColor(R.color.white));
        //  radioGrp.setOutlineAmbientShadowColor(context.getResources().getColor(R.color.white));
        //get string array from source
        Utility.DEBUG_LOG(TAG,"type:"+type);
        Utility.DEBUG_LOG(TAG,"title:"+title);
        Utility.DEBUG_LOG(TAG,"content:"+content);

        String[] websitesArray = context.getResources().getStringArray(R.array.market_Seg);

        if ( type.equals("marketseg"))
        {
            Utility.DEBUG_LOG(TAG,"Loading market segment array");
            websitesArray = context.getResources().getStringArray(R.array.market_Seg);
        }
        if ( type.equals("maintenance_threshold"))
        {
            Utility.DEBUG_LOG(TAG,"Loading maintenance threshold array");
            websitesArray = context.getResources().getStringArray(R.array.maintenance_threshold);
        }

        //create radio buttons
        for (int i = 0; i < websitesArray.length; i++) {
            RadioButton radioButton = new RadioButton(context);
            radioButton.setText(websitesArray[i]);
            radioButton.setId(i);
            radioGrp.addView(radioButton);
            radioButton.setTextColor(context.getResources().getColor(R.color.white));
            radioButton.setChecked(false);
        }

        //  radioGrp.setOutlineSpotShadowColor(context.getResources().getColor(R.color.white));


        //set listener to radio button group
        radioGrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // Utility.DEBUG_LOG("radio button", String.valueOf(group)) ;
                int maintenanceThreshold=0;
                Utility.DEBUG_LOG(TAG,"checkedId:"+checkedId);
                Utility.DEBUG_LOG(TAG,"radio button selected:"+ String.valueOf(checkedId)) ;
                switch (type){
                    case "maintenance_threshold":
                        maintenanceThreshold = checkedId == 0 ? 0 : (checkedId*25+100);
                        Utility.DEBUG_LOG(TAG,"new maintenanceThreshold:"+maintenanceThreshold);
                        SharedPref.write("maintenanceThreshold", maintenanceThreshold);
                        break;
                    case "marketseg" :
                        if(checkedId == 0){
                            SharedPref.write("MARKET_SEG", "G");
                        }
                        else if(checkedId == 1){
                            SharedPref.write("MARKET_SEG", "I");
                        }
                        else if(checkedId == 2){
                            SharedPref.write("MARKET_SEG", "R");
                        }
                        else if(checkedId == 3){
                            SharedPref.write("MARKET_SEG", "O");
                        }
                        else if(checkedId == 4){
                            SharedPref.write("MARKET_SEG", "C");
                        }
                        else if(checkedId == 5){
                            SharedPref.write("MARKET_SEG", "T");

                        }
                        else if(checkedId == 6){
                            SharedPref.write("MARKET_SEG", "W");
                        }
                        else if(checkedId == 7){
                            SharedPref.write("MARKET_SEG", "F");
                        }
                        else if(checkedId == 8){
                            SharedPref.write("MARKET_SEG", "J");
                        }
                        else if(checkedId == 9){
                            SharedPref.write("MARKET_SEG", "A");
                        }
                        break;


                }


                int checkedRadioButtonId = radioGrp.getCheckedRadioButtonId();
                //  RadioButton radioBtn = context(checkedRadioButtonId);
//                RadioButton radioBtn = new  RadioButton(context);
//                Utility.DEBUG_LOG("radio button", String.valueOf(radioBtn.getText())) ;
                // Toast.makeText(context, (CharSequence) group, Toast.LENGTH_SHORT).show();
                //radioBtn.setTextColor(context.getResources().getColor(R.color.white));
            }
        });

        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
//        textView.setGravity(Gravity.CENTER);
//        textView.setText(content);
//        textView.setTextColor(context.getResources().getColor(R.color.seagreen));
//        editText.setTextColor(context.getResources().getColor(R.color.white));
//        linearLayout.addView(textView);
//        linearLayout.addView(editText);
        linearLayout.addView(radioGrp);
        SweetAlertDialog.DARK_STYLE =true;

        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(title)
                .setCustomView(linearLayout)
                .setConfirmText("Confirm")
                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.seagreen))
                .setCancelText("Cancel");
        dialog.setConfirmClickListener(clickListener);
        dialog.setCancelClickListener(sweetAlertDialog -> {
            dialog.dismissWithAnimation();

        });
        dialog.setCancelable(false);
        dialog.show();

    }
    public static void radioButtonForCurrency(String type ,Context context, String title, String content, SweetAlertDialog.OnSweetClickListener clickListener, int inputType, TransformationMethod transformationMethod, int Length)
    {
        final RadioGroup radioGrp = new RadioGroup(context);
        //radioGrp.setOutlineSpotShadowColor(context.getResources().getColor(R.color.white));
        //  radioGrp.setOutlineAmbientShadowColor(context.getResources().getColor(R.color.white));
        //get string array from source

        String[] websitesArray = context.getResources().getStringArray(R.array.currency);

        //create radio buttons
        for (int i = 0; i < websitesArray.length; i++) {
            RadioButton radioButton = new RadioButton(context);
            radioButton.setText(websitesArray[i]);
            radioButton.setId(i);
            radioGrp.addView(radioButton);
            radioButton.setTextColor(context.getResources().getColor(R.color.white));
            radioButton.setChecked(false);
        }

        //  radioGrp.setOutlineSpotShadowColor(context.getResources().getColor(R.color.white));


        //set listener to radio button group
        radioGrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // Utility.DEBUG_LOG("radio button", String.valueOf(group)) ;
                Utility.DEBUG_LOG("radio button", String.valueOf(checkedId)) ;
                switch (type){
                    case "currency" :
                        if(checkedId == 0){
                            SharedPref.write("CURRENCY", "PKR");
                            //  Utility.DEBUG_LOG("if = 0" , "if = 0") ;
                        }
                        else if(checkedId == 1){
                            SharedPref.write("CURRENCY", "USD");
                        }


                }


                int checkedRadioButtonId = radioGrp.getCheckedRadioButtonId();
                //  RadioButton radioBtn = context(checkedRadioButtonId);
//                RadioButton radioBtn = new  RadioButton(context);
//                Utility.DEBUG_LOG("radio button", String.valueOf(radioBtn.getText())) ;
                // Toast.makeText(context, (CharSequence) group, Toast.LENGTH_SHORT).show();
                //radioBtn.setTextColor(context.getResources().getColor(R.color.white));
            }
        });

        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
//        textView.setGravity(Gravity.CENTER);
//        textView.setText(content);
//        textView.setTextColor(context.getResources().getColor(R.color.seagreen));
//        editText.setTextColor(context.getResources().getColor(R.color.white));
//        linearLayout.addView(textView);
//        linearLayout.addView(editText);
        linearLayout.addView(radioGrp);
        SweetAlertDialog.DARK_STYLE =true;

        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText(title)
                .setCustomView(linearLayout)
                .setConfirmText("Confirm")
                .setConfirmButtonBackgroundColor(context.getResources().getColor(R.color.seagreen))
                .setCancelText("Cancel");
        dialog.setConfirmClickListener(clickListener);
        dialog.setCancelClickListener(sweetAlertDialog -> {
            dialog.dismissWithAnimation();

        });
        dialog.setCancelable(false);
        dialog.show();

    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        Utility.DEBUG_LOG("SHIVAM","HERE");
    }

    public static void showKey(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.SHOW_FORCED);
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        Utility.DEBUG_LOG("SHIVAM","ShowKey");
    }

    public static void confirmDialogCustomer(Context context,String confirmText, String title, String contentText,SweetAlertDialog.OnSweetClickListener sweetClickListener){
        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(contentText)
                .setCancelText("No, cancel pls!")
                .setConfirmText("Print")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        // reuse previous dialog instance, keep widget user state, reset them if you need
                        sDialog.cancel();
                        // reuse previous dialog instance, keep widget user state, reset them if you need
                        DashboardContainer.backStack();
                    }


                })
                .setConfirmClickListener(sweetClickListener);
        dialog.setCancelable(false);
        dialog.show();
    }
    public static void confirmPrintCustomerDialog(Context context,String confirmText,SweetAlertDialog.OnSweetClickListener sweetClickListener){
        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Print Customer Copy?")
                .setContentText("Won't be able to undo this!")
                .setCancelText("No, cancel pls!")
                .setConfirmText(confirmText)
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        // reuse previous dialog instance, keep widget user state, reset them if you need
                        sDialog.setTitleText("Cancelled!")
                                .setContentText("Your transaction is safe")
                                .setConfirmText("OK")
                                .showCancelButton(false)
                                .setCancelClickListener(null)
                                .setConfirmClickListener(null)
                                .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    }
                })
                .setConfirmClickListener(sweetClickListener);
        dialog.setCancelable(false);
        dialog.show();
    }
    public static void confirmPrintBankDialog(Context context,String confirmText,SweetAlertDialog.OnSweetClickListener sweetClickListener){
        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Print Bank Copy?")
                .setContentText("Won't be able to undo this!")
                .setCancelText("No, cancel pls!")
                .setConfirmText(confirmText)
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        // reuse previous dialog instance, keep widget user state, reset them if you need
                        sDialog.setTitleText("Cancelled!")
                                .setContentText("Your transaction is safe")
                                .setConfirmText("OK")
                                .showCancelButton(false)
                                .setCancelClickListener(null)
                                .setConfirmClickListener(null)
                                .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    }
                })
                .setConfirmClickListener(sweetClickListener);
        dialog.setCancelable(false);
        dialog.show();
    }
}
