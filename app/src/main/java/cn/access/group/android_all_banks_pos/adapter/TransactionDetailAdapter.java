package cn.access.group.android_all_banks_pos.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

/**
 * on 8/30/2019.
 */
public class TransactionDetailAdapter extends RecyclerView.Adapter<TransactionDetailAdapter.TransactionDetailViewHolder> {
    private List<TransactionDetail> transactionDetailList;


    public TransactionDetailAdapter( List<TransactionDetail> transactionDetailList) {
        this.transactionDetailList = transactionDetailList;
        Utility.DEBUG_LOG("SHIVAM", String.valueOf(transactionDetailList));
    }

    @NonNull
    @Override
    public TransactionDetailViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.transaction_item, viewGroup, false);
        return new TransactionDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionDetailViewHolder transactionDetailViewHolder, int i) {
        transactionDetailViewHolder.txnAmount.setText(transactionDetailList.get(i).getAmount());
        transactionDetailViewHolder.txnDate.setText((DataFormatterUtil.formattedDate(transactionDetailList.get(i).getTxnDate())));
        transactionDetailViewHolder.txnStan.setText(transactionDetailList.get(i).getStan());
        transactionDetailViewHolder.txnCardType.setText(transactionDetailList.get(i).getCardScheme());
        transactionDetailViewHolder.txnType.setText(transactionDetailList.get(i).getTxnType());
        transactionDetailViewHolder.txnTime.setText(DataFormatterUtil.formattedTime(transactionDetailList.get(i).getTxnTime()));
        transactionDetailViewHolder.txnInvoiceNo.setText(transactionDetailList.get(i).getInvoiceNo());
        transactionDetailViewHolder.txnCardType.setText(transactionDetailList.get(i).getCardScheme());
        transactionDetailViewHolder.txnCardNo.setText(transactionDetailList.get(i).getCardNo());

    }


    @Override
    public int getItemCount() {
        int a ;
        if(transactionDetailList != null && !transactionDetailList.isEmpty()) {
            a = transactionDetailList.size();
        }
        else {
            a = 0;
        }
        return a;
    }


    static class TransactionDetailViewHolder  extends RecyclerView.ViewHolder{
        @BindView(R.id.txn_date)
        TextView txnDate;
        @BindView(R.id.txn_stan)
        TextView txnStan;
        @BindView(R.id.txn_type)
        TextView txnType;
        @BindView(R.id.txn_cardno)
        TextView txnCardNo;
        @BindView(R.id.textView7)
        TextView textView7;
        @BindView(R.id.txn_time)
        TextView txnTime;
        @BindView(R.id.txn_invoiceno)
        TextView txnInvoiceNo;
        @BindView(R.id.txn_cardtype)
        TextView txnCardType;
        @BindView(R.id.txn_amount)
        TextView txnAmount;

        TransactionDetailViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
