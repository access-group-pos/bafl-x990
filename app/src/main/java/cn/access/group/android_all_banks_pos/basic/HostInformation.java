package cn.access.group.android_all_banks_pos.basic;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.usecase.MultiHostsConfig;


/**
 * on 2019/11/14.
 */

public class HostInformation<T extends ISO8583> {
    private static final String TAG = "EMVDemo-HostInformation";

    public String description;
    public int index;

    public MultiHostsConfig.Category category;

    // merchant
    public String terminalID;
    public String merchantID;
    public String merchantName;

    // host
    public String hostAddr;
    public int hostPort;
    public  String nii;

    // cards
    public List<String> cardBinList;
    public List<String> AIDList;

    // keys
    public int masterKeyIndex ;
    public int pinKeyIndex ;
    public int dataKeyIndex;    // TD KEY
    public int macKeyIndex;
    public int dukptIndex;

    // date time format/pattern
    public SimpleDateFormat dateFormat;
    public SimpleDateFormat timeFormat;

    // 8583
    public Class<T> iso8583Class;
    public T iso8583;

    // print setting
    public String prn_card_mask = null;
    public int[] prn_card_mask_range;   // [a,b], a -> start offset, b -> end offset ,  negative value mean from end to start. such as [6 , -5] means mask from [6] to [len-5]
    protected static String DATA_FORMAT = "DDdd";
    protected static String TIME_FORMAT = "HHmmss";
    public HostInformation(T t, MultiHostsConfig.Category category, String description){
        iso8583Class = (Class<T>) t.getClass();
        iso8583 = t;
        this.category = category;
        this.description = description;
        this.AIDList = null;
        this.cardBinList = null;

        this.dateFormat = new SimpleDateFormat(DATA_FORMAT);
        this.timeFormat = new SimpleDateFormat(TIME_FORMAT);

    }

    public void setMerchant(String terminalID, String merchantID, String name ){
        this.terminalID = terminalID;
        this.merchantID = merchantID;
        this.merchantName = name;
    }
    public void setHost(String addr, int port,String nii ){
        this.hostAddr = addr;
        this.hostPort = port;
        this.nii=nii;
    }
    public void setKeysIndex( int masterKeyIndex, int pinKeyIndex, int dataKeyIndex, int macKeyIndex, int dukptIndex ){
        this.masterKeyIndex = masterKeyIndex;
        this.pinKeyIndex = pinKeyIndex;
        this.dataKeyIndex = dataKeyIndex;
        this.macKeyIndex = macKeyIndex;
        this.dukptIndex = dukptIndex;
    }

    public void setDateTimePattern(String datePattern, String timePattern ){
        this.dateFormat = new SimpleDateFormat(datePattern);
        this.timeFormat = new SimpleDateFormat(timePattern);
    }
    public void AIDAppend( String aid ){
        if( this.AIDList == null  ){
            this.AIDList = new ArrayList<String>();
        }
        this.AIDList.add( aid );
    }
    public void CardBINAppend( String cardBIN ){
        if( this.cardBinList == null  ){
            this.cardBinList = new ArrayList<String>();
        }
        this.cardBinList.add( cardBIN );
    }

    boolean checkValid(String cardBin, String aid){
        if( null == cardBinList && null == AIDList ){
            Utility.DEBUG_LOG(TAG, "there is no cardBind & AID be set, it's valid for all cards." + this.description);
            return true;
        }
        if( null != aid && null != AIDList ){
            for( String a : AIDList ){
                int len = a.length();
                if( aid.length() >= len ){
                    if( aid.substring(0, len).compareTo(a) == 0 ) {
                        Utility.DEBUG_LOG(TAG, "AID hit to " + this.description);
                        return true;
                    }
                }
            }
        }
        if( null != cardBin && null != cardBinList ){
            for( String card : cardBinList ){
                int len = card.length();
                if( len <= 0 ){
                    continue;
                }
                if( cardBin.length() >= len ){
                    if( cardBin.substring(0, len).compareTo(card) == 0 ) {
                        Utility.DEBUG_LOG(TAG, "cardbin=" + cardBin +
                                " hit to " + this.description + ", value" + card );
                        return true;
                    }
                }
            }

        }
        return false;
    }

    public T new8583(){
        try {
//            iso8583 = iso8583Class.newInstance();
            T t = (T) iso8583Class.newInstance();
            return t;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        // return iso8583;
        return null;
    }
}
