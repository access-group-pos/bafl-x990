package cn.access.group.android_all_banks_pos.presenter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.SparseArray;

import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;

import cn.access.group.android_all_banks_pos.PrinterFonts;
import cn.access.group.android_all_banks_pos.Utilities.Comm;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.ErrorMap;
import cn.access.group.android_all_banks_pos.Utilities.PostRequest;
import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.basePackets.transactionPacketGenerator.VoidPacketGenerator;
import cn.access.group.android_all_banks_pos.basic.ISO8583;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.contracts.VoidContract;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.Merchant;
import cn.access.group.android_all_banks_pos.repository.model.Reversal;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.usecase.GenerateReversal;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_IP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_PORT;

/**
 * @author muhammad.humayun
 * on 8/21/2019.
 */
public class VoidTransactionPresenter {
    private static final String TAG = "EMVDemo";

    private IBeeper iBeeper;
    private IPrinter printer;

    // some client static
    private String headerType="";
   /* private String pinKey_WorkKey = "89B07B35A1B3F47E89B07B35A1B3F488";
    private String macKey = "";
    String mainKey_MasterKey = "758F0CD0C866348099109BAF9EADFA6E";*/
    /**
     * field-value map to save iso data
     */
    private SparseArray<String> data8583 = null;
    private SparseArray<String> tagOfF55 = null;
    String cardType = "UNKNOWN";
    private AssetManager assetManager;
    private TransactionContract transactionContract;
    private ReceiptPrinter receiptPrinter;
    private Merchant merchant = new Merchant();
    private ErrorMap errorMap;
    private TransactionDetail transactionToVoid;
    private VoidContract voidContract;
    private GenerateReversal generateReversal;
    private VoidPacketGenerator voidPacketGenerator;
    PostRequest postRequest;
    private WeakReference<Context> context;
    String Bal = "";
    String redeemAmount = "";
    String redeemBal = "";
    String redeemKey = "";
//    TransPrinter transPrinter; // SHIVAM UPDATE HERE FOR TEXT PRINTER

    String txnType = "VOID"; // SHIVAM UPDATE HERE FOR TEXT PRINTER






    public VoidTransactionPresenter(TransactionContract transactionContract, VoidContract voidContract, Handler handler, boolean isSucc, AssetManager assetManager,
                                    IPrinter printer, TransactionDetail transactionDetail, Context context){
        this.transactionContract = transactionContract;
        this.printer = printer;
        this.assetManager = assetManager;

        receiptPrinter = new ReceiptPrinter(handler);
        transactionToVoid = transactionDetail;
        this.context = new WeakReference<>(context);
        errorMap = new ErrorMap();
        this.voidContract = voidContract;
        this.postRequest = new PostRequest();
        loadConfiguration();
        ISO8583u.SimulatorType st;
        switch (headerType)
        {
            case "SimulatorHost":
                st=ISO8583u.SimulatorType.SimulatorHost;
                break;
            case "ActualBank":
                st=ISO8583u.SimulatorType.ActualBank;
                break;
            default:
                st=ISO8583u.SimulatorType.AuthAll;
                break;
        }
        initialize8583data(st);

        // check assets fonts and copy to file system for Service
        InitializeFontFiles();
        if (!isSucc) {
            Utility.DEBUG_LOG("TAG", "deviceService connect fail!");
        } else {
            Utility.DEBUG_LOG("TAG", "deviceService connect success");

        }

    }

    private boolean IsReversalNeeded(TransactionDetail txn)
    {
        boolean rv=false;
        Utility.DEBUG_LOG(TAG,"+ IsReversalNeeded() +");

        if ( txn.getMessageType().equals("0200") )
            rv = true;
        Utility.DEBUG_LOG(TAG,"rv:"+rv);
        return rv;
    }

    protected  void loadConfiguration()
    {
        try {

            JSONObject obj = new JSONObject(loadJSONFromAsset());
           /* this.terminalID = obj.getString("TID");
            this.merchantID = obj.getString("MID");*/
           /* this.hostIP = obj.getString("Host IP");
            this.hostPort = obj.getInt("Host Port");*/
            this.headerType = obj.getString("Header Type");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = assetManager.open("config.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    void initialize8583data(ISO8583u.SimulatorType st) {
        // build up the iso data
        ISO8583u.simType = st;
    }

    // check assets fonts and copy to file system for Service -- start
    protected void InitializeFontFiles () {
        PrinterFonts.initialize(assetManager);
    }

    Handler onlineResponse = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Utility.DEBUG_LOG(TAG,"before getData");
            Bundle data = msg.getData();
            String val = data.getString("value");
            Utility.DEBUG_LOG(TAG, "handle message:" + val);
        }
    };

    public ISO8583u isoResponse = null;

    private Thread isoReq = new Thread(new Runnable() {
        @Override
        public void run() {
            ISO8583u iso8583u;
            if(Constants.HOST_INDEX_FROM_DB!=0) {
                iso8583u = new ISO8583u(16);
            }
            else{
                iso8583u = new ISO8583u();
            }
            generateReversal = new GenerateReversal(AppDatabase.getAppDatabase(context.get()));
            byte[] packet;
            Utility.DEBUG_LOG(TAG,"before generateReversal.isReversalPresentInDB");
            if(generateReversal.isReversalPresentInDB()){
                Utility.DEBUG_LOG(TAG,"reversal found");
                SparseArray<String> reversal = generateReversal.generateReversalPacket();
                if( reversal.get(ISO8583u.F_55)!=null){
                    SparseArray<String> tagOfF55Reversal = generateReversal.createTagOf55();
                    if (tagOfF55Reversal != null) {
                        for (int i = 0; i < tagOfF55Reversal.size(); i++) {
                            int tag = tagOfF55Reversal.keyAt(i);
                            String value = tagOfF55Reversal.valueAt(i);
                            if (value.length() > 0) {
                                byte[] tmp = iso8583u.appendF55(tag, value);
                                if (tmp == null) {
                                    Utility.DEBUG_LOG(TAG, "error of tag:" + Integer.toHexString(tag) + ", value:" + value);
                                } else {
                                    Utility.DEBUG_LOG(TAG, "append F55 tag:" + Integer.toHexString(tag) + ", value:" + Utility.byte2HexStr(tmp));
                                }
                            }
                        }
                    }
                }
                packet = iso8583u.makePacket(reversal, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);
            }
            else {
                Utility.DEBUG_LOG(TAG,"reversal not found");
                if (tagOfF55 != null) {
                    for (int i = 0; i < tagOfF55.size(); i++) {
                        int tag = tagOfF55.keyAt(i);
                        String value = tagOfF55.valueAt(i);
                        if (value.length() > 0) {
                            byte[] tmp = iso8583u.appendF55(tag, value);
                            if (tmp == null) {
                                Utility.DEBUG_LOG(TAG, "error of tag:" + Integer.toHexString(tag) + ", value:" + value);
                            } else {
                                Utility.DEBUG_LOG(TAG, "append F55 tag:" + Integer.toHexString(tag) + ", value:" + Utility.byte2HexStr(tmp));
                            }
                        }
                    }
                    tagOfF55 = null;

                }

                // data8583.put(ISO8583u.F_DateOfExpired_14, transactionDetail.getCardexpiry());
                data8583.put(ISO8583u.F_AccountNumber_02, transactionToVoid.getCardNo());
                Utility.DEBUG_LOG("card number", transactionToVoid.getCardNo());

//                transactionToVoid.setIsVoided("Voided");
//                Utility.DEBUG_LOG("voided", transactionToVoid.getIsVoided());
                data8583.put(ISO8583u.F_DateOfExpired_14, transactionToVoid.getCardexpiry());
                Utility.DEBUG_LOG("expriy", String.valueOf(transactionToVoid.getCardexpiry()));
                Utility.DEBUG_LOG("data", String.valueOf(data8583));
//                data8583.put(ISO8583u.F_AuthorizationIdentificationResponseCode_38, "654321");
//                Utility.DEBUG_LOG("response code", transactionToVoid.getAuthorizationIdentificationResponseCode());
                packet = iso8583u.makePacket(data8583, ISO8583.PACKET_TYPE.PACKET_TYPE_HEXLEN_BUF);
//                Utility.DEBUG_LOG("data", String.valueOf(data8583));

            }
            if ( transactionToVoid.getTxnType().equals("REDEEM"))
            {
                transactionToVoid.setProcessingCode(Constants.VOID_PROCESSING_CODE_REDEEM);
                txnType="VOID_REDEEM"; // SHIVAM UPDATE HERE FOR TEXT PRINTER
            }

            if ( !transactionToVoid.getTxnType().equals("COMPLETION") && !transactionToVoid.isCompletion() ) {
                Comm comm = new Comm(Constants.hostIP, Constants.hostPort, context.get());
                Comm comm1 = new Comm(SEC_IP, SEC_PORT, context.get());
                transactionContract.showProgress("processing");
                if (!comm.connect()) {
                    if (comm1.connect()) {
                        transactionContract.hideProgress();
                        int packetLength = packet.length;
                        String packetToPrint = DataFormatterUtil.insertCharacterForEveryNDistance(2, Utility.byte2HexStr(packet), ' ');
                        Utility.DEBUG_LOG(TAG, packetToPrint);
                        // receiptPrinter.printPacket(printer,"Request  ",packetLength,packetToPrint);
                        comm1.send(packet);
                    } else {
                        Utility.DEBUG_LOG(TAG, "Unable to establish connection with server.");
                        transactionContract.hideProgress();
                        transactionContract.transactionFailed("Error", "Unable to establish connection with server");
//                    transactionDetail.setStatus("reversal");
                        return;
                    }
                }

                //+ taha 19-01-2021 create a reversal before send/receive
                if (generateReversal.isReversalPresentInDB())//do not re-enter reversal in case reversal was being processed
                {
                    Utility.DEBUG_LOG(TAG, "do not create reversal of reversal");
                } else if (IsReversalNeeded(transactionToVoid)) {
//                Utility.DEBUG_LOG(TAG,"before deleteReversal - safe side");
//                generateReversal.deleteReversal();
                    Reversal r = new Reversal();
                    r.copy(transactionToVoid);
                    Utility.DEBUG_LOG(TAG, "before insertReversal3");
                    generateReversal.insertReversal(r);
                }

                int packetLength = packet.length;
                String packetToPrint = DataFormatterUtil.insertCharacterForEveryNDistance(2, Utility.byte2HexStr(packet), ' ');
                Utility.DEBUG_LOG(TAG, packetToPrint);
                //  receiptPrinter.printPacket(printer,"Request  ",packetLength,packetToPrint);

                comm.send(packet);
                byte[] response = new byte[0];
                try {
                    if (!comm.connect()) {
                        response = comm1.receive(1024, 40);
                    } else {
                        response = comm.receive(1024, 40);
                    }
                } catch (IOException e) {
                    transactionContract.hideProgress();
                    transactionContract.transactionFailed("Error", e.getMessage());
//                if(generateReversal.isReversalPresentInDB()) return;
//                else updateTransaction(appDatabase,"reversal","No",transactionToVoid.getProcessingCode(),transactionToVoid.getTdid());
                }
                if (null == response) {

                    Utility.DEBUG_LOG(TAG, "receive error");
                }
                comm.disconnect();
                comm1.disconnect();
                transactionContract.hideProgress();

                if (response == null || response.length == 0) {
                    Utility.DEBUG_LOG(TAG, "response is null");
                    Utility.DEBUG_LOG(TAG, "Test fails");
                    Utility.DEBUG_LOG(TAG, "receive error");
                    isoResponse = null;
//                if(generateReversal.isReversalPresentInDB()) return;
//                else updateTransaction(appDatabase,"reversal","No",transactionToVoid.getProcessingCode(),transactionToVoid.getTdid());
                } else {
                    Utility.DEBUG_LOG(TAG, "Test return length:" + response.length);
                    Utility.DEBUG_LOG(TAG, Utility.byte2HexStr(response));
                    packetLength = response.length;
                    packetToPrint = DataFormatterUtil.insertCharacterForEveryNDistance(2, Utility.byte2HexStr(response), ' ');
                    Utility.DEBUG_LOG(TAG, packetToPrint);
                    //  receiptPrinter.printPacket(printer,"Response  ",packetLength,packetToPrint);
                    isoResponse = new ISO8583u();
                    if (isoResponse.unpack(response, 2)) {
                        String message = "";
                        String s;
                        String type = "";

                        s = isoResponse.getUnpack(0);
                        if (null != s) {
                            type = s;
                            message += "Message Type:";
                            message += s;
                            message += "\n";
                        }

                        s = isoResponse.getUnpack(39);
                        if (null != s) {
                            message += "Response(39):";
                            message += s;
                            message += "\n";
                        }
                    /*if (type.equals("0810")) {
                        s = isoResponse.getUnpack(62 + 200);
                        if (null != s) {
                            Utility.DEBUG_LOG(TAG, "Field62:" + s);
                            if (s.length() == 48) {
                                pinKey_WorkKey = s.substring(0, 32);
                                macKey = s.substring(32, 48);
                            } else if (s.length() == 80) {
                                pinKey_WorkKey = s.substring(0, 64);
                                macKey = s.substring(64, 80);
                            } else if (s.length() == 120) {
                                pinKey_WorkKey = s.substring(0, 64);
                                macKey = s.substring(64, 80);
                            }
                            message += "pinKey:";
                            message += pinKey_WorkKey;
                            message += "\n";
                            message += "macKey:";
                            message += macKey;
                            message += "\n";
                        }
                    }*/
                        if (type.equals("0210")) {
                            s = isoResponse.getUnpack(54);
                            if (null != s) {
                                message += "Balance(54):";
                                message += s.substring(0, 2) + "," + s.substring(2, 4) + "," + s.substring(4, 7) + "," + s.substring(7, 8);
                                message += "\n" + Integer.valueOf(s.substring(8, s.length() - 1));
                                message += "\n";
                            }
                        }

                        transactionContract.hideProgress();

                        transactionContract.toastShow(message);
                    }
                    Utility.DEBUG_LOG(TAG, "before deleteReversal after getting response");
                    generateReversal.deleteReversal();
                }


                Message msg = new Message();
                Bundle data = new Bundle();
                data.putString("value", "receive finished");
                msg.setData(data);
                onlineResponse.sendMessage(msg);

                if (isoResponse == null) {
                    //transactionContract.transactionFailed("Response is Null","Transaction failed");
                } else {
                    // String stan2 = isoResponse.getUnpack(ISO8583u.F_STAN_11);
                    String responseCode = isoResponse.getUnpack(ISO8583u.F_ResponseCode_39);
                    String messageType = isoResponse.getUnpack(ISO8583u.F_MessageType_00);
                    String processingCode = isoResponse.getUnpack(ISO8583u.F_ProcessingCode_03);
                    if (responseCode.equals("00"))//success
                    {
                        if (messageType.equals("0410")) {
//                        generateReversal.deleteReversal();
                            // generateReversal.printReversal(receiptPrinter,printer,assetManager,merchant,cardName,cardExpiry);
                            Utility.DEBUG_LOG(TAG, "before isoReq.run()");
                            isoReq.run();
                            return;
                            // receiptPrinter.printSaleReceipt(printer, assetManager, merchant, "card name", "Void", "30/10", transactionToVoid, "0980980980909");

                        } else /*if(messageType.equals("0210"))*/ {
                            String cancelTxnType = transactionToVoid.getTxnType();
                            Utility.DEBUG_LOG(TAG, "transactionToVoid.getTxnType:" + transactionToVoid.getTxnType());
                            Utility.DEBUG_LOG(TAG, "transaction AID" + transactionToVoid.getAidCode());
                            Utility.DEBUG_LOG(TAG, "transactionToVoid.getCancelTxnType:" + transactionToVoid.getCancelTxnType());
                            transactionToVoid.setCancelTxnType(transactionToVoid.getTxnType());
                            transactionToVoid.setTxnType("VOID");
                            transactionToVoid.setIsVoid(true);
                            updateTransaction("VOID", cancelTxnType, processingCode, transactionToVoid.getTdid());
                            transactionContract.transactionSuccess("void success");
                            try {
                                Thread.sleep(500);
                            } catch (Exception e) {
                                Utility.DEBUG_LOG(TAG, "Exception: " + e.getMessage().toString());
                            }

                            transactionContract.hideProgress();
                            Utility.DEBUG_LOG(TAG, "before transPrinter.print() 6");
// SHIVAM UPDATE HERE FOR TEXT PRINTER
//                        transPrinter = new PrintRecpSale(context);
//                        transPrinter.initializeData(transactionToVoid,Bal,false, "MERCHANT COPY");
//                        transPrinter.print();


                            receiptPrinter.printSaleReceipt(printer, assetManager, transactionToVoid, transactionToVoid.getAidCode(), "MERCHANT COPY", txnType, "", Bal, false);
                            postRequest.makeJsonObjReq(context.get(), transactionToVoid);
                            transactionContract.transactionSuccessCustomer("CUSTOMER COPY", transactionToVoid, "CUSTOMER COPY", txnType, Bal, false);
                        }
// SHIVAM UPDATE HERE FOR TEXT PRINTER

                    } else {
                        transactionContract.transactionFailed(responseCode, errorMap.errorCodes.get(responseCode));
                    }

                }
                isoResponse = null;
            }
            else{
                String cancelTxnType = transactionToVoid.getTxnType();
                Log.d(TAG, "else transactionToVoid.getTxnType:" + transactionToVoid.getTxnType());
                Log.d(TAG, " else transaction AID" + transactionToVoid.getAidCode());
                Log.d(TAG, "else transactionToVoid.getCancelTxnType:" + transactionToVoid.getCancelTxnType());
                transactionToVoid.setCancelTxnType(transactionToVoid.getTxnType());
                transactionToVoid.setTxnType("VOID");
                transactionToVoid.setIsVoid(true);

                updateTransaction("VOID", cancelTxnType, "020000", transactionToVoid.getTdid());
                transactionContract.transactionSuccess("void success");
                receiptPrinter.printSaleReceipt(printer, assetManager, transactionToVoid, transactionToVoid.getAidCode(), "MERCHANT COPY", txnType, "", Bal, false);

                try {
                    Thread.sleep(500);
                } catch (Exception e) {
                    Log.d(TAG, "Exception: " + e.getMessage().toString());
                }

                transactionContract.hideProgress();
                Log.d(TAG, "else before transPrinter.print() 6");



                //receiptPrinter.printSaleReceipt(printer, assetManager,transactionToVoid, "","Merchant Copy","VOID","",Bal,false);
                postRequest.makeJsonObjReq(context.get(), transactionToVoid);
                transactionContract.transactionSuccessCustomer("Customer Copy", transactionToVoid, "Customer Copy", "VOID", Bal, false);

            }
        }
    });



    /**
     * \Brief make purchase fields
     */
    public void doVoid() {
        //  invoiceNoToVoid
        ///01,2,3 7 11 12 13 18 19 22 24 25 32 33 37 41 42 43 49 60 62 90
        SparseArray<String> data8583_u_void = new SparseArray<>();
        Utility.DEBUG_LOG(TAG,"+ doVoid() +");

//
//        data8583_u_void= voidPacketGenerator.getVoidPacket(transactionToVoid);
//        data8583_u_void.put(ISO8583u.F_MessageType_00, Constants.SALE_MESSAGE_TYPE);//
//        data8583_u_void.put(ISO8583u.F_AccountNumber_02,transactionToVoid.getCardNo());
//        data8583_u_void.put(ISO8583u.F_AmountOfTransactions_04,String.format("%012d", (int)(Double.parseDouble(transactionToVoid.getAmount())*100)));
//        data8583_u_void.put(ISO8583u.F_ProcessingCode_03, Constants.VOID_PROCESSING_CODE);
//        data8583_u_void.put(ISO8583u.F_TransmissionDateAndTime_07, DataFormatterUtil.systemDateAndTime());
//        data8583_u_void.put(ISO8583u.F_STAN_11, transactionToVoid.getStan());
//        data8583_u_void.put(ISO8583u.F_TxTime_12, DataFormatterUtil.systemTime());
//        data8583_u_void.put(ISO8583u.F_TxDate_13, DataFormatterUtil.systemDate());
//        if(transactionToVoid.getCardType().equals("magnetic")){
//            data8583_u_void.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE);
//            data8583_u_void.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);
//        }
//        else if(transactionToVoid.getCardType().equals("chip")){
//            data8583_u_void.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE_CHIP);
//            data8583_u_void.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE_CHIP);
//            data8583_u_void.put(ISO8583u.F_ApplicationPANSequenceNumber_23,"001");
//            data8583_u_void.put(ISO8583u.F_AuthorizationIdentificationResponseCode_38,transactionToVoid.getAuthorizationIdentificationResponseCode());
//
//        }
//        else if(transactionToVoid.getCardType().equals("ctls")){
//            data8583_u_void.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE_CTLS);
//            data8583_u_void.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE_CTLS);   // 02 mag, 05 smart, 07 ctls; 1 pin
//            data8583_u_void.put(ISO8583u.F_ApplicationPANSequenceNumber_23,"001");
//
//            //data8583_u_void.put(ISO8583u.F_PointOfServiceCaptureCode_26, Constants.POS_CONDITION_CODE_CTLS);
//            //data8583_u_void.put(ISO8583u.F_AuthorizationIdentificationResponseCode_38,transactionToVoid.getAuthorizationIdentificationResponseCode());
//
//        }
//
//
//        data8583_u_void.put(ISO8583u.F_AcquiringInstitution_19, Constants.ACQUIRING_INSTITUTION);
//        data8583_u_void.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
//        data8583_u_void.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
//        data8583_u_void.put(ISO8583u.F_AcquiringInstitutionIdentificationCode_32, Constants.ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
//        data8583_u_void.put(ISO8583u.F_ForwardingInstitutionIdentificationCode_33, Constants.FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
//        data8583_u_void.put(ISO8583u.F_Track_2_Data_35,transactionToVoid.getTrack2()); ///?????
//        data8583_u_void.put(ISO8583u.F_RRN_37,transactionToVoid.getRrn());
//        data8583_u_void.put(ISO8583u.F_TID_41, Constants.TERMINAL_ID);
//        data8583_u_void.put(ISO8583u.F_MerchantID_42, Constants.MERCHANT_ID);
//        data8583_u_void.put(ISO8583u.F_CardAcceptorNameLocation_43, Constants.CARD_ACCEPTOR_NAME_LOCATION);
//        data8583_u_void.put( ISO8583u.F_TransactionCurrencyCode_49 , Constants.TRANSACTION_CURRENCY_CODE );
//        data8583_u_void.put( ISO8583u.F_ReservedNational_60 , Constants.RESERVED_NATIONAL );
//        data8583_u_void.put( ISO8583u.F_KeyExchange_62 , Constants.BATCH_NO);
//
//        Utility.DEBUG_LOG(TAG,transactionToVoid.toString());
//        String Field90 = DataFormatterUtil.generateField90(transactionToVoid);
//        data8583_u_void.put( ISO8583u.F_OriginalDataElements_90 , Field90 );
        //0200 MESSAGE TYPE 00
        // 000016  STAN  11
        // 1005185331  DATE AND TIME 07
        // 00035370586  32
        // 00047740586  33 ??

//        String STAN=String.format(Locale.getDefault(),"%06d", SharedPref.read("stan"));
//        int stan =  SharedPref.read("stan");
//        stan=stan+1;
//        SharedPref.write("stan",stan);
        voidPacketGenerator = new VoidPacketGenerator();
        Utility.DEBUG_LOG(TAG,"before voidPacketGenerator.getVoidPacket");
        data8583_u_void = voidPacketGenerator.getVoidPacket(transactionToVoid);
        data8583 = data8583_u_void;


        Utility.DEBUG_LOG(TAG,"before doTransaction");
        doTransaction();


    }

    @SuppressLint("DefaultLocale")
    private void doTransaction() {
        Utility.DEBUG_LOG(TAG,"before isoReq.start()");
        isoReq.start();
    }


    @SuppressLint("CheckResult")
    private void updateTransaction(String txntype, String cancelTxnType,String proc,int tdid) {
        Completable.fromAction(() -> AppDatabase.getAppDatabase(context.get()).transactionDetailDao().updateStatus(txntype,cancelTxnType, proc,tdid))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{ }// completed with success
                        ,throwable ->{}// there was an error
                );
    }
}
