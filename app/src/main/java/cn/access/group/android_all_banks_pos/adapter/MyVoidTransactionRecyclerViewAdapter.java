package cn.access.group.android_all_banks_pos.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import butterknife.ButterKnife;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.R;

/**
 * {@link RecyclerView.Adapter} that can display a {@link TransactionDetail} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 *
 */
public class MyVoidTransactionRecyclerViewAdapter extends RecyclerView.Adapter<MyVoidTransactionRecyclerViewAdapter.ViewHolder> implements Filterable {

    private final List<TransactionDetail> mValues;
    private  OnListFragmentInteractionListener mListener=null;
    private Context mContext;
    List<TransactionDetail> filterTransaction;


    public MyVoidTransactionRecyclerViewAdapter(List<TransactionDetail> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        filterTransaction = new ArrayList<>(items);
        mListener = listener;
    }

    public MyVoidTransactionRecyclerViewAdapter(List<TransactionDetail> items, Context context) {
        mValues = items;
        mContext=context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_voidtransaction, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mItem = mValues.get(position);

            if (mValues.get(position).getTxnDate() != null)
                holder.voidTrxDate.setText(DataFormatterUtil.formattedDate(mValues.get(position).getTxnDate()));
            holder.voidInvoiceNo.setText(mValues.get(position).getInvoiceNo());
            if (mValues.get(position).getCardNo() != null)
                holder.voidCardNo.setText(DataFormatterUtil.maskCardNo(mValues.get(position).getCardNo()));
            holder.voidTxnType.setText(mValues.get(position).getTxnType());
            holder.voidStatus.setText(mValues.get(position).getStatus());
            holder.isVoided.setText(mValues.get(position).getCancelTxnType());
            //+ taha 21-01-2021 default red
            holder.voidImage.setImageTintList(ColorStateList.valueOf(Color.RED));
            //holder.isVoided.setText(mValues.get(position).getCancelTxnType());
            if (mListener == null) {
//            if(mValues.get(position).getTxnType().equals("VOID")){
//                holder.voidImage.setImageDrawable(mContext.getDrawable(R.drawable.ic_tick));
////                holder.isVoided.setText(mValues.get(position).getIsVoided());
//                holder.isVoided.setText(mValues.get(position).getCancelTxnType());
//                holder.voidImage.setClickable(false);
//
//
//            }

            } else {
                if (mValues.get(position).getTxnType().equals("VOID") || mValues.get(position).getStatus().equals("reversal"))
                {
                    holder.voidImage.setImageTintList(ColorStateList.valueOf(Color.GRAY));
                    holder.isVoided.setText(mValues.get(position).getCancelTxnType());
                    holder.voidImage.setClickable(false);
                }
                else
                    {
                    holder.voidImage.setOnClickListener(v -> {
                        if (null != mListener) {
                            mListener.onListFragmentInteraction(holder.mItem);
                        }
                    });
                }
            }

    }

    @Override
    public int getItemCount() {
        int a ;
        if(mValues != null && !mValues.isEmpty()) {
            a = mValues.size();
        }
        else {
            a = 0;
        }
        Utility.DEBUG_LOG("count func", String.valueOf(a));
        return a;
    }

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }
    private Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
           List<TransactionDetail> filteredList = new ArrayList<>();
           if(charSequence == null || charSequence.length() == 0){
               filteredList.addAll(filterTransaction);
           }
           else{
               String filterPattern = charSequence.toString().toLowerCase().trim();
               for(TransactionDetail item : filterTransaction){
                   if(item.getInvoiceNo().toLowerCase().contains(filterPattern)){
                       filteredList.add(item);
                   }
               }
           }
           FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
           return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            mValues.clear();
            mValues.addAll((List)filterResults.values);
            notifyDataSetChanged();
        }
    };

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        TransactionDetail mItem;
        @BindView(R.id.void_invoiceNo)
        TextView voidInvoiceNo;
        @BindView(R.id.void_cardNo)
        TextView voidCardNo;
        @BindView(R.id.void_trxDate)
        TextView voidTrxDate;
        @BindView(R.id.void_txnType)
        TextView voidTxnType;
        @BindView(R.id.void_status)
        TextView voidStatus;
        @BindView(R.id.void_isVoided)
        TextView isVoided;
        @BindView(R.id.void_image)
        ImageView voidImage;
        ViewHolder(View view) {
            super(view);
            mView= view;
            ButterKnife.bind(this, view);

        }
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(TransactionDetail item);
    }
}
