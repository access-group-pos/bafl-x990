package cn.access.group.android_all_banks_pos.repository.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.access.group.android_all_banks_pos.Utilities.Utility;

/**
 * on 10/29/2019.
 */
@Entity(tableName = "TerminalConfig")
public class TerminalConfig implements Serializable {
    @PrimaryKey()
    private int tcid;
    @ColumnInfo(name = "merchantID")
    private String merchantID;
    @ColumnInfo(name = "terminalID")
    private String terminalID;
    @ColumnInfo(name = "merchantName")
    private String merchantName;
    @ColumnInfo(name = "hostIp")
    private String hostIp;
    @ColumnInfo(name = "hostPort")
    private String hostPort;
    @ColumnInfo(name = "niiNum")
    private String niiNum;
    @ColumnInfo(name = "tipEnabled")
    private String tipEnabled;
    @ColumnInfo(name = "comEthernet")
    private String comEthernet;
    @ColumnInfo(name = "comGPRS")
    private String comGPRS;
    @ColumnInfo(name = "enableLogs")
    private String enableLogs;
    @ColumnInfo(name = "marketSeg")
    private String marketSeg;
    @ColumnInfo(name = "tipThershold")
    private String tipThershold;
    @ColumnInfo(name = "fallbackEnable")
    private String fallbackEnable;
    @ColumnInfo(name = "msgHeaderLenHex")
    private String msgHeaderLenHex;
    @ColumnInfo(name = "sslEnable")
    private String sslEnable;
    @ColumnInfo(name = "topUpEnable")
    private String topUpEnable;
    @ColumnInfo(name = "manualEntry")
    private String manualEntry;
    @ColumnInfo(name = "autoSettlement")
    private String autoSettlement;
    @ColumnInfo(name = "emvLogs")
    private String emvLogs;
    @ColumnInfo(name = "autoSettleTime")
    private String autoSettleTime;
    @ColumnInfo(name = "amexID")
    private String amexID;
    @ColumnInfo(name = "pinByPass")
    private String pinByPass;
    @ColumnInfo(name = "mod10")
    private String mod10;
    @ColumnInfo(name = "checkExpiry")
    private String checkExpiry;
    @ColumnInfo(name = "manualEntryIssuer")
    private String manualEntryIssuer;
    @ColumnInfo(name = "last4digitMag")
    private String last4digitMag;
    @ColumnInfo(name = "last4digitIcc")
    private String last4digitIcc;
    @ColumnInfo(name = "acquirer")
    private String acquirer;
    @ColumnInfo(name = "lowBIN")
    private String lowBIN;
    @ColumnInfo(name = "highBIN")
    private String highBIN;
    @ColumnInfo(name = "issuer")
    private String issuer;
    @ColumnInfo(name = "currency")
    private String currency;
    @ColumnInfo(name = "userPassword")
    private String userPassword;
    @ColumnInfo(name = "managerPassword")
    private String managerPassword;
    @ColumnInfo(name = "systemuserPassword")
    private String systemuserPassword;
    @ColumnInfo(name = "superuserPassword")
    private String superuserPassword;
    @ColumnInfo(name = "headerLine1")
    private String headerLine1;
    @ColumnInfo(name = "headerLine2")
    private String headerLine2;
    @ColumnInfo(name = "headerLine3")
    private String headerLine3;
    @ColumnInfo(name = "headerLine4")
    private String headerLine4;
    @ColumnInfo(name = "footerLine1")
    private String footerLine1;
    @ColumnInfo(name = "footerLine2")
    private String footerLine2;
    @ColumnInfo(name = "priMedium")
    private String priMedium;
    @ColumnInfo(name = "secMedium")
    private String secMdeium;
    @ColumnInfo(name = "batchNo")
    private String batchNo;
    @ColumnInfo(name = "tmsIp")
    private String tmsIp;
    @ColumnInfo(name = "tmsPort")
    private String tmsPort;
    @ColumnInfo(name = "secIp")
    private String secIp;
    @ColumnInfo(name = "secPort")
    private String secPort;

    @ColumnInfo(name = "cvmLimitVisa")
    private String cvmLimitVisa;
    @ColumnInfo(name = "cvmLimitMc")
    private String cvmLimitMc;
    @ColumnInfo(name = "cvmLimitUpi")
    private String cvmLimitUpi;
    @ColumnInfo(name = "cvmLimitPaypak")
    private String cvmLimitPaypak;
    @ColumnInfo(name = "cvmLimitJcb")
    private String cvmLimitJcb;
    @ColumnInfo(name = "cvmLimitAmex")
    private String cvmLimitAmex;

    @ColumnInfo(name = "saleOfflineLimit")
    private String saleOfflineLimit;
    @ColumnInfo(name = "saleMinAmtLimit")
    private String saleMinAmtLimit;
    @ColumnInfo(name = "saleMaxAmtLimit")
    private String saleMaxAmtLimit;

    @ColumnInfo(name = "vepsLimit")
    private String vepsLimit;
    @ColumnInfo(name = "vepsLimitMc")
    private String vepsLimitMc;
    @ColumnInfo(name = "vepsLimitUpi")
    private String vepsLimitUpi;
    @ColumnInfo(name = "vepsLimitPaypak")
    private String vepsLimitPaypak;
    @ColumnInfo(name = "vepsLimitJcb")
    private String vepsLimitJcb;
    @ColumnInfo(name = "vepsLimitAmex")
    private String vepsLimitAmex;


    @ColumnInfo(name = "PortalUrl")
    private String PortalUrl;
    @ColumnInfo(name = "PortalUserName")
    private String PortalUserName;
    @ColumnInfo(name = "PortalPassword")
    private String PortalPassword;
    @ColumnInfo(name = "PortalTimeout")
    private String PortalTimeout;
    @ColumnInfo(name = "PortalTxnUpload")
    private String PortalTxnUpload;
    @ColumnInfo(name = "settleCounter")
    private int settleCounter;
    @ColumnInfo(name = "settleRange")
    private int settleRange;
    @ColumnInfo(name = "tipNumAdjust")
    private int tipNumAdjust;
    @ColumnInfo(name = "ecrEnabled")
    private String ecrEnabled;
    @ColumnInfo(name = "ecrType")
    private String ecrType;





    public TerminalConfig() {
    }

//    public String getAMEX_ISSNUM() {
//            return AMEX_ISSNUM;
//        }
//
//        public void setAMEX_ISSNUM(String AMEX_ISSNUM) {
//            this.AMEX_ISSNUM = AMEX_ISSNUM;
//        }
//
//        public String getUPI_ADJUSTENABLE() {
//            return UPI_ADJUSTENABLE;
//        }
//
//        public void setUPI_ADJUSTENABLE(String UPI_ADJUSTENABLE) {
//            this.UPI_ADJUSTENABLE = UPI_ADJUSTENABLE;
//        }
//
//        public String getMASTER_ADJUSTENABLE() {
//            return MASTER_ADJUSTENABLE;
//        }
//
//        public void setMASTER_ADJUSTENABLE(String MASTER_ADJUSTENABLE) {
//            this.MASTER_ADJUSTENABLE = MASTER_ADJUSTENABLE;
//        }
//
//        public String getAMEX_MANUALENTRY() {
//            return AMEX_MANUALENTRY;
//        }
//
//        public void setAMEX_MANUALENTRY(String AMEX_MANUALENTRY) {
//            this.AMEX_MANUALENTRY = AMEX_MANUALENTRY;
//        }
//
//        public String getISRECEIPTPRINTED() {
//            return ISRECEIPTPRINTED;
//        }
//
//        public void setISRECEIPTPRINTED(String ISRECEIPTPRINTED) {
//            this.ISRECEIPTPRINTED = ISRECEIPTPRINTED;
//        }
//
//        public String getTUPPRIMARYIP() {
//            return TUPPRIMARYIP;
//        }
//
//        public void setTUPPRIMARYIP(String TUPPRIMARYIP) {
//            this.TUPPRIMARYIP = TUPPRIMARYIP;
//        }
//
//        public String getCONNPROFILESCREATED() {
//            return CONNPROFILESCREATED;
//        }
//
//        public void setCONNPROFILESCREATED(String CONNPROFILESCREATED) {
//            this.CONNPROFILESCREATED = CONNPROFILESCREATED;
//        }
//
//        public String getISEXTERNALPPCONN() {
//            return ISEXTERNALPPCONN;
//        }
//
//        public void setISEXTERNALPPCONN(String ISEXTERNALPPCONN) {
//            this.ISEXTERNALPPCONN = ISEXTERNALPPCONN;
//        }
//
//        public String getVISA_LAST4DMAG() {
//            return VISA_LAST4DMAG;
//        }
//
//        public void setVISA_LAST4DMAG(String VISA_LAST4DMAG) {
//            this.VISA_LAST4DMAG = VISA_LAST4DMAG;
//        }
//
//        public String getPAYPAK_FORCEDLBPENABLE() {
//            return PAYPAK_FORCEDLBPENABLE;
//        }
//
//        public void setPAYPAK_FORCEDLBPENABLE(String PAYPAK_FORCEDLBPENABLE) {
//            this.PAYPAK_FORCEDLBPENABLE = PAYPAK_FORCEDLBPENABLE;
//        }
//
//        public String getPSTNNUMSEC() {
//            return PSTNNUMSEC;
//        }
//
//        public void setPSTNNUMSEC(String PSTNNUMSEC) {
//            this.PSTNNUMSEC = PSTNNUMSEC;
//        }
//
//        public String getVISA_BANKFUNDED() {
//            return VISA_BANKFUNDED;
//        }
//
//        public void setVISA_BANKFUNDED(String VISA_BANKFUNDED) {
//            this.VISA_BANKFUNDED = VISA_BANKFUNDED;
//        }
//
//        public String getVISA_PRINTEXPIRY() {
//            return VISA_PRINTEXPIRY;
//        }
//
//        public void setVISA_PRINTEXPIRY(String VISA_PRINTEXPIRY) {
//            this.VISA_PRINTEXPIRY = VISA_PRINTEXPIRY;
//        }
//
//        public String getMASTER_CHSERVCODE() {
//            return MASTER_CHSERVCODE;
//        }
//
//        public void setMASTER_CHSERVCODE(String MASTER_CHSERVCODE) {
//            this.MASTER_CHSERVCODE = MASTER_CHSERVCODE;
//        }
//
//        public String getPAYPAK_CHECKPIN() {
//            return PAYPAK_CHECKPIN;
//        }
//
//        public void setPAYPAK_CHECKPIN(String PAYPAK_CHECKPIN) {
//            this.PAYPAK_CHECKPIN = PAYPAK_CHECKPIN;
//        }
//
//        public String getHELPDESKNUMBER() {
//            return HELPDESKNUMBER;
//        }
//
//        public void setHELPDESKNUMBER(String HELPDESKNUMBER) {
//            this.HELPDESKNUMBER = HELPDESKNUMBER;
//        }
//
//        public String getACQNAME() {
//            return ACQNAME;
//        }
//
//        public void setACQNAME(String ACQNAME) {
//            this.ACQNAME = ACQNAME;
//        }
//
//        public String getAUDITPRINTDETAILS() {
//            return AUDITPRINTDETAILS;
//        }
//
//        public void setAUDITPRINTDETAILS(String AUDITPRINTDETAILS) {
//            this.AUDITPRINTDETAILS = AUDITPRINTDETAILS;
//        }
//
//        public String getTUPSECONDARYIP() {
//            return TUPSECONDARYIP;
//        }
//
//        public void setTUPSECONDARYIP(String TUPSECONDARYIP) {
//            this.TUPSECONDARYIP = TUPSECONDARYIP;
//        }
//
//        public String getPASSWORDMANAGEMENT() {
//            return PASSWORDMANAGEMENT;
//        }
//
//        public void setPASSWORDMANAGEMENT(String PASSWORDMANAGEMENT) {
//            this.PASSWORDMANAGEMENT = PASSWORDMANAGEMENT;
//        }
//
//        public String getVISA_MOD10() {
//            return VISA_MOD10;
//        }
//
//        public void setVISA_MOD10(String VISA_MOD10) {
//            this.VISA_MOD10 = VISA_MOD10;
//        }
//
//        public String getSETGROUP() {
//            return SETGROUP;
//        }
//
//        public void setSETGROUP(String SETGROUP) {
//            this.SETGROUP = SETGROUP;
//        }
//
//        public String getMASTER_CHSIGN() {
//            return MASTER_CHSIGN;
//        }
//
//        public void setMASTER_CHSIGN(String MASTER_CHSIGN) {
//            this.MASTER_CHSIGN = MASTER_CHSIGN;
//        }
//
//        public String getPAYPAK_ISSNAME() {
//            return PAYPAK_ISSNAME;
//        }
//
//        public void setPAYPAK_ISSNAME(String PAYPAK_ISSNAME) {
//            this.PAYPAK_ISSNAME = PAYPAK_ISSNAME;
//        }
//
//        public String getMSGHEADERLENHEX() {
//            return MSGHEADERLENHEX;
//        }
//
//        public void setMSGHEADERLENHEX(String MSGHEADERLENHEX) {
//            this.MSGHEADERLENHEX = MSGHEADERLENHEX;
//        }
//
//        public String getUPI_SCHEME() {
//            return UPI_SCHEME;
//        }
//
//        public void setUPI_SCHEME(String UPI_SCHEME) {
//            this.UPI_SCHEME = UPI_SCHEME;
//        }
//
//        public String getSECONDARYPORT() {
//            return SECONDARYPORT;
//        }
//
//        public void setSECONDARYPORT(String SECONDARYPORT) {
//            this.SECONDARYPORT = SECONDARYPORT;
//        }
//
//        public String getVISA_PINBYPASS() {
//            return VISA_PINBYPASS;
//        }
//
//        public void setVISA_PINBYPASS(String VISA_PINBYPASS) {
//            this.VISA_PINBYPASS = VISA_PINBYPASS;
//        }
//
//        public String getDESTADDTPDU() {
//            return DESTADDTPDU;
//        }
//
//        public void setDESTADDTPDU(String DESTADDTPDU) {
//            this.DESTADDTPDU = DESTADDTPDU;
//        }
//
//        public String getFLASH() {
//            return FLASH;
//        }
//
//        public void setFLASH(String FLASH) {
//            this.FLASH = FLASH;
//        }
//
//        public String getJCB_FORCEDLBPENABLE() {
//            return JCB_FORCEDLBPENABLE;
//        }
//
//        public void setJCB_FORCEDLBPENABLE(String JCB_FORCEDLBPENABLE) {
//            this.JCB_FORCEDLBPENABLE = JCB_FORCEDLBPENABLE;
//        }
//
//        public String getAMEX_CHSERVCODE() {
//            return AMEX_CHSERVCODE;
//        }
//
//        public void setAMEX_CHSERVCODE(String AMEX_CHSERVCODE) {
//            this.AMEX_CHSERVCODE = AMEX_CHSERVCODE;
//        }
//
//        public String getPAYPAK_CHSIGN() {
//            return PAYPAK_CHSIGN;
//        }
//
//        public void setPAYPAK_CHSIGN(String PAYPAK_CHSIGN) {
//            this.PAYPAK_CHSIGN = PAYPAK_CHSIGN;
//        }
//
//        public String getCOUNTRYCODE() {
//            return COUNTRYCODE;
//        }
//
//        public void setCOUNTRYCODE(String COUNTRYCODE) {
//            this.COUNTRYCODE = COUNTRYCODE;
//        }
//
//        public String getPSTNTIMEOUT() {
//            return PSTNTIMEOUT;
//        }
//
//        public void setPSTNTIMEOUT(String PSTNTIMEOUT) {
//            this.PSTNTIMEOUT = PSTNTIMEOUT;
//        }
//
//        public String getSAVEDEODPRINTDETAILS() {
//            return SAVEDEODPRINTDETAILS;
//        }
//
//        public void setSAVEDEODPRINTDETAILS(String SAVEDEODPRINTDETAILS) {
//            this.SAVEDEODPRINTDETAILS = SAVEDEODPRINTDETAILS;
//        }
//
//        public String getPRTPOWERFAILURE() {
//            return PRTPOWERFAILURE;
//        }
//
//        public void setPRTPOWERFAILURE(String PRTPOWERFAILURE) {
//            this.PRTPOWERFAILURE = PRTPOWERFAILURE;
//        }
//
//        public String getPASSWORDRETRYON() {
//            return PASSWORDRETRYON;
//        }
//
//        public void setPASSWORDRETRYON(String PASSWORDRETRYON) {
//            this.PASSWORDRETRYON = PASSWORDRETRYON;
//        }
//
//        public String getJCB_LAST4DMAG() {
//            return JCB_LAST4DMAG;
//        }
//
//        public void setJCB_LAST4DMAG(String JCB_LAST4DMAG) {
//            this.JCB_LAST4DMAG = JCB_LAST4DMAG;
//        }
//
//        public String getCLOCK() {
//            return CLOCK;
//        }
//
//        public void setCLOCK(String CLOCK) {
//            this.CLOCK = CLOCK;
//        }
//
//        public String getTERMINALIP() {
//            return TERMINALIP;
//        }
//
//        public void setTERMINALIP(String TERMINALIP) {
//            this.TERMINALIP = TERMINALIP;
//        }
//
//        public String getPSTNNUMPRI() {
//            return PSTNNUMPRI;
//        }
//
//        public void setPSTNNUMPRI(String PSTNNUMPRI) {
//            this.PSTNNUMPRI = PSTNNUMPRI;
//        }
//
//        public String getBATCHNUMBER() {
//            return BATCHNUMBER;
//        }
//
//        public void setBATCHNUMBER(String BATCHNUMBER) {
//            this.BATCHNUMBER = BATCHNUMBER;
//        }
//
//        public String getMASTER_PINBYPASS() {
//            return MASTER_PINBYPASS;
//        }
//
//        public void setMASTER_PINBYPASS(String MASTER_PINBYPASS) {
//            this.MASTER_PINBYPASS = MASTER_PINBYPASS;
//        }
//
//        public String getPAYPAK_LAST4DSMART() {
//            return PAYPAK_LAST4DSMART;
//        }
//
//        public void setPAYPAK_LAST4DSMART(String PAYPAK_LAST4DSMART) {
//            this.PAYPAK_LAST4DSMART = PAYPAK_LAST4DSMART;
//        }
//
//        public String getTERMINALNETMASK() {
//            return TERMINALNETMASK;
//        }
//
//        public void setTERMINALNETMASK(String TERMINALNETMASK) {
//            this.TERMINALNETMASK = TERMINALNETMASK;
//        }
//
//        public String getDECIMALSEPARATOR() {
//            return DECIMALSEPARATOR;
//        }
//
//        public void setDECIMALSEPARATOR(String DECIMALSEPARATOR) {
//            this.DECIMALSEPARATOR = DECIMALSEPARATOR;
//        }
//
//        public String getPSTNINTERCHARTO() {
//            return PSTNINTERCHARTO;
//        }
//
//        public void setPSTNINTERCHARTO(String PSTNINTERCHARTO) {
//            this.PSTNINTERCHARTO = PSTNINTERCHARTO;
//        }
//
//        public String getSETTLECOUNTER() {
//            return SETTLECOUNTER;
//        }
//
//        public void setSETTLECOUNTER(String SETTLECOUNTER) {
//            this.SETTLECOUNTER = SETTLECOUNTER;
//        }
//
//        public String getACQNUM() {
//            return ACQNUM;
//        }
//
//        public void setACQNUM(String ACQNUM) {
//            this.ACQNUM = ACQNUM;
//        }
//
//        public String getMASTER_PREAUTHENABLE() {
//            return MASTER_PREAUTHENABLE;
//        }
//
//        public void setMASTER_PREAUTHENABLE(String MASTER_PREAUTHENABLE) {
//            this.MASTER_PREAUTHENABLE = MASTER_PREAUTHENABLE;
//        }
//
//        public String getMARKETSEGMENT() {
//            return MARKETSEGMENT;
//        }
//
//        public void setMARKETSEGMENT(String MARKETSEGMENT) {
//            this.MARKETSEGMENT = MARKETSEGMENT;
//        }
//
//        public String getAUTOSETTLE() {
//            return AUTOSETTLE;
//        }
//
//        public void setAUTOSETTLE(String AUTOSETTLE) {
//            this.AUTOSETTLE = AUTOSETTLE;
//        }
//
//        public String getTERMINALDNS2() {
//            return TERMINALDNS2;
//        }
//
//        public void setTERMINALDNS2(String TERMINALDNS2) {
//            this.TERMINALDNS2 = TERMINALDNS2;
//        }
//
//        public String getTERMINALDNS1() {
//            return TERMINALDNS1;
//        }
//
//        public void setTERMINALDNS1(String TERMINALDNS1) {
//            this.TERMINALDNS1 = TERMINALDNS1;
//        }
//
//        public String getJCB_CHSERVCODE() {
//            return JCB_CHSERVCODE;
//        }
//
//        public void setJCB_CHSERVCODE(String JCB_CHSERVCODE) {
//            this.JCB_CHSERVCODE = JCB_CHSERVCODE;
//        }
//
//        public String getBULKDOWNLOADPENDING() {
//            return BULKDOWNLOADPENDING;
//        }
//
//        public void setBULKDOWNLOADPENDING(String BULKDOWNLOADPENDING) {
//            this.BULKDOWNLOADPENDING = BULKDOWNLOADPENDING;
//        }
//
//        public String getTUPPRIMARYPORT() {
//            return TUPPRIMARYPORT;
//        }
//
//        public void setTUPPRIMARYPORT(String TUPPRIMARYPORT) {
//            this.TUPPRIMARYPORT = TUPPRIMARYPORT;
//        }
//
//        public String getTERMINALCOUNTRYCODE() {
//            return TERMINALCOUNTRYCODE;
//        }
//
//        public void setTERMINALCOUNTRYCODE(String TERMINALCOUNTRYCODE) {
//            this.TERMINALCOUNTRYCODE = TERMINALCOUNTRYCODE;
//        }
//
//        public String getVISA_SCHEME() {
//            return VISA_SCHEME;
//        }
//
//        public void setVISA_SCHEME(String VISA_SCHEME) {
//            this.VISA_SCHEME = VISA_SCHEME;
//        }
//
//        public String getMASTER_LAST4DMAG() {
//            return MASTER_LAST4DMAG;
//        }
//
//        public void setMASTER_LAST4DMAG(String MASTER_LAST4DMAG) {
//            this.MASTER_LAST4DMAG = MASTER_LAST4DMAG;
//        }
//
//        public String getJCB_MANUALENTRY() {
//            return JCB_MANUALENTRY;
//        }
//
//        public void setJCB_MANUALENTRY(String JCB_MANUALENTRY) {
//            this.JCB_MANUALENTRY = JCB_MANUALENTRY;
//        }
//
//        public String getSTAN() {
//            return STAN;
//        }
//
//        public void setSTAN(String STAN) {
//            this.STAN = STAN;
//        }
//
//        public String getPAYPAK_ACQUIRER() {
//            return PAYPAK_ACQUIRER;
//        }
//
//        public void setPAYPAK_ACQUIRER(String PAYPAK_ACQUIRER) {
//            this.PAYPAK_ACQUIRER = PAYPAK_ACQUIRER;
//        }
//
//        public String getPAYPAK_MOD10() {
//            return PAYPAK_MOD10;
//        }
//
//        public void setPAYPAK_MOD10(String PAYPAK_MOD10) {
//            this.PAYPAK_MOD10 = PAYPAK_MOD10;
//        }
//
//        public String getISSIMSWDEBUGENABLE() {
//            return ISSIMSWDEBUGENABLE;
//        }
//
//        public void setISSIMSWDEBUGENABLE(String ISSIMSWDEBUGENABLE) {
//            this.ISSIMSWDEBUGENABLE = ISSIMSWDEBUGENABLE;
//        }
//
//        public String getJCB_SCHEME() {
//            return JCB_SCHEME;
//        }
//
//        public void setJCB_SCHEME(String JCB_SCHEME) {
//            this.JCB_SCHEME = JCB_SCHEME;
//        }
//
//        public String getPRINTBARCODE() {
//            return PRINTBARCODE;
//        }
//
//        public void setPRINTBARCODE(String PRINTBARCODE) {
//            this.PRINTBARCODE = PRINTBARCODE;
//        }
//
//        public String getTERMINALID() {
//            return TERMINALID;
//        }
//
//        public void setTERMINALID(String TERMINALID) {
//            this.TERMINALID = TERMINALID;
//        }
//
//        public String getAMEXID() {
//            return AMEXID;
//        }
//
//        public void setAMEXID(String AMEXID) {
//            this.AMEXID = AMEXID;
//        }
//
//        public String getJCB_PRINTEXPIRY() {
//            return JCB_PRINTEXPIRY;
//        }
//
//        public void setJCB_PRINTEXPIRY(String JCB_PRINTEXPIRY) {
//            this.JCB_PRINTEXPIRY = JCB_PRINTEXPIRY;
//        }
//
//        public String getCLIENTNAME() {
//            return CLIENTNAME;
//        }
//
//        public void setCLIENTNAME(String CLIENTNAME) {
//            this.CLIENTNAME = CLIENTNAME;
//        }
//
//        public String getMASTER_LAST4DSMART() {
//            return MASTER_LAST4DSMART;
//        }
//
//        public void setMASTER_LAST4DSMART(String MASTER_LAST4DSMART) {
//            this.MASTER_LAST4DSMART = MASTER_LAST4DSMART;
//        }
//
//        public String getUPI_ADVCASHENABLE() {
//            return UPI_ADVCASHENABLE;
//        }
//
//        public void setUPI_ADVCASHENABLE(String UPI_ADVCASHENABLE) {
//            this.UPI_ADVCASHENABLE = UPI_ADVCASHENABLE;
//        }
//
//        public String getTUPSSLENABLE() {
//            return TUPSSLENABLE;
//        }
//
//        public void setTUPSSLENABLE(String TUPSSLENABLE) {
//            this.TUPSSLENABLE = TUPSSLENABLE;
//        }
//
//        public String getVISA_MANUALENTRY() {
//            return VISA_MANUALENTRY;
//        }
//
//        public void setVISA_MANUALENTRY(String VISA_MANUALENTRY) {
//            this.VISA_MANUALENTRY = VISA_MANUALENTRY;
//        }
//
//        public String getUPI_COMPLETIONENABLE() {
//            return UPI_COMPLETIONENABLE;
//        }
//
//        public void setUPI_COMPLETIONENABLE(String UPI_COMPLETIONENABLE) {
//            this.UPI_COMPLETIONENABLE = UPI_COMPLETIONENABLE;
//        }
//
//        public String getMASTER_ACQUIRER() {
//            return MASTER_ACQUIRER;
//        }
//
//        public void setMASTER_ACQUIRER(String MASTER_ACQUIRER) {
//            this.MASTER_ACQUIRER = MASTER_ACQUIRER;
//        }
//
//        public String getJCB_BANKFUNDED() {
//            return JCB_BANKFUNDED;
//        }
//
//        public void setJCB_BANKFUNDED(String JCB_BANKFUNDED) {
//            this.JCB_BANKFUNDED = JCB_BANKFUNDED;
//        }
//
//        public String getTERMINALWIFIDHCP() {
//            return TERMINALWIFIDHCP;
//        }
//
//        public void setTERMINALWIFIDHCP(String TERMINALWIFIDHCP) {
//            this.TERMINALWIFIDHCP = TERMINALWIFIDHCP;
//        }
//
//        public String getAUTOSETTLETIME() {
//            return AUTOSETTLETIME;
//        }
//
//        public void setAUTOSETTLETIME(String AUTOSETTLETIME) {
//            this.AUTOSETTLETIME = AUTOSETTLETIME;
//        }
//
//        public String getAMEX_LAST4DSMART() {
//            return AMEX_LAST4DSMART;
//        }
//
//        public void setAMEX_LAST4DSMART(String AMEX_LAST4DSMART) {
//            this.AMEX_LAST4DSMART = AMEX_LAST4DSMART;
//        }
//
//        public String getEMVCOUNTER() {
//            return EMVCOUNTER;
//        }
//
//        public void setEMVCOUNTER(String EMVCOUNTER) {
//            this.EMVCOUNTER = EMVCOUNTER;
//        }
//
//        public String getRECEIVETIMEOUT() {
//            return RECEIVETIMEOUT;
//        }
//
//        public void setRECEIVETIMEOUT(String RECEIVETIMEOUT) {
//            this.RECEIVETIMEOUT = RECEIVETIMEOUT;
//        }
//
//        public String getCARDTIMEOUT() {
//            return CARDTIMEOUT;
//        }
//
//        public void setCARDTIMEOUT(String CARDTIMEOUT) {
//            this.CARDTIMEOUT = CARDTIMEOUT;
//        }
//
//        public String getSERVICEFIRSTTIME() {
//            return SERVICEFIRSTTIME;
//        }
//
//        public void setSERVICEFIRSTTIME(String SERVICEFIRSTTIME) {
//            this.SERVICEFIRSTTIME = SERVICEFIRSTTIME;
//        }
//
//        public String getISACTIVATED() {
//            return ISACTIVATED;
//        }
//
//        public void setISACTIVATED(String ISACTIVATED) {
//            this.ISACTIVATED = ISACTIVATED;
//        }
//
//        public String getAMEX_PRINTEXPIRY() {
//            return AMEX_PRINTEXPIRY;
//        }
//
//        public void setAMEX_PRINTEXPIRY(String AMEX_PRINTEXPIRY) {
//            this.AMEX_PRINTEXPIRY = AMEX_PRINTEXPIRY;
//        }
//
//        public String getJCB_VOIDENABLE() {
//            return JCB_VOIDENABLE;
//        }
//
//        public void setJCB_VOIDENABLE(String JCB_VOIDENABLE) {
//            this.JCB_VOIDENABLE = JCB_VOIDENABLE;
//        }
//
//        public String getPASSWRD() {
//            return PASSWRD;
//        }
//
//        public void setPASSWRD(String PASSWRD) {
//            this.PASSWRD = PASSWRD;
//        }
//
//        public String getTIPENABLED() {
//            return TIPENABLED;
//        }
//
//        public void setTIPENABLED(String TIPENABLED) {
//            this.TIPENABLED = TIPENABLED;
//        }
//
//        public String getMASTER_ISSUERACTIVATED() {
//            return MASTER_ISSUERACTIVATED;
//        }
//
//        public void setMASTER_ISSUERACTIVATED(String MASTER_ISSUERACTIVATED) {
//            this.MASTER_ISSUERACTIVATED = MASTER_ISSUERACTIVATED;
//        }
//
//        public String getUPI_PRINTEXPIRY() {
//            return UPI_PRINTEXPIRY;
//        }
//
//        public void setUPI_PRINTEXPIRY(String UPI_PRINTEXPIRY) {
//            this.UPI_PRINTEXPIRY = UPI_PRINTEXPIRY;
//        }
//
//        public String getMASTER_BANKFUNDED() {
//            return MASTER_BANKFUNDED;
//        }
//
//        public void setMASTER_BANKFUNDED(String MASTER_BANKFUNDED) {
//            this.MASTER_BANKFUNDED = MASTER_BANKFUNDED;
//        }
//
//        public String getOFFLINEPIN() {
//            return OFFLINEPIN;
//        }
//
//        public void setOFFLINEPIN(String OFFLINEPIN) {
//            this.OFFLINEPIN = OFFLINEPIN;
//        }
//
//        public String getPOSSERIALNO() {
//            return POSSERIALNO;
//        }
//
//        public void setPOSSERIALNO(String POSSERIALNO) {
//            this.POSSERIALNO = POSSERIALNO;
//        }
//
//        public String getPAYPAK_COMPLETIONENABLE() {
//            return PAYPAK_COMPLETIONENABLE;
//        }
//
//        public void setPAYPAK_COMPLETIONENABLE(String PAYPAK_COMPLETIONENABLE) {
//            this.PAYPAK_COMPLETIONENABLE = PAYPAK_COMPLETIONENABLE;
//        }
//
//        public String getTOPUPPIN() {
//            return TOPUPPIN;
//        }
//
//        public void setTOPUPPIN(String TOPUPPIN) {
//            this.TOPUPPIN = TOPUPPIN;
//        }
//
//        public String getPAYPAK_REFUNDENABLE() {
//            return PAYPAK_REFUNDENABLE;
//        }
//
//        public void setPAYPAK_REFUNDENABLE(String PAYPAK_REFUNDENABLE) {
//            this.PAYPAK_REFUNDENABLE = PAYPAK_REFUNDENABLE;
//        }
//
//        public String getTERMINALGATEWAY() {
//            return TERMINALGATEWAY;
//        }
//
//        public void setTERMINALGATEWAY(String TERMINALGATEWAY) {
//            this.TERMINALGATEWAY = TERMINALGATEWAY;
//        }
//
//        public String getUPI_REFUNDENABLE() {
//            return UPI_REFUNDENABLE;
//        }
//
//        public void setUPI_REFUNDENABLE(String UPI_REFUNDENABLE) {
//            this.UPI_REFUNDENABLE = UPI_REFUNDENABLE;
//        }
//
//        public String getAMEX_MOD10() {
//            return AMEX_MOD10;
//        }
//
//        public void setAMEX_MOD10(String AMEX_MOD10) {
//            this.AMEX_MOD10 = AMEX_MOD10;
//        }
//
//        public String getVISA_COMPLETIONENABLE() {
//            return VISA_COMPLETIONENABLE;
//        }
//
//        public void setVISA_COMPLETIONENABLE(String VISA_COMPLETIONENABLE) {
//            this.VISA_COMPLETIONENABLE = VISA_COMPLETIONENABLE;
//        }
//
//        public String getPAYPAK_ADVCASHENABLE() {
//            return PAYPAK_ADVCASHENABLE;
//        }
//
//        public void setPAYPAK_ADVCASHENABLE(String PAYPAK_ADVCASHENABLE) {
//            this.PAYPAK_ADVCASHENABLE = PAYPAK_ADVCASHENABLE;
//        }
//
//        public String getHELPTOUCHMSG() {
//            return HELPTOUCHMSG;
//        }
//
//        public void setHELPTOUCHMSG(String HELPTOUCHMSG) {
//            this.HELPTOUCHMSG = HELPTOUCHMSG;
//        }
//
//        public String getTERMINALWIFIGATEWAY() {
//            return TERMINALWIFIGATEWAY;
//        }
//
//        public void setTERMINALWIFIGATEWAY(String TERMINALWIFIGATEWAY) {
//            this.TERMINALWIFIGATEWAY = TERMINALWIFIGATEWAY;
//        }
//
//        public String getUPI_ISSNAME() {
//            return UPI_ISSNAME;
//        }
//
//        public void setUPI_ISSNAME(String UPI_ISSNAME) {
//            this.UPI_ISSNAME = UPI_ISSNAME;
//        }
//
//        public String getEODPRINTDETAILS() {
//            return EODPRINTDETAILS;
//        }
//
//        public void setEODPRINTDETAILS(String EODPRINTDETAILS) {
//            this.EODPRINTDETAILS = EODPRINTDETAILS;
//        }
//
//        public String getPAYPAK_VOIDENABLE() {
//            return PAYPAK_VOIDENABLE;
//        }
//
//        public void setPAYPAK_VOIDENABLE(String PAYPAK_VOIDENABLE) {
//            this.PAYPAK_VOIDENABLE = PAYPAK_VOIDENABLE;
//        }
//
//        public String getTUPSECONDARYPORT() {
//            return TUPSECONDARYPORT;
//        }
//
//        public void setTUPSECONDARYPORT(String TUPSECONDARYPORT) {
//            this.TUPSECONDARYPORT = TUPSECONDARYPORT;
//        }
//
//        public String getRSPTIMEOUT() {
//            return RSPTIMEOUT;
//        }
//
//        public void setRSPTIMEOUT(String RSPTIMEOUT) {
//            this.RSPTIMEOUT = RSPTIMEOUT;
//        }
//
//        public String getPAYPAK_ISSUERACTIVATED() {
//            return PAYPAK_ISSUERACTIVATED;
//        }
//
//        public void setPAYPAK_ISSUERACTIVATED(String PAYPAK_ISSUERACTIVATED) {
//            this.PAYPAK_ISSUERACTIVATED = PAYPAK_ISSUERACTIVATED;
//        }
//
//        public String getPRIMARYIP() {
//            return PRIMARYIP;
//        }
//
//        public void setPRIMARYIP(String PRIMARYIP) {
//            this.PRIMARYIP = PRIMARYIP;
//        }
//
//        public String getVISA_ISSNUM() {
//            return VISA_ISSNUM;
//        }
//
//        public void setVISA_ISSNUM(String VISA_ISSNUM) {
//            this.VISA_ISSNUM = VISA_ISSNUM;
//        }
//
//        public String getCASHBACKENABLED() {
//            return CASHBACKENABLED;
//        }
//
//        public void setCASHBACKENABLED(String CASHBACKENABLED) {
//            this.CASHBACKENABLED = CASHBACKENABLED;
//        }
//
//        public String getPANLENGTHMAX() {
//            return PANLENGTHMAX;
//        }
//
//        public void setPANLENGTHMAX(String PANLENGTHMAX) {
//            this.PANLENGTHMAX = PANLENGTHMAX;
//        }
//
//        public String getTERMINALWIFIIP() {
//            return TERMINALWIFIIP;
//        }
//
//        public void setTERMINALWIFIIP(String TERMINALWIFIIP) {
//            this.TERMINALWIFIIP = TERMINALWIFIIP;
//        }
//
//        public String getPSTNSETTLNUMSEC() {
//            return PSTNSETTLNUMSEC;
//        }
//
//        public void setPSTNSETTLNUMSEC(String PSTNSETTLNUMSEC) {
//            this.PSTNSETTLNUMSEC = PSTNSETTLNUMSEC;
//        }
//
//        public String getJCB_ADJUSTENABLE() {
//            return JCB_ADJUSTENABLE;
//        }
//
//        public void setJCB_ADJUSTENABLE(String JCB_ADJUSTENABLE) {
//            this.JCB_ADJUSTENABLE = JCB_ADJUSTENABLE;
//        }
//
//        public String getAMEX_ISSNAME() {
//            return AMEX_ISSNAME;
//        }
//
//        public void setAMEX_ISSNAME(String AMEX_ISSNAME) {
//            this.AMEX_ISSNAME = AMEX_ISSNAME;
//        }
//
//        public String getALLOWFALLBACK() {
//            return ALLOWFALLBACK;
//        }
//
//        public void setALLOWFALLBACK(String ALLOWFALLBACK) {
//            this.ALLOWFALLBACK = ALLOWFALLBACK;
//        }
//
//        public String getVISA_ISSNAME() {
//            return VISA_ISSNAME;
//        }
//
//        public void setVISA_ISSNAME(String VISA_ISSNAME) {
//            this.VISA_ISSNAME = VISA_ISSNAME;
//        }
//
//        public String getMASTER_FORCEDLBPENABLE() {
//            return MASTER_FORCEDLBPENABLE;
//        }
//
//        public void setMASTER_FORCEDLBPENABLE(String MASTER_FORCEDLBPENABLE) {
//            this.MASTER_FORCEDLBPENABLE = MASTER_FORCEDLBPENABLE;
//        }
//
//        public String getMASTER_MOD10() {
//            return MASTER_MOD10;
//        }
//
//        public void setMASTER_MOD10(String MASTER_MOD10) {
//            this.MASTER_MOD10 = MASTER_MOD10;
//        }
//
//        public String getJCB_ISSNUM() {
//            return JCB_ISSNUM;
//        }
//
//        public void setJCB_ISSNUM(String JCB_ISSNUM) {
//            this.JCB_ISSNUM = JCB_ISSNUM;
//        }
//
//        public String getISLOCALHOST() {
//            return ISLOCALHOST;
//        }
//
//        public void setISLOCALHOST(String ISLOCALHOST) {
//            this.ISLOCALHOST = ISLOCALHOST;
//        }
//
//        public String getVCSETHTIMEOUT() {
//            return VCSETHTIMEOUT;
//        }
//
//        public void setVCSETHTIMEOUT(String VCSETHTIMEOUT) {
//            this.VCSETHTIMEOUT = VCSETHTIMEOUT;
//        }
//
//        public String getWIFISSID() {
//            return WIFISSID;
//        }
//
//        public void setWIFISSID(String WIFISSID) {
//            this.WIFISSID = WIFISSID;
//        }
//
//        public String getTERMINALWIFIDNS2() {
//            return TERMINALWIFIDNS2;
//        }
//
//        public void setTERMINALWIFIDNS2(String TERMINALWIFIDNS2) {
//            this.TERMINALWIFIDNS2 = TERMINALWIFIDNS2;
//        }
//
//        public String getPSTNPREFIX() {
//            return PSTNPREFIX;
//        }
//
//        public void setPSTNPREFIX(String PSTNPREFIX) {
//            this.PSTNPREFIX = PSTNPREFIX;
//        }
//
//        public String getSECONDARYMEDIUM() {
//            return SECONDARYMEDIUM;
//        }
//
//        public void setSECONDARYMEDIUM(String SECONDARYMEDIUM) {
//            this.SECONDARYMEDIUM = SECONDARYMEDIUM;
//        }
//
//        public String getTERMINALWIFIDNS1() {
//            return TERMINALWIFIDNS1;
//        }
//
//        public void setTERMINALWIFIDNS1(String TERMINALWIFIDNS1) {
//            this.TERMINALWIFIDNS1 = TERMINALWIFIDNS1;
//        }
//
//        public String getPAYPAK_ISSNUM() {
//            return PAYPAK_ISSNUM;
//        }
//
//        public void setPAYPAK_ISSNUM(String PAYPAK_ISSNUM) {
//            this.PAYPAK_ISSNUM = PAYPAK_ISSNUM;
//        }
//
//        public String getAMEX_SCHEME() {
//            return AMEX_SCHEME;
//        }
//
//        public void setAMEX_SCHEME(String AMEX_SCHEME) {
//            this.AMEX_SCHEME = AMEX_SCHEME;
//        }
//
//        public String getTHOUSANDSEPARATOR() {
//            return THOUSANDSEPARATOR;
//        }
//
//        public void setTHOUSANDSEPARATOR(String THOUSANDSEPARATOR) {
//            this.THOUSANDSEPARATOR = THOUSANDSEPARATOR;
//        }
//
//        public String getAGENTCODE() {
//            return AGENTCODE;
//        }
//
//        public void setAGENTCODE(String AGENTCODE) {
//            this.AGENTCODE = AGENTCODE;
//        }
//
//        public String getPRINTBANKRECON() {
//            return PRINTBANKRECON;
//        }
//
//        public void setPRINTBANKRECON(String PRINTBANKRECON) {
//            this.PRINTBANKRECON = PRINTBANKRECON;
//        }
//
//        public String getVISA_FORCEDLBPENABLE() {
//            return VISA_FORCEDLBPENABLE;
//        }
//
//        public void setVISA_FORCEDLBPENABLE(String VISA_FORCEDLBPENABLE) {
//            this.VISA_FORCEDLBPENABLE = VISA_FORCEDLBPENABLE;
//        }
//
//        public String getUPI_CHECKEXPIRY() {
//            return UPI_CHECKEXPIRY;
//        }
//
//        public void setUPI_CHECKEXPIRY(String UPI_CHECKEXPIRY) {
//            this.UPI_CHECKEXPIRY = UPI_CHECKEXPIRY;
//        }
//
//        public String getPRINTREVERSALRCPT() {
//            return PRINTREVERSALRCPT;
//        }
//
//        public void setPRINTREVERSALRCPT(String PRINTREVERSALRCPT) {
//            this.PRINTREVERSALRCPT = PRINTREVERSALRCPT;
//        }
//
//        public String getINVOICENUMBER() {
//            return INVOICENUMBER;
//        }
//
//        public void setINVOICENUMBER(String INVOICENUMBER) {
//            this.INVOICENUMBER = INVOICENUMBER;
//        }
//
//        public String getBOOTFIRSTTIME() {
//            return BOOTFIRSTTIME;
//        }
//
//        public void setBOOTFIRSTTIME(String BOOTFIRSTTIME) {
//            this.BOOTFIRSTTIME = BOOTFIRSTTIME;
//        }
//
//        public String getTERMINALDHCP() {
//            return TERMINALDHCP;
//        }
//
//        public void setTERMINALDHCP(String TERMINALDHCP) {
//            this.TERMINALDHCP = TERMINALDHCP;
//        }
//
//        public String getPREAUTHMAXCOMPLETIONDAYS() {
//            return PREAUTHMAXCOMPLETIONDAYS;
//        }
//
//        public void setPREAUTHMAXCOMPLETIONDAYS(String PREAUTHMAXCOMPLETIONDAYS) {
//            this.PREAUTHMAXCOMPLETIONDAYS = PREAUTHMAXCOMPLETIONDAYS;
//        }
//
//        public String getCOMPLETIONTHERSHOLD() {
//            return COMPLETIONTHERSHOLD;
//        }
//
//        public void setCOMPLETIONTHERSHOLD(String COMPLETIONTHERSHOLD) {
//            this.COMPLETIONTHERSHOLD = COMPLETIONTHERSHOLD;
//        }
//
//        public String getMASTER_REFUNDENABLE() {
//            return MASTER_REFUNDENABLE;
//        }
//
//        public void setMASTER_REFUNDENABLE(String MASTER_REFUNDENABLE) {
//            this.MASTER_REFUNDENABLE = MASTER_REFUNDENABLE;
//        }
//
//        public String getUPI_FORCEDLBPENABLE() {
//            return UPI_FORCEDLBPENABLE;
//        }
//
//        public void setUPI_FORCEDLBPENABLE(String UPI_FORCEDLBPENABLE) {
//            this.UPI_FORCEDLBPENABLE = UPI_FORCEDLBPENABLE;
//        }
//
//        public String getMASTER_MANUALENTRY() {
//            return MASTER_MANUALENTRY;
//        }
//
//        public void setMASTER_MANUALENTRY(String MASTER_MANUALENTRY) {
//            this.MASTER_MANUALENTRY = MASTER_MANUALENTRY;
//        }
//
//        public String getPRINTEMVLOGS() {
//            return PRINTEMVLOGS;
//        }
//
//        public void setPRINTEMVLOGS(String PRINTEMVLOGS) {
//            this.PRINTEMVLOGS = PRINTEMVLOGS;
//        }
//
//        public String getJCB_REFUNDENABLE() {
//            return JCB_REFUNDENABLE;
//        }
//
//        public void setJCB_REFUNDENABLE(String JCB_REFUNDENABLE) {
//            this.JCB_REFUNDENABLE = JCB_REFUNDENABLE;
//        }
//
//        public String getUPI_ISSUERACTIVATED() {
//            return UPI_ISSUERACTIVATED;
//        }
//
//        public void setUPI_ISSUERACTIVATED(String UPI_ISSUERACTIVATED) {
//            this.UPI_ISSUERACTIVATED = UPI_ISSUERACTIVATED;
//        }
//
//        public String getPASSWORDSUPERUSER() {
//            return PASSWORDSUPERUSER;
//        }
//
//        public void setPASSWORDSUPERUSER(String PASSWORDSUPERUSER) {
//            this.PASSWORDSUPERUSER = PASSWORDSUPERUSER;
//        }
//
//        public String getVISA_ISSUERACTIVATED() {
//            return VISA_ISSUERACTIVATED;
//        }
//
//        public void setVISA_ISSUERACTIVATED(String VISA_ISSUERACTIVATED) {
//            this.VISA_ISSUERACTIVATED = VISA_ISSUERACTIVATED;
//        }
//
//        public String getUPI_PREAUTHENABLE() {
//            return UPI_PREAUTHENABLE;
//        }
//
//        public void setUPI_PREAUTHENABLE(String UPI_PREAUTHENABLE) {
//            this.UPI_PREAUTHENABLE = UPI_PREAUTHENABLE;
//        }
//
//        public String getPAYPAK_CHECKEXPIRY() {
//            return PAYPAK_CHECKEXPIRY;
//        }
//
//        public void setPAYPAK_CHECKEXPIRY(String PAYPAK_CHECKEXPIRY) {
//            this.PAYPAK_CHECKEXPIRY = PAYPAK_CHECKEXPIRY;
//        }
//
//        public String getISSTATUSDEBUGENABLE() {
//            return ISSTATUSDEBUGENABLE;
//        }
//
//        public void setISSTATUSDEBUGENABLE(String ISSTATUSDEBUGENABLE) {
//            this.ISSTATUSDEBUGENABLE = ISSTATUSDEBUGENABLE;
//        }
//
//        public String getTPKGISKE() {
//            return TPKGISKE;
//        }
//
//        public void setTPKGISKE(String TPKGISKE) {
//            this.TPKGISKE = TPKGISKE;
//        }
//
//        public String getMASTER_COMPLETIONENABLE() {
//            return MASTER_COMPLETIONENABLE;
//        }
//
//        public void setMASTER_COMPLETIONENABLE(String MASTER_COMPLETIONENABLE) {
//            this.MASTER_COMPLETIONENABLE = MASTER_COMPLETIONENABLE;
//        }
//
//        public String getLANGUAGEDEFAULT() {
//            return LANGUAGEDEFAULT;
//        }
//
//        public void setLANGUAGEDEFAULT(String LANGUAGEDEFAULT) {
//            this.LANGUAGEDEFAULT = LANGUAGEDEFAULT;
//        }
//
//        public String getUPI_MANUALENTRY() {
//            return UPI_MANUALENTRY;
//        }
//
//        public void setUPI_MANUALENTRY(String UPI_MANUALENTRY) {
//            this.UPI_MANUALENTRY = UPI_MANUALENTRY;
//        }
//
//        public String getVISA_LAST4DSMART() {
//            return VISA_LAST4DSMART;
//        }
//
//        public void setVISA_LAST4DSMART(String VISA_LAST4DSMART) {
//            this.VISA_LAST4DSMART = VISA_LAST4DSMART;
//        }
//
//        public String getAMEX_ADVCASHENABLE() {
//            return AMEX_ADVCASHENABLE;
//        }
//
//        public void setAMEX_ADVCASHENABLE(String AMEX_ADVCASHENABLE) {
//            this.AMEX_ADVCASHENABLE = AMEX_ADVCASHENABLE;
//        }
//
//        public String getPSTNSETTLNUMPRI() {
//            return PSTNSETTLNUMPRI;
//        }
//
//        public void setPSTNSETTLNUMPRI(String PSTNSETTLNUMPRI) {
//            this.PSTNSETTLNUMPRI = PSTNSETTLNUMPRI;
//        }
//
//        public String getVISA_PREAUTHENABLE() {
//            return VISA_PREAUTHENABLE;
//        }
//
//        public void setVISA_PREAUTHENABLE(String VISA_PREAUTHENABLE) {
//            this.VISA_PREAUTHENABLE = VISA_PREAUTHENABLE;
//        }
//
//        public String getTIPTHERSHOLD() {
//            return TIPTHERSHOLD;
//        }
//
//        public void setTIPTHERSHOLD(String TIPTHERSHOLD) {
//            this.TIPTHERSHOLD = TIPTHERSHOLD;
//        }
//
//        public String getMASTER_SCHEME() {
//            return MASTER_SCHEME;
//        }
//
//        public void setMASTER_SCHEME(String MASTER_SCHEME) {
//            this.MASTER_SCHEME = MASTER_SCHEME;
//        }
//
//        public String getUPI_ISSNUM() {
//            return UPI_ISSNUM;
//        }
//
//        public void setUPI_ISSNUM(String UPI_ISSNUM) {
//            this.UPI_ISSNUM = UPI_ISSNUM;
//        }
//
//        public String getPASSWORDUSER() {
//            return PASSWORDUSER;
//        }
//
//        public void setPASSWORDUSER(String PASSWORDUSER) {
//            this.PASSWORDUSER = PASSWORDUSER;
//        }
//
//        public String getAMEX_REFUNDENABLE() {
//            return AMEX_REFUNDENABLE;
//        }
//
//        public void setAMEX_REFUNDENABLE(String AMEX_REFUNDENABLE) {
//            this.AMEX_REFUNDENABLE = AMEX_REFUNDENABLE;
//        }
//
//        public String getPREAUTHCLEARCOUNT() {
//            return PREAUTHCLEARCOUNT;
//        }
//
//        public void setPREAUTHCLEARCOUNT(String PREAUTHCLEARCOUNT) {
//            this.PREAUTHCLEARCOUNT = PREAUTHCLEARCOUNT;
//        }
//
//        public String getLANGUAGE() {
//            return LANGUAGE;
//        }
//
//        public void setLANGUAGE(String LANGUAGE) {
//            this.LANGUAGE = LANGUAGE;
//        }
//
//        public String getRECEIPTFOOTER1() {
//            return RECEIPTFOOTER1;
//        }
//
//        public void setRECEIPTFOOTER1(String RECEIPTFOOTER1) {
//            this.RECEIPTFOOTER1 = RECEIPTFOOTER1;
//        }
//
//        public String getRECEIPTFOOTER2() {
//            return RECEIPTFOOTER2;
//        }
//
//        public void setRECEIPTFOOTER2(String RECEIPTFOOTER2) {
//            this.RECEIPTFOOTER2 = RECEIPTFOOTER2;
//        }
//
//        public String getAMEX_ISSUERACTIVATED() {
//            return AMEX_ISSUERACTIVATED;
//        }
//
//        public void setAMEX_ISSUERACTIVATED(String AMEX_ISSUERACTIVATED) {
//            this.AMEX_ISSUERACTIVATED = AMEX_ISSUERACTIVATED;
//        }
//
//        public String getJCB_CHECKPIN() {
//            return JCB_CHECKPIN;
//        }
//
//        public void setJCB_CHECKPIN(String JCB_CHECKPIN) {
//            this.JCB_CHECKPIN = JCB_CHECKPIN;
//        }
//
//        public String getCLIENTTYPE() {
//            return CLIENTTYPE;
//        }
//
//        public void setCLIENTTYPE(String CLIENTTYPE) {
//            this.CLIENTTYPE = CLIENTTYPE;
//        }
//
//        public String getUPI_MOD10() {
//            return UPI_MOD10;
//        }
//
//        public void setUPI_MOD10(String UPI_MOD10) {
//            this.UPI_MOD10 = UPI_MOD10;
//        }
//
//        public String getPAYPAK_SCHEME() {
//            return PAYPAK_SCHEME;
//        }
//
//        public void setPAYPAK_SCHEME(String PAYPAK_SCHEME) {
//            this.PAYPAK_SCHEME = PAYPAK_SCHEME;
//        }
//
//        public String getUPI_ACQUIRER() {
//            return UPI_ACQUIRER;
//        }
//
//        public void setUPI_ACQUIRER(String UPI_ACQUIRER) {
//            this.UPI_ACQUIRER = UPI_ACQUIRER;
//        }
//
//        public String getUPI_CHSIGN() {
//            return UPI_CHSIGN;
//        }
//
//        public void setUPI_CHSIGN(String UPI_CHSIGN) {
//            this.UPI_CHSIGN = UPI_CHSIGN;
//        }
//
//        public String getMASTER_CHECKPIN() {
//            return MASTER_CHECKPIN;
//        }
//
//        public void setMASTER_CHECKPIN(String MASTER_CHECKPIN) {
//            this.MASTER_CHECKPIN = MASTER_CHECKPIN;
//        }
//
//        public String getVISA_ADJUSTENABLE() {
//            return VISA_ADJUSTENABLE;
//        }
//
//        public void setVISA_ADJUSTENABLE(String VISA_ADJUSTENABLE) {
//            this.VISA_ADJUSTENABLE = VISA_ADJUSTENABLE;
//        }
//
//        public String getUPI_LAST4DMAG() {
//            return UPI_LAST4DMAG;
//        }
//
//        public void setUPI_LAST4DMAG(String UPI_LAST4DMAG) {
//            this.UPI_LAST4DMAG = UPI_LAST4DMAG;
//        }
//
//        public String getAMEX_FORCEDLBPENABLE() {
//            return AMEX_FORCEDLBPENABLE;
//        }
//
//        public void setAMEX_FORCEDLBPENABLE(String AMEX_FORCEDLBPENABLE) {
//            this.AMEX_FORCEDLBPENABLE = AMEX_FORCEDLBPENABLE;
//        }
//
//        public String getPAYPAK_PRINTEXPIRY() {
//            return PAYPAK_PRINTEXPIRY;
//        }
//
//        public void setPAYPAK_PRINTEXPIRY(String PAYPAK_PRINTEXPIRY) {
//            this.PAYPAK_PRINTEXPIRY = PAYPAK_PRINTEXPIRY;
//        }
//
//        public String getBTMACADDRESS() {
//            return BTMACADDRESS;
//        }
//
//        public void setBTMACADDRESS(String BTMACADDRESS) {
//            this.BTMACADDRESS = BTMACADDRESS;
//        }
//
//        public String getVISA_REFUNDENABLE() {
//            return VISA_REFUNDENABLE;
//        }
//
//        public void setVISA_REFUNDENABLE(String VISA_REFUNDENABLE) {
//            this.VISA_REFUNDENABLE = VISA_REFUNDENABLE;
//        }
//
//        public String getSECONDARYIP() {
//            return SECONDARYIP;
//        }
//
//        public void setSECONDARYIP(String SECONDARYIP) {
//            this.SECONDARYIP = SECONDARYIP;
//        }
//
//        public String getJCB_CHECKEXPIRY() {
//            return JCB_CHECKEXPIRY;
//        }
//
//        public void setJCB_CHECKEXPIRY(String JCB_CHECKEXPIRY) {
//            this.JCB_CHECKEXPIRY = JCB_CHECKEXPIRY;
//        }
//
//        public String getISSSLENABLE() {
//            return ISSSLENABLE;
//        }
//
//        public void setISSSLENABLE(String ISSSLENABLE) {
//            this.ISSSLENABLE = ISSSLENABLE;
//        }
//
//        public String getENABLEAUTOPINENTER() {
//            return ENABLEAUTOPINENTER;
//        }
//
//        public void setENABLEAUTOPINENTER(String ENABLEAUTOPINENTER) {
//            this.ENABLEAUTOPINENTER = ENABLEAUTOPINENTER;
//        }
//
//        public String getLANGUAGEACQNUM() {
//            return LANGUAGEACQNUM;
//        }
//
//        public void setLANGUAGEACQNUM(String LANGUAGEACQNUM) {
//            this.LANGUAGEACQNUM = LANGUAGEACQNUM;
//        }
//
//        public String getPAYPAK_ADJUSTENABLE() {
//            return PAYPAK_ADJUSTENABLE;
//        }
//
//        public void setPAYPAK_ADJUSTENABLE(String PAYPAK_ADJUSTENABLE) {
//            this.PAYPAK_ADJUSTENABLE = PAYPAK_ADJUSTENABLE;
//        }
//
//        public String getJCB_ADVCASHENABLE() {
//            return JCB_ADVCASHENABLE;
//        }
//
//        public void setJCB_ADVCASHENABLE(String JCB_ADVCASHENABLE) {
//            this.JCB_ADVCASHENABLE = JCB_ADVCASHENABLE;
//        }
//
//        public String getTIPNUMADJUST() {
//            return TIPNUMADJUST;
//        }
//
//        public void setTIPNUMADJUST(String TIPNUMADJUST) {
//            this.TIPNUMADJUST = TIPNUMADJUST;
//        }
//
//        public String getTERMFORCEDLBPMAGENABLE() {
//            return TERMFORCEDLBPMAGENABLE;
//        }
//
//        public void setTERMFORCEDLBPMAGENABLE(String TERMFORCEDLBPMAGENABLE) {
//            this.TERMFORCEDLBPMAGENABLE = TERMFORCEDLBPMAGENABLE;
//        }
//
//        public String getVISA_ADVCASHENABLE() {
//            return VISA_ADVCASHENABLE;
//        }
//
//        public void setVISA_ADVCASHENABLE(String VISA_ADVCASHENABLE) {
//            this.VISA_ADVCASHENABLE = VISA_ADVCASHENABLE;
//        }
//
//        public String getCTLSENABLED() {
//            return CTLSENABLED;
//        }
//
//        public void setCTLSENABLED(String CTLSENABLED) {
//            this.CTLSENABLED = CTLSENABLED;
//        }
//
//        public String getUPI_PINBYPASS() {
//            return UPI_PINBYPASS;
//        }
//
//        public void setUPI_PINBYPASS(String UPI_PINBYPASS) {
//            this.UPI_PINBYPASS = UPI_PINBYPASS;
//        }
//
//        public String getPAYPAK_BANKFUNDED() {
//            return PAYPAK_BANKFUNDED;
//        }
//
//        public void setPAYPAK_BANKFUNDED(String PAYPAK_BANKFUNDED) {
//            this.PAYPAK_BANKFUNDED = PAYPAK_BANKFUNDED;
//        }
//
//        public String getJCB_ACQUIRER() {
//            return JCB_ACQUIRER;
//        }
//
//        public void setJCB_ACQUIRER(String JCB_ACQUIRER) {
//            this.JCB_ACQUIRER = JCB_ACQUIRER;
//        }
//
//        public String getAMEX_COMPLETIONENABLE() {
//            return AMEX_COMPLETIONENABLE;
//        }
//
//        public void setAMEX_COMPLETIONENABLE(String AMEX_COMPLETIONENABLE) {
//            this.AMEX_COMPLETIONENABLE = AMEX_COMPLETIONENABLE;
//        }
//
//        public String getF49ENABLE() {
//            return F49ENABLE;
//        }
//
//        public void setF49ENABLE(String f49ENABLE) {
//            F49ENABLE = f49ENABLE;
//        }
//
//        public String getTOPUPENABLED() {
//            return TOPUPENABLED;
//        }
//
//        public void setTOPUPENABLED(String TOPUPENABLED) {
//            this.TOPUPENABLED = TOPUPENABLED;
//        }
//
//        public String getUPI_CHSERVCODE() {
//            return UPI_CHSERVCODE;
//        }
//
//        public void setUPI_CHSERVCODE(String UPI_CHSERVCODE) {
//            this.UPI_CHSERVCODE = UPI_CHSERVCODE;
//        }
//
//        public String getUPDATEME() {
//            return UPDATEME;
//        }
//
//        public void setUPDATEME(String UPDATEME) {
//            this.UPDATEME = UPDATEME;
//        }
//
//        public String getAMEX_PREAUTHENABLE() {
//            return AMEX_PREAUTHENABLE;
//        }
//
//        public void setAMEX_PREAUTHENABLE(String AMEX_PREAUTHENABLE) {
//            this.AMEX_PREAUTHENABLE = AMEX_PREAUTHENABLE;
//        }
//
//        public String getEMVLOGSENABLED() {
//            return EMVLOGSENABLED;
//        }
//
//        public void setEMVLOGSENABLED(String EMVLOGSENABLED) {
//            this.EMVLOGSENABLED = EMVLOGSENABLED;
//        }
//
//        public String getTMSIP() {
//            return TMSIP;
//        }
//
//        public void setTMSIP(String TMSIP) {
//            this.TMSIP = TMSIP;
//        }
//
//        public String getVISA_CHSIGN() {
//            return VISA_CHSIGN;
//        }
//
//        public void setVISA_CHSIGN(String VISA_CHSIGN) {
//            this.VISA_CHSIGN = VISA_CHSIGN;
//        }
//
//        public String getAMEX_CHECKEXPIRY() {
//            return AMEX_CHECKEXPIRY;
//        }
//
//        public void setAMEX_CHECKEXPIRY(String AMEX_CHECKEXPIRY) {
//            this.AMEX_CHECKEXPIRY = AMEX_CHECKEXPIRY;
//        }
//
//        public String getMERCHANTID() {
//            return MERCHANTID;
//        }
//
//        public void setMERCHANTID(String MERCHANTID) {
//            this.MERCHANTID = MERCHANTID;
//        }
//
//        public String getMASTER_VOIDENABLE() {
//            return MASTER_VOIDENABLE;
//        }
//
//        public void setMASTER_VOIDENABLE(String MASTER_VOIDENABLE) {
//            this.MASTER_VOIDENABLE = MASTER_VOIDENABLE;
//        }
//
//        public String getVENDORCODE() {
//            return VENDORCODE;
//        }
//
//        public void setVENDORCODE(String VENDORCODE) {
//            this.VENDORCODE = VENDORCODE;
//        }
//
//        public String getRECEIPTPRINTED() {
//            return RECEIPTPRINTED;
//        }
//
//        public void setRECEIPTPRINTED(String RECEIPTPRINTED) {
//            this.RECEIPTPRINTED = RECEIPTPRINTED;
//        }
//
//        public String getJCB_CHSIGN() {
//            return JCB_CHSIGN;
//        }
//
//        public void setJCB_CHSIGN(String JCB_CHSIGN) {
//            this.JCB_CHSIGN = JCB_CHSIGN;
//        }
//
//        public String getVISA_VOIDENABLE() {
//            return VISA_VOIDENABLE;
//        }
//
//        public void setVISA_VOIDENABLE(String VISA_VOIDENABLE) {
//            this.VISA_VOIDENABLE = VISA_VOIDENABLE;
//        }
//
//        public String getMASTER_ISSNAME() {
//            return MASTER_ISSNAME;
//        }
//
//        public void setMASTER_ISSNAME(String MASTER_ISSNAME) {
//            this.MASTER_ISSNAME = MASTER_ISSNAME;
//        }
//
//        public String getReponsemessage() {
//            return Reponsemessage;
//        }
//
//        public void setReponsemessage(String reponsemessage) {
//            Reponsemessage = reponsemessage;
//        }
//
//        public String getHIDECARDHOLDERNAME() {
//            return HIDECARDHOLDERNAME;
//        }
//
//        public void setHIDECARDHOLDERNAME(String HIDECARDHOLDERNAME) {
//            this.HIDECARDHOLDERNAME = HIDECARDHOLDERNAME;
//        }
//
//        public String getMERCHANTNAME() {
//            return MERCHANTNAME;
//        }
//
//        public void setMERCHANTNAME(String MERCHANTNAME) {
//            this.MERCHANTNAME = MERCHANTNAME;
//        }
//
//        public String getJCB_MOD10() {
//            return JCB_MOD10;
//        }
//
//        public void setJCB_MOD10(String JCB_MOD10) {
//            this.JCB_MOD10 = JCB_MOD10;
//        }
//
//        public String getPRINTDECLINEDRCPT() {
//            return PRINTDECLINEDRCPT;
//        }
//
//        public void setPRINTDECLINEDRCPT(String PRINTDECLINEDRCPT) {
//            this.PRINTDECLINEDRCPT = PRINTDECLINEDRCPT;
//        }
//
//        public String getUPI_LAST4DSMART() {
//            return UPI_LAST4DSMART;
//        }
//
//        public void setUPI_LAST4DSMART(String UPI_LAST4DSMART) {
//            this.UPI_LAST4DSMART = UPI_LAST4DSMART;
//        }
//
//        public String getADVCASHENABLE() {
//            return ADVCASHENABLE;
//        }
//
//        public void setADVCASHENABLE(String ADVCASHENABLE) {
//            this.ADVCASHENABLE = ADVCASHENABLE;
//        }
//
//        public String getAMEX_LAST4DMAG() {
//            return AMEX_LAST4DMAG;
//        }
//
//        public void setAMEX_LAST4DMAG(String AMEX_LAST4DMAG) {
//            this.AMEX_LAST4DMAG = AMEX_LAST4DMAG;
//        }
//
//        public String getUPI_VOIDENABLE() {
//            return UPI_VOIDENABLE;
//        }
//
//        public void setUPI_VOIDENABLE(String UPI_VOIDENABLE) {
//            this.UPI_VOIDENABLE = UPI_VOIDENABLE;
//        }
//
//        public String getPREAUTHBATCHMAXCOUNT() {
//            return PREAUTHBATCHMAXCOUNT;
//        }
//
//        public void setPREAUTHBATCHMAXCOUNT(String PREAUTHBATCHMAXCOUNT) {
//            this.PREAUTHBATCHMAXCOUNT = PREAUTHBATCHMAXCOUNT;
//        }
//
//        public String getADVICEENABLED() {
//            return ADVICEENABLED;
//        }
//
//        public void setADVICEENABLED(String ADVICEENABLED) {
//            this.ADVICEENABLED = ADVICEENABLED;
//        }
//
//        public String getTERMFORCEDLBPMANUALENABLE() {
//            return TERMFORCEDLBPMANUALENABLE;
//        }
//
//        public void setTERMFORCEDLBPMANUALENABLE(String TERMFORCEDLBPMANUALENABLE) {
//            this.TERMFORCEDLBPMANUALENABLE = TERMFORCEDLBPMANUALENABLE;
//        }
//
//        public String getMASTER_ADVCASHENABLE() {
//            return MASTER_ADVCASHENABLE;
//        }
//
//        public void setMASTER_ADVCASHENABLE(String MASTER_ADVCASHENABLE) {
//            this.MASTER_ADVCASHENABLE = MASTER_ADVCASHENABLE;
//        }
//
//        public String getPAYPAK_PREAUTHENABLE() {
//            return PAYPAK_PREAUTHENABLE;
//        }
//
//        public void setPAYPAK_PREAUTHENABLE(String PAYPAK_PREAUTHENABLE) {
//            this.PAYPAK_PREAUTHENABLE = PAYPAK_PREAUTHENABLE;
//        }
//
//        public String getAMEX_VOIDENABLE() {
//            return AMEX_VOIDENABLE;
//        }
//
//        public void setAMEX_VOIDENABLE(String AMEX_VOIDENABLE) {
//            this.AMEX_VOIDENABLE = AMEX_VOIDENABLE;
//        }
//
//        public String getPAYPAK_PINBYPASS() {
//            return PAYPAK_PINBYPASS;
//        }
//
//        public void setPAYPAK_PINBYPASS(String PAYPAK_PINBYPASS) {
//            this.PAYPAK_PINBYPASS = PAYPAK_PINBYPASS;
//        }
//
//        public String getAMEX_BANKFUNDED() {
//            return AMEX_BANKFUNDED;
//        }
//
//        public void setAMEX_BANKFUNDED(String AMEX_BANKFUNDED) {
//            this.AMEX_BANKFUNDED = AMEX_BANKFUNDED;
//        }
//
//        public String getMERCHANTADDRESS3() {
//            return MERCHANTADDRESS3;
//        }
//
//        public void setMERCHANTADDRESS3(String MERCHANTADDRESS3) {
//            this.MERCHANTADDRESS3 = MERCHANTADDRESS3;
//        }
//
//        public String getMERCHANTADDRESS2() {
//            return MERCHANTADDRESS2;
//        }
//
//        public void setMERCHANTADDRESS2(String MERCHANTADDRESS2) {
//            this.MERCHANTADDRESS2 = MERCHANTADDRESS2;
//        }
//
//        public String getMERCHANTADDRESS1() {
//            return MERCHANTADDRESS1;
//        }
//
//        public void setMERCHANTADDRESS1(String MERCHANTADDRESS1) {
//            this.MERCHANTADDRESS1 = MERCHANTADDRESS1;
//        }
//
//        public String getJCB_PREAUTHENABLE() {
//            return JCB_PREAUTHENABLE;
//        }
//
//        public void setJCB_PREAUTHENABLE(String JCB_PREAUTHENABLE) {
//            this.JCB_PREAUTHENABLE = JCB_PREAUTHENABLE;
//        }
//
//        public String getUPI_CHECKPIN() {
//            return UPI_CHECKPIN;
//        }
//
//        public void setUPI_CHECKPIN(String UPI_CHECKPIN) {
//            this.UPI_CHECKPIN = UPI_CHECKPIN;
//        }
//
//        public String getUSERID() {
//            return USERID;
//        }
//
//        public void setUSERID(String USERID) {
//            this.USERID = USERID;
//        }
//
//        public String getTERMINALMANUALENTRY() {
//            return TERMINALMANUALENTRY;
//        }
//
//        public void setTERMINALMANUALENTRY(String TERMINALMANUALENTRY) {
//            this.TERMINALMANUALENTRY = TERMINALMANUALENTRY;
//        }
//
//        public String getECRENABLED() {
//            return ECRENABLED;
//        }
//
//        public void setECRENABLED(String ECRENABLED) {
//            this.ECRENABLED = ECRENABLED;
//        }
//
//        public String getVISA_CHSERVCODE() {
//            return VISA_CHSERVCODE;
//        }
//
//        public void setVISA_CHSERVCODE(String VISA_CHSERVCODE) {
//            this.VISA_CHSERVCODE = VISA_CHSERVCODE;
//        }
//
//        public String getMERCHANTADDRESS4() {
//            return MERCHANTADDRESS4;
//        }
//
//        public void setMERCHANTADDRESS4(String MERCHANTADDRESS4) {
//            this.MERCHANTADDRESS4 = MERCHANTADDRESS4;
//        }
//
//        public String getVISA_CHECKEXPIRY() {
//            return VISA_CHECKEXPIRY;
//        }
//
//        public void setVISA_CHECKEXPIRY(String VISA_CHECKEXPIRY) {
//            this.VISA_CHECKEXPIRY = VISA_CHECKEXPIRY;
//        }
//
//        public String getPINTIMEOUT() {
//            return PINTIMEOUT;
//        }
//
//        public void setPINTIMEOUT(String PINTIMEOUT) {
//            this.PINTIMEOUT = PINTIMEOUT;
//        }
//
//        public String getPREDIALENABLED() {
//            return PREDIALENABLED;
//        }
//
//        public void setPREDIALENABLED(String PREDIALENABLED) {
//            this.PREDIALENABLED = PREDIALENABLED;
//        }
//
//        public String getPRINTSERIALNUM() {
//            return PRINTSERIALNUM;
//        }
//
//        public void setPRINTSERIALNUM(String PRINTSERIALNUM) {
//            this.PRINTSERIALNUM = PRINTSERIALNUM;
//        }
//
//        public String getPANLENGTHMIN() {
//            return PANLENGTHMIN;
//        }
//
//        public void setPANLENGTHMIN(String PANLENGTHMIN) {
//            this.PANLENGTHMIN = PANLENGTHMIN;
//        }
//
//        public String getPOWERDOWNTIME() {
//            return POWERDOWNTIME;
//        }
//
//        public void setPOWERDOWNTIME(String POWERDOWNTIME) {
//            this.POWERDOWNTIME = POWERDOWNTIME;
//        }
//
//        public String getSETTLERANGE() {
//            return SETTLERANGE;
//        }
//
//        public void setSETTLERANGE(String SETTLERANGE) {
//            this.SETTLERANGE = SETTLERANGE;
//        }
//
//        public String getMASTER_CHECKEXPIRY() {
//            return MASTER_CHECKEXPIRY;
//        }
//
//        public void setMASTER_CHECKEXPIRY(String MASTER_CHECKEXPIRY) {
//            this.MASTER_CHECKEXPIRY = MASTER_CHECKEXPIRY;
//        }
//
//        public String getAMEX_ADJUSTENABLE() {
//            return AMEX_ADJUSTENABLE;
//        }
//
//        public void setAMEX_ADJUSTENABLE(String AMEX_ADJUSTENABLE) {
//            this.AMEX_ADJUSTENABLE = AMEX_ADJUSTENABLE;
//        }
//
//        public String getPAYPAK_MANUALENTRY() {
//            return PAYPAK_MANUALENTRY;
//        }
//
//        public void setPAYPAK_MANUALENTRY(String PAYPAK_MANUALENTRY) {
//            this.PAYPAK_MANUALENTRY = PAYPAK_MANUALENTRY;
//        }
//
//        public String getJCB_ISSUERACTIVATED() {
//            return JCB_ISSUERACTIVATED;
//        }
//
//        public void setJCB_ISSUERACTIVATED(String JCB_ISSUERACTIVATED) {
//            this.JCB_ISSUERACTIVATED = JCB_ISSUERACTIVATED;
//        }
//
//        public String getSRCADDTPDU() {
//            return SRCADDTPDU;
//        }
//
//        public void setSRCADDTPDU(String SRCADDTPDU) {
//            this.SRCADDTPDU = SRCADDTPDU;
//        }
//
//        public String getINITTERM() {
//            return INITTERM;
//        }
//
//        public void setINITTERM(String INITTERM) {
//            this.INITTERM = INITTERM;
//        }
//
//        public String getPRIMARYPORT() {
//            return PRIMARYPORT;
//        }
//
//        public void setPRIMARYPORT(String PRIMARYPORT) {
//            this.PRIMARYPORT = PRIMARYPORT;
//        }
//
//        public String getISTPDUENABLE() {
//            return ISTPDUENABLE;
//        }
//
//        public void setISTPDUENABLE(String ISTPDUENABLE) {
//            this.ISTPDUENABLE = ISTPDUENABLE;
//        }
//
//        public String getVISA_ACQUIRER() {
//            return VISA_ACQUIRER;
//        }
//
//        public void setVISA_ACQUIRER(String VISA_ACQUIRER) {
//            this.VISA_ACQUIRER = VISA_ACQUIRER;
//        }
//
//        public String getAMEX_ACQUIRER() {
//            return AMEX_ACQUIRER;
//        }
//
//        public void setAMEX_ACQUIRER(String AMEX_ACQUIRER) {
//            this.AMEX_ACQUIRER = AMEX_ACQUIRER;
//        }
//
//        public String getAMEX_CHECKPIN() {
//            return AMEX_CHECKPIN;
//        }
//
//        public void setAMEX_CHECKPIN(String AMEX_CHECKPIN) {
//            this.AMEX_CHECKPIN = AMEX_CHECKPIN;
//        }
//
//        public String getBATCHMAXCOUNT() {
//            return BATCHMAXCOUNT;
//        }
//
//        public void setBATCHMAXCOUNT(String BATCHMAXCOUNT) {
//            this.BATCHMAXCOUNT = BATCHMAXCOUNT;
//        }
//
//        public String getPRIMARYMEDIUM() {
//            return PRIMARYMEDIUM;
//        }
//
//        public void setPRIMARYMEDIUM(String PRIMARYMEDIUM) {
//            this.PRIMARYMEDIUM = PRIMARYMEDIUM;
//        }
//
//        public String getVISA_CHECKPIN() {
//            return VISA_CHECKPIN;
//        }
//
//        public void setVISA_CHECKPIN(String VISA_CHECKPIN) {
//            this.VISA_CHECKPIN = VISA_CHECKPIN;
//        }
//
//        public String getMASTER_ISSNUM() {
//            return MASTER_ISSNUM;
//        }
//
//        public void setMASTER_ISSNUM(String MASTER_ISSNUM) {
//            this.MASTER_ISSNUM = MASTER_ISSNUM;
//        }
//
//        public String getPASSWORDSYSTEM() {
//            return PASSWORDSYSTEM;
//        }
//
//        public void setPASSWORDSYSTEM(String PASSWORDSYSTEM) {
//            this.PASSWORDSYSTEM = PASSWORDSYSTEM;
//        }
//
//        public String getJCB_ISSNAME() {
//            return JCB_ISSNAME;
//        }
//
//        public void setJCB_ISSNAME(String JCB_ISSNAME) {
//            this.JCB_ISSNAME = JCB_ISSNAME;
//        }
//
//        public String getTMSPORT() {
//            return TMSPORT;
//        }
//
//        public void setTMSPORT(String TMSPORT) {
//            this.TMSPORT = TMSPORT;
//        }
//
//        public String getINTERPACKETTIMEOUT() {
//            return INTERPACKETTIMEOUT;
//        }
//
//        public void setINTERPACKETTIMEOUT(String INTERPACKETTIMEOUT) {
//            this.INTERPACKETTIMEOUT = INTERPACKETTIMEOUT;
//        }
//
//        public String getPRINTCARDREMRCPT() {
//            return PRINTCARDREMRCPT;
//        }
//
//        public void setPRINTCARDREMRCPT(String PRINTCARDREMRCPT) {
//            this.PRINTCARDREMRCPT = PRINTCARDREMRCPT;
//        }
//
//        public String getAMEX_PINBYPASS() {
//            return AMEX_PINBYPASS;
//        }
//
//        public void setAMEX_PINBYPASS(String AMEX_PINBYPASS) {
//            this.AMEX_PINBYPASS = AMEX_PINBYPASS;
//        }
//
//        public String getTERMINALWIFINETMASK() {
//            return TERMINALWIFINETMASK;
//        }
//
//        public void setTERMINALWIFINETMASK(String TERMINALWIFINETMASK) {
//            this.TERMINALWIFINETMASK = TERMINALWIFINETMASK;
//        }
//
//        public String getJCB_LAST4DSMART() {
//            return JCB_LAST4DSMART;
//        }
//
//        public void setJCB_LAST4DSMART(String JCB_LAST4DSMART) {
//            this.JCB_LAST4DSMART = JCB_LAST4DSMART;
//        }
//
//        public String getAMEX_CHSIGN() {
//            return AMEX_CHSIGN;
//        }
//
//        public void setAMEX_CHSIGN(String AMEX_CHSIGN) {
//            this.AMEX_CHSIGN = AMEX_CHSIGN;
//        }
//
//        public String getPAYPAK_CHSERVCODE() {
//            return PAYPAK_CHSERVCODE;
//        }
//
//        public void setPAYPAK_CHSERVCODE(String PAYPAK_CHSERVCODE) {
//            this.PAYPAK_CHSERVCODE = PAYPAK_CHSERVCODE;
//        }
//
//        public String getTERMINALTOTCURRENCIES() {
//            return TERMINALTOTCURRENCIES;
//        }
//
//        public void setTERMINALTOTCURRENCIES(String TERMINALTOTCURRENCIES) {
//            this.TERMINALTOTCURRENCIES = TERMINALTOTCURRENCIES;
//        }
//
//        public String getISDEBUGENABLE() {
//            return ISDEBUGENABLE;
//        }
//
//        public void setISDEBUGENABLE(String ISDEBUGENABLE) {
//            this.ISDEBUGENABLE = ISDEBUGENABLE;
//        }
//
//        public String getPAYPAK_LAST4DMAG() {
//            return PAYPAK_LAST4DMAG;
//        }
//
//        public void setPAYPAK_LAST4DMAG(String PAYPAK_LAST4DMAG) {
//            this.PAYPAK_LAST4DMAG = PAYPAK_LAST4DMAG;
//        }
//
//        public String getPSTNINITNUMBER() {
//            return PSTNINITNUMBER;
//        }
//
//        public void setPSTNINITNUMBER(String PSTNINITNUMBER) {
//            this.PSTNINITNUMBER = PSTNINITNUMBER;
//        }
//
//        public String getJCB_COMPLETIONENABLE() {
//            return JCB_COMPLETIONENABLE;
//        }
//
//        public void setJCB_COMPLETIONENABLE(String JCB_COMPLETIONENABLE) {
//            this.JCB_COMPLETIONENABLE = JCB_COMPLETIONENABLE;
//        }
//
//        public String getAMOUNTENTRYTIMEOUT() {
//            return AMOUNTENTRYTIMEOUT;
//        }
//
//        public void setAMOUNTENTRYTIMEOUT(String AMOUNTENTRYTIMEOUT) {
//            this.AMOUNTENTRYTIMEOUT = AMOUNTENTRYTIMEOUT;
//        }
//
//        public String getWIFIPASSWORD() {
//            return WIFIPASSWORD;
//        }
//
//        public void setWIFIPASSWORD(String WIFIPASSWORD) {
//            this.WIFIPASSWORD = WIFIPASSWORD;
//        }
//
//        public String getTERMFORCEDLBPENABLE() {
//            return TERMFORCEDLBPENABLE;
//        }
//
//        public void setTERMFORCEDLBPENABLE(String TERMFORCEDLBPENABLE) {
//            this.TERMFORCEDLBPENABLE = TERMFORCEDLBPENABLE;
//        }
//
//        public String getHELPALFAMSG() {
//            return HELPALFAMSG;
//        }
//
//        public void setHELPALFAMSG(String HELPALFAMSG) {
//            this.HELPALFAMSG = HELPALFAMSG;
//        }
//
//        public String getUPI_BANKFUNDED() {
//            return UPI_BANKFUNDED;
//        }
//
//        public void setUPI_BANKFUNDED(String UPI_BANKFUNDED) {
//            this.UPI_BANKFUNDED = UPI_BANKFUNDED;
//        }
//
//        public String getTERMINALCURRENCY2() {
//            return TERMINALCURRENCY2;
//        }
//
//        public void setTERMINALCURRENCY2(String TERMINALCURRENCY2) {
//            this.TERMINALCURRENCY2 = TERMINALCURRENCY2;
//        }
//
//        public String getTERMINALCURRENCY1() {
//            return TERMINALCURRENCY1;
//        }
//
//        public void setTERMINALCURRENCY1(String TERMINALCURRENCY1) {
//            this.TERMINALCURRENCY1 = TERMINALCURRENCY1;
//        }
//
//        public String getTERMINALAPN() {
//            return TERMINALAPN;
//        }
//
//        public void setTERMINALAPN(String TERMINALAPN) {
//            this.TERMINALAPN = TERMINALAPN;
//        }
//
//        public String getJCB_PINBYPASS() {
//            return JCB_PINBYPASS;
//        }
//
//        public void setJCB_PINBYPASS(String JCB_PINBYPASS) {
//            this.JCB_PINBYPASS = JCB_PINBYPASS;
//        }
//
//        public String getMASTER_PRINTEXPIRY() {
//            return MASTER_PRINTEXPIRY;
//        }
//
//        public void setMASTER_PRINTEXPIRY(String MASTER_PRINTEXPIRY) {
//            this.MASTER_PRINTEXPIRY = MASTER_PRINTEXPIRY;
//        }

    // 0th entry in db is ubl configuration initial Single configuration
//    public static ProfileInit insertInitialConfig() {
//        return new ProfileInit(0, "200000005000508", "12345603");
//    }

    // 1st entry in db is UPI configuration initial Single configuration
   /* public static TerminalConfig insertUPIConfig(){
        return new TerminalConfig(1,"200000005000510","42017890",
                "UPI TEST HOST", "202.61.40.171","7006","444","N");// initial UPI config
    }*/

//    public static List<ProfileInit> terminalConfigList(){
//        List<ProfileInit> terminalConfigs = new ArrayList<>();
//       // terminalConfigs.add(0,insertInitialConfig());
//        //terminalConfigs.add(1,insertUPIConfig());
//        return terminalConfigs;
//    }





/* @Ignore
    public TerminalConfig(int tcid, String merchantID, String terminalID, String merchantName, String hostIp, String hostPort, String niiNum, String tipEnabled) {
        this.tcid = tcid;
        this.merchantID = merchantID;
        this.terminalID = terminalID;
        this.merchantName = merchantName;
        this.hostIp = hostIp;
        this.hostPort = hostPort;
        this.niiNum = niiNum;
        this.tipEnabled = tipEnabled;
    }*/

    public TerminalConfig(int tcid, String merchantID, String terminalID, String merchantName, String niiNum, String tipEnabled,
                          String comEthernet, String comGPRS, String enableLogs, String marketSeg, String tipThershold, String fallbackEnable, String msgHeaderLenHex,
                          String sslEnable, String topUpEnable, String manualEntry, String autoSettlement, String emvLogs, String autoSettleTime, String amexID, String pinByPass, String mod10,
                          String checkExpiry, String manualEntryIssuer, String last4digitMag, String last4digitIcc, String acquirer, String lowBIN, String highBIN, String issuer,
                          String currency, String userPassword, String managerPassword, String systemuserPassword, String superuserPassword, String headerLine1,
                          String headerLine2, String headerLine3, String headerLine4, String footerLine1, String footerLine2, String priMedium, String secMdeium,
                          String tmsIp, String tmsPort, String batchNo, String hostIp, String hostPort, String secIp, String secPort,

                          String cvmLimitVisa,
                          String cvmLimitMc,
                          String cvmLimitUpi,
                          String cvmLimitPaypak,
                          String cvmLimitJcb,
                          String cvmLimitAmex,

                          String saleOfflineLimit, String saleMinAmtLimit, String saleMaxAmtLimit,

                          String vepsLimit,
                          String vepsLimitMc,
                          String vepsLimitUpi,
                          String vepsLimitPaypak,
                          String vepsLimitJcb,
                          String vepsLimitAmex,

                          String PortalUrl,
                          String PortalUserName,
                          String PortalPassword,
                          String PortalTimeout,
                          String PortalTxnUpload,
                          int settleCounter,
                          int settleRange,
                          int tipNumAdjust,
                          String ecrEnabled,
                          String ecrType
    ) {
        this.tcid = tcid;
        this.merchantID = merchantID;
        this.terminalID = terminalID;
        this.merchantName = merchantName;
        this.niiNum = niiNum;
        this.tipEnabled = tipEnabled;
        this.comEthernet = comEthernet;
        this.comGPRS = comGPRS;
        this.enableLogs = enableLogs;
        this.marketSeg = marketSeg;
        this.tipThershold = tipThershold;
        this.fallbackEnable = fallbackEnable;
        this.msgHeaderLenHex = msgHeaderLenHex;
        this.sslEnable = sslEnable;
        this.topUpEnable = topUpEnable;
        this.manualEntry = manualEntry;
        this.autoSettlement = autoSettlement;
        this.emvLogs = emvLogs;
        this.autoSettleTime = autoSettleTime;
        this.amexID = amexID;
        this.pinByPass = pinByPass;
        this.mod10 = mod10;
        this.checkExpiry = checkExpiry;
        this.manualEntryIssuer = manualEntryIssuer;
        this.last4digitMag = last4digitMag;
        this.last4digitIcc = last4digitIcc;
        this.acquirer = acquirer;
        this.lowBIN = lowBIN;
        this.highBIN = highBIN;
        this.issuer = issuer;
        this.currency = currency;
        this.userPassword = userPassword;
        this.managerPassword = managerPassword;
        this.systemuserPassword = systemuserPassword;
        this.superuserPassword = superuserPassword;
        this.headerLine1 = headerLine1;
        this.headerLine2 = headerLine2;
        this.headerLine3 = headerLine3;
        this.headerLine4 = headerLine4;
        this.footerLine1 = footerLine1;
        this.footerLine2 = footerLine2;
        this.priMedium = priMedium;
        this.secMdeium = secMdeium;
        this.batchNo = batchNo;
        this.tmsIp = tmsIp;
        this.tmsPort = tmsPort;
        this.hostIp = hostIp;
        this.hostPort = hostPort;
        this.secIp = secIp;
        this.secPort = secPort;

        this.cvmLimitVisa = cvmLimitVisa;
        this.cvmLimitMc = cvmLimitMc;
        this.cvmLimitUpi = cvmLimitUpi;
        this.cvmLimitPaypak = cvmLimitPaypak;
        this.cvmLimitJcb = cvmLimitJcb;
        this.cvmLimitAmex = cvmLimitAmex;

        this.saleOfflineLimit = saleOfflineLimit;
        this.saleMinAmtLimit = saleMinAmtLimit;
        this.saleMaxAmtLimit = saleMaxAmtLimit;

        this.vepsLimit = vepsLimit;
        this.vepsLimitMc = vepsLimitMc;
        this.vepsLimitUpi = vepsLimitUpi;
        this.vepsLimitPaypak = vepsLimitPaypak;
        this.vepsLimitJcb = vepsLimitJcb;
        this.vepsLimitAmex = vepsLimitAmex;

        this.PortalUrl = PortalUrl;
        this.PortalUserName = PortalUserName;
        this.PortalPassword = PortalPassword;
        this.PortalTimeout = PortalTimeout;
        this.PortalTxnUpload = PortalTxnUpload;
        this.settleCounter = settleCounter;
        this.settleRange = settleRange;
        this.tipNumAdjust = tipNumAdjust;
        this.ecrEnabled = ecrEnabled;
        this.ecrType = ecrType;

    }

    // 0th entry in db is ubl configuration initial Single configuration
    public static TerminalConfig insertInitialConfig() {
        Utility.DEBUG_LOG("TerminalConfig", "+ insertInitialConfig +");
        return new TerminalConfig(0, "200000005010234", "21038505",
                "SIT Terminal, ", "555", "N", "COM_ETHERNET", "COM_GPRS",
                "ENABLE_LOGS", "R", "2.5", "FALLBACK_ENABLE", "MSGHEADER_LENHEX",
                "Y", "TOPUP_ENABLE", "MANUAL_ENTRY", "N", "EMV_LOGS", "2359",
                "AMEX_ID", "PIN_BYPASS", "MOD10", "CHECK_EXPIRY", "MANUAL_ENTRY_ISSUER", "LAST_4DIGIT_MAG",
                "LAST_4DIGIT_ICC", "ACQUIRER", "LOW_BIN", "HIGH_BIN", "ISSUER", "CURRENCY", "4848",
                "4848", "4848", "4848", " X990",
                "Bank Alfalah", "Lahore", "Lahore", "FOOTER_LINE_1", "FOOTER_LINE_2", "PRI_MEDIUM", "SEC_MEDIUM",
                "TMS_IP", "TMS_PORT", "21", "103.55.136.68", "5225", "103.55.136.68", "5225",
                "3000.00",
                "2999.99",
                "3000.00",
                "3000.00",
                "3000.00",
                "3000.00",
                "0.00", "0.00", "999999.99",
                "3000.00",
                "50.00",
                ".00",
                "0.00",
                "0.00",
                "0.00",

                "https://103.232.225.125/BAFL-PORTAL",
                "bafl.service",
                "bafl.service!",
                "3",
                "Y",
                0,
                3,
                2,
                "Y",
                "U"
        );
//        return new TerminalConfig(0, "200000005000508", "12345603",
//                "SIT Terminal, X990 " , "636", "N", "COM_ETHERNET", "COM_GPRS",
//                "ENABLE_LOGS", "G", "TIP_THRESHOLD", "FALLBACK_ENABLE", "MSGHEADER_LENHEX",
//                "SSL_ENABLE", "TOPUP_ENABLE", "MANUAL_ENTRY", "AUTO_SETTLEMENT", "EMV_LOGS", "AUTO_SETTLETIME",
//                "AMEX_ID", "PIN_BYPASS", "MOD10", "CHECK_EXPIRY", "MANUAL_ENTRY_ISSUER", "LAST_4DIGIT_MAG",
//                "LAST_4DIGIT_ICC", "ACQUIRER", "LOW_BIN", "HIGH_BIN", "ISSUER", "CURRENCY", "USER_PASSWORD_CHANGE",
//                "MANAGER_PASSWORD_CHANGE", "SYSTEM_PASSWORD_CHANGE", "SUPER_USER_PASSWORD_CHANGE", "HEADER_LINE_1",
//                "HEADER_LINE_2", "HEADER_LINE_3", "HEADER_LINE_4", "FOOTER_LINE_1", "FOOTER_LINE_2", "PRI_MEDIUM", "SEC_MEDIUM",
//                "TMS_IP", "TMS_PORT", "21" ,"202.59.254.155", "7188", "202.59.254.155", "7188", "300.00", "0.00", "0.00", "999999.99", "1500.00");
    }

    public static List<TerminalConfig> terminalConfigList() {
        List<TerminalConfig> terminalConfigs = new ArrayList<>();
        terminalConfigs.add(0, insertInitialConfig());
        //terminalConfigs.add(1,insertUPIConfig());
        return terminalConfigs;
    }

    public int getTcid() {
        return tcid;
    }

    public void setTcid(int tcid) {
        this.tcid = tcid;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    public String getHostPort() {
        return hostPort;
    }

    public void setHostPort(String hostPort) {
        this.hostPort = hostPort;
    }

    public String getNiiNum() {
        return niiNum;
    }

    public void setNiiNum(String niiNum) {
        this.niiNum = niiNum;
    }

    public String getTipEnabled() {
        return tipEnabled;
    }

    public void setTipEnabled(String tipEnabled) {
        this.tipEnabled = tipEnabled;
    }

    public String getComEthernet() {
        return comEthernet;
    }

    public void setComEthernet(String comEthernet) {
        this.comEthernet = comEthernet;
    }

    public String getComGPRS() {
        return comGPRS;
    }

    public void setComGPRS(String comGPRS) {
        this.comGPRS = comGPRS;
    }

    public String getEnableLogs() {
        return enableLogs;
    }

    public void setEnableLogs(String enableLogs) {
        this.enableLogs = enableLogs;
    }

    public String getMarketSeg() {
        return marketSeg;
    }

    public void setMarketSeg(String marketSeg) {
        this.marketSeg = marketSeg;
    }

    public String getTipThershold() {
        return tipThershold;
    }

    public void setTipThershold(String tipThershold) {
        this.tipThershold = tipThershold;
    }

    public String getFallbackEnable() {
        return fallbackEnable;
    }

    public void setFallbackEnable(String fallbackEnable) {
        this.fallbackEnable = fallbackEnable;
    }

    public String getMsgHeaderLenHex() {
        return msgHeaderLenHex;
    }

    public void setMsgHeaderLenHex(String msgHeaderLenHex) {
        this.msgHeaderLenHex = msgHeaderLenHex;
    }

    public String getSslEnable() {
        return sslEnable;
    }

    public void setSslEnable(String sslEnable) {
        this.sslEnable = sslEnable;
    }

    public String getTopUpEnable() {
        return topUpEnable;
    }

    public void setTopUpEnable(String topUpEnable) {
        this.topUpEnable = topUpEnable;
    }

    public String getManualEntry() {
        return manualEntry;
    }

    public void setManualEntry(String manualEntry) {
        this.manualEntry = manualEntry;
    }

    public String getAutoSettlement() {
        return autoSettlement;
    }

    public void setAutoSettlement(String autoSettlement) {
        this.autoSettlement = autoSettlement;
    }

    public String getEmvLogs() {
        return this.emvLogs;
    }

    public void setEmvLogs(String emvLogs) {
        this.emvLogs = emvLogs;
    }

    public String getAutoSettleTime() {
        return autoSettleTime;
    }

    public void setAutoSettleTime(String autoSettleTime) {
        this.autoSettleTime = autoSettleTime;
    }

    public String getAmexID() {
        return amexID;
    }

    public void setAmexID(String amexID) {
        this.amexID = amexID;
    }

    public String getPinByPass() {
        return pinByPass;
    }

    public void setPinByPass(String pinByPass) {
        this.pinByPass = pinByPass;
    }

    public String getMod10() {
        return mod10;
    }

    public void setMod10(String mod10) {
        this.mod10 = mod10;
    }

    public String getCheckExpiry() {
        return checkExpiry;
    }

    public void setCheckExpiry(String checkExpiry) {
        this.checkExpiry = checkExpiry;
    }

    public String getManualEntryIssuer() {
        return manualEntryIssuer;
    }

    public void setManualEntryIssuer(String manualEntryIssuer) {
        this.manualEntryIssuer = manualEntryIssuer;
    }

    public String getLast4digitMag() {
        return last4digitMag;
    }

    public void setLast4digitMag(String las) {
        this.last4digitMag = last4digitMag;
    }

    public String getLast4digitIcc() {
        return last4digitIcc;
    }

    public void setLast4digitIcc(String last4digitIcc) {
        this.last4digitIcc = last4digitIcc;
    }

    public String getAcquirer() {
        return acquirer;
    }

    public void setAcquirer(String acquirer) {
        this.acquirer = acquirer;
    }

    public String getLowBIN() {
        return lowBIN;
    }

    public void setLowBIN(String lowBIN) {
        this.lowBIN = lowBIN;
    }

    public String getHighBIN() {
        return highBIN;
    }

    public void setHighBIN(String highBIN) {
        this.highBIN = highBIN;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getuserPassword() {
        return userPassword;
    }

    public void setuserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getmanagerPassword() {
        return managerPassword;
    }

    public void setmanagerPassword(String managerPassword) {
        this.managerPassword = managerPassword;
    }

    public String getSystemuserPassword() {
        return systemuserPassword;
    }

    public void setSystemuserPassword(String systemuserPassword) {
        this.systemuserPassword = systemuserPassword;
    }

    public String getSuperuserPassword() {
        return superuserPassword;
    }

    public void setSuperuserPassword(String superuserPassword) {
        this.superuserPassword = superuserPassword;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getManagerPassword() {
        return managerPassword;
    }

    public void setManagerPassword(String managerPassword) {
        this.managerPassword = managerPassword;
    }

    public String getHeaderLine1() {
        return headerLine1;
    }

    public void setHeaderLine1(String headerLine1) {
        this.headerLine1 = headerLine1;
    }

    public String getHeaderLine2() {
        return headerLine2;
    }

    public void setHeaderLine2(String headerLine2) {
        this.headerLine2 = headerLine2;
    }

    public String getHeaderLine3() {
        return headerLine3;
    }

    public void setHeaderLine3(String headerLine3) {
        this.headerLine3 = headerLine3;
    }

    public String getHeaderLine4() {
        return headerLine4;
    }

    public void setHeaderLine4(String headerLine4) {
        this.headerLine4 = headerLine4;
    }

    public String getFooterLine1() {
        return footerLine1;
    }

    public void setFooterLine1(String footerLine1) {
        this.footerLine1 = footerLine1;
    }

    public String getFooterLine2() {
        return footerLine2;
    }

    public void setFooterLine2(String footerLine2) {
        this.footerLine2 = footerLine2;
    }

    public String getPriMedium() {
        return priMedium;
    }

    public void setPriMedium(String priMedium) {
        this.priMedium = priMedium;
    }

    public String getSecMdeium() {
        return secMdeium;
    }

    public void setSecMdeium(String secMdeium) {
        this.secMdeium = secMdeium;
    }

    public String getBatchNo() {

        Utility.DEBUG_LOG("SL","Batch No" + batchNo);
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        Utility.DEBUG_LOG("SL", "Batch No" + batchNo);

        this.batchNo = batchNo;
    }

    public String getTmsIp() {
        return tmsIp;
    }

    public void setTmsIp(String tmsIp) {
        this.tmsIp = tmsIp;
    }

    public String getTmsPort() {
        return tmsPort;
    }

    public void setTmsPort(String tmsPort) {
        this.tmsPort = tmsPort;
    }

    public String getSecIp() {
        return secIp;
    }

    public void setSecIp(String secIp) {
        this.secIp = secIp;
    }

    public String getSecPort() {
        return secPort;
    }

    public void setSecPort(String secPort) {
        this.secPort = secPort;
    }

    public String getCvmLimitVisa() {
        return cvmLimitVisa;
    }

    public void setCvmLimitVisa(String cvmLimit) {
        this.cvmLimitVisa = cvmLimit;
    }

    public String getCvmLimitMc() {
        return cvmLimitMc;
    }

    public void setCvmLimitMc(String cvmLimit) {
        this.cvmLimitMc = cvmLimit;
    }

    public String getCvmLimitUpi() {
        return cvmLimitUpi;
    }

    public void setCvmLimitUpi(String cvmLimit) {
        this.cvmLimitUpi = cvmLimit;
    }

    public String getCvmLimitPaypak() {
        return cvmLimitPaypak;
    }

    public void setCvmLimitPaypak(String cvmLimit) {
        this.cvmLimitPaypak = cvmLimit;
    }

    public String getCvmLimitJcb() {
        return cvmLimitJcb;
    }

    public void setCvmLimitJcb(String cvmLimit) {
        this.cvmLimitJcb = cvmLimit;
    }

    public String getCvmLimitAmex() {
        return cvmLimitAmex;
    }

    public void setCvmLimitAmex(String cvmLimit) {
        this.cvmLimitAmex = cvmLimit;
    }

    public String getSaleOfflineLimit() {
        return saleOfflineLimit;
    }

    public void setSaleOfflineLimit(String saleOfflineLimit) {
        this.saleOfflineLimit = saleOfflineLimit;
    }

    public String getSaleMinAmtLimit() {
        return saleMinAmtLimit;
    }

    public void setSaleMinAmtLimit(String saleMinAmtLimit) {
        this.saleMinAmtLimit = saleMinAmtLimit;
    }

    public String getSaleMaxAmtLimit() {
        return saleMaxAmtLimit;
    }

    public void setSaleMaxAmtLimit(String saleMaxAmtLimit) {
        this.saleMaxAmtLimit = saleMaxAmtLimit;
    }

    public String getVepsLimit() {
        return vepsLimit;
    }

    public void setVepsLimit(String vepsLimit) {
        this.vepsLimit = vepsLimit;
    }

    public String getVepsLimitMc() {
        return vepsLimitMc;
    }

    public void setVepsLimitMc(String vepsLimit) {
        this.vepsLimitMc = vepsLimit;
    }

    public String getVepsLimitUpi() {
        return vepsLimitUpi;
    }

    public void setVepsLimitUpi(String vepsLimit) {
        this.vepsLimitUpi = vepsLimit;
    }

    public String getVepsLimitPaypak() {
        return vepsLimitPaypak;
    }

    public void setVepsLimitPaypak(String vepsLimit) {
        this.vepsLimitPaypak = vepsLimit;
    }

    public String getVepsLimitJcb() {
        return vepsLimitJcb;
    }

    public void setVepsLimitJcb(String vepsLimit) {
        this.vepsLimitJcb = vepsLimit;
    }

    public String getVepsLimitAmex() {
        return vepsLimitAmex;
    }

    public void setVepsLimitAmex(String vepsLimit) {
        this.vepsLimitAmex = vepsLimit;
    }

    public String getPortalUrl() {
        return PortalUrl;
    }

    public void setPortalUrl(String portalUrl) {
        PortalUrl = portalUrl;
    }

    public String getPortalUserName() {
        return PortalUserName;
    }

    public void setPortalUserName(String portalUserName) {
        PortalUserName = portalUserName;
    }

    public String getPortalPassword() {
        return PortalPassword;
    }

    public void setPortalPassword(String portalPassword) {
        PortalPassword = portalPassword;
    }

    public String getPortalTimeout() {
        return PortalTimeout;
    }

    public void setPortalTimeout(String portalTimeout) {
        PortalTimeout = portalTimeout;
    }

    public String getPortalTxnUpload() {
        return PortalTxnUpload;
    }

    // 1st entry in db is UPI configuration initial Single configuration
   /* public static TerminalConfig insertUPIConfig(){
        return new TerminalConfig(1,"200000005000510","42017890",
                "UPI TEST HOST", "202.61.40.171","7006","444","N");// initial UPI config
    }*/

    public void setPortalTxnUpload(String portalTxnUpload) {
        PortalTxnUpload = portalTxnUpload;
    }

    public int getSettleCounter() {
        return settleCounter;
    }

    public void setSettleCounter(int settleCounter) {
        this.settleCounter = settleCounter;
    }

    public int getSettleRange() {
        return settleRange;
    }

    public void setSettleRange(int settleRange) {
        this.settleRange = settleRange;
    }


    public int getTipNumAdjust() {
        return tipNumAdjust;
    }

    public void setTipNumAdjust(int tipNumAdjust) {
        this.tipNumAdjust = tipNumAdjust;
    }

    public String getEcrEnabled() {
        return ecrEnabled;
    }

    public void setEcrEnabled(String ecrEnabled) {
        this.ecrEnabled = ecrEnabled;
    }

    public String getEcrType() {
        return ecrType;
    }

    public void setEcrType(String ecrType) {
        this.ecrType = ecrType;
    }





}
