package cn.access.group.android_all_banks_pos.Utilities.canvas_printer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.util.Printer;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.vfi.smartpos.deviceservice.aidl.FontFamily;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.PrinterConfig;

import java.io.ByteArrayOutputStream;
import java.util.Hashtable;

/**
 * Created by Simon on 2019/4/10.
 *
 * Class {@code PrinterExBase} is for draw content on canvas
 * and the canvas can be set to printer or activity
 */

public class PrinterExBase {

    private static final String TAG = "PrinterExBase";
    public static final int MAX_WIDTH = 384;
    boolean isPaperReceipt = true;

    int offsetY;
    int offsetX;
    Canvas canvas;
    Bitmap bitmap;
    Paint paint;
    IPrinter printer;

    int MaxHeight = 1000;

    public PrinterExBase( boolean isPaperReceipt ) {
        this.isPaperReceipt = isPaperReceipt;

        Utility.DEBUG_LOG(TAG,"+ PrinterExBase +");
        offsetY = 0;
        offsetX = 0;
//        int MAX_HEIGHT = MAX_WIDTH * 3;

        paint = new Paint();
        paint.setStrokeWidth(1);
        paint.setTextSize(50);
//        paint.setColor(Color.BLACK);
        paint.setColor(Color.BLACK);
        //add underline when drawing text
//        paint.setUnderlineText(true);
        StringBuilder str = new StringBuilder();
        str.append("before Bitmap.createBitmap[");
        str.append(MAX_WIDTH);
        str.append(",");
        str.append(MaxHeight);
        str.append("]");
        Utility.DEBUG_LOG(TAG,str.toString());
//        bitmap = Bitmap.createBitmap(MAX_WIDTH, MAX_HEIGHT, Bitmap.Config.ALPHA_8);
        bitmap = Bitmap.createBitmap(MAX_WIDTH, MaxHeight, Bitmap.Config.ARGB_8888);
        if( isPaperReceipt ){
            bitmap.eraseColor(Color.WHITE);
        }

        canvas = new Canvas(bitmap);
//        writeRuler(0);
    }

    public int getMaxHeight() {
        return MaxHeight;
    }

    public int getOffsetY() {
        return offsetY;
    }

    public int addText(Bundle fontFormat, String text) throws RemoteException {
        String fontStyle = fontFormat.getString("fontStyle", "");
       // Utility.DEBUG_LOG("FontPrinter",text);

        //
        boolean isBoldFont = fontFormat.getBoolean("bold", false);
        boolean isNewLine = fontFormat.getBoolean("newline", true);
        int offset = fontFormat.getInt("offset", 0);
        int typeFaceStyle = Typeface.NORMAL;


        int fontSize = fontFormat.getInt(PrinterConfig.BUNDLE_PRINT_FONT);
        Utility.DEBUG_LOG(TAG,"default fontSize:"+fontSize);

        if (fontSize == FontFamily.MIDDLE) {
            Utility.DEBUG_LOG(TAG,"setting fontSize 24");
            fontSize = 24;
        }
        else if (fontSize == FontFamily.SMALL) {
            Utility.DEBUG_LOG(TAG,"setting fontSize 16");
            fontSize = 16;
        } else if (fontSize == FontFamily.BIG) {
            fontSize = 24;
//            entity.setMultipleHeight(2);
//            entity.setMultipleWidth(1);
            fontSize = 28;
            Utility.DEBUG_LOG(TAG,"setting fontSize 28");
            paint.setTextScaleX(0.5f);
            isBoldFont = true;
        } else if (fontSize == 3) {
            Utility.DEBUG_LOG(TAG,"setting fontSize 32");
            fontSize = 32;
        } else if (fontSize == 4) {
            Utility.DEBUG_LOG(TAG,"setting fontSize 32");
            fontSize = 32;
//            entity.setMultipleHeight(2);
//            entity.setMultipleWidth(1);
            isBoldFont = true;
        } else if (fontSize == 5) {
            Utility.DEBUG_LOG(TAG,"setting fontSize 48");
            fontSize = 48;
        }

        if( isBoldFont ){
            typeFaceStyle = Typeface.BOLD;
        }

        Utility.DEBUG_LOG(TAG, "font size:"+ fontSize+", Style:" + fontStyle );
        paint.setTypeface(Typeface.createFromFile(fontStyle));

        int alignType = fontFormat.getInt(PrinterConfig.BUNDLE_PRINT_ALIGN);
        Paint.Align align = Paint.Align.LEFT;
        int x = 0;
        if (alignType == FontFamily.CENTER) {
            align = Paint.Align.CENTER;
            x = MAX_WIDTH / 2;
        } else if (alignType == FontFamily.RIGHT) {
            align = Paint.Align.RIGHT;
            x = MAX_WIDTH;
        }

        paint.setTextSize(fontSize);
        paint.setTextAlign(align);
        offsetY += fontSize;
        int width = (int) paint.measureText( text,0, text.length() );
        Utility.DEBUG_LOG(TAG, "pixel "+width+" of " + text + ", try print at [" + offsetX + ", " + offsetY + "]");
        switch (alignType){
            case FontFamily.LEFT:
                if( width + offsetX > MAX_WIDTH ) {
                    Utility.DEBUG_LOG(TAG, "Over size, write in new line");
                    offsetY += fontSize;
                    offsetX = 0;
                } else {
                    offsetX += width;
                }
                break;
            case FontFamily.CENTER:
                if( width + offsetX*2 > MAX_WIDTH ) {
                    Utility.DEBUG_LOG(TAG, "Over size, write in new line");
                    offsetY += fontSize;
                    offsetX = 0;
                } else {
                    offsetX = (MAX_WIDTH/2+width/2);
                }
                break;
            case FontFamily.RIGHT:
                if( width + offsetX > MAX_WIDTH ) {
                    Utility.DEBUG_LOG(TAG, "Over size, write in new line");
                    offsetY += fontSize;
                    offsetX = 0;
                } else {
                    offsetX = MAX_WIDTH;
                }
                break;

        }
        canvas.drawText(text, x, offsetY, paint);
        if( isNewLine ) {
            offsetX = 0;
            offsetY += 2;
        } else {
            offsetY -= fontSize;
        }
        return offsetX;
    }

    public void addTextInLine(Bundle fontFormat, String left, String center, String right, int mode) throws RemoteException {
        // todo f
        printer.addTextInLine(fontFormat,left,center,right,mode);

    }
    public void addImage(Bundle format, byte[] imageData) throws RemoteException {
        Utility.DEBUG_LOG(TAG,"+ addImage + setting bitmap 1");
        Bitmap bitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length );
        addImage( format, bitmap );
    }
    public Bitmap convertImage(Bitmap bitmap){
        Utility.DEBUG_LOG(TAG,"+ convertImage + setting bitmap");

        if (bitmap != null) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    int color = bitmap.getPixel(j, i);
                    int g = Color.green(color);
                    int r = Color.red(color);
                    int b = Color.blue(color);
                    int a = Color.alpha(color);
                    if(g>=196&&r>=196&&b>=196){
                        a = 0;
                    }
                    color = Color.argb(a, r, g, b);
                    createBitmap.setPixel(j, i, color);
                }
            }
            return createBitmap;
        }
        return null;
    }
    public void addImage(Bundle format, Bitmap bitmap) {
        Utility.DEBUG_LOG(TAG,"+ addImage + setting bitmap 2");
        int offset = 0;
        if( null == bitmap ){
            Utility.DEBUG_LOG(TAG, "addImage:null" );
            return;
        }
        if( null != format ){
            offset = format.getInt("offset", 0);
            switch (format.getInt( PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT ) ){
                case PrinterConfig.addText.Alignment.CENTER:
                    offset = (MAX_WIDTH-bitmap.getWidth())/2;
                    break;
                case PrinterConfig.addText.Alignment.RIGHT:
                    offset = (MAX_WIDTH-bitmap.getWidth());
                    break;
            }
        }

        Utility.DEBUG_LOG(TAG,"isPaperReceipt:"+isPaperReceipt);

        Utility.DEBUG_LOG(TAG, "offset:" + offset );
        Utility.DEBUG_LOG(TAG, "offsetY:" + offsetY );
        if( !isPaperReceipt ){
            bitmap = convertImage(bitmap);
        }

        canvas.drawBitmap( bitmap, offset, offsetY, null );
        scrollDown( bitmap.getHeight() + 2);

    }
    public void feedline(int width ) throws RemoteException {
//        paint.setPathEffect(null);
//        paint.setStrokeWidth(width);
       // canvas.drawLine(1,offsetY,MAX_WIDTH-1, offsetY, paint );
        offsetY += width;
    }
    // draw horizontal line
    public void addLine(Bundle format, int width ) throws RemoteException {
        paint.setPathEffect(null);
        paint.setStrokeWidth(width);
        canvas.drawLine(1,offsetY,MAX_WIDTH-1, offsetY, paint );
        offsetY += width;
        offsetY += 2;
    }
    public void addQrCode(Bundle format, String qrCode){
        int size = format.getInt(PrinterConfig.addQrCode.Height.BundleName);
        Utility.DEBUG_LOG(TAG,"+ addQrCode + setting bitmap");
        Bitmap bitmap = create1D2DcodeImage(qrCode,size, BarcodeFormat.QR_CODE );
        addImage( format, bitmap );

    }
    public void addBarCode(Bundle format, String barcode){
        // BarcodeFormat.CODE_128
        int size = format.getInt(PrinterConfig.addBarCode.Height.BundleName);
        Utility.DEBUG_LOG(TAG,"+ addBarCode + setting bitmap");
        Bitmap bitmap = create1D2DcodeImage(barcode,size, BarcodeFormat.CODE_128 );
        addImage( format, bitmap );
    }

    public void feedPixel(Bundle format, int pixel ) throws RemoteException {
        offsetY += pixel;
    }
    public void writeRuler( int mode ){
        int x = 0;
        int y = 0;
        paint.setStrokeWidth(1);
        int c = paint.getColor();
        paint.setColor(Color.GRAY);
        paint.setPathEffect(new DashPathEffect(new float[]{16f,48f}, 0));
        for( x = -8; x <= MAX_WIDTH;  ){
            canvas.drawLine(x,0,x, MAX_WIDTH, paint );
            canvas.drawLine(0,x,MAX_WIDTH, x, paint );
            x+= 32;
        }
        paint.setColor(c);
    }

    protected int lastScrollDownPixel = 0;
    public int scrollDown( int pixel ){
        offsetY += pixel;
        lastScrollDownPixel = pixel;
        return offsetY;
    }

    public int scrollBack(){
        int a = lastScrollDownPixel;
        offsetY -= lastScrollDownPixel;
        lastScrollDownPixel = 0;
        return a;
    }

    public Bitmap getBitmap(){
        Utility.DEBUG_LOG(TAG,"+ getBitmap + setting bitmap");
        Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0,0,MAX_WIDTH, offsetY);
        return newBitmap;
    }
    public int getHeight() {
        Utility.DEBUG_LOG(TAG,"getHeight:offsetY:"+offsetY);
        return offsetY;
    }

    public byte[] getData() {
        Utility.DEBUG_LOG(TAG,"+ getData +");
        Utility.DEBUG_LOG(TAG,"+ getData + setting bitmap");
        return getBytesByBitmap(bitmap);
    }

    public byte[] getBytesByBitmap(Bitmap bitmap) {
        Utility.DEBUG_LOG(TAG,"+ getBytesByBitmap +");
        Utility.DEBUG_LOG(TAG,"+ getBytesByBitmap + setting bitmap");
        Utility.DEBUG_LOG(TAG,"MAX_WIDTH:"+MAX_WIDTH);
        Utility.DEBUG_LOG(TAG,"offsetY:"+offsetY);
        Utility.DEBUG_LOG(TAG,"orignal bitmap.getWidth():"+bitmap.getWidth());
        Utility.DEBUG_LOG(TAG,"orignal bitmap.getHeight():"+bitmap.getHeight());
//        Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0,0,MAX_WIDTH, bitmap.getHeight());

        try {
            Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0, 1, MAX_WIDTH, offsetY);
            Utility.DEBUG_LOG(TAG,"newBitmap:"+newBitmap);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            newBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            return baos.toByteArray();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }



    }

    private Bitmap create1D2DcodeImage(String content, int size, BarcodeFormat barcodeFormat) {
        Utility.DEBUG_LOG(TAG, "create1D2DcodeImage, size:" + size + ", content:" + content);
        if( content.length() == 0 ){
            return null;
        }
        Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8" );
        hints.put(EncodeHintType.MARGIN, 1);
        if( size == 0 ){
            size = 360;
        }
        int QRCODE_SIZE = size;
        int QRCODE_SIZEf = size;
        int width = size;
        int height = size;
        if( barcodeFormat != BarcodeFormat.QR_CODE ){
            // bar code
            width = MAX_WIDTH - 16;
        }
        BitMatrix bitMatrix = null;
        try {
//                bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, QRCODE_SIZE, QRCODE_SIZE, hints);
            bitMatrix = new MultiFormatWriter().encode(content, barcodeFormat, width, height, hints);

            width = bitMatrix.getWidth();
            height = bitMatrix.getHeight();

            Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            b.eraseColor(Color.WHITE);
            for( int y=0; y< height ; y++){
                for( int x=0; x<width; x++){
                    if( bitMatrix.get(x,y)){
                        b.setPixel(x,y, Color.BLACK);
                    }
                }
            }
            return b;

        } catch (WriterException e) {
            e.printStackTrace();
        } catch ( Exception e ){
            e.printStackTrace();
        }
        return null;
    }



}

