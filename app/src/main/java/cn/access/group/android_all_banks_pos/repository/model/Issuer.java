package cn.access.group.android_all_banks_pos.repository.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

    @Entity(tableName = "Issuer")
    public class Issuer implements Serializable {
        @PrimaryKey()
        private int Issuerid;
        @ColumnInfo(name = "IssName")
        private String IssName;
        @ColumnInfo(name = "IssNum")
        private String IssNum;
        @ColumnInfo(name = "Acquirer")
        private String Acquirer;
        @ColumnInfo(name = "Scheme")
        private String Scheme;
        @ColumnInfo(name = "BankFunded")
        private String BankFunded;
        @ColumnInfo(name = "Mod10")
        private String Mod10;
        @ColumnInfo(name = "CheckExpiry")
        private String CheckExpiry;
        @ColumnInfo(name = "ManualEntry")
        private String ManualEntry;
        @ColumnInfo(name = "PrintExpiry")
        private String PrintExpiry;
        @ColumnInfo(name = "Last4DMag")
        private String Last4DMag;
        @ColumnInfo(name = "Last4DSmart")
        private String Last4DSmart;
        @ColumnInfo(name = "PINBypass")
        private String PINBypass;
        @ColumnInfo(name = "ChServCode")
        private String ChServCode;
        @ColumnInfo(name = "CheckPin")
        private String CheckPin;
        @ColumnInfo(name = "ChSign")
        private String ChSign;
        @ColumnInfo(name = "IssuerActivated")
        private String IssuerActivated;
        @ColumnInfo(name = "RefundEnable")
        private String RefundEnable;
        @ColumnInfo(name = "VoidEnable")
        private String VoidEnable;
        @ColumnInfo(name = "PreAuthEnable")
        private String PreAuthEnable;
        @ColumnInfo(name = "CompletionEnable")
        private String CompletionEnable;
        @ColumnInfo(name = "AdjustEnable")
        private String AdjustEnable;
        @ColumnInfo(name = "AdvCashEnable")
        private String AdvCashEnable;
        @ColumnInfo(name = "OrbitRedeemEnable")
        private String OrbitRedeemEnable;
        @ColumnInfo(name = "ForcedLBPEnable")
        private String ForcedLBPEnable;

        public Issuer(){

        }

        public Issuer(int issuerid, String issName, String issNum, String acquirer, String scheme, String bankFunded, String mod10, String checkExpiry, String manualEntry, String printExpiry, String last4DMag, String last4DSmart, String PINBypass, String chServCode, String checkPin, String chSign, String issuerActivated, String refundEnable, String voidEnable, String preAuthEnable, String completionEnable, String adjustEnable, String advCashEnable, String orbitRedeemEnable, String forcedLBPEnable) {
            this.Issuerid = issuerid;
            this.IssName = issName;
            this.IssNum = issNum;
            this.Acquirer = acquirer;
            this.Scheme = scheme;
            this.BankFunded = bankFunded;
            this.Mod10 = mod10;
            this.CheckExpiry = checkExpiry;
            this.ManualEntry = manualEntry;
            this.PrintExpiry = printExpiry;
            this.Last4DMag = last4DMag;
            this.Last4DSmart = last4DSmart;
            this.PINBypass = PINBypass;
            this.ChServCode = chServCode;
            this.CheckPin = checkPin;
            this.ChSign = chSign;
            this.IssuerActivated = issuerActivated;
            this.RefundEnable = refundEnable;
            this.VoidEnable = voidEnable;
            this.PreAuthEnable = preAuthEnable;
            this.CompletionEnable = completionEnable;
            this.AdjustEnable = adjustEnable;
            this.AdvCashEnable = advCashEnable;
            this.OrbitRedeemEnable = orbitRedeemEnable;
            this.ForcedLBPEnable = forcedLBPEnable;
        }

        public int getIssuerid() {
            return Issuerid;
        }

        public void setIssuerid(int issuerid) {
            Issuerid = issuerid;
        }

        public String getIssName() {
            return IssName;
        }

        public void setIssName(String issName) {
            IssName = issName;
        }

        public String getIssNum() {
            return IssNum;
        }

        public void setIssNum(String issNum) {
            IssNum = issNum;
        }

        public String getAcquirer() {
            return Acquirer;
        }

        public void setAcquirer(String acquirer) {
            Acquirer = acquirer;
        }

        public String getScheme() {
            return Scheme;
        }

        public void setScheme(String scheme) {
            Scheme = scheme;
        }

        public String getBankFunded() {
            return BankFunded;
        }

        public void setBankFunded(String bankFunded) {
            BankFunded = bankFunded;
        }

        public String getMod10() {
            return Mod10;
        }

        public void setMod10(String mod10) {
            Mod10 = mod10;
        }

        public String getCheckExpiry() {
            return CheckExpiry;
        }

        public void setCheckExpiry(String checkExpiry) {
            CheckExpiry = checkExpiry;
        }

        public String getManualEntry() {
            return ManualEntry;
        }

        public void setManualEntry(String manualEntry) {
            ManualEntry = manualEntry;
        }

        public String getPrintExpiry() {
            return PrintExpiry;
        }

        public void setPrintExpiry(String printExpiry) {
            PrintExpiry = printExpiry;
        }

        public String getLast4DMag() {
            return Last4DMag;
        }

        public void setLast4DMag(String last4DMag) {
            Last4DMag = last4DMag;
        }

        public String getLast4DSmart() {
            return Last4DSmart;
        }

        public void setLast4DSmart(String last4DSmart) {
            Last4DSmart = last4DSmart;
        }

        public String getPINBypass() {
            return PINBypass;
        }

        public void setPINBypass(String PINBypass) {
            this.PINBypass = PINBypass;
        }

        public String getChServCode() {
            return ChServCode;
        }

        public void setChServCode(String chServCode) {
            ChServCode = chServCode;
        }

        public String getCheckPin() {
            return CheckPin;
        }

        public void setCheckPin(String checkPin) {
            CheckPin = checkPin;
        }

        public String getChSign() {
            return ChSign;
        }

        public void setChSign(String chSign) {
            ChSign = chSign;
        }

        public String getIssuerActivated() {
            return IssuerActivated;
        }

        public void setIssuerActivated(String issuerActivated) {
            IssuerActivated = issuerActivated;
        }

        public String getRefundEnable() {
            return RefundEnable;
        }

        public void setRefundEnable(String refundEnable) {
            RefundEnable = refundEnable;
        }

        public String getVoidEnable() {
            return VoidEnable;
        }

        public void setVoidEnable(String voidEnable) {
            VoidEnable = voidEnable;
        }

        public String getPreAuthEnable() {
            return PreAuthEnable;
        }

        public void setPreAuthEnable(String preAuthEnable) {
            PreAuthEnable = preAuthEnable;
        }

        public String getCompletionEnable() {
            return CompletionEnable;
        }

        public void setCompletionEnable(String completionEnable) {
            CompletionEnable = completionEnable;
        }

        public String getAdjustEnable() {
            return AdjustEnable;
        }

        public void setAdjustEnable(String adjustEnable) {
            AdjustEnable = adjustEnable;
        }

        public String getAdvCashEnable() {
            return AdvCashEnable;
        }

        public void setAdvCashEnable(String advCashEnable) {
            AdvCashEnable = advCashEnable;
        }

        public String getOrbitRedeemEnable() {
            return OrbitRedeemEnable;
        }

        public void setOrbitRedeemEnable(String orbitRedeemEnable) {
            OrbitRedeemEnable = orbitRedeemEnable;
        }

        public String getForcedLBPEnable() {
            return ForcedLBPEnable;
        }

        public void setForcedLBPEnable(String forcedLBPEnable) {
            ForcedLBPEnable = forcedLBPEnable;
        }

        // 0th entry in db is ubl configuration initial Single configuration
//        public static Issuer insertInitialConfig() {
//            return new Issuer(0, "VISA", "1", "1", "VISA", "N","Y","Y","Y","Y","Y","N","N","Y","Y","N","Y","Y","Y","Y","Y","Y","Y","N","Y");
//        }

        // 1st entry in db is UPI configuration initial Single configuration
   /* public static TerminalConfig insertUPIConfig(){
        return new TerminalConfig(1,"200000005000510","42017890",
                "UPI TEST HOST", "202.61.40.171","7006","444","N");// initial UPI config
    }*/

        public static List<Issuer> issuerList(){
            List<Issuer> issuerConfig = new ArrayList<>();
            //issuerConfig.add(0,insertInitialConfig());
            issuerConfig.add(0, new Issuer(0, "VISA", "1", "1", "VISA", "N","Y","Y","Y","Y","Y","N","N","Y","Y","N","Y","Y","Y","Y","Y","Y","Y","N","Y"));
            issuerConfig.add(1, new Issuer(1, "MASTER", "2", "1", "MASTER", "N","Y","Y","Y","Y","Y","N","Y","Y","Y","N","Y","Y","Y","Y","Y","Y","Y","N","Y"));
            issuerConfig.add(2, new Issuer(2, "JCB", "3", "1", "JCB", "N","Y","Y","Y","Y","Y","N","N","Y","Y","N","Y","Y","Y","Y","Y","Y","Y","N","Y"));
            issuerConfig.add(3, new Issuer(3, "AMEX", "4", "1", "AMEX", "N","Y","Y","Y","Y","Y","N","N","Y","Y","N","Y","Y","Y","Y","Y","Y","Y","N","Y"));
            issuerConfig.add(4, new Issuer(4, "UPI", "5", "1", "UPI", "N","Y","Y","Y","Y","Y","N","N","Y","Y","N","Y","Y","Y","Y","Y","Y","Y","N","Y"));
            issuerConfig.add(5, new Issuer(5, "PAYPAK", "6", "1", "PAYPAK", "N","Y","Y","Y","Y","Y","N","N","Y","Y","N","Y","Y","Y","Y","Y","Y","Y","N","Y"));
            return issuerConfig;
        }
    }


