package cn.access.group.android_all_banks_pos.usecase;

import android.annotation.SuppressLint;

import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ClearBatch {
    @SuppressLint("CheckResult")
    private  void ClearBatch(final AppDatabase db){
        Completable.fromAction(() -> db.transactionDetailDao().nukeTable())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(()->{ }// completed with success
                        ,throwable ->{}// there was an error
                );
    }
}
