package cn.access.group.android_all_banks_pos.Utilities.canvas_printer;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import cn.access.group.android_all_banks_pos.Utilities.Utility;

import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.PrinterConfig;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.TransPrinter;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.repository.model.CountedTransactionItem;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

import static cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication.showProgressDialog;


/**
 * Created by Simon on 2019/5/15.
 */

public class PrintRecpDetail extends TransPrinter {
    private static final String TAG = "PrintRecpDetail";
    List<TransactionDetail> transactionDetailList;
    List<CountedTransactionItem> countedTransactionItemList;

    public PrintRecpDetail(Context context, List<TransactionDetail> transactionDetailList, List<CountedTransactionItem> countedTransactionItemList) {
        super(context);
        this.transactionDetailList = transactionDetailList;
        this.countedTransactionItemList = countedTransactionItemList;
    }


    public String getCardTypeMode(int im_pan) {
        String cardTypeMode;
        switch (im_pan) {
            case 1:
                cardTypeMode = "M";
                break;
            case 2:
                cardTypeMode = "S";
                break;
            case 5:
                cardTypeMode = "C";
                break;
            case 7:
                cardTypeMode = "Q";
                break;
            default:
                cardTypeMode = "M";
                break;
        }
        return cardTypeMode;
    }

    public void setPrinterItems(String set) {
        PrinterItem.MERCHANT_ID.value.sValue = set;
        printerItems.add(PrinterItem.MERCHANT_ID);
    }

    // Here to "draw" your receipt with various PrinterItem
    @SuppressLint("LongLogTag")
    public void initializeData(List<TransactionDetail> transactionDetailList, List<CountedTransactionItem> countedTransactionItemList) {
        super.initializeData(transactionDetailList, countedTransactionItemList);
        Utility.DEBUG_LOG(TAG, "initializeData");
        try {

            String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
            String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
            Utility.DEBUG_LOG(TAG,"current time:"+currentTime );

            //Get some extra infos about this receipt, like the index of copy and if it is reprint
            String tmp;
            int copyIndex = 2;
            //int copyIndex = extraItems.getInt("copyIndex");
            //Boolean isReprint = extraItems.getBoolean("reprint", false);

            // A List to put PrintItems, printCanvas will resolve this list to draw receipt
            printerItems = new ArrayList<>();
            Utility.DEBUG_LOG(TAG,"This Works");
            // The LOGO on the top of receipt, set stype to align center.
            PrinterItem.LOGO.title.sValue = "logo4.jpg";
            PrinterItem.LOGO.title.style = PrinterDefine.PStyle_align_center;
            printerItems.add(PrinterItem.LOGO);
            //Merchant name
            // PrinterItem.MERCHANT_NAME.title. = PrinterDefine.PStyle_align_center;
            printerItems.add(PrinterItem.MERCHANT_NAME);
            // HEADER LINES
            printerItems.add(PrinterItem.HEADER1);
            printerItems.add(PrinterItem.HEADER2);
            printerItems.add(PrinterItem.HEADER3);
            printerItems.add(PrinterItem.HEADER4);


            // Merchant id
            PrinterItem.MERCHANT_ID.value.sValue = Constants.MERCHANT_ID;
            printerItems.add(PrinterItem.MERCHANT_ID);
            PrinterItem.TERMINAL_ID.value.sValue = Constants.TERMINAL_ID;
            ;
            printerItems.add(PrinterItem.TERMINAL_ID);
            // DATE AND TIME
            PrinterItem.DATE_TIME.title.sValue = "DATE: " + currentDate;
            PrinterItem.DATE_TIME.value.sValue = "TIME: " + currentTime;
            //Utility.DEBUG_LOG(" PrinterItem.DATE_TIME.value.sValue", PrinterItem.DATE_TIME.value.sValue);
            printerItems.add(PrinterItem.DATE_TIME);
            // BATCH AND INVOICE
            PrinterItem.BATCH_NO_DETAIL.title.sValue = "BATCH: " + Constants.BATCH_NO;
            printerItems.add(PrinterItem.BATCH_NO_DETAIL);

            //SPACE
            printerItems.add(PrinterItem.FEED_LINE);


            //Heading summary report
            PrinterItem.DETAIL_REPORT_HEADING.title.sValue = "DETAIL REPORT";
            printerItems.add(PrinterItem.DETAIL_REPORT_HEADING);
            //SPACE
            printerItems.add(PrinterItem.FEED_LINE);


            printerItems.add(PrinterItem.DETAIL_LIST_3);

            //SPACE
            printerItems.add(PrinterItem.FEED_LINE);

            //SUMMARY HEADING 2
            printerItems.add(PrinterItem.DETAL_SUMMARY_HEADING);
            //SPACE
            printerItems.add(PrinterItem.FEED_LINE);

            int sumfinalcount = 0;
            double sumfinalamount = 0.00;
            String finalCount, finalSum, finalstatus = "",finalTip;
            String saleType = "SALE";
            String saleVoid = "VOID";


            String cashOut = "CASHOUT";
            String orbitRedeem = "REDEEM";
            int saleCount = 0, voidCount = 0, cashOutCount = 0, orbitRedeemCount = 0, saleWithTipCount = 0;
            String tipCount;
            double saleAmountStr = 0.00;
            double voidAmountStr = 0.00;
            double cashOutAmountStr = 0.00;
            double orbitRedeemAmountStr = 0.00;
            double saleWithTipAmount = 0.00;

            for (int i = 0; i < countedTransactionItemList.size(); i++) {
                //+ taha 17-03-2021 adding completion
                if (countedTransactionItemList.get(i).getTxnType().equals("SALE") || countedTransactionItemList.get(i).getTxnType().equals("SALEIPP") || countedTransactionItemList.get(i).getTxnType().equals("ADJUST")|| countedTransactionItemList.get(i).getTxnType().equals("COMPLETION") )
                {
                    saleType = "SALE";
                    saleCount += countedTransactionItemList.get(i).getCount();
                    saleAmountStr = saleAmountStr + countedTransactionItemList.get(i).getTotalAmount();
                    Utility.DEBUG_LOG("Detail report", String.valueOf(saleAmountStr));
                }
                if (countedTransactionItemList.get(i).getTxnType().equals("VOID")) {
                    saleVoid = "VOID";
                    voidCount += countedTransactionItemList.get(i).getCount();
                    voidAmountStr = voidAmountStr + (countedTransactionItemList.get(i).getTotalAmount());
                }
                if (countedTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                    cashOut = "CASH OUT";
                    cashOutCount += countedTransactionItemList.get(i).getCount();
                    cashOutAmountStr = cashOutAmountStr + countedTransactionItemList.get(i).getTotalAmount();
                }
                if (countedTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                    orbitRedeem = "REDEEM";
                    orbitRedeemCount += countedTransactionItemList.get(i).getCount();
                    orbitRedeemAmountStr = orbitRedeemAmountStr + countedTransactionItemList.get(i).getTotalAmount();
                }
                if (!(countedTransactionItemList.get(i).getTxnType().equals("VOID"))) {
                    sumfinalcount += countedTransactionItemList.get(i).getCount();
                    sumfinalamount += countedTransactionItemList.get(i).getTotalAmount();
                    if(Constants.TIP_ENABLED.equals("Y")) {
                        saleWithTipAmount += countedTransactionItemList.get(i).getTotaTiplAmount();
                    }
                }
            }
            Utility.DEBUG_LOG("summaryTotalAmount", String.valueOf((new DecimalFormat("0.00").format(sumfinalamount))));
            Utility.DEBUG_LOG("summaryTotalAmount", String.valueOf((new DecimalFormat("0.00").format(saleWithTipAmount))));


            PrinterItem.VISA_SALETYPE.title.sValue = saleType;
            PrinterItem.VISA_SALETYPE.title.secValue = String.valueOf(saleCount);
            PrinterItem.VISA_SALETYPE.value.sValue = String.valueOf((new DecimalFormat("0.00").format(saleAmountStr)));
            printerItems.add(PrinterItem.VISA_SALETYPE);

            PrinterItem.VISA_VOID.title.sValue = saleVoid;
            PrinterItem.VISA_VOID.title.secValue = String.valueOf(voidCount);
            PrinterItem.VISA_VOID.value.sValue = String.valueOf((new DecimalFormat("0.00").format(voidAmountStr)));
            printerItems.add(PrinterItem.VISA_VOID);

            PrinterItem.VISA_CASHOUT.title.sValue = cashOut;
            PrinterItem.VISA_CASHOUT.title.secValue = String.valueOf(cashOutCount);
            PrinterItem.VISA_CASHOUT.value.sValue = String.valueOf((new DecimalFormat("0.00").format(cashOutAmountStr)));
            printerItems.add(PrinterItem.VISA_CASHOUT);

            PrinterItem.VISA_REDEEM.title.sValue = orbitRedeem;
            PrinterItem.VISA_REDEEM.title.secValue = String.valueOf(orbitRedeemCount);
            PrinterItem.VISA_REDEEM.value.sValue = String.valueOf((new DecimalFormat("0.00").format(orbitRedeemAmountStr)));
            printerItems.add(PrinterItem.VISA_REDEEM);

            //printerItems.add(PrinterItem.FEED_LINE);

            finalCount = String.valueOf(sumfinalcount);
            finalSum = String.valueOf(new DecimalFormat("0.00").format((sumfinalamount)));
            finalTip = String.valueOf(new DecimalFormat("0.00").format((saleWithTipAmount)));



            printerItems.add(PrinterItem.FEED_LINE);
            PrinterItem.SUMMARY_TOTAL_SALE.title.secValue = finalCount;
            PrinterItem.SUMMARY_TOTAL_SALE.value.sValue = finalSum;
            printerItems.add(PrinterItem.SUMMARY_TOTAL_SALE);

            if(Constants.TIP_ENABLED.equals("Y")) {
                if(saleWithTipAmount != 0) {
                    tipCount = String.valueOf(new DecimalFormat("0.00").format(((saleWithTipAmount / sumfinalamount) * 100)));
                    Utility.DEBUG_LOG("tip count", tipCount);

                    // printerItems.add(PrinterItem.FEED_LINE);
                    PrinterItem.SUMMARY_TOTAL_TIP_SALE.title.secValue = tipCount + "%";
                    Utility.DEBUG_LOG("tip count", tipCount);
                }
                else{
                    PrinterItem.SUMMARY_TOTAL_TIP_SALE.title.secValue = "0.00%";
                    Utility.DEBUG_LOG("tip count = 0.00%","");
                }
                PrinterItem.SUMMARY_TOTAL_TIP_SALE.value.sValue = String.valueOf(new DecimalFormat("0.00").format((saleWithTipAmount)));
                Utility.DEBUG_LOG("tip count", String.valueOf(new DecimalFormat("0.00").format((saleWithTipAmount))));
                printerItems.add(PrinterItem.SUMMARY_TOTAL_TIP_SALE);
            }


//            printerItems.add(PrinterItem.FEED_LINE);
 //           printerItems.add(PrinterItem.TERMINAL_SERIAL);
            printerItems.add(PrinterItem.FEED_LINE);

//            //footer
//            printer.addTextInLine(fmtAddTextInLine, "","***END***","",0);
//            printer.feedLine(4);

//            //CUT BREAK
//            printerItems.add(PrinterItem.CUT_BREAK);
//            //SPACE
//            printerItems.add(PrinterItem.FEED_LINE);
//            //SPACE
//            printerItems.add(PrinterItem.FEED_LINE);


            //TRANS TYPE
//            PrinterItem.TRANS_TYPE.title.sValue = "SALE";
//            printerItems.add(PrinterItem.TRANS_TYPE);


//            switch (copyIndex) {
//                case 1:
//                    tmp = getResources().getString(R.string.prn_merchantCopy); //"商户存根                           请妥善保管";
//                    break;
//                case 2:
//                    tmp = getResources().getString(R.string.prn_cardholderCopy); //"持卡人存根                         请妥善保管";
//                    break;
//                case 3:
//                default:
//                    tmp = getResources().getString(R.string.prn_bankCopy); //"银行存根                           请妥善保管";
//                    break;
//            }
//            PrinterItem.SUBTITLE.value.sValue = tmp;
//            printerItems.add(PrinterItem.SUBTITLE);
//            printerItems.add(PrinterItem.LINE);
//
//            // MERCHANT NAME
////            PrinterItem.MERCHANT_NAME.value.sValue = hostInformation.merchantName;
//            //printerItems.add(PrinterItem.MERCHANT_NAME);
//
//            // MERCHANT NO.
////            PrinterItem.MERCHANT_ID.value.sValue = hostInformation.merchantID;
//            printerItems.add(PrinterItem.MERCHANT_ID);
//
//            // TERMINAL NO
//            PrinterItem.TERMINAL_ID.value.sValue = "123456789";
////            PrinterItem.TERMINAL_ID.value.sValue = hostInformation.terminalID;
//            printerItems.add(PrinterItem.TERMINAL_ID);
//
//            // OPERATOR NO
////            PrinterItem.OPERATOR_ID.value.sValue = getAppParam(AppParam.System.oper_no);
////            printerItems.add(PrinterItem.OPERATOR_ID);
////            printerItems.add(PrinterItem.LINE);
//
//            // ISSUE
//            PrinterItem.CARD_ISSUE.value.sValue = "VISA"; // extraItems.getString(TXNREC.ISSBANKNAME).trim();
//            printerItems.add(PrinterItem.CARD_ISSUE);
//
//            // CARD NO.
////            String pansn = extraItems.getString(TXNREC.PANSN);
////            String pan = TransactionParams.getInstance().getPan();
////            PrinterItem.CARD_NO.title.sValue = getResources().getString(R.string.cardno);
////            PrinterItem.CARD_NO.value.sValue = pan;
//            String pan = TransactionParams.getInstance().getPan();
//            PrinterItem.CARD_NO.title.sValue = "getResources().getString(R.string.cardno)";
//            PrinterItem.CARD_NO.value.sValue = "pan";
//            // todo, add & print the card type
////            if (pan != null && pan.trim().length() > 4) {
////                if (pansn != null && !pansn.isEmpty()) {
////                    PrinterItem.CARD_NO.title.sValue = getResources().getString(R.string.cardno1) + pansn.substring(1);
////                } else {
////                }
///                PrinterItem.CARD_NO.value.sValue = fixCardNumWithMask(pan);
////            } else {
////                Utility.DEBUG_LOG(TAG, "No Card No. got!");
////            }
//            printerItems.add(PrinterItem.CARD_NO);
//
//            // EXP. DATE
//            String expiredDate = extraItems.getString(TransactionParams.getInstance().getExpiredDate());
//            if (expiredDate != null && !expiredDate.isEmpty()) {
//                PrinterItem.CARD_VALID.value.sValue = expiredDate.substring(0, 4) + "/" + expiredDate.substring(0, 2);
//            } else {
//                Utility.DEBUG_LOG(TAG, "no card expire date got");
//                PrinterItem.CARD_VALID.value.sValue = "";
//            }
//            printerItems.add(PrinterItem.CARD_VALID);
//
//            // TRANS TYPE
//            PrinterItem.TRANS_TYPE.value.sValue = TransactionParams.getInstance().getTransactionType();
//            printerItems.add(PrinterItem.TRANS_TYPE);
//
//
//            // BATCH NO. TODO
////            String batchNo =  getAppParam(AppParam.System.batch_num);
////            PrinterItem.BATCH_NO.value.sValue = getAppParam(AppParam.System.batch_num);
////            printerItems.add(PrinterItem.BATCH_NO);
//
//            // TRACE NO. TODO
////            String traceNo = extraItems.getString(TXNREC.TRACE);
////            if (traceNo != null && !traceNo.isEmpty()) {
////                PrinterItem.TRACK_NO.value.sValue = traceNo;
////                printerItems.add(PrinterItem.TRACK_NO);
////            }
//
//            // AUTH NO. TODO
////            tmp = extraItems.getString(TXNREC.AUTHID);
////            if (tmp != null && !tmp.isEmpty()) {
////                PrinterItem.AUTH_NO.value.sValue = tmp;
////                printerItems.add(PrinterItem.AUTH_NO);
////            }
//
//            // REF NO. TODO
////            String referenceNo = extraItems.getString(TXNREC.REFERNUM);
////            if (referenceNo != null && !referenceNo.isEmpty()) {
////                PrinterItem.REFER_NO.value.sValue = referenceNo;
////                printerItems.add(PrinterItem.REFER_NO);
////            }
//
//            // DATE/TIME
////            String dateString = extraItems.getString(TXNREC.DATE) + extraItems.getString(TXNREC.TIME);
////            String dateString = "1996-03-07";
////            if (dateString != null && !dateString.isEmpty()) {
////                dateString = "getSystemDatetime";
////                PrinterItem.DATE_TIME.value.sValue = dateString;
////            } else {
////                PrinterItem.DATE_TIME.value.sValue = "";
////            }
////            printerItems.add(PrinterItem.DATE_TIME);
//
//            // AMOUNT
//            String retamount = TransactionParams.getInstance().getTransactionAmount();
//            if (retamount != null && !retamount.isEmpty()) {
//
//                PrinterItem.AMOUNT.value.sValue = getResources().getString(R.string.prn_currency) + retamount;
//                printerItems.add(PrinterItem.AMOUNT);
//            }
//            printerItems.add(PrinterItem.LINE);
//
//            printerItems.add(PrinterItem.REFERENCE);
//            printerItems.add(PrinterItem.FEED_LINE);
//            printerItems.add(PrinterItem.FEED_LINE);
//
//            // TC TODO
////            String ac = extraItems.getString(TXNREC.AC);
////            int im_pan = 0;
////            try {
////                im_pan = Integer.parseInt(extraItems.getString(TXNREC.MODE).substring(0, 2));
////            } catch (Exception e) {
////            }
//
////            if (ac != null && "C".equals(getCardTypeMode(im_pan))) {
////                PrinterItem.TC.value.sValue = ac;
////            }
//
//            // REPRINT
//            if (isReprint) {
//                printerItems.add(PrinterItem.RE_PRINT_NOTE);
//            }
//            printerItems.add(PrinterItem.LINE);
//
//            // CARDHOLDER SIGNATURE
//
//            PrinterItem.E_SIGN.value.sValue = TransactionParams.getInstance().getEsignData();
//            printerItems.add(PrinterItem.E_SIGN);
//
////            if (!printEsign()) {
////                printerItems.add(PrinterItem.FEED_LINE);
////                printerItems.add(PrinterItem.FEED_LINE);
////            }
//
//          //  PrinterItem.QRCODE_1.value.sValue = getResources().getString(R.string.prn_qrcode2);
//
//           // PrinterItem.BARCODE_1.value.sValue = getResources().getString(R.string.prn_barcode);
//
//            printerItems.add(PrinterItem.FEED);
//           // printerItems.add(PrinterItem.BARCODE_1);
//            printerItems.add(PrinterItem.FEED);
//            printerItems.add(PrinterItem.FEED);
//            //printerItems.add(PrinterItem.QRCODE_1);
//            printerItems.add(PrinterItem.LINE);
//
//            printerItems.add(PrinterItem.COMMENT_1);
//            printerItems.add(PrinterItem.COMMENT_2);
//            printerItems.add(PrinterItem.COMMENT_3);


        } catch (Exception e) {
            Utility.DEBUG_LOG(TAG, "Exception :" + e.getMessage());
            for (StackTraceElement m : e.getStackTrace()
            ) {
                Utility.DEBUG_LOG(TAG, "Exception :" + m);

            }
        }
    }
}

