package cn.access.group.android_all_banks_pos.Utilities;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalPassword;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalTxnUpload;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUrl;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUserName;

public class PostRequest {


    public void makeJsonObjReq(Context context,TransactionDetail td) {
        if (PortalTxnUpload.equals("Y")) {
            RequestQueue queue = Volley.newRequestQueue(context);

            String url = PortalUrl + "/baflservicetxn";
            Utility.DEBUG_LOG("PostRequest",url);

            String cardNo = td.getCardNo();
            cardNo = cardNo.substring(0, 6);
            Utility.DEBUG_LOG("cardNo", cardNo);
            Map<String, String> postParam = new HashMap<String, String>();
            postParam.put("UserName", PortalUserName);
            postParam.put("PASSWORD", PortalPassword);
            postParam.put("Bin", cardNo);
            postParam.put("FieldOne", DataFormatterUtil.maskCardNo(td.getCardNo()));
            postParam.put("AuthID", td.getAuthorizationIdentificationResponseCode());
            postParam.put("Amount", td.getAmount());
            postParam.put("MID", td.getMId());
            postParam.put("TID", td.getTId());
            postParam.put("TxnDate", td.getTxnDate());
            postParam.put("TxnTime", td.getTxnTime());
            postParam.put("POSEntryMode", td.getPosEntryMode());
            postParam.put("BatchNo", td.getBatchNo());
            postParam.put("RRN", td.getRrn());
            postParam.put("InvoiceNo", td.getInvoiceNo());
            postParam.put("Stan", td.getStan());
            postParam.put("TxnType", td.getTxnType());
            postParam.put("Acquirer", "BAF-Payment");
            postParam.put("Model", "X990");
            postParam.put("CardScheme", td.getCardScheme());
            postParam.put("SerialNumber", Constants.TERMINAL_SERIAL);
            postParam.put("Reciept", "");
            Utility.DEBUG_LOG("params", String.valueOf(postParam));


            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, new JSONObject(postParam),
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
//                        try {
//                            Toast.makeText(context,"Response :" + response.getString("Reponsemessage"), Toast.LENGTH_LONG).show();
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                            Utility.DEBUG_LOG("Volley", response.toString());
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utility.DEBUG_LOG("Volley", "Error: " + error.getMessage());
                    Utility.DEBUG_LOG("Volley", error.toString());
                    //Toast.makeText(context,"Error :" + error.toString(), Toast.LENGTH_LONG).show();
//                DialogUtil.errorDialog(context,"Error!", "Connection Failed!");
                    error.printStackTrace();
                }
            }) {

                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };
            new NukeSSLCerts().nuke();

            jsonObjReq.setTag("Volley");
            // Adding request to request queue
            queue.add(jsonObjReq);

            // Cancelling request
//     if (queue!= null) {
//    queue.cancelAll(TAG);
        }
        else
        {
            Utility.DEBUG_LOG("PostRequest","Not allowed");
        }
    }

        public static class NukeSSLCerts {
            protected static final String TAG = "NukeSSLCerts";

            public static void nuke() {
                try {
                    TrustManager[] trustAllCerts = new TrustManager[]{
                            new X509TrustManager() {
                                public X509Certificate[] getAcceptedIssuers() {
                                    X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                                    return myTrustedAnchors;
                                }

                                @Override
                                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                                }

                                @Override
                                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                                }
                            }
                    };

                    SSLContext sc = SSLContext.getInstance("SSL");
                    sc.init(null, trustAllCerts, new SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String arg0, SSLSession arg1) {
                            return true;
                        }
                    });
                } catch (Exception e) {
                }
            }
        }
    }



