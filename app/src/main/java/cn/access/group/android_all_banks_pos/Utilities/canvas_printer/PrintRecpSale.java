package cn.access.group.android_all_banks_pos.Utilities.canvas_printer;

import android.annotation.SuppressLint;
import android.content.Context;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.TransPrinter;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;


/**
 * Created by Simon on 2019/5/15.
 */

public class PrintRecpSale extends TransPrinter {
    private static final String TAG = "PrintRecpSale";

    public PrintRecpSale(Context context) {
        super(context);

    }


    public String getCardTypeMode(int im_pan) {
        String cardTypeMode;
        switch (im_pan) {
            case 1:
                cardTypeMode = "M";
                break;
            case 2:
                cardTypeMode = "S";
                break;
            case 5:
                cardTypeMode = "C";
                break;
            case 7:
                cardTypeMode = "Q";
                break;
            default:
                cardTypeMode = "M";
                break;
        }
        return cardTypeMode;
    }

    boolean isUpiAndCtls(TransactionDetail transactionDetail) {
        boolean rv = false;
        Utility.DEBUG_LOG(TAG, "+ isUpiAndCtls +");

        String aid = transactionDetail.getAidCode();
        String cardType = transactionDetail.getCardType();
        Utility.DEBUG_LOG(TAG, "aid:" + aid);
        Utility.DEBUG_LOG(TAG, "cardType:" + cardType);
        if (
                (aid.equals("A000000333010101") || aid.equals("A000000333010102") || aid.equals("A000000333010103"))
                        && cardType.equals("ctls")
        ) {
            rv = true;
            Utility.DEBUG_LOG(TAG, "UPI CTLS aid found, return: " + rv);
        }

        return rv;
    }

    // Here to "draw" your receipt with various PrinterItem
    @SuppressLint("LongLogTag")
    public void initializeData(TransactionDetail transactionDetail, String balInquiry, boolean redeemCheck, String copy) {
        super.initializeData(transactionDetail, balInquiry, redeemCheck, copy);
        Utility.DEBUG_LOG(TAG, "initializeData");
        try {

            //Get some extra infos about this receipt, like the index of copy and if it is reprint
            String tmp;
            int copyIndex = 2;
            //int copyIndex = extraItems.getInt("copyIndex");
            //Boolean isReprint = extraItems.getBoolean("reprint", false);

            // A List to put PrintItems, printCanvas will resolve this list to draw receipt
            printerItems = new ArrayList<>();
            Utility.DEBUG_LOG(TAG, "This Works");
            // The LOGO on the top of receipt, set stype to align center.
            PrinterItem.LOGO.title.sValue = "logo4.jpg";
            PrinterItem.LOGO.title.style = PrinterDefine.PStyle_align_center;
            printerItems.add(PrinterItem.LOGO);

            //Merchant name
            // PrinterItem.MERCHANT_NAME.title. = PrinterDefine.PStyle_align_center;
            printerItems.add(PrinterItem.MERCHANT_NAME);
            // HEADER LINES
            printerItems.add(PrinterItem.HEADER1);
            printerItems.add(PrinterItem.HEADER2);
            printerItems.add(PrinterItem.HEADER3);
            printerItems.add(PrinterItem.HEADER4);


            // Merchant id
            PrinterItem.MERCHANT_ID.value.sValue = Constants.MERCHANT_ID;
            printerItems.add(PrinterItem.MERCHANT_ID);
            PrinterItem.TERMINAL_ID.value.sValue = Constants.TERMINAL_ID;
            ;
            printerItems.add(PrinterItem.TERMINAL_ID);
            // DATE AND TIME
            new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
            PrinterItem.DATE_TIME.title.sValue = "DATE: " + DataFormatterUtil.formattedDate(transactionDetail.getTxnDate());
            PrinterItem.DATE_TIME.value.sValue = "TIME: " + DataFormatterUtil.formattedTimeWithSec(transactionDetail.getTxnTime());
            Utility.DEBUG_LOG("TIME", String.valueOf(new Date()));
            //Utility.DEBUG_LOG(" PrinterItem.DATE_TIME.value.sValue", PrinterItem.DATE_TIME.value.sValue);
            printerItems.add(PrinterItem.DATE_TIME);
            // BATCH AND INVOICE
            PrinterItem.BATCH_NO.title.sValue = "BATCH: " + transactionDetail.getBatchNo();
            PrinterItem.BATCH_NO.value.sValue = "INVOICE NO: " + transactionDetail.getInvoiceNo();
            printerItems.add(PrinterItem.BATCH_NO);

            //RRN
            PrinterItem.REFER_NO.value.sValue = transactionDetail.getRrn();
            printerItems.add(PrinterItem.REFER_NO);

            //AUTH NO
            PrinterItem.AUTH_NO.value.sValue = transactionDetail.getAuthorizationIdentificationResponseCode();
            printerItems.add(PrinterItem.AUTH_NO);

            //SPACE
            printerItems.add(PrinterItem.FEED_LINE);

            //ISSUER NAME and TRANS TYPE
            PrinterItem.CARD_ISSUE_TRANS_TYPE.title.sValue = transactionDetail.getCardScheme();
            PrinterItem.CARD_ISSUE_TRANS_TYPE.value.fontFile = PrinterDefine.Font_default;
            PrinterItem.CARD_ISSUE_TRANS_TYPE.value.sValue = transactionDetail.getTxnType();

            printerItems.add(PrinterItem.CARD_ISSUE_TRANS_TYPE);

            //CARD NUMBER AND CARD SCHEME
            PrinterItem.CARD_NO_AND_SCHEME.title.sValue = DataFormatterUtil.maskCardNo(transactionDetail.getCardNo());
            PrinterItem.CARD_NO_AND_SCHEME.value.fontFile = PrinterDefine.Font_default;
            PrinterItem.CARD_NO_AND_SCHEME.value.sValue = transactionDetail.getCardType();
            printerItems.add(PrinterItem.CARD_NO_AND_SCHEME);

            //CARD EXPIRY
            PrinterItem.CARD_VALID.title.sValue = "EXP: " + transactionDetail.getCardexpiry();
            printerItems.add(PrinterItem.CARD_VALID);

            if (!(transactionDetail.getCardType().equals("Magstripe"))) {
                //AID
                PrinterItem.AID.value.sValue = transactionDetail.getAidCode();
                printerItems.add(PrinterItem.AID);
                //TVR
                PrinterItem.TVR.value.sValue = transactionDetail.getTvrCode();
                printerItems.add(PrinterItem.TVR);

            }
            Utility.DEBUG_LOG(TAG, "transactionDetail.getTxnType():" + transactionDetail.getTxnType());
            if (transactionDetail.getTxnType().equals("ORBIT INQUIRY")) {
                printerItems.add(PrinterItem.FEED_LINE);
                PrinterItem.BALANCE.value.sValue = balInquiry;
                Utility.DEBUG_LOG("balInquiry", balInquiry);
                printerItems.add(PrinterItem.BALANCE);
            } else {
                if (transactionDetail.getTxnType().equals("VOID")) {

                    Utility.DEBUG_LOG("transactionAmount", transactionDetail.getAmount());
                    if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")) {
                        PrinterItem.AMOUNT.value.sValue = "-" + transactionDetail.getTransactionAmount();
                        printerItems.add(PrinterItem.AMOUNT);
                        PrinterItem.TIP_AMOUNT.value.sValue = "-" + transactionDetail.getTipAmount();
                        printerItems.add(PrinterItem.TIP_AMOUNT);
                    } else {
                        PrinterItem.AMOUNT.value.sValue = "-" + transactionDetail.getAmount();
                        printerItems.add(PrinterItem.AMOUNT);
                    }
                } else {
                    if (transactionDetail.getTxnType().equals("SALEIPP")) {
                        PrinterItem.TENURE.value.sValue = transactionDetail.getSaleippMonth();
                        printerItems.add(PrinterItem.TENURE);
                        DecimalFormat decimalFormat = new DecimalFormat("0.00");
                        PrinterItem.INSTALLMENT.value.sValue = decimalFormat.format(Double.parseDouble(transactionDetail.getSaleippInstallments()));
                        printerItems.add(PrinterItem.INSTALLMENT);
                    }

                    if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")) {
                        PrinterItem.AMOUNT.value.sValue = transactionDetail.getTransactionAmount();
                        printerItems.add(PrinterItem.AMOUNT);
                        PrinterItem.TIP_AMOUNT.value.sValue = transactionDetail.getTipAmount();
                        printerItems.add(PrinterItem.TIP_AMOUNT);
                    } else {
                        PrinterItem.AMOUNT.value.sValue = transactionDetail.getAmount();
                        printerItems.add(PrinterItem.AMOUNT);
                    }
                }
                //SPACE


                printerItems.add(PrinterItem.FEED_LINE);
                if (!transactionDetail.getTxnType().equals("REDEEM")) {
                    if (Constants.TIP_ENABLED.equalsIgnoreCase("Y") && !transactionDetail.getTxnType().equals("VOID")) {
                        PrinterItem.TOTAL_AMOUNT.value.sValue = transactionDetail.getAmount();
                        printerItems.add(PrinterItem.TOTAL_AMOUNT);
                    } else if (Constants.TIP_ENABLED.equalsIgnoreCase("Y") && transactionDetail.getTxnType().equals("VOID")) {
                        PrinterItem.TOTAL_AMOUNT.value.sValue = "-" + transactionDetail.getAmount();
                        printerItems.add(PrinterItem.TOTAL_AMOUNT);
                    }

                } else {
                    if (redeemCheck) {
                        if (copy.equalsIgnoreCase("CUSTOMER COPY")) {
//                    printer.addTextInLine(fmtAddTextInLine, "Orbit Redeemed:", "", transactionDetail.getRedeemAmount(), 0);
                            PrinterItem.ORBIT_AVAILABLE.value.sValue = transactionDetail.getRedeemBalance();
                            printerItems.add(PrinterItem.ORBIT_AVAILABLE);
                            PrinterItem.ORBIT_REDEEM.value.sValue = transactionDetail.getAmount();
                            printerItems.add(PrinterItem.ORBIT_REDEEM);
                        }
                        PrinterItem.REFERENCE_KEY.value.sValue = transactionDetail.getRedeemKey();
                        printerItems.add(PrinterItem.REFERENCE_KEY);
                    }
                }
            }

            //CARD HOLDER
            if (transactionDetail.getCardHolderName() != null) {
                PrinterItem.CARD_HOLDER.title.sValue = transactionDetail.getCardHolderName();
                printerItems.add(PrinterItem.CARD_HOLDER);
            }

            //SPACE
            printerItems.add(PrinterItem.FEED_LINE);


            /*
             *     |---0 NO_CVM<br>
             *     |---1 CVM_PIN<br>
             *     |---2 CVM_SIGN<br>
             *     |---3 CVM_CDCVM<br>
             */
            Utility.DEBUG_LOG(TAG, "Constants.CARD_AUTH_METHOD=" + Constants.CARD_AUTH_METHOD);
//            if ( isUpiAndCtls(transactionDetail) )
//            {
//                Utility.DEBUG_LOG(TAG,"UPI and CTLS transaction, so just used previously set CVM method");
//            }
//            else
            {
                if (transactionDetail.getTxnType().equals("VOID")) {
                    Utility.DEBUG_LOG(TAG, "Void transaction detected");
                    PrinterItem.CARD_AUTH_METHOD.title.sValue = "APPROVED";
                    printerItems.add(PrinterItem.CARD_AUTH_METHOD);
                } else {
                    if (Constants.CARD_AUTH_METHOD.equals("SIGNATURE")) {
                        printerItems.add(PrinterItem.SIGN);
                        printerItems.add(PrinterItem.FEED_LINE);
                        PrinterItem.CARD_AUTH_METHOD.title.sValue = "APPROVED WITH SIGNATURE";
                        printerItems.add(PrinterItem.CARD_AUTH_METHOD);
                        //printerItems.add(PrinterItem.FEED_LINE);
                    } else if (Constants.CARD_AUTH_METHOD.equals("ONLINE_PIN")) {
                        PrinterItem.CARD_AUTH_METHOD.title.sValue = "APPROVED WITH PIN";
                        printerItems.add(PrinterItem.CARD_AUTH_METHOD);
                    } else if (Constants.CARD_AUTH_METHOD.equals("OFFLINE_PIN")) {
                        PrinterItem.CARD_AUTH_METHOD.title.sValue = "APPROVED WITH OFFLINE PIN";
                        printerItems.add(PrinterItem.CARD_AUTH_METHOD);
                    } else if (Constants.CARD_AUTH_METHOD.equals("NO_CVM")) {
                        PrinterItem.CARD_AUTH_METHOD.title.sValue = "APPROVED";
                        printerItems.add(PrinterItem.CARD_AUTH_METHOD);
                    }
                }
            }

            printerItems.add(PrinterItem.FEED_LINE);
            if (transactionDetail.getTxnType().equals("SALEIPP")) {
                PrinterItem.FOOTER1.title.sValue = "I agree to pay the above final amount according to the card/merchant issuer agreement and SBS";
                PrinterItem.FOOTER2.title.sValue = "installment payments plan's terms and condition. Please note 5% of remaining loan amount or PKR 1,000 (whichever is high)";
                PrinterItem.FOOTER2.title.sValue = "is applicable on premature termination of SBS plan";
                printerItems.add(PrinterItem.FOOTER1);
                printerItems.add(PrinterItem.FOOTER2);
                printerItems.add(PrinterItem.FOOTER3);
            } else {
                if (transactionDetail.getTxnType().equals("CASH ADV")) {
                    printerItems.add(PrinterItem.FEED_LINE);
                    printerItems.add(PrinterItem.SIGN_CASH_ADV1);
                    printerItems.add(PrinterItem.FEED_LINE);
                    printerItems.add(PrinterItem.SIGN_CASH_ADV2);
                    printerItems.add(PrinterItem.FEED_LINE);
                    printerItems.add(PrinterItem.SIGN_CASH_ADV3);
                    printerItems.add(PrinterItem.FEED_LINE);
                }
                printerItems.add(PrinterItem.FOOTER1);
                printerItems.add(PrinterItem.FOOTER2);
                printerItems.add(PrinterItem.FOOTER3);
            }
            printerItems.add(PrinterItem.FEED_LINE);

            PrinterItem.COPY.title.sValue = copy;
            printerItems.add(PrinterItem.COPY);
            printerItems.add(PrinterItem.FEED_LINE);

            if (SharedPref.read("button_fragment", "").equals("REPORTS")) {

                printerItems.add(PrinterItem.TERMINAL_SERIAL);
                printerItems.add(PrinterItem.FEED_LINE);
                printerItems.add(PrinterItem.DUPLICATE);
            } else {
                printerItems.add(PrinterItem.TERMINAL_SERIAL);
                printerItems.add(PrinterItem.FEED_LINE);
            }


//            //CUT BREAK
//            printerItems.add(PrinterItem.CUT_BREAK);
//            //SPACE
//            printerItems.add(PrinterItem.FEED_LINE);
//            //SPACE
//            printerItems.add(PrinterItem.FEED_LINE);


            //TRANS TYPE
//            PrinterItem.TRANS_TYPE.title.sValue = "SALE";
//            printerItems.add(PrinterItem.TRANS_TYPE);


//            switch (copyIndex) {
//                case 1:
//                    tmp = getResources().getString(R.string.prn_merchantCopy); //"商户存根                           请妥善保管";
//                    break;
//                case 2:
//                    tmp = getResources().getString(R.string.prn_cardholderCopy); //"持卡人存根                         请妥善保管";
//                    break;
//                case 3:
//                default:
//                    tmp = getResources().getString(R.string.prn_bankCopy); //"银行存根                           请妥善保管";
//                    break;
//            }
//            PrinterItem.SUBTITLE.value.sValue = tmp;
//            printerItems.add(PrinterItem.SUBTITLE);
//            printerItems.add(PrinterItem.LINE);
//
//            // MERCHANT NAME
////            PrinterItem.MERCHANT_NAME.value.sValue = hostInformation.merchantName;
//            //printerItems.add(PrinterItem.MERCHANT_NAME);
//
//            // MERCHANT NO.
////            PrinterItem.MERCHANT_ID.value.sValue = hostInformation.merchantID;
//            printerItems.add(PrinterItem.MERCHANT_ID);
//
//            // TERMINAL NO
//            PrinterItem.TERMINAL_ID.value.sValue = "123456789";
////            PrinterItem.TERMINAL_ID.value.sValue = hostInformation.terminalID;
//            printerItems.add(PrinterItem.TERMINAL_ID);
//
//            // OPERATOR NO
////            PrinterItem.OPERATOR_ID.value.sValue = getAppParam(AppParam.System.oper_no);
////            printerItems.add(PrinterItem.OPERATOR_ID);
////            printerItems.add(PrinterItem.LINE);
//
//            // ISSUE
//            PrinterItem.CARD_ISSUE.value.sValue = "VISA"; // extraItems.getString(TXNREC.ISSBANKNAME).trim();
//            printerItems.add(PrinterItem.CARD_ISSUE);
//
//            // CARD NO.
////            String pansn = extraItems.getString(TXNREC.PANSN);
////            String pan = TransactionParams.getInstance().getPan();
////            PrinterItem.CARD_NO.title.sValue = getResources().getString(R.string.cardno);
////            PrinterItem.CARD_NO.value.sValue = pan;
//            String pan = TransactionParams.getInstance().getPan();
//            PrinterItem.CARD_NO.title.sValue = "getResources().getString(R.string.cardno)";
//            PrinterItem.CARD_NO.value.sValue = "pan";
//            // todo, add & print the card type
////            if (pan != null && pan.trim().length() > 4) {
////                if (pansn != null && !pansn.isEmpty()) {
////                    PrinterItem.CARD_NO.title.sValue = getResources().getString(R.string.cardno1) + pansn.substring(1);
////                } else {
////                }
///                PrinterItem.CARD_NO.value.sValue = fixCardNumWithMask(pan);
////            } else {
////                Utility.DEBUG_LOG(TAG, "No Card No. got!");
////            }
//            printerItems.add(PrinterItem.CARD_NO);
//
//            // EXP. DATE
//            String expiredDate = extraItems.getString(TransactionParams.getInstance().getExpiredDate());
//            if (expiredDate != null && !expiredDate.isEmpty()) {
//                PrinterItem.CARD_VALID.value.sValue = expiredDate.substring(0, 4) + "/" + expiredDate.substring(0, 2);
//            } else {
//                Utility.DEBUG_LOG(TAG, "no card expire date got");
//                PrinterItem.CARD_VALID.value.sValue = "";
//            }
//            printerItems.add(PrinterItem.CARD_VALID);
//
//            // TRANS TYPE
//            PrinterItem.TRANS_TYPE.value.sValue = TransactionParams.getInstance().getTransactionType();
//            printerItems.add(PrinterItem.TRANS_TYPE);
//
//
//            // BATCH NO. TODO
////            String batchNo =  getAppParam(AppParam.System.batch_num);
////            PrinterItem.BATCH_NO.value.sValue = getAppParam(AppParam.System.batch_num);
////            printerItems.add(PrinterItem.BATCH_NO);
//
//            // TRACE NO. TODO
////            String traceNo = extraItems.getString(TXNREC.TRACE);
////            if (traceNo != null && !traceNo.isEmpty()) {
////                PrinterItem.TRACK_NO.value.sValue = traceNo;
////                printerItems.add(PrinterItem.TRACK_NO);
////            }
//
//            // AUTH NO. TODO
////            tmp = extraItems.getString(TXNREC.AUTHID);
////            if (tmp != null && !tmp.isEmpty()) {
////                PrinterItem.AUTH_NO.value.sValue = tmp;
////                printerItems.add(PrinterItem.AUTH_NO);
////            }
//
//            // REF NO. TODO
////            String referenceNo = extraItems.getString(TXNREC.REFERNUM);
////            if (referenceNo != null && !referenceNo.isEmpty()) {
////                PrinterItem.REFER_NO.value.sValue = referenceNo;
////                printerItems.add(PrinterItem.REFER_NO);
////            }
//
//            // DATE/TIME
////            String dateString = extraItems.getString(TXNREC.DATE) + extraItems.getString(TXNREC.TIME);
////            String dateString = "1996-03-07";
////            if (dateString != null && !dateString.isEmpty()) {
////                dateString = "getSystemDatetime";
////                PrinterItem.DATE_TIME.value.sValue = dateString;
////            } else {
////                PrinterItem.DATE_TIME.value.sValue = "";
////            }
////            printerItems.add(PrinterItem.DATE_TIME);
//
//            // AMOUNT
//            String retamount = TransactionParams.getInstance().getTransactionAmount();
//            if (retamount != null && !retamount.isEmpty()) {
//
//                PrinterItem.AMOUNT.value.sValue = getResources().getString(R.string.prn_currency) + retamount;
//                printerItems.add(PrinterItem.AMOUNT);
//            }
//            printerItems.add(PrinterItem.LINE);
//
//            printerItems.add(PrinterItem.REFERENCE);
//            printerItems.add(PrinterItem.FEED_LINE);
//            printerItems.add(PrinterItem.FEED_LINE);
//
//            // TC TODO
////            String ac = extraItems.getString(TXNREC.AC);
////            int im_pan = 0;
////            try {
////                im_pan = Integer.parseInt(extraItems.getString(TXNREC.MODE).substring(0, 2));
////            } catch (Exception e) {
////            }
//
////            if (ac != null && "C".equals(getCardTypeMode(im_pan))) {
////                PrinterItem.TC.value.sValue = ac;
////            }
//
//            // REPRINT
//            if (isReprint) {
//                printerItems.add(PrinterItem.RE_PRINT_NOTE);
//            }
//            printerItems.add(PrinterItem.LINE);
//
//            // CARDHOLDER SIGNATURE
//
//            PrinterItem.E_SIGN.value.sValue = TransactionParams.getInstance().getEsignData();
//            printerItems.add(PrinterItem.E_SIGN);
//
////            if (!printEsign()) {
////                printerItems.add(PrinterItem.FEED_LINE);
////                printerItems.add(PrinterItem.FEED_LINE);
////            }
//
//          //  PrinterItem.QRCODE_1.value.sValue = getResources().getString(R.string.prn_qrcode2);
//
//           // PrinterItem.BARCODE_1.value.sValue = getResources().getString(R.string.prn_barcode);
//
//            printerItems.add(PrinterItem.FEED);
//           // printerItems.add(PrinterItem.BARCODE_1);
//            printerItems.add(PrinterItem.FEED);
//            printerItems.add(PrinterItem.FEED);
//            //printerItems.add(PrinterItem.QRCODE_1);
//            printerItems.add(PrinterItem.LINE);
//
//            printerItems.add(PrinterItem.COMMENT_1);
//            printerItems.add(PrinterItem.COMMENT_2);
//            printerItems.add(PrinterItem.COMMENT_3);

           //f (Constants.isEcrEnable.equals("Y"))
             //   if (copy.equalsIgnoreCase("MERCHANT COPY"))
                    //ECR.ecrWrite(transactionDetail.getTxnType(), DataFormatterUtil.formattedDateECR(transactionDetail.getTxnDate()), DataFormatterUtil.formattedTimeWithSec(transactionDetail.getTxnTime()), DataFormatterUtil.maskCardNo(transactionDetail.getCardNo()), transactionDetail.getCardType(), transactionDetail.getCardHolderName(), transactionDetail.getAmount(), transactionDetail.getRrn(), transactionDetail.getAuthorizationIdentificationResponseCode(), "00", "Approved",transactionDetail.getTipAmount());

        } catch (Exception e) {
            Utility.DEBUG_LOG(TAG, "Exception :" + e.getMessage());
            for (StackTraceElement m : e.getStackTrace()
            ) {
                Utility.DEBUG_LOG(TAG, "Exception :" + m);

            }
        }
    }
}
