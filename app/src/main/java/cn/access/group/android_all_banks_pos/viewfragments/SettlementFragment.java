package cn.access.group.android_all_banks_pos.viewfragments;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.system_service.aidl.ISystemManager;
import com.vfi.smartpos.system_service.aidl.settings.ISettingsManager;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.TransPrinter;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.repository.model.CountedTransactionItem;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.adapter.MyVoidTransactionRecyclerViewAdapter;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.presenter.SettlementPresenter;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication.hideProgressDialog;
import static cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication.showProgressDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettlementFragment extends Fragment implements TransactionContract {

    Unbinder unbinder;
    IBeeper iBeeper;
    IPrinter printer;
    public static IDeviceService idevice;
    private static final String TAG = "SettlementFrag";
    Intent intent = new Intent();
    boolean isSucc;
    AssetManager assetManager;
    @BindView(R.id.settlement_merchant_id)
    TextView settlementMerchantId;
    @BindView(R.id.settlement_terminal_id)
    TextView settlementTerminalId;
    @BindView(R.id.settlement_perform)
    Button settlementPerform;
    @BindView(R.id.saleCount)
    TextView saleCount;
    @BindView(R.id.saleAmount)
    TextView saleAmount;
    @BindView(R.id.voidCount)
    TextView voidCount;
    @BindView(R.id.voidAmount)
    TextView voidAmount;
    @BindView(R.id.cashOutCount)
    TextView cashOutCount;
    @BindView(R.id.cashOutAmount)
    TextView cashOutAmount;
    @BindView(R.id.orbitRedeemCount)
    TextView orbitRedeemCount;
    @BindView(R.id.orbitRedeemAmount)
    TextView orbitRedeemAmount;
    @BindView(R.id.refundCount)
    TextView refundCount;
    @BindView(R.id.refundAmount)
    TextView refundAmount;
    private WeakReference<Context> mContext;
    private MyVoidTransactionRecyclerViewAdapter.OnListFragmentInteractionListener mListener;
    SettlementPresenter settlementPresenter;
    TransactionContract transactionContract;
    ISystemManager systemManager = null;
    final String[] s = {""};
    String printFininshed = "";
    int sumCount = 0;
    int grandTotalCount = 0;
    double sumAmount = 0.00;

    boolean isbuttonClicked = false;
//    int batch = 0;

    @SuppressLint("ValidFragment")
    public SettlementFragment() {
        // Required empty public constructor
    }
    @SuppressLint("ValidFragment")
    public SettlementFragment(ISystemManager systemManager) {
        this.systemManager = systemManager;
    }

    @SuppressLint("ValidFragment")
    public SettlementFragment(TransactionContract transactionContract) {
        // Required empty public constructor
        this.transactionContract = transactionContract;
    }

    private List<CountedTransactionItem> sumTransactionItemList;

    @SuppressLint("CheckResult")
    private void sumAndCountTransactions() {
        AppDatabase.getAppDatabase(mContext.get()).transactionDetailDao().transactionSumCount()
                .subscribeOn(Schedulers.newThread())
                .subscribe(count ->
                        {
                            sumTransactionItemList = count;
                            Utility.DEBUG_LOG("Shivam", String.valueOf(sumTransactionItemList));
                        }
                );
    }

    private List<CountedTransactionItem> cardTransactionItemList;

    @SuppressLint("CheckResult")
    private void cardAndCountTransactions() {
        Utility.DEBUG_LOG(TAG, "+ cardAndCountTransactions +");
        AppDatabase.getAppDatabase(mContext.get()).transactionDetailDao().transactionCardSchemeCount()
                .subscribeOn(Schedulers.newThread())
                .subscribe(count ->
                        {
                            cardTransactionItemList = count;
                            Utility.DEBUG_LOG(TAG, "count:" + count);
                            Utility.DEBUG_LOG(TAG, "cardTransactionItemList:" + cardTransactionItemList);
                        }
                );
    }


    @SuppressLint({"CheckResult", "SetTextI18n"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settlement, container, false);
        // Inflate the layout for this fragment
        unbinder = ButterKnife.bind(this, view);
        assetManager = getResources().getAssets();
        settlementMerchantId.setText(Constants.MERCHANT_ID);
        settlementTerminalId.setText(Constants.TERMINAL_ID);



        // Set the adapter
        //   Context context = view.mContext;
//         recyclerView.setLayoutManager(new LinearLayoutManager(context));
//         appDatabase.transactionDetailDao().getAllTransactionsLive()
//                .observeOn(AndroidSchedulers.mainThread())
//              //  .subscribe(this::setRecycleViewData);

        //SHIVAM CODE HERE

       /* for(int i =0;i<sumTransactionItemList.size();i++){
            try {
                if (sumTransactionItemList.get(i).getTxnType().equals("SALE")) {
                    saleCount.setText(sumTransactionItemList.get(i).getCount().toString());
                    saleAmount.setText(new DecimalFormat("##.##").format(sumTransactionItemList.get(i).getTotalAmount()));
                } else if (sumTransactionItemList.get(i).getTxnType().equals("VOID")) {
                    voidCount.setText(sumTransactionItemList.get(i).getCount().toString());
                    voidAmount.setText(new DecimalFormat("##.##").format(sumTransactionItemList.get(i).getTotalAmount()));
                } else if (sumTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                    cashOutCount.setText(sumTransactionItemList.get(i).getCount().toString());
                    cashOutAmount.setText(new DecimalFormat("##.##").format(sumTransactionItemList.get(i).getTotalAmount()));
                } else if (sumTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                    orbitRedeemCount.setText(sumTransactionItemList.get(i).getCount().toString());
                    orbitRedeemAmount.setText(new DecimalFormat("##.##").format(sumTransactionItemList.get(i).getTotalAmount()));
                } else if (sumTransactionItemList.get(i).getTxnType().equals("REFUND")) {
                    refundCount.setText(sumTransactionItemList.get(i).getCount().toString());
                    refundAmount.setText(new DecimalFormat("##.##").format(sumTransactionItemList.get(i).getTotalAmount()));
                }
            }*/ /// Shivam's code update on 15 Jan 2021 for amount decimal points calculation


        for (int i = 0; i < sumTransactionItemList.size(); i++) {
            try {
                grandTotalCount += sumTransactionItemList.get(i).getCount();
                if (sumTransactionItemList.get(i).getTxnType().equals("COMPLETION") || sumTransactionItemList.get(i).getTxnType().equals("SALE") || sumTransactionItemList.get(i).getTxnType().equals("ADJUST") || sumTransactionItemList.get(i).getTxnType().equals("SALEIPP")) {
                    sumCount = sumCount + sumTransactionItemList.get(i).getCount();
                    sumAmount = sumAmount + sumTransactionItemList.get(i).getTotalAmount();
                    Utility.DEBUG_LOG(TAG, "sumCount:" + sumCount);
                    Utility.DEBUG_LOG(TAG, "sumAmount:" + new DecimalFormat("0.00").format(sumAmount));
                    saleCount.setText(String.valueOf(sumCount));
                    saleAmount.setText(new DecimalFormat("0.00").format(sumAmount));
                } else if (sumTransactionItemList.get(i).getTxnType().equals("VOID")) {
                    voidCount.setText(sumTransactionItemList.get(i).getCount().toString());
                    voidAmount.setText(new DecimalFormat("0.00").format(sumTransactionItemList.get(i).getTotalAmount()));
                } else if (sumTransactionItemList.get(i).getTxnType().equals("CASH OUT")) {
                    cashOutCount.setText(sumTransactionItemList.get(i).getCount().toString());
                    cashOutAmount.setText(new DecimalFormat("0.00").format(sumTransactionItemList.get(i).getTotalAmount()));
                } else if (sumTransactionItemList.get(i).getTxnType().equals("REDEEM")) {
                    orbitRedeemCount.setText(sumTransactionItemList.get(i).getCount().toString());
                    orbitRedeemAmount.setText(new DecimalFormat("0.00").format(sumTransactionItemList.get(i).getTotalAmount()));
                } else if (sumTransactionItemList.get(i).getTxnType().equals("REFUND")) {
                    refundCount.setText(sumTransactionItemList.get(i).getCount().toString());
                    refundAmount.setText(new DecimalFormat("0.00").format(sumTransactionItemList.get(i).getTotalAmount()));
                }
            } catch (Exception e) {
                Utility.DEBUG_LOG("catch", String.valueOf(e));
            }

        }
        Utility.DEBUG_LOG(TAG, "grandTotalCount:" + grandTotalCount);
        if (grandTotalCount == 0) {
            Constants.autoSettleTask = false;
            //Constants.isEcrReq=false;
            DialogUtil.errorDialog(mContext.get(), "Settlement!", "Batch is Empty");
            DashboardContainer.backStack();
        }




        /*if(sumTransactionItemList.size() > 0){
        //    Utility.DEBUG_LOG("TYPE txn", sumTransactionItemList.get(0).txnType);
            saleCount.setText(sumTransactionItemList.get(0).getCount().toString());
            saleAmount.setText(sumTransactionItemList.get(0).getTotalAmount().toString());
            if(sumTransactionItemList.size() > 1) {
                voidCount.setText(sumTransactionItemList.get(1).getCount().toString());
                voidAmount.setText((sumTransactionItemList.get(1).getTotalAmount()).toString());
            }
        }*/


        //initCountDownTimer(view);
        return view;
    }

    // masood count down timer on payment success
//    public void initCountDownTimer(View v) {
//        new CountDownTimer(300000, 1000) {
//            public void onTick(long millisUntilFinished) {
//                if (isbuttonClicked) {
//                    cancel();
//                }
//            }
//
//            public void onFinish() {
//                DashboardContainer.backStack();
//                //DialogUtil.hideKeyboard(getActivity());
//                Utility.DEBUG_LOG("timer", "working");
//            }
//        }.start();
//    }

    // log & display handler
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Utility.DEBUG_LOG(TAG, "before getData");

            printFininshed = msg.getData().getString("msg");
            Utility.DEBUG_LOG(TAG, "printfinihsed:" + msg.getData().getString("msg"));

            try {
                if (printer.getStatus() == 240) {
                    DialogUtil.errorDialog(mContext.get(), "Printer Error", "Roc empty, Refill it!");
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            // Toast.makeText(mContext.get(), msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //Pass your adapter interface in the constructor
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");



        mContext = new WeakReference<>(context);
        context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        Utility.DEBUG_LOG(TAG, "Before sumAndCountTransactions");
        sumAndCountTransactions();
        Utility.DEBUG_LOG(TAG, "Before cardAndCountTransactions");
        cardAndCountTransactions();

//        List<TransactionDetail>  allTransactions = getAllTransactions();
//        Utility.DEBUG_LOG(TAG,"allTransactions:"+allTransactions);
//        if ( allTransactions == null || allTransactions.size() == 0 )
//        {
//            Utility.DEBUG_LOG(TAG,"No transactions found, return");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (getActivity() != null) {
            getActivity().unbindService(conn);
        }
    }

//    private void setRecycleViewData(List<TransactionDetail> transactionDetails) {
//        detailList=transactionDetails;
//
//
//        if (detailList.size() == 0) {
//            if(settlementPerform!=null)
//               settlementPerform.setEnabled(false);
//        }
//        if (recyclerView != null) {
//            MyVoidTransactionRecyclerViewAdapter adapter = new MyVoidTransactionRecyclerViewAdapter(transactionDetails, mContext);
//            recyclerView.setAdapter(adapter);
//            adapter.notifyDataSetChanged();
//        }
//
//
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        hideProgress();
    }


    @Override
    public void toastShow(String str) {
        // getActivity().runOnUiThread(() -> Toast.makeText(mContext, str, Toast.LENGTH_SHORT).show());

    }

    @Override
    public void showProgress(String title) {
        if (getActivity() != null) {

            getActivity().runOnUiThread(() -> showProgressDialog(mContext.get(), title));

        } else {
            Utility.DEBUG_LOG("showProgress", "error");
        }

    }

    @Override
    public void hideProgress() {
        if (getActivity() != null) {
            //Looper.prepare();
            getActivity().runOnUiThread(() -> hideProgressDialog());
            //  Looper.loop();
        } else {
            Utility.DEBUG_LOG("hideProgress", "error");
        }
    }

    @Override
    public void transactionFailed(String title, String error) {
        if (getActivity() != null) {
            // Looper.prepare();
            getActivity().runOnUiThread(() -> DialogUtil.errorDialog(mContext.get(), title, error));
            /// Looper.loop();
        } else {
            Utility.DEBUG_LOG("transactionFailed", "error");
        }
    }


    @Override
    public void transactionSuccess(String content) {
        if (getActivity() != null) {
            // Looper.prepare();
            getActivity().runOnUiThread(() -> DialogUtil.successDialog(mContext.get(), content));
            // Looper.loop();
        } else {
            Utility.DEBUG_LOG("transactionSuccess", "error");
        }
    }

    @Override
    public void transactionSuccessCustomer(String content, TransactionDetail transactionDetail, String copy, String txnType, String Balanace, boolean redeemCheck) {

    }


    @Override
    public void rocEmpty(String content, TransactionDetail transactionDetail, String copy, String txnType) {

    }

    @Override
    public boolean last4Digits(String text) {
        return false;
    }

    @Override
    public boolean last4Digits2(TransactionDetail transactionDetail, String text) {
        return false;
    }

    @Override
    public boolean panManualEntry(TransactionDetail transactionDetail, String text) {
        return false;
    }

    @OnClick(R.id.settlement_perform)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.settlement_perform:
                Utility.DEBUG_LOG(TAG, "R.id.settlement_perform clicked");
                settlementPerform.setEnabled(false);

                DialogUtil.confirmDialog(mContext.get(), "Settle", sweetAlertDialog1 -> {
                    SharedPref.write("isSettleRequiured","Y");
                    doSettlement();

//                    //+ taha 11-03-2021 test
                    Utility.DEBUG_LOG(TAG, "before dismissWithAnimation");
                    sweetAlertDialog1.dismissWithAnimation();
//                    DashboardContainer.switchFragment(new HomeFragment(), "HomeFragment");
//                    //- taha 11-03-2021 test

                    new CountDownTimer(90000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            Utility.DEBUG_LOG(TAG, "+ onTick +");
                            Utility.DEBUG_LOG(TAG, "printFininshed:" + printFininshed);
                            if (settlementPresenter.isPrintFinished() || printFininshed.equals("print finished")) {
                                Utility.DEBUG_LOG(TAG, "printfinish:" + printFininshed);
                                sweetAlertDialog1.dismissWithAnimation();
                                Utility.DEBUG_LOG(TAG, "before hideProgressDialog");
                                hideProgressDialog();
                                Utility.DEBUG_LOG(TAG, "before cancel");
                                cancel();
                                DialogUtil.successDialog(mContext.get(), "Settlement Successful");
                                Utility.checkAutoDownload(mContext.get(),systemManager);
                                //DashboardContainer.switchFragment(new HomeFragment(), "HomeFragment");
                            }
                        }

                        public void onFinish() {
                            Utility.DEBUG_LOG(TAG, "+ onFinish +");
                            Utility.DEBUG_LOG(TAG, "before hideProgressDialog");
                            hideProgressDialog();
                            Utility.DEBUG_LOG(TAG, "before dismissWithAnimation");
                            sweetAlertDialog1.dismissWithAnimation();
                            Utility.DEBUG_LOG(TAG, "before switchFragment(new HomeFragment");
                            //DashboardContainer.switchFragment(new HomeFragment(), "HomeFragment");
                        }
                    }.start();

                });
                hideProgressDialog();


                //+ taha 11-03-2021 test code
//                    List<TransactionDetail> transactionDetails = null;
//                    try {
//                        transactionDetails =getAllTransactions();
//                    } catch (ExecutionException e) {
//                        e.printStackTrace();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    Utility.DEBUG_LOG(TAG,"transactionDetails:"+transactionDetails);
//                    if ( transactionDetails != null)
//                    {
//                        Utility.DEBUG_LOG(TAG,"before new SettlementPresenter");
//                        settlementPresenter = new SettlementPresenter(SettlementFragment.this, handler, isSucc, assetManager, printer, appDatabase, transactionDetails,mContext);
//                        SharedPref.countedTransactionWrite(mContext, cardTransactionItemList);
//
//                        Utility.DEBUG_LOG(TAG,"before SharedPref.writeList(mContext, transactionDetails)");
//                        SharedPref.writeList(mContext, transactionDetails);
//                        Utility.DEBUG_LOG(TAG,"before settlementPresenter.doSettlement");
//                        settlementPresenter.doSettlement();
//                    }
//                    - taha 11-03-2021 test code


        }
    }
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.settlement_perform:
//                Utility.DEBUG_LOG(TAG, "R.id.settlement_perform clicked");
//                settlementPerform.setEnabled(false);
//
//                DialogUtil.confirmDialog(mContext, "Settle", sweetAlertDialog1 -> {
//
//                    doSettlement();
//
////                    //+ taha 11-03-2021 test
//                    Utility.DEBUG_LOG(TAG, "before dismissWithAnimation");
//                    sweetAlertDialog1.dismissWithAnimation();
////                    DashboardContainer.switchFragment(new HomeFragment(), "HomeFragment");
////                    //- taha 11-03-2021 test
//                            Utility.DEBUG_LOG(TAG, "+ onTick +");
//                          //  Utility.DEBUG_LOG(TAG, "printFininshed:" + printFininshed);
//                            if (settlementPresenter.isPrintFinished()) {
//                                Utility.DEBUG_LOG(TAG, "printfinish:" + settlementPresenter.isPrintFinished());
//                                sweetAlertDialog1.dismissWithAnimation();
//                                Utility.DEBUG_LOG(TAG, "before hideProgressDialog");
//                                hideProgressDialog();
//                                Utility.DEBUG_LOG(TAG, "before cancel");
//                                DialogUtil.successDialog(mContext, "Settlement Successful");
//                                //DashboardContainer.switchFragment(new HomeFragment(), "HomeFragment");
//                                Utility.checkAutoDownload(mContext, systemManager);
//                            }else{
//
//                                Utility.DEBUG_LOG(TAG, "+ onFinish +");
//                                Utility.DEBUG_LOG(TAG, "before hideProgressDialog");
//                                hideProgressDialog();
//                                Utility.DEBUG_LOG(TAG, "before dismissWithAnimation");
//                                sweetAlertDialog1.dismissWithAnimation();
//                                Utility.DEBUG_LOG(TAG, "before switchFragment(new HomeFragment");
//                               // DashboardContainer.switchFragment(new HomeFragment(), "HomeFragment");
//
//                            }
//                });
//                hideProgressDialog();
//
//
//                //+ taha 11-03-2021 test code
////                    List<TransactionDetail> transactionDetails = null;
////                    try {
////                        transactionDetails =getAllTransactions();
////                    } catch (ExecutionException e) {
////                        e.printStackTrace();
////                    } catch (InterruptedException e) {
////                        e.printStackTrace();
////                    }
////                    Utility.DEBUG_LOG(TAG,"transactionDetails:"+transactionDetails);
////                    if ( transactionDetails != null)
////                    {
////                        Utility.DEBUG_LOG(TAG,"before new SettlementPresenter");
////                        settlementPresenter = new SettlementPresenter(SettlementFragment.this, handler, isSucc, assetManager, printer, appDatabase, transactionDetails,mContext);
////                        SharedPref.countedTransactionWrite(mContext, cardTransactionItemList);
////
////                        Utility.DEBUG_LOG(TAG,"before SharedPref.writeList(mContext, transactionDetails)");
////                        SharedPref.writeList(mContext, transactionDetails);
////                        Utility.DEBUG_LOG(TAG,"before settlementPresenter.doSettlement");
////                        settlementPresenter.doSettlement();
////                    }
////                    - taha 11-03-2021 test code
//
//
//        }
//    }


    private void doSettlement() {

        isbuttonClicked = true;

            Utility.DEBUG_LOG(TAG, "before new SettlementPresenter");
            settlementPresenter = new SettlementPresenter(SettlementFragment.this, handler, isSucc, assetManager, printer,  getAllTransactions(), mContext.get());

//        SharedPref.countedTransactionWrite(mContext.get(), cardTransactionItemList);
//
//        try {
//            Utility.DEBUG_LOG(TAG, "before getAllTransactions() 2");
//            SharedPref.writeList(mContext.get(), getAllTransactions());
//        } catch (ExecutionException | InterruptedException e) {
//            e.printStackTrace();
//        }


        Utility.DEBUG_LOG(TAG, "before settlementPresenter.doSettlement");
        settlementPresenter.doSettlement();

    }

    private List<TransactionDetail> getAllTransactions(){

        try {
            return ((DashboardContainer)mContext.get()).getAllBatchTransactions();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return new ArrayList<TransactionDetail>();

        }
    }
    // connect service -- start
    /**
     * \Brief connect the VFI-Service and set some devices
     */
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            idevice = IDeviceService.Stub.asInterface(service);
            try {
                iBeeper = idevice.getBeeper();
                printer = idevice.getPrinter();
                TransPrinter.initialize(printer);

                if (Constants.autoSettleTask) {
                    Constants.autoSettleTask = false;
                    Utility.DEBUG_LOG(TAG, "autoSettleTask = false");
                    doSettlement();
                    DashboardContainer.backStack();
                }


                if (Constants.isEcrReq) {
                  //  Constants.isEcrReq = false;
                    //Utility.DEBUG_LOG(TAG, "isEcrReq = false");
                    doSettlement();

                   // DashboardContainer.switchFragment(new HomeFragment(), "HomeFragment");


                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            // Toast.makeText(mContext, "bind service success", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

}
