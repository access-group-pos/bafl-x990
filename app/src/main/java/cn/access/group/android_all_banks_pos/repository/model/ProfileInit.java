package cn.access.group.android_all_banks_pos.repository.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * on 10/29/2019.
 */
@Entity(tableName = "ProfileInit")
public class ProfileInit implements Serializable {
    @PrimaryKey()
    private int tcid;

    @ColumnInfo(name = "AMEX_ISSNUM")
    private String AMEX_ISSNUM;
    @ColumnInfo(name = "UPI_ADJUSTENABLE")
    private String UPI_ADJUSTENABLE;
    @ColumnInfo(name = "MASTER_ADJUSTENABLE")
    private String MASTER_ADJUSTENABLE;
    @ColumnInfo(name = "AMEX_MANUALENTRY")
    private String AMEX_MANUALENTRY;
    @ColumnInfo(name = "ISRECEIPTPRINTED")
    private String ISRECEIPTPRINTED;
    @ColumnInfo(name = "TUPPRIMARYIP")
    private String TUPPRIMARYIP;
    @ColumnInfo(name = "CONNPROFILESCREATED")
    private String CONNPROFILESCREATED;
    @ColumnInfo(name = "ISEXTERNALPPCONN")
    private String ISEXTERNALPPCONN;
    @ColumnInfo(name = "VISA_LAST4DMAG")
    private String VISA_LAST4DMAG;
    @ColumnInfo(name = "PAYPAK_FORCEDLBPENABLE")
    private String PAYPAK_FORCEDLBPENABLE;
    @ColumnInfo(name = "PSTNNUMSEC")
    private String PSTNNUMSEC;
    @ColumnInfo(name = "VISA_BANKFUNDED")
    private String VISA_BANKFUNDED;
    @ColumnInfo(name = "VISA_PRINTEXPIRY")
    private String VISA_PRINTEXPIRY;
    @ColumnInfo(name = "MASTER_CHSERVCODE")
    private String MASTER_CHSERVCODE;
    @ColumnInfo(name = "PAYPAK_CHECKPIN")
    private String PAYPAK_CHECKPIN;
    @ColumnInfo(name = "HELPDESKNUMBER")
    private String HELPDESKNUMBER;
    @ColumnInfo(name = "ACQNAME")
    private String ACQNAME;
    @ColumnInfo(name = "AUDITPRINTDETAILS")
    private String AUDITPRINTDETAILS;
    @ColumnInfo(name = "TUPSECONDARYIP")
    private String TUPSECONDARYIP;
    @ColumnInfo(name = "PASSWORDMANAGEMENT")
    private String PASSWORDMANAGEMENT;
    @ColumnInfo(name = "VISA_MOD10")
    private String VISA_MOD10;
    @ColumnInfo(name = "SETGROUP")
    private String SETGROUP;
    @ColumnInfo(name = "MASTER_CHSIGN")
    private String MASTER_CHSIGN;
    @ColumnInfo(name = "PAYPAK_ISSNAME")
    private String PAYPAK_ISSNAME;
    @ColumnInfo(name = "MSGHEADERLENHEX")
    private String MSGHEADERLENHEX;
    @ColumnInfo(name = "UPI_SCHEME")
    private String UPI_SCHEME;
    @ColumnInfo(name = "SECONDARYPORT")
    private String SECONDARYPORT;
    @ColumnInfo(name = "VISA_PINBYPASS")
    private String VISA_PINBYPASS;
    @ColumnInfo(name = "DESTADDTPDU")
    private String DESTADDTPDU;
    @ColumnInfo(name = "FLASH")
    private String FLASH;
    @ColumnInfo(name = "JCB_FORCEDLBPENABLE")
    private String JCB_FORCEDLBPENABLE;
    @ColumnInfo(name = "AMEX_CHSERVCODE")
    private String AMEX_CHSERVCODE;
    @ColumnInfo(name = "PAYPAK_CHSIGN")
    private String PAYPAK_CHSIGN;
    @ColumnInfo(name = "COUNTRYCODE")
    private String COUNTRYCODE;
    @ColumnInfo(name = "PSTNTIMEOUT")
    private String PSTNTIMEOUT;
    @ColumnInfo(name = "SAVEDEODPRINTDETAILS")
    private String SAVEDEODPRINTDETAILS;
    @ColumnInfo(name = "PRTPOWERFAILURE")
    private String PRTPOWERFAILURE;
    @ColumnInfo(name = "PASSWORDRETRYON")
    private String PASSWORDRETRYON;
    @ColumnInfo(name = "JCB_LAST4DMAG")
    private String JCB_LAST4DMAG;
    @ColumnInfo(name = "CLOCK")
    private String CLOCK;
    @ColumnInfo(name = "TERMINALIP")
    private String TERMINALIP;
    @ColumnInfo(name = "PSTNNUMPRI")
    private String PSTNNUMPRI;
    @ColumnInfo(name = "BATCHNUMBER")
    private String BATCHNUMBER;
    @ColumnInfo(name = "MASTER_PINBYPASS")
    private String MASTER_PINBYPASS;
    @ColumnInfo(name = "PAYPAK_LAST4DSMART")
    private String PAYPAK_LAST4DSMART;
    @ColumnInfo(name = "TERMINALNETMASK")
    private String TERMINALNETMASK;
    @ColumnInfo(name = "DECIMALSEPARATOR")
    private String DECIMALSEPARATOR;
    @ColumnInfo(name = "PSTNINTERCHARTO")
    private String PSTNINTERCHARTO;
    @ColumnInfo(name = "SETTLECOUNTER")
    private String SETTLECOUNTER;
    @ColumnInfo(name = "ACQNUM")
    private String ACQNUM;
    @ColumnInfo(name = "MASTER_PREAUTHENABLE")
    private String MASTER_PREAUTHENABLE;
    @ColumnInfo(name = "MARKETSEGMENT")
    private String MARKETSEGMENT;
    @ColumnInfo(name = "AUTOSETTLE")
    private String AUTOSETTLE;
    @ColumnInfo(name = "TERMINALDNS2")
    private String TERMINALDNS2;
    @ColumnInfo(name = "TERMINALDNS1")
    private String TERMINALDNS1;
    @ColumnInfo(name = "JCB_CHSERVCODE")
    private String JCB_CHSERVCODE;
    @ColumnInfo(name = "BULKDOWNLOADPENDING")
    private String BULKDOWNLOADPENDING;
    @ColumnInfo(name = "TUPPRIMARYPORT")
    private String TUPPRIMARYPORT;
    @ColumnInfo(name = "TERMINALCOUNTRYCODE")
    private String TERMINALCOUNTRYCODE;
    @ColumnInfo(name = "VISA_SCHEME")
    private String VISA_SCHEME;
    @ColumnInfo(name = "MASTER_LAST4DMAG")
    private String MASTER_LAST4DMAG;
    @ColumnInfo(name = "JCB_MANUALENTRY")
    private String JCB_MANUALENTRY;
    @ColumnInfo(name = "STAN")
    private String STAN;
    @ColumnInfo(name = "PAYPAK_ACQUIRER")
    private String PAYPAK_ACQUIRER;
    @ColumnInfo(name = "PAYPAK_MOD10")
    private String PAYPAK_MOD10;
    @ColumnInfo(name = "ISSIMSWDEBUGENABLE")
    private String ISSIMSWDEBUGENABLE;
    @ColumnInfo(name = "JCB_SCHEME")
    private String JCB_SCHEME;
    @ColumnInfo(name = "PRINTBARCODE")
    private String PRINTBARCODE;
    @ColumnInfo(name = "TERMINALID")
    private String TERMINALID;
    @ColumnInfo(name = "AMEXID")
    private String AMEXID;
    @ColumnInfo(name = "JCB_PRINTEXPIRY")
    private String JCB_PRINTEXPIRY;
    @ColumnInfo(name = "CLIENTNAME")
    private String CLIENTNAME;
    @ColumnInfo(name = "MASTER_LAST4DSMART")
    private String MASTER_LAST4DSMART;
    @ColumnInfo(name = "UPI_ADVCASHENABLE")
    private String UPI_ADVCASHENABLE;
    @ColumnInfo(name = "TUPSSLENABLE")
    private String TUPSSLENABLE;
    @ColumnInfo(name = "VISA_MANUALENTRY")
    private String VISA_MANUALENTRY;
    @ColumnInfo(name = "UPI_COMPLETIONENABLE")
    private String UPI_COMPLETIONENABLE;
    @ColumnInfo(name = "MASTER_ACQUIRER")
    private String MASTER_ACQUIRER;
    @ColumnInfo(name = "JCB_BANKFUNDED")
    private String JCB_BANKFUNDED;
    @ColumnInfo(name = "TERMINALWIFIDHCP")
    private String TERMINALWIFIDHCP;
    @ColumnInfo(name = "AUTOSETTLETIME")
    private String AUTOSETTLETIME;
    @ColumnInfo(name = "AMEX_LAST4DSMART")
    private String AMEX_LAST4DSMART;
    @ColumnInfo(name = "EMVCOUNTER")
    private String EMVCOUNTER;
    @ColumnInfo(name = "RECEIVETIMEOUT")
    private String RECEIVETIMEOUT;
    @ColumnInfo(name = "CARDTIMEOUT")
    private String CARDTIMEOUT;
    @ColumnInfo(name = "SERVICEFIRSTTIME")
    private String SERVICEFIRSTTIME;
    @ColumnInfo(name = "ISACTIVATED")
    private String ISACTIVATED;
    @ColumnInfo(name = "AMEX_PRINTEXPIRY")
    private String AMEX_PRINTEXPIRY;
    @ColumnInfo(name = "JCB_VOIDENABLE")
    private String JCB_VOIDENABLE;
    @ColumnInfo(name = "PASSWRD")
    private String PASSWRD;
    @ColumnInfo(name = "TIPENABLED")
    private String TIPENABLED;
    @ColumnInfo(name = "MASTER_ISSUERACTIVATED")
    private String MASTER_ISSUERACTIVATED;
    @ColumnInfo(name = "UPI_PRINTEXPIRY")
    private String UPI_PRINTEXPIRY;
    @ColumnInfo(name = "MASTER_BANKFUNDED")
    private String MASTER_BANKFUNDED;
    @ColumnInfo(name = "OFFLINEPIN")
    private String OFFLINEPIN;
    @ColumnInfo(name = "POSSERIALNO")
    private String POSSERIALNO;
    @ColumnInfo(name = "PAYPAK_COMPLETIONENABLE")
    private String PAYPAK_COMPLETIONENABLE;
    @ColumnInfo(name = "TOPUPPIN")
    private String TOPUPPIN;
    @ColumnInfo(name = "PAYPAK_REFUNDENABLE")
    private String PAYPAK_REFUNDENABLE;
    @ColumnInfo(name = "TERMINALGATEWAY")
    private String TERMINALGATEWAY;
    @ColumnInfo(name = "UPI_REFUNDENABLE")
    private String UPI_REFUNDENABLE;
    @ColumnInfo(name = "AMEX_MOD10")
    private String AMEX_MOD10;
    @ColumnInfo(name = "VISA_COMPLETIONENABLE")
    private String VISA_COMPLETIONENABLE;
    @ColumnInfo(name = "PAYPAK_ADVCASHENABLE")
    private String PAYPAK_ADVCASHENABLE;
    @ColumnInfo(name = "HELPTOUCHMSG")
    private String HELPTOUCHMSG;
    @ColumnInfo(name = "TERMINALWIFIGATEWAY")
    private String TERMINALWIFIGATEWAY;
    @ColumnInfo(name = "UPI_ISSNAME")
    private String UPI_ISSNAME;
    @ColumnInfo(name = "EODPRINTDETAILS")
    private String EODPRINTDETAILS;
    @ColumnInfo(name = "PAYPAK_VOIDENABLE")
    private String PAYPAK_VOIDENABLE;
    @ColumnInfo(name = "TUPSECONDARYPORT")
    private String TUPSECONDARYPORT;
    @ColumnInfo(name = "RSPTIMEOUT")
    private String RSPTIMEOUT;
    @ColumnInfo(name = "PAYPAK_ISSUERACTIVATED")
    private String PAYPAK_ISSUERACTIVATED;
    @ColumnInfo(name = "PRIMARYIP")
    private String PRIMARYIP;
    @ColumnInfo(name = "VISA_ISSNUM")
    private String VISA_ISSNUM;
    @ColumnInfo(name = "CASHBACKENABLED")
    private String CASHBACKENABLED;
    @ColumnInfo(name = "PANLENGTHMAX")
    private String PANLENGTHMAX;
    @ColumnInfo(name = "TERMINALWIFIIP")
    private String TERMINALWIFIIP;
    @ColumnInfo(name = "PSTNSETTLNUMSEC")
    private String PSTNSETTLNUMSEC;
    @ColumnInfo(name = "JCB_ADJUSTENABLE")
    private String JCB_ADJUSTENABLE;
    @ColumnInfo(name = "AMEX_ISSNAME")
    private String AMEX_ISSNAME;
    @ColumnInfo(name = "ALLOWFALLBACK")
    private String ALLOWFALLBACK;
    @ColumnInfo(name = "VISA_ISSNAME")
    private String VISA_ISSNAME;
    @ColumnInfo(name = "MASTER_FORCEDLBPENABLE")
    private String MASTER_FORCEDLBPENABLE;
    @ColumnInfo(name = "MASTER_MOD10")
    private String MASTER_MOD10;
    @ColumnInfo(name = "JCB_ISSNUM")
    private String JCB_ISSNUM;
    @ColumnInfo(name = "ISLOCALHOST")
    private String ISLOCALHOST;
    @ColumnInfo(name = "VCSETHTIMEOUT")
    private String VCSETHTIMEOUT;
    @ColumnInfo(name = "WIFISSID")
    private String WIFISSID;
    @ColumnInfo(name = "TERMINALWIFIDNS2")
    private String TERMINALWIFIDNS2;
    @ColumnInfo(name = "PSTNPREFIX")
    private String PSTNPREFIX;
    @ColumnInfo(name = "SECONDARYMEDIUM")
    private String SECONDARYMEDIUM;
    @ColumnInfo(name = "TERMINALWIFIDNS1")
    private String TERMINALWIFIDNS1;
    @ColumnInfo(name = "PAYPAK_ISSNUM")
    private String PAYPAK_ISSNUM;
    @ColumnInfo(name = "AMEX_SCHEME")
    private String AMEX_SCHEME;
    @ColumnInfo(name = "THOUSANDSEPARATOR")
    private String THOUSANDSEPARATOR;
    @ColumnInfo(name = "AGENTCODE")
    private String AGENTCODE;
    @ColumnInfo(name = "PRINTBANKRECON")
    private String PRINTBANKRECON;
    @ColumnInfo(name = "VISA_FORCEDLBPENABLE")
    private String VISA_FORCEDLBPENABLE;
    @ColumnInfo(name = "UPI_CHECKEXPIRY")
    private String UPI_CHECKEXPIRY;
    @ColumnInfo(name = "PRINTREVERSALRCPT")
    private String PRINTREVERSALRCPT;
    @ColumnInfo(name = "INVOICENUMBER")
    private String INVOICENUMBER;
    @ColumnInfo(name = "BOOTFIRSTTIME")
    private String BOOTFIRSTTIME;
    @ColumnInfo(name = "TERMINALDHCP")
    private String TERMINALDHCP;
    @ColumnInfo(name = "PREAUTHMAXCOMPLETIONDAYS")
    private String PREAUTHMAXCOMPLETIONDAYS;
    @ColumnInfo(name = "COMPLETIONTHERSHOLD")
    private String COMPLETIONTHERSHOLD;
    @ColumnInfo(name = "MASTER_REFUNDENABLE")
    private String MASTER_REFUNDENABLE;
    @ColumnInfo(name = "UPI_FORCEDLBPENABLE")
    private String UPI_FORCEDLBPENABLE;
    @ColumnInfo(name = "MASTER_MANUALENTRY")
    private String MASTER_MANUALENTRY;
    @ColumnInfo(name = "PRINTEMVLOGS")
    private String PRINTEMVLOGS;
    @ColumnInfo(name = "JCB_REFUNDENABLE")
    private String JCB_REFUNDENABLE;
    @ColumnInfo(name = "UPI_ISSUERACTIVATED")
    private String UPI_ISSUERACTIVATED;
    @ColumnInfo(name = "PASSWORDSUPERUSER")
    private String PASSWORDSUPERUSER;
    @ColumnInfo(name = "VISA_ISSUERACTIVATED")
    private String VISA_ISSUERACTIVATED;
    @ColumnInfo(name = "UPI_PREAUTHENABLE")
    private String UPI_PREAUTHENABLE;
    @ColumnInfo(name = "PAYPAK_CHECKEXPIRY")
    private String PAYPAK_CHECKEXPIRY;
    @ColumnInfo(name = "ISSTATUSDEBUGENABLE")
    private String ISSTATUSDEBUGENABLE;
    @ColumnInfo(name = "TPKGISKE")
    private String TPKGISKE;
    @ColumnInfo(name = "MASTER_COMPLETIONENABLE")
    private String MASTER_COMPLETIONENABLE;
    @ColumnInfo(name = "LANGUAGEDEFAULT")
    private String LANGUAGEDEFAULT;
    @ColumnInfo(name = "UPI_MANUALENTRY")
    private String UPI_MANUALENTRY;
    @ColumnInfo(name = "VISA_LAST4DSMART")
    private String VISA_LAST4DSMART;
    @ColumnInfo(name = "AMEX_ADVCASHENABLE")
    private String AMEX_ADVCASHENABLE;
    @ColumnInfo(name = "PSTNSETTLNUMPRI")
    private String PSTNSETTLNUMPRI;
    @ColumnInfo(name = "VISA_PREAUTHENABLE")
    private String VISA_PREAUTHENABLE;
    @ColumnInfo(name = "TIPTHERSHOLD")
    private String TIPTHERSHOLD;
    @ColumnInfo(name = "MASTER_SCHEME")
    private String MASTER_SCHEME;
    @ColumnInfo(name = "UPI_ISSNUM")
    private String UPI_ISSNUM;
    @ColumnInfo(name = "PASSWORDUSER")
    private String PASSWORDUSER;
    @ColumnInfo(name = "AMEX_REFUNDENABLE")
    private String AMEX_REFUNDENABLE;
    @ColumnInfo(name = "PREAUTHCLEARCOUNT")
    private String PREAUTHCLEARCOUNT;
    @ColumnInfo(name = "LANGUAGE")
    private String LANGUAGE;
    @ColumnInfo(name = "RECEIPTFOOTER1")
    private String RECEIPTFOOTER1;
    @ColumnInfo(name = "RECEIPTFOOTER2")
    private String RECEIPTFOOTER2;
    @ColumnInfo(name = "AMEX_ISSUERACTIVATED")
    private String AMEX_ISSUERACTIVATED;
    @ColumnInfo(name = "JCB_CHECKPIN")
    private String JCB_CHECKPIN;
    @ColumnInfo(name = "CLIENTTYPE")
    private String CLIENTTYPE;
    @ColumnInfo(name = "UPI_MOD10")
    private String UPI_MOD10;
    @ColumnInfo(name = "PAYPAK_SCHEME")
    private String PAYPAK_SCHEME;
    @ColumnInfo(name = "UPI_ACQUIRER")
    private String UPI_ACQUIRER;
    @ColumnInfo(name = "UPI_CHSIGN")
    private String UPI_CHSIGN;
    @ColumnInfo(name = "MASTER_CHECKPIN")
    private String MASTER_CHECKPIN;
    @ColumnInfo(name = "VISA_ADJUSTENABLE")
    private String VISA_ADJUSTENABLE;
    @ColumnInfo(name = "UPI_LAST4DMAG")
    private String UPI_LAST4DMAG;
    @ColumnInfo(name = "AMEX_FORCEDLBPENABLE")
    private String AMEX_FORCEDLBPENABLE;
    @ColumnInfo(name = "PAYPAK_PRINTEXPIRY")
    private String PAYPAK_PRINTEXPIRY;
    @ColumnInfo(name = "BTMACADDRESS")
    private String BTMACADDRESS;
    @ColumnInfo(name = "VISA_REFUNDENABLE")
    private String VISA_REFUNDENABLE;
    @ColumnInfo(name = "SECONDARYIP")
    private String SECONDARYIP;
    @ColumnInfo(name = "JCB_CHECKEXPIRY")
    private String JCB_CHECKEXPIRY;
    @ColumnInfo(name = "ISSSLENABLE")
    private String ISSSLENABLE;
    @ColumnInfo(name = "ENABLEAUTOPINENTER")
    private String ENABLEAUTOPINENTER;
    @ColumnInfo(name = "LANGUAGEACQNUM")
    private String LANGUAGEACQNUM;
    @ColumnInfo(name = "PAYPAK_ADJUSTENABLE")
    private String PAYPAK_ADJUSTENABLE;
    @ColumnInfo(name = "JCB_ADVCASHENABLE")
    private String JCB_ADVCASHENABLE;
    @ColumnInfo(name = "TIPNUMADJUST")
    private String TIPNUMADJUST;
    @ColumnInfo(name = "TERMFORCEDLBPMAGENABLE")
    private String TERMFORCEDLBPMAGENABLE;
    @ColumnInfo(name = "VISA_ADVCASHENABLE")
    private String VISA_ADVCASHENABLE;
    @ColumnInfo(name = "CTLSENABLED")
    private String CTLSENABLED;
    @ColumnInfo(name = "UPI_PINBYPASS")
    private String UPI_PINBYPASS;
    @ColumnInfo(name = "PAYPAK_BANKFUNDED")
    private String PAYPAK_BANKFUNDED;
    @ColumnInfo(name = "JCB_ACQUIRER")
    private String JCB_ACQUIRER;
    @ColumnInfo(name = "AMEX_COMPLETIONENABLE")
    private String AMEX_COMPLETIONENABLE;
    @ColumnInfo(name = "F49ENABLE")
    private String F49ENABLE;
    @ColumnInfo(name = "TOPUPENABLED")
    private String TOPUPENABLED;
    @ColumnInfo(name = "UPI_CHSERVCODE")
    private String UPI_CHSERVCODE;
    @ColumnInfo(name = "UPDATEME")
    private String UPDATEME;
    @ColumnInfo(name = "AMEX_PREAUTHENABLE")
    private String AMEX_PREAUTHENABLE;
    @ColumnInfo(name = "EMVLOGSENABLED")
    private String EMVLOGSENABLED;
    @ColumnInfo(name = "TMSIP")
    private String TMSIP;
    @ColumnInfo(name = "VISA_CHSIGN")
    private String VISA_CHSIGN;
    @ColumnInfo(name = "AMEX_CHECKEXPIRY")
    private String AMEX_CHECKEXPIRY;
    @ColumnInfo(name = "MERCHANTID")
    private String MERCHANTID;
    @ColumnInfo(name = "MASTER_VOIDENABLE")
    private String MASTER_VOIDENABLE;
    @ColumnInfo(name = "VENDORCODE")
    private String VENDORCODE;
    @ColumnInfo(name = "RECEIPTPRINTED")
    private String RECEIPTPRINTED;
    @ColumnInfo(name = "JCB_CHSIGN")
    private String JCB_CHSIGN;
    @ColumnInfo(name = "VISA_VOIDENABLE")
    private String VISA_VOIDENABLE;
    @ColumnInfo(name = "MASTER_ISSNAME")
    private String MASTER_ISSNAME;
    @ColumnInfo(name = "Reponsemessage")
    private String Reponsemessage;
    @ColumnInfo(name = "HIDECARDHOLDERNAME")
    private String HIDECARDHOLDERNAME;
    @ColumnInfo(name = "MERCHANTNAME")
    private String MERCHANTNAME;
    @ColumnInfo(name = "JCB_MOD10")
    private String JCB_MOD10;
    @ColumnInfo(name = "PRINTDECLINEDRCPT")
    private String PRINTDECLINEDRCPT;
    @ColumnInfo(name = "UPI_LAST4DSMART")
    private String UPI_LAST4DSMART;
    @ColumnInfo(name = "ADVCASHENABLE")
    private String ADVCASHENABLE;
    @ColumnInfo(name = "AMEX_LAST4DMAG")
    private String AMEX_LAST4DMAG;
    @ColumnInfo(name = "UPI_VOIDENABLE")
    private String UPI_VOIDENABLE;
    @ColumnInfo(name = "PREAUTHBATCHMAXCOUNT")
    private String PREAUTHBATCHMAXCOUNT;
    @ColumnInfo(name = "ADVICEENABLED")
    private String ADVICEENABLED;
    @ColumnInfo(name = "TERMFORCEDLBPMANUALENABLE")
    private String TERMFORCEDLBPMANUALENABLE;
    @ColumnInfo(name = "MASTER_ADVCASHENABLE")
    private String MASTER_ADVCASHENABLE;
    @ColumnInfo(name = "PAYPAK_PREAUTHENABLE")
    private String PAYPAK_PREAUTHENABLE;
    @ColumnInfo(name = "AMEX_VOIDENABLE")
    private String AMEX_VOIDENABLE;
    @ColumnInfo(name = "PAYPAK_PINBYPASS")
    private String PAYPAK_PINBYPASS;
    @ColumnInfo(name = "AMEX_BANKFUNDED")
    private String AMEX_BANKFUNDED;
    @ColumnInfo(name = "MERCHANTADDRESS3")
    private String MERCHANTADDRESS3;
    @ColumnInfo(name = "MERCHANTADDRESS2")
    private String MERCHANTADDRESS2;
    @ColumnInfo(name = "MERCHANTADDRESS1")
    private String MERCHANTADDRESS1;
    @ColumnInfo(name = "JCB_PREAUTHENABLE")
    private String JCB_PREAUTHENABLE;
    @ColumnInfo(name = "UPI_CHECKPIN")
    private String UPI_CHECKPIN;
    @ColumnInfo(name = "USERID")
    private String USERID;
    @ColumnInfo(name = "TERMINALMANUALENTRY")
    private String TERMINALMANUALENTRY;
    @ColumnInfo(name = "ECRENABLED")
    private String ECRENABLED;
    @ColumnInfo(name = "VISA_CHSERVCODE")
    private String VISA_CHSERVCODE;
    @ColumnInfo(name = "MERCHANTADDRESS4")
    private String MERCHANTADDRESS4;
    @ColumnInfo(name = "VISA_CHECKEXPIRY")
    private String VISA_CHECKEXPIRY;
    @ColumnInfo(name = "PINTIMEOUT")
    private String PINTIMEOUT;
    @ColumnInfo(name = "PREDIALENABLED")
    private String PREDIALENABLED;
    @ColumnInfo(name = "PRINTSERIALNUM")
    private String PRINTSERIALNUM;
    @ColumnInfo(name = "PANLENGTHMIN")
    private String PANLENGTHMIN;
    @ColumnInfo(name = "POWERDOWNTIME")
    private String POWERDOWNTIME;
    @ColumnInfo(name = "SETTLERANGE")
    private String SETTLERANGE;
    @ColumnInfo(name = "MASTER_CHECKEXPIRY")
    private String MASTER_CHECKEXPIRY;
    @ColumnInfo(name = "AMEX_ADJUSTENABLE")
    private String AMEX_ADJUSTENABLE;
    @ColumnInfo(name = "PAYPAK_MANUALENTRY")
    private String PAYPAK_MANUALENTRY;
    @ColumnInfo(name = "JCB_ISSUERACTIVATED")
    private String JCB_ISSUERACTIVATED;
    @ColumnInfo(name = "SRCADDTPDU")
    private String SRCADDTPDU;
    @ColumnInfo(name = "INITTERM")
    private String INITTERM;
    @ColumnInfo(name = "PRIMARYPORT")
    private String PRIMARYPORT;
    @ColumnInfo(name = "ISTPDUENABLE")
    private String ISTPDUENABLE;
    @ColumnInfo(name = "VISA_ACQUIRER")
    private String VISA_ACQUIRER;
    @ColumnInfo(name = "AMEX_ACQUIRER")
    private String AMEX_ACQUIRER;
    @ColumnInfo(name = "AMEX_CHECKPIN")
    private String AMEX_CHECKPIN;
    @ColumnInfo(name = "BATCHMAXCOUNT")
    private String BATCHMAXCOUNT;
    @ColumnInfo(name = "PRIMARYMEDIUM")
    private String PRIMARYMEDIUM;
    @ColumnInfo(name = "VISA_CHECKPIN")
    private String VISA_CHECKPIN;
    @ColumnInfo(name = "MASTER_ISSNUM")
    private String MASTER_ISSNUM;
    @ColumnInfo(name = "PASSWORDSYSTEM")
    private String PASSWORDSYSTEM;
    @ColumnInfo(name = "JCB_ISSNAME")
    private String JCB_ISSNAME;
    @ColumnInfo(name = "TMSPORT")
    private String TMSPORT;
    @ColumnInfo(name = "INTERPACKETTIMEOUT")
    private String INTERPACKETTIMEOUT;
    @ColumnInfo(name = "PRINTCARDREMRCPT")
    private String PRINTCARDREMRCPT;
    @ColumnInfo(name = "AMEX_PINBYPASS")
    private String AMEX_PINBYPASS;
    @ColumnInfo(name = "TERMINALWIFINETMASK")
    private String TERMINALWIFINETMASK;
    @ColumnInfo(name = "JCB_LAST4DSMART")
    private String JCB_LAST4DSMART;
    @ColumnInfo(name = "AMEX_CHSIGN")
    private String AMEX_CHSIGN;
    @ColumnInfo(name = "PAYPAK_CHSERVCODE")
    private String PAYPAK_CHSERVCODE;
    @ColumnInfo(name = "TERMINALTOTCURRENCIES")
    private String TERMINALTOTCURRENCIES;
    @ColumnInfo(name = "ISDEBUGENABLE")
    private String ISDEBUGENABLE;
    @ColumnInfo(name = "PAYPAK_LAST4DMAG")
    private String PAYPAK_LAST4DMAG;
    @ColumnInfo(name = "PSTNINITNUMBER")
    private String PSTNINITNUMBER;
    @ColumnInfo(name = "JCB_COMPLETIONENABLE")
    private String JCB_COMPLETIONENABLE;
    @ColumnInfo(name = "AMOUNTENTRYTIMEOUT")
    private String AMOUNTENTRYTIMEOUT;
    @ColumnInfo(name = "WIFIPASSWORD")
    private String WIFIPASSWORD;
    @ColumnInfo(name = "TERMFORCEDLBPENABLE")
    private String TERMFORCEDLBPENABLE;
    @ColumnInfo(name = "HELPALFAMSG")
    private String HELPALFAMSG;
    @ColumnInfo(name = "UPI_BANKFUNDED")
    private String UPI_BANKFUNDED;
    @ColumnInfo(name = "TERMINALCURRENCY2")
    private String TERMINALCURRENCY2;
    @ColumnInfo(name = "TERMINALCURRENCY1")
    private String TERMINALCURRENCY1;
    @ColumnInfo(name = "TERMINALAPN")
    private String TERMINALAPN;
    @ColumnInfo(name = "JCB_PINBYPASS")
    private String JCB_PINBYPASS;
    @ColumnInfo(name = "MASTER_PRINTEXPIRY")
    private String MASTER_PRINTEXPIRY;






    public ProfileInit(){

    }


//    public ProfileInit(String amex_issnum, String upi_adjustenable, String master_adjustenable, String amex_manualentry, String isreceiptprinted, String tupprimaryip, String connprofilescreated, String isexternalppconn, String visa_last4DMAG, String paypak_forcedlbpenable, String pstnnumsec, String visa_bankfunded, String visa_printexpiry, String master_chservcode, String paypak_checkpin, String helpdesknumber, String acqname, String auditprintdetails, String tupsecondaryip, String passwordmanagement, String visa_mod10, String setgroup, String master_chsign, String paypak_issname, String msgheaderlenhex, String upi_scheme, String secondaryport, String visa_pinbypass, String destaddtpdu, String flash, String jcb_forcedlbpenable, String amex_chservcode, String paypak_chsign, String countrycode, String pstntimeout, String savedeodprintdetails, String prtpowerfailure, String passwordretryon, String jcb_last4DMAG, String clock, String terminalip, String pstnnumpri, String batchnumber, String master_pinbypass, String paypak_last4DSMART, String terminalnetmask, String decimalseparator, String pstnintercharto, String settlecounter, String acqnum, String master_preauthenable, String marketsegment, String autosettle, String terminaldns2, String terminaldns1, String jcb_chservcode, String bulkdownloadpending, String tupprimaryport, String terminalcountrycode, String visa_scheme, String master_last4DMAG, String jcb_manualentry, String stan, String paypak_acquirer, String paypak_mod10, String issimswdebugenable, String jcb_scheme, String printbarcode, String terminalid, String amexid, String jcb_printexpiry, String clientname, String master_last4DSMART, String upi_advcashenable, String tupsslenable, String visa_manualentry, String upi_completionenable, String master_acquirer, String jcb_bankfunded, String terminalwifidhcp, String autosettletime, String amex_last4DSMART, String emvcounter, String receivetimeout, String cardtimeout, String servicefirsttime, String isactivated, String amex_printexpiry, String jcb_voidenable, String passwrd, String tipenabled, String master_issueractivated, String upi_printexpiry, String master_bankfunded, String offlinepin, String posserialno, String paypak_completionenable, String topuppin, String paypak_refundenable, String terminalgateway, String upi_refundenable, String amex_mod10, String visa_completionenable, String paypak_advcashenable, String helptouchmsg, String terminalwifigateway, String upi_issname, String eodprintdetails, String paypak_voidenable, String tupsecondaryport, String rsptimeout, String paypak_issueractivated, String primaryip, String visa_issnum, String cashbackenabled, String panlengthmax, String terminalwifiip, String pstnsettlnumsec, String jcb_adjustenable, String amex_issname, String allowfallback, String visa_issname, String master_forcedlbpenable, String master_mod10, String jcb_issnum, String islocalhost, String vcsethtimeout, String wifissid, String terminalwifidns2, String pstnprefix, String secondarymedium, String terminalwifidns1, String paypak_issnum, String amex_scheme, String thousandseparator, String agentcode, String printbankrecon, String visa_forcedlbpenable, String upi_checkexpiry, String printreversalrcpt, String invoicenumber, String bootfirsttime, String terminaldhcp, String preauthmaxcompletiondays, String completionthershold, String master_refundenable, String upi_forcedlbpenable, String master_manualentry, String printemvlogs, String jcb_refundenable, String upi_issueractivated, String passwordsuperuser, String visa_issueractivated, String upi_preauthenable, String paypak_checkexpiry, String isstatusdebugenable, String tpkgiske, String master_completionenable, String languagedefault, String upi_manualentry, String visa_last4DSMART, String amex_advcashenable, String pstnsettlnumpri, String visa_preauthenable, String tipthershold, String master_scheme, String upi_issnum, String passworduser, String amex_refundenable, String preauthclearcount, String language, String receiptfooter1, String receiptfooter2, String amex_issueractivated, String jcb_checkpin, String clienttype, String upi_mod10, String paypak_scheme, String upi_acquirer, String upi_chsign, String master_checkpin, String visa_adjustenable, String upi_last4DMAG, String amex_forcedlbpenable, String paypak_printexpiry, String btmacaddress, String visa_refundenable, String secondaryip, String jcb_checkexpiry, String issslenable, String enableautopinenter, String languageacqnum, String paypak_adjustenable, String jcb_advcashenable, String tipnumadjust, String termforcedlbpmagenable, String visa_advcashenable, String ctlsenabled, String upi_pinbypass, String paypak_bankfunded, String jcb_acquirer, String amex_completionenable, String f49ENABLE, String topupenabled, String upi_chservcode, String updateme, String amex_preauthenable, String emvlogsenabled, String tmsip, String visa_chsign, String amex_checkexpiry, String merchantid, String master_voidenable, String vendorcode, String receiptprinted, String jcb_chsign, String visa_voidenable, String master_issname, String reponsemessage, String hidecardholdername, String merchantname, String jcb_mod10, String printdeclinedrcpt, String upi_last4DSMART, String advcashenable, String amex_last4DMAG, String upi_voidenable, String preauthbatchmaxcount, String adviceenabled, String termforcedlbpmanualenable, String master_advcashenable, String paypak_preauthenable, String amex_voidenable, String paypak_pinbypass, String amex_bankfunded, String merchantaddress3, String merchantaddress2, String merchantaddress1, String jcb_preauthenable, String upi_checkpin, String userid, String terminalmanualentry, String ecrenabled, String visa_chservcode, String merchantaddress4, String visa_checkexpiry, String pintimeout, String predialenabled, String printserialnum, String panlengthmin, String powerdowntime, String settlerange, String master_checkexpiry, String amex_adjustenable, String paypak_manualentry, String jcb_issueractivated, String srcaddtpdu, String initterm, String primaryport, String istpduenable, String visa_acquirer, String amex_acquirer, String amex_checkpin, String batchmaxcount, String primarymedium, String visa_checkpin, String master_issnum, String passwordsystem, String jcb_issname, String tmsport, String interpackettimeout, String printcardremrcpt, String amex_pinbypass, String terminalwifinetmask, String jcb_last4DSMART, String amex_chsign, String paypak_chservcode, String terminaltotcurrencies, String isdebugenable, String paypak_last4DMAG, String pstninitnumber, String jcb_completionenable, String amountentrytimeout, String wifipassword, String termforcedlbpenable, String helpalfamsg, String upi_bankfunded, String terminalcurrency2, String terminalcurrency1, String terminalapn, String jcb_pinbypass, String master_printexpiry) {
//        AMEX_ISSNUM = amex_issnum;
//        UPI_ADJUSTENABLE = upi_adjustenable;
//        MASTER_ADJUSTENABLE = master_adjustenable;
//        AMEX_MANUALENTRY = amex_manualentry;
//        ISRECEIPTPRINTED = isreceiptprinted;
//        TUPPRIMARYIP = tupprimaryip;
//        CONNPROFILESCREATED = connprofilescreated;
//        ISEXTERNALPPCONN = isexternalppconn;
//        VISA_LAST4DMAG = visa_last4DMAG;
//        PAYPAK_FORCEDLBPENABLE = paypak_forcedlbpenable;
//        PSTNNUMSEC = pstnnumsec;
//        VISA_BANKFUNDED = visa_bankfunded;
//        VISA_PRINTEXPIRY = visa_printexpiry;
//        MASTER_CHSERVCODE = master_chservcode;
//        PAYPAK_CHECKPIN = paypak_checkpin;
//        HELPDESKNUMBER = helpdesknumber;
//        ACQNAME = acqname;
//        AUDITPRINTDETAILS = auditprintdetails;
//        TUPSECONDARYIP = tupsecondaryip;
//        PASSWORDMANAGEMENT = passwordmanagement;
//        VISA_MOD10 = visa_mod10;
//        SETGROUP = setgroup;
//        MASTER_CHSIGN = master_chsign;
//        PAYPAK_ISSNAME = paypak_issname;
//        MSGHEADERLENHEX = msgheaderlenhex;
//        UPI_SCHEME = upi_scheme;
//        SECONDARYPORT = secondaryport;
//        VISA_PINBYPASS = visa_pinbypass;
//        DESTADDTPDU = destaddtpdu;
//        FLASH = flash;
//        JCB_FORCEDLBPENABLE = jcb_forcedlbpenable;
//        AMEX_CHSERVCODE = amex_chservcode;
//        PAYPAK_CHSIGN = paypak_chsign;
//        COUNTRYCODE = countrycode;
//        PSTNTIMEOUT = pstntimeout;
//        SAVEDEODPRINTDETAILS = savedeodprintdetails;
//        PRTPOWERFAILURE = prtpowerfailure;
//        PASSWORDRETRYON = passwordretryon;
//        JCB_LAST4DMAG = jcb_last4DMAG;
//        CLOCK = clock;
//        TERMINALIP = terminalip;
//        PSTNNUMPRI = pstnnumpri;
//        BATCHNUMBER = batchnumber;
//        MASTER_PINBYPASS = master_pinbypass;
//        PAYPAK_LAST4DSMART = paypak_last4DSMART;
//        TERMINALNETMASK = terminalnetmask;
//        DECIMALSEPARATOR = decimalseparator;
//        PSTNINTERCHARTO = pstnintercharto;
//        SETTLECOUNTER = settlecounter;
//        ACQNUM = acqnum;
//        MASTER_PREAUTHENABLE = master_preauthenable;
//        MARKETSEGMENT = marketsegment;
//        AUTOSETTLE = autosettle;
//        TERMINALDNS2 = terminaldns2;
//        TERMINALDNS1 = terminaldns1;
//        JCB_CHSERVCODE = jcb_chservcode;
//        BULKDOWNLOADPENDING = bulkdownloadpending;
//        TUPPRIMARYPORT = tupprimaryport;
//        TERMINALCOUNTRYCODE = terminalcountrycode;
//        VISA_SCHEME = visa_scheme;
//        MASTER_LAST4DMAG = master_last4DMAG;
//        JCB_MANUALENTRY = jcb_manualentry;
//        STAN = stan;
//        PAYPAK_ACQUIRER = paypak_acquirer;
//        PAYPAK_MOD10 = paypak_mod10;
//        ISSIMSWDEBUGENABLE = issimswdebugenable;
//        JCB_SCHEME = jcb_scheme;
//        PRINTBARCODE = printbarcode;
//        TERMINALID = terminalid;
//        AMEXID = amexid;
//        JCB_PRINTEXPIRY = jcb_printexpiry;
//        CLIENTNAME = clientname;
//        MASTER_LAST4DSMART = master_last4DSMART;
//        UPI_ADVCASHENABLE = upi_advcashenable;
//        TUPSSLENABLE = tupsslenable;
//        VISA_MANUALENTRY = visa_manualentry;
//        UPI_COMPLETIONENABLE = upi_completionenable;
//        MASTER_ACQUIRER = master_acquirer;
//        JCB_BANKFUNDED = jcb_bankfunded;
//        TERMINALWIFIDHCP = terminalwifidhcp;
//        AUTOSETTLETIME = autosettletime;
//        AMEX_LAST4DSMART = amex_last4DSMART;
//        EMVCOUNTER = emvcounter;
//        RECEIVETIMEOUT = receivetimeout;
//        CARDTIMEOUT = cardtimeout;
//        SERVICEFIRSTTIME = servicefirsttime;
//        ISACTIVATED = isactivated;
//        AMEX_PRINTEXPIRY = amex_printexpiry;
//        JCB_VOIDENABLE = jcb_voidenable;
//        PASSWRD = passwrd;
//        TIPENABLED = tipenabled;
//        MASTER_ISSUERACTIVATED = master_issueractivated;
//        UPI_PRINTEXPIRY = upi_printexpiry;
//        MASTER_BANKFUNDED = master_bankfunded;
//        OFFLINEPIN = offlinepin;
//        POSSERIALNO = posserialno;
//        PAYPAK_COMPLETIONENABLE = paypak_completionenable;
//        TOPUPPIN = topuppin;
//        PAYPAK_REFUNDENABLE = paypak_refundenable;
//        TERMINALGATEWAY = terminalgateway;
//        UPI_REFUNDENABLE = upi_refundenable;
//        AMEX_MOD10 = amex_mod10;
//        VISA_COMPLETIONENABLE = visa_completionenable;
//        PAYPAK_ADVCASHENABLE = paypak_advcashenable;
//        HELPTOUCHMSG = helptouchmsg;
//        TERMINALWIFIGATEWAY = terminalwifigateway;
//        UPI_ISSNAME = upi_issname;
//        EODPRINTDETAILS = eodprintdetails;
//        PAYPAK_VOIDENABLE = paypak_voidenable;
//        TUPSECONDARYPORT = tupsecondaryport;
//        RSPTIMEOUT = rsptimeout;
//        PAYPAK_ISSUERACTIVATED = paypak_issueractivated;
//        PRIMARYIP = primaryip;
//        VISA_ISSNUM = visa_issnum;
//        CASHBACKENABLED = cashbackenabled;
//        PANLENGTHMAX = panlengthmax;
//        TERMINALWIFIIP = terminalwifiip;
//        PSTNSETTLNUMSEC = pstnsettlnumsec;
//        JCB_ADJUSTENABLE = jcb_adjustenable;
//        AMEX_ISSNAME = amex_issname;
//        ALLOWFALLBACK = allowfallback;
//        VISA_ISSNAME = visa_issname;
//        MASTER_FORCEDLBPENABLE = master_forcedlbpenable;
//        MASTER_MOD10 = master_mod10;
//        JCB_ISSNUM = jcb_issnum;
//        ISLOCALHOST = islocalhost;
//        VCSETHTIMEOUT = vcsethtimeout;
//        WIFISSID = wifissid;
//        TERMINALWIFIDNS2 = terminalwifidns2;
//        PSTNPREFIX = pstnprefix;
//        SECONDARYMEDIUM = secondarymedium;
//        TERMINALWIFIDNS1 = terminalwifidns1;
//        PAYPAK_ISSNUM = paypak_issnum;
//        AMEX_SCHEME = amex_scheme;
//        THOUSANDSEPARATOR = thousandseparator;
//        AGENTCODE = agentcode;
//        PRINTBANKRECON = printbankrecon;
//        VISA_FORCEDLBPENABLE = visa_forcedlbpenable;
//        UPI_CHECKEXPIRY = upi_checkexpiry;
//        PRINTREVERSALRCPT = printreversalrcpt;
//        INVOICENUMBER = invoicenumber;
//        BOOTFIRSTTIME = bootfirsttime;
//        TERMINALDHCP = terminaldhcp;
//        PREAUTHMAXCOMPLETIONDAYS = preauthmaxcompletiondays;
//        COMPLETIONTHERSHOLD = completionthershold;
//        MASTER_REFUNDENABLE = master_refundenable;
//        UPI_FORCEDLBPENABLE = upi_forcedlbpenable;
//        MASTER_MANUALENTRY = master_manualentry;
//        PRINTEMVLOGS = printemvlogs;
//        JCB_REFUNDENABLE = jcb_refundenable;
//        UPI_ISSUERACTIVATED = upi_issueractivated;
//        PASSWORDSUPERUSER = passwordsuperuser;
//        VISA_ISSUERACTIVATED = visa_issueractivated;
//        UPI_PREAUTHENABLE = upi_preauthenable;
//        PAYPAK_CHECKEXPIRY = paypak_checkexpiry;
//        ISSTATUSDEBUGENABLE = isstatusdebugenable;
//        TPKGISKE = tpkgiske;
//        MASTER_COMPLETIONENABLE = master_completionenable;
//        LANGUAGEDEFAULT = languagedefault;
//        UPI_MANUALENTRY = upi_manualentry;
//        VISA_LAST4DSMART = visa_last4DSMART;
//        AMEX_ADVCASHENABLE = amex_advcashenable;
//        PSTNSETTLNUMPRI = pstnsettlnumpri;
//        VISA_PREAUTHENABLE = visa_preauthenable;
//        TIPTHERSHOLD = tipthershold;
//        MASTER_SCHEME = master_scheme;
//        UPI_ISSNUM = upi_issnum;
//        PASSWORDUSER = passworduser;
//        AMEX_REFUNDENABLE = amex_refundenable;
//        PREAUTHCLEARCOUNT = preauthclearcount;
//        LANGUAGE = language;
//        RECEIPTFOOTER1 = receiptfooter1;
//        RECEIPTFOOTER2 = receiptfooter2;
//        AMEX_ISSUERACTIVATED = amex_issueractivated;
//        JCB_CHECKPIN = jcb_checkpin;
//        CLIENTTYPE = clienttype;
//        UPI_MOD10 = upi_mod10;
//        PAYPAK_SCHEME = paypak_scheme;
//        UPI_ACQUIRER = upi_acquirer;
//        UPI_CHSIGN = upi_chsign;
//        MASTER_CHECKPIN = master_checkpin;
//        VISA_ADJUSTENABLE = visa_adjustenable;
//        UPI_LAST4DMAG = upi_last4DMAG;
//        AMEX_FORCEDLBPENABLE = amex_forcedlbpenable;
//        PAYPAK_PRINTEXPIRY = paypak_printexpiry;
//        BTMACADDRESS = btmacaddress;
//        VISA_REFUNDENABLE = visa_refundenable;
//        SECONDARYIP = secondaryip;
//        JCB_CHECKEXPIRY = jcb_checkexpiry;
//        ISSSLENABLE = issslenable;
//        ENABLEAUTOPINENTER = enableautopinenter;
//        LANGUAGEACQNUM = languageacqnum;
//        PAYPAK_ADJUSTENABLE = paypak_adjustenable;
//        JCB_ADVCASHENABLE = jcb_advcashenable;
//        TIPNUMADJUST = tipnumadjust;
//        TERMFORCEDLBPMAGENABLE = termforcedlbpmagenable;
//        VISA_ADVCASHENABLE = visa_advcashenable;
//        CTLSENABLED = ctlsenabled;
//        UPI_PINBYPASS = upi_pinbypass;
//        PAYPAK_BANKFUNDED = paypak_bankfunded;
//        JCB_ACQUIRER = jcb_acquirer;
//        AMEX_COMPLETIONENABLE = amex_completionenable;
//        F49ENABLE = f49ENABLE;
//        TOPUPENABLED = topupenabled;
//        UPI_CHSERVCODE = upi_chservcode;
//        UPDATEME = updateme;
//        AMEX_PREAUTHENABLE = amex_preauthenable;
//        EMVLOGSENABLED = emvlogsenabled;
//        TMSIP = tmsip;
//        VISA_CHSIGN = visa_chsign;
//        AMEX_CHECKEXPIRY = amex_checkexpiry;
//        MERCHANTID = merchantid;
//        MASTER_VOIDENABLE = master_voidenable;
//        VENDORCODE = vendorcode;
//        RECEIPTPRINTED = receiptprinted;
//        JCB_CHSIGN = jcb_chsign;
//        VISA_VOIDENABLE = visa_voidenable;
//        MASTER_ISSNAME = master_issname;
//        Reponsemessage = reponsemessage;
//        HIDECARDHOLDERNAME = hidecardholdername;
//        MERCHANTNAME = merchantname;
//        JCB_MOD10 = jcb_mod10;
//        PRINTDECLINEDRCPT = printdeclinedrcpt;
//        UPI_LAST4DSMART = upi_last4DSMART;
//        ADVCASHENABLE = advcashenable;
//        AMEX_LAST4DMAG = amex_last4DMAG;
//        UPI_VOIDENABLE = upi_voidenable;
//        PREAUTHBATCHMAXCOUNT = preauthbatchmaxcount;
//        ADVICEENABLED = adviceenabled;
//        TERMFORCEDLBPMANUALENABLE = termforcedlbpmanualenable;
//        MASTER_ADVCASHENABLE = master_advcashenable;
//        PAYPAK_PREAUTHENABLE = paypak_preauthenable;
//        AMEX_VOIDENABLE = amex_voidenable;
//        PAYPAK_PINBYPASS = paypak_pinbypass;
//        AMEX_BANKFUNDED = amex_bankfunded;
//        MERCHANTADDRESS3 = merchantaddress3;
//        MERCHANTADDRESS2 = merchantaddress2;
//        MERCHANTADDRESS1 = merchantaddress1;
//        JCB_PREAUTHENABLE = jcb_preauthenable;
//        UPI_CHECKPIN = upi_checkpin;
//        USERID = userid;
//        TERMINALMANUALENTRY = terminalmanualentry;
//        ECRENABLED = ecrenabled;
//        VISA_CHSERVCODE = visa_chservcode;
//        MERCHANTADDRESS4 = merchantaddress4;
//        VISA_CHECKEXPIRY = visa_checkexpiry;
//        PINTIMEOUT = pintimeout;
//        PREDIALENABLED = predialenabled;
//        PRINTSERIALNUM = printserialnum;
//        PANLENGTHMIN = panlengthmin;
//        POWERDOWNTIME = powerdowntime;
//        SETTLERANGE = settlerange;
//        MASTER_CHECKEXPIRY = master_checkexpiry;
//        AMEX_ADJUSTENABLE = amex_adjustenable;
//        PAYPAK_MANUALENTRY = paypak_manualentry;
//        JCB_ISSUERACTIVATED = jcb_issueractivated;
//        SRCADDTPDU = srcaddtpdu;
//        INITTERM = initterm;
//        PRIMARYPORT = primaryport;
//        ISTPDUENABLE = istpduenable;
//        VISA_ACQUIRER = visa_acquirer;
//        AMEX_ACQUIRER = amex_acquirer;
//        AMEX_CHECKPIN = amex_checkpin;
//        BATCHMAXCOUNT = batchmaxcount;
//        PRIMARYMEDIUM = primarymedium;
//        VISA_CHECKPIN = visa_checkpin;
//        MASTER_ISSNUM = master_issnum;
//        PASSWORDSYSTEM = passwordsystem;
//        JCB_ISSNAME = jcb_issname;
//        TMSPORT = tmsport;
//        INTERPACKETTIMEOUT = interpackettimeout;
//        PRINTCARDREMRCPT = printcardremrcpt;
//        AMEX_PINBYPASS = amex_pinbypass;
//        TERMINALWIFINETMASK = terminalwifinetmask;
//        JCB_LAST4DSMART = jcb_last4DSMART;
//        AMEX_CHSIGN = amex_chsign;
//        PAYPAK_CHSERVCODE = paypak_chservcode;
//        TERMINALTOTCURRENCIES = terminaltotcurrencies;
//        ISDEBUGENABLE = isdebugenable;
//        PAYPAK_LAST4DMAG = paypak_last4DMAG;
//        PSTNINITNUMBER = pstninitnumber;
//        JCB_COMPLETIONENABLE = jcb_completionenable;
//        AMOUNTENTRYTIMEOUT = amountentrytimeout;
//        WIFIPASSWORD = wifipassword;
//        TERMFORCEDLBPENABLE = termforcedlbpenable;
//        HELPALFAMSG = helpalfamsg;
//        UPI_BANKFUNDED = upi_bankfunded;
//        TERMINALCURRENCY2 = terminalcurrency2;
//        TERMINALCURRENCY1 = terminalcurrency1;
//        TERMINALAPN = terminalapn;
//        JCB_PINBYPASS = jcb_pinbypass;
//        MASTER_PRINTEXPIRY = master_printexpiry;
//    }


    public int getTcid() {
        return tcid;
    }

    public void setTcid(int tcid) {
        this.tcid = tcid;
    }

    public String getAMEX_ISSNUM() {
        return AMEX_ISSNUM;
    }

    public void setAMEX_ISSNUM(String AMEX_ISSNUM) {
        this.AMEX_ISSNUM = AMEX_ISSNUM;
    }

    public String getUPI_ADJUSTENABLE() {
        return UPI_ADJUSTENABLE;
    }

    public void setUPI_ADJUSTENABLE(String UPI_ADJUSTENABLE) {
        this.UPI_ADJUSTENABLE = UPI_ADJUSTENABLE;
    }

    public String getMASTER_ADJUSTENABLE() {
        return MASTER_ADJUSTENABLE;
    }

    public void setMASTER_ADJUSTENABLE(String MASTER_ADJUSTENABLE) {
        this.MASTER_ADJUSTENABLE = MASTER_ADJUSTENABLE;
    }

    public String getAMEX_MANUALENTRY() {
        return AMEX_MANUALENTRY;
    }

    public void setAMEX_MANUALENTRY(String AMEX_MANUALENTRY) {
        this.AMEX_MANUALENTRY = AMEX_MANUALENTRY;
    }

    public String getISRECEIPTPRINTED() {
        return ISRECEIPTPRINTED;
    }

    public void setISRECEIPTPRINTED(String ISRECEIPTPRINTED) {
        this.ISRECEIPTPRINTED = ISRECEIPTPRINTED;
    }

    public String getTUPPRIMARYIP() {
        return TUPPRIMARYIP;
    }

    public void setTUPPRIMARYIP(String TUPPRIMARYIP) {
        this.TUPPRIMARYIP = TUPPRIMARYIP;
    }

    public String getCONNPROFILESCREATED() {
        return CONNPROFILESCREATED;
    }

    public void setCONNPROFILESCREATED(String CONNPROFILESCREATED) {
        this.CONNPROFILESCREATED = CONNPROFILESCREATED;
    }

    public String getISEXTERNALPPCONN() {
        return ISEXTERNALPPCONN;
    }

    public void setISEXTERNALPPCONN(String ISEXTERNALPPCONN) {
        this.ISEXTERNALPPCONN = ISEXTERNALPPCONN;
    }

    public String getVISA_LAST4DMAG() {
        return VISA_LAST4DMAG;
    }

    public void setVISA_LAST4DMAG(String VISA_LAST4DMAG) {
        this.VISA_LAST4DMAG = VISA_LAST4DMAG;
    }

    public String getPAYPAK_FORCEDLBPENABLE() {
        return PAYPAK_FORCEDLBPENABLE;
    }

    public void setPAYPAK_FORCEDLBPENABLE(String PAYPAK_FORCEDLBPENABLE) {
        this.PAYPAK_FORCEDLBPENABLE = PAYPAK_FORCEDLBPENABLE;
    }

    public String getPSTNNUMSEC() {
        return PSTNNUMSEC;
    }

    public void setPSTNNUMSEC(String PSTNNUMSEC) {
        this.PSTNNUMSEC = PSTNNUMSEC;
    }

    public String getVISA_BANKFUNDED() {
        return VISA_BANKFUNDED;
    }

    public void setVISA_BANKFUNDED(String VISA_BANKFUNDED) {
        this.VISA_BANKFUNDED = VISA_BANKFUNDED;
    }

    public String getVISA_PRINTEXPIRY() {
        return VISA_PRINTEXPIRY;
    }

    public void setVISA_PRINTEXPIRY(String VISA_PRINTEXPIRY) {
        this.VISA_PRINTEXPIRY = VISA_PRINTEXPIRY;
    }

    public String getMASTER_CHSERVCODE() {
        return MASTER_CHSERVCODE;
    }

    public void setMASTER_CHSERVCODE(String MASTER_CHSERVCODE) {
        this.MASTER_CHSERVCODE = MASTER_CHSERVCODE;
    }

    public String getPAYPAK_CHECKPIN() {
        return PAYPAK_CHECKPIN;
    }

    public void setPAYPAK_CHECKPIN(String PAYPAK_CHECKPIN) {
        this.PAYPAK_CHECKPIN = PAYPAK_CHECKPIN;
    }

    public String getHELPDESKNUMBER() {
        return HELPDESKNUMBER;
    }

    public void setHELPDESKNUMBER(String HELPDESKNUMBER) {
        this.HELPDESKNUMBER = HELPDESKNUMBER;
    }

    public String getACQNAME() {
        return ACQNAME;
    }

    public void setACQNAME(String ACQNAME) {
        this.ACQNAME = ACQNAME;
    }

    public String getAUDITPRINTDETAILS() {
        return AUDITPRINTDETAILS;
    }

    public void setAUDITPRINTDETAILS(String AUDITPRINTDETAILS) {
        this.AUDITPRINTDETAILS = AUDITPRINTDETAILS;
    }

    public String getTUPSECONDARYIP() {
        return TUPSECONDARYIP;
    }

    public void setTUPSECONDARYIP(String TUPSECONDARYIP) {
        this.TUPSECONDARYIP = TUPSECONDARYIP;
    }

    public String getPASSWORDMANAGEMENT() {
        return PASSWORDMANAGEMENT;
    }

    public void setPASSWORDMANAGEMENT(String PASSWORDMANAGEMENT) {
        this.PASSWORDMANAGEMENT = PASSWORDMANAGEMENT;
    }

    public String getVISA_MOD10() {
        return VISA_MOD10;
    }

    public void setVISA_MOD10(String VISA_MOD10) {
        this.VISA_MOD10 = VISA_MOD10;
    }

    public String getSETGROUP() {
        return SETGROUP;
    }

    public void setSETGROUP(String SETGROUP) {
        this.SETGROUP = SETGROUP;
    }

    public String getMASTER_CHSIGN() {
        return MASTER_CHSIGN;
    }

    public void setMASTER_CHSIGN(String MASTER_CHSIGN) {
        this.MASTER_CHSIGN = MASTER_CHSIGN;
    }

    public String getPAYPAK_ISSNAME() {
        return PAYPAK_ISSNAME;
    }

    public void setPAYPAK_ISSNAME(String PAYPAK_ISSNAME) {
        this.PAYPAK_ISSNAME = PAYPAK_ISSNAME;
    }

    public String getMSGHEADERLENHEX() {
        return MSGHEADERLENHEX;
    }

    public void setMSGHEADERLENHEX(String MSGHEADERLENHEX) {
        this.MSGHEADERLENHEX = MSGHEADERLENHEX;
    }

    public String getUPI_SCHEME() {
        return UPI_SCHEME;
    }

    public void setUPI_SCHEME(String UPI_SCHEME) {
        this.UPI_SCHEME = UPI_SCHEME;
    }

    public String getSECONDARYPORT() {
        return SECONDARYPORT;
    }

    public void setSECONDARYPORT(String SECONDARYPORT) {
        this.SECONDARYPORT = SECONDARYPORT;
    }

    public String getVISA_PINBYPASS() {
        return VISA_PINBYPASS;
    }

    public void setVISA_PINBYPASS(String VISA_PINBYPASS) {
        this.VISA_PINBYPASS = VISA_PINBYPASS;
    }

    public String getDESTADDTPDU() {
        return DESTADDTPDU;
    }

    public void setDESTADDTPDU(String DESTADDTPDU) {
        this.DESTADDTPDU = DESTADDTPDU;
    }

    public String getFLASH() {
        return FLASH;
    }

    public void setFLASH(String FLASH) {
        this.FLASH = FLASH;
    }

    public String getJCB_FORCEDLBPENABLE() {
        return JCB_FORCEDLBPENABLE;
    }

    public void setJCB_FORCEDLBPENABLE(String JCB_FORCEDLBPENABLE) {
        this.JCB_FORCEDLBPENABLE = JCB_FORCEDLBPENABLE;
    }

    public String getAMEX_CHSERVCODE() {
        return AMEX_CHSERVCODE;
    }

    public void setAMEX_CHSERVCODE(String AMEX_CHSERVCODE) {
        this.AMEX_CHSERVCODE = AMEX_CHSERVCODE;
    }

    public String getPAYPAK_CHSIGN() {
        return PAYPAK_CHSIGN;
    }

    public void setPAYPAK_CHSIGN(String PAYPAK_CHSIGN) {
        this.PAYPAK_CHSIGN = PAYPAK_CHSIGN;
    }

    public String getCOUNTRYCODE() {
        return COUNTRYCODE;
    }

    public void setCOUNTRYCODE(String COUNTRYCODE) {
        this.COUNTRYCODE = COUNTRYCODE;
    }

    public String getPSTNTIMEOUT() {
        return PSTNTIMEOUT;
    }

    public void setPSTNTIMEOUT(String PSTNTIMEOUT) {
        this.PSTNTIMEOUT = PSTNTIMEOUT;
    }

    public String getSAVEDEODPRINTDETAILS() {
        return SAVEDEODPRINTDETAILS;
    }

    public void setSAVEDEODPRINTDETAILS(String SAVEDEODPRINTDETAILS) {
        this.SAVEDEODPRINTDETAILS = SAVEDEODPRINTDETAILS;
    }

    public String getPRTPOWERFAILURE() {
        return PRTPOWERFAILURE;
    }

    public void setPRTPOWERFAILURE(String PRTPOWERFAILURE) {
        this.PRTPOWERFAILURE = PRTPOWERFAILURE;
    }

    public String getPASSWORDRETRYON() {
        return PASSWORDRETRYON;
    }

    public void setPASSWORDRETRYON(String PASSWORDRETRYON) {
        this.PASSWORDRETRYON = PASSWORDRETRYON;
    }

    public String getJCB_LAST4DMAG() {
        return JCB_LAST4DMAG;
    }

    public void setJCB_LAST4DMAG(String JCB_LAST4DMAG) {
        this.JCB_LAST4DMAG = JCB_LAST4DMAG;
    }

    public String getCLOCK() {
        return CLOCK;
    }

    public void setCLOCK(String CLOCK) {
        this.CLOCK = CLOCK;
    }

    public String getTERMINALIP() {
        return TERMINALIP;
    }

    public void setTERMINALIP(String TERMINALIP) {
        this.TERMINALIP = TERMINALIP;
    }

    public String getPSTNNUMPRI() {
        return PSTNNUMPRI;
    }

    public void setPSTNNUMPRI(String PSTNNUMPRI) {
        this.PSTNNUMPRI = PSTNNUMPRI;
    }

    public String getBATCHNUMBER() {
        return BATCHNUMBER;
    }

    public void setBATCHNUMBER(String BATCHNUMBER) {
        this.BATCHNUMBER = BATCHNUMBER;
    }

    public String getMASTER_PINBYPASS() {
        return MASTER_PINBYPASS;
    }

    public void setMASTER_PINBYPASS(String MASTER_PINBYPASS) {
        this.MASTER_PINBYPASS = MASTER_PINBYPASS;
    }

    public String getPAYPAK_LAST4DSMART() {
        return PAYPAK_LAST4DSMART;
    }

    public void setPAYPAK_LAST4DSMART(String PAYPAK_LAST4DSMART) {
        this.PAYPAK_LAST4DSMART = PAYPAK_LAST4DSMART;
    }

    public String getTERMINALNETMASK() {
        return TERMINALNETMASK;
    }

    public void setTERMINALNETMASK(String TERMINALNETMASK) {
        this.TERMINALNETMASK = TERMINALNETMASK;
    }

    public String getDECIMALSEPARATOR() {
        return DECIMALSEPARATOR;
    }

    public void setDECIMALSEPARATOR(String DECIMALSEPARATOR) {
        this.DECIMALSEPARATOR = DECIMALSEPARATOR;
    }

    public String getPSTNINTERCHARTO() {
        return PSTNINTERCHARTO;
    }

    public void setPSTNINTERCHARTO(String PSTNINTERCHARTO) {
        this.PSTNINTERCHARTO = PSTNINTERCHARTO;
    }

    public String getSETTLECOUNTER() {
        return SETTLECOUNTER;
    }

    public void setSETTLECOUNTER(String SETTLECOUNTER) {
        this.SETTLECOUNTER = SETTLECOUNTER;
    }

    public String getACQNUM() {
        return ACQNUM;
    }

    public void setACQNUM(String ACQNUM) {
        this.ACQNUM = ACQNUM;
    }

    public String getMASTER_PREAUTHENABLE() {
        return MASTER_PREAUTHENABLE;
    }

    public void setMASTER_PREAUTHENABLE(String MASTER_PREAUTHENABLE) {
        this.MASTER_PREAUTHENABLE = MASTER_PREAUTHENABLE;
    }

    public String getMARKETSEGMENT() {
        return MARKETSEGMENT;
    }

    public void setMARKETSEGMENT(String MARKETSEGMENT) {
        this.MARKETSEGMENT = MARKETSEGMENT;
    }

    public String getAUTOSETTLE() {
        return AUTOSETTLE;
    }

    public void setAUTOSETTLE(String AUTOSETTLE) {
        this.AUTOSETTLE = AUTOSETTLE;
    }

    public String getTERMINALDNS2() {
        return TERMINALDNS2;
    }

    public void setTERMINALDNS2(String TERMINALDNS2) {
        this.TERMINALDNS2 = TERMINALDNS2;
    }

    public String getTERMINALDNS1() {
        return TERMINALDNS1;
    }

    public void setTERMINALDNS1(String TERMINALDNS1) {
        this.TERMINALDNS1 = TERMINALDNS1;
    }

    public String getJCB_CHSERVCODE() {
        return JCB_CHSERVCODE;
    }

    public void setJCB_CHSERVCODE(String JCB_CHSERVCODE) {
        this.JCB_CHSERVCODE = JCB_CHSERVCODE;
    }

    public String getBULKDOWNLOADPENDING() {
        return BULKDOWNLOADPENDING;
    }

    public void setBULKDOWNLOADPENDING(String BULKDOWNLOADPENDING) {
        this.BULKDOWNLOADPENDING = BULKDOWNLOADPENDING;
    }

    public String getTUPPRIMARYPORT() {
        return TUPPRIMARYPORT;
    }

    public void setTUPPRIMARYPORT(String TUPPRIMARYPORT) {
        this.TUPPRIMARYPORT = TUPPRIMARYPORT;
    }

    public String getTERMINALCOUNTRYCODE() {
        return TERMINALCOUNTRYCODE;
    }

    public void setTERMINALCOUNTRYCODE(String TERMINALCOUNTRYCODE) {
        this.TERMINALCOUNTRYCODE = TERMINALCOUNTRYCODE;
    }

    public String getVISA_SCHEME() {
        return VISA_SCHEME;
    }

    public void setVISA_SCHEME(String VISA_SCHEME) {
        this.VISA_SCHEME = VISA_SCHEME;
    }

    public String getMASTER_LAST4DMAG() {
        return MASTER_LAST4DMAG;
    }

    public void setMASTER_LAST4DMAG(String MASTER_LAST4DMAG) {
        this.MASTER_LAST4DMAG = MASTER_LAST4DMAG;
    }

    public String getJCB_MANUALENTRY() {
        return JCB_MANUALENTRY;
    }

    public void setJCB_MANUALENTRY(String JCB_MANUALENTRY) {
        this.JCB_MANUALENTRY = JCB_MANUALENTRY;
    }

    public String getSTAN() {
        return STAN;
    }

    public void setSTAN(String STAN) {
        this.STAN = STAN;
    }

    public String getPAYPAK_ACQUIRER() {
        return PAYPAK_ACQUIRER;
    }

    public void setPAYPAK_ACQUIRER(String PAYPAK_ACQUIRER) {
        this.PAYPAK_ACQUIRER = PAYPAK_ACQUIRER;
    }

    public String getPAYPAK_MOD10() {
        return PAYPAK_MOD10;
    }

    public void setPAYPAK_MOD10(String PAYPAK_MOD10) {
        this.PAYPAK_MOD10 = PAYPAK_MOD10;
    }

    public String getISSIMSWDEBUGENABLE() {
        return ISSIMSWDEBUGENABLE;
    }

    public void setISSIMSWDEBUGENABLE(String ISSIMSWDEBUGENABLE) {
        this.ISSIMSWDEBUGENABLE = ISSIMSWDEBUGENABLE;
    }

    public String getJCB_SCHEME() {
        return JCB_SCHEME;
    }

    public void setJCB_SCHEME(String JCB_SCHEME) {
        this.JCB_SCHEME = JCB_SCHEME;
    }

    public String getPRINTBARCODE() {
        return PRINTBARCODE;
    }

    public void setPRINTBARCODE(String PRINTBARCODE) {
        this.PRINTBARCODE = PRINTBARCODE;
    }

    public String getTERMINALID() {
        return TERMINALID;
    }

    public void setTERMINALID(String TERMINALID) {
        this.TERMINALID = TERMINALID;
    }

    public String getAMEXID() {
        return AMEXID;
    }

    public void setAMEXID(String AMEXID) {
        this.AMEXID = AMEXID;
    }

    public String getJCB_PRINTEXPIRY() {
        return JCB_PRINTEXPIRY;
    }

    public void setJCB_PRINTEXPIRY(String JCB_PRINTEXPIRY) {
        this.JCB_PRINTEXPIRY = JCB_PRINTEXPIRY;
    }

    public String getCLIENTNAME() {
        return CLIENTNAME;
    }

    public void setCLIENTNAME(String CLIENTNAME) {
        this.CLIENTNAME = CLIENTNAME;
    }

    public String getMASTER_LAST4DSMART() {
        return MASTER_LAST4DSMART;
    }

    public void setMASTER_LAST4DSMART(String MASTER_LAST4DSMART) {
        this.MASTER_LAST4DSMART = MASTER_LAST4DSMART;
    }

    public String getUPI_ADVCASHENABLE() {
        return UPI_ADVCASHENABLE;
    }

    public void setUPI_ADVCASHENABLE(String UPI_ADVCASHENABLE) {
        this.UPI_ADVCASHENABLE = UPI_ADVCASHENABLE;
    }

    public String getTUPSSLENABLE() {
        return TUPSSLENABLE;
    }

    public void setTUPSSLENABLE(String TUPSSLENABLE) {
        this.TUPSSLENABLE = TUPSSLENABLE;
    }

    public String getVISA_MANUALENTRY() {
        return VISA_MANUALENTRY;
    }

    public void setVISA_MANUALENTRY(String VISA_MANUALENTRY) {
        this.VISA_MANUALENTRY = VISA_MANUALENTRY;
    }

    public String getUPI_COMPLETIONENABLE() {
        return UPI_COMPLETIONENABLE;
    }

    public void setUPI_COMPLETIONENABLE(String UPI_COMPLETIONENABLE) {
        this.UPI_COMPLETIONENABLE = UPI_COMPLETIONENABLE;
    }

    public String getMASTER_ACQUIRER() {
        return MASTER_ACQUIRER;
    }

    public void setMASTER_ACQUIRER(String MASTER_ACQUIRER) {
        this.MASTER_ACQUIRER = MASTER_ACQUIRER;
    }

    public String getJCB_BANKFUNDED() {
        return JCB_BANKFUNDED;
    }

    public void setJCB_BANKFUNDED(String JCB_BANKFUNDED) {
        this.JCB_BANKFUNDED = JCB_BANKFUNDED;
    }

    public String getTERMINALWIFIDHCP() {
        return TERMINALWIFIDHCP;
    }

    public void setTERMINALWIFIDHCP(String TERMINALWIFIDHCP) {
        this.TERMINALWIFIDHCP = TERMINALWIFIDHCP;
    }

    public String getAUTOSETTLETIME() {
        return AUTOSETTLETIME;
    }

    public void setAUTOSETTLETIME(String AUTOSETTLETIME) {
        this.AUTOSETTLETIME = AUTOSETTLETIME;
    }

    public String getAMEX_LAST4DSMART() {
        return AMEX_LAST4DSMART;
    }

    public void setAMEX_LAST4DSMART(String AMEX_LAST4DSMART) {
        this.AMEX_LAST4DSMART = AMEX_LAST4DSMART;
    }

    public String getEMVCOUNTER() {
        return EMVCOUNTER;
    }

    public void setEMVCOUNTER(String EMVCOUNTER) {
        this.EMVCOUNTER = EMVCOUNTER;
    }

    public String getRECEIVETIMEOUT() {
        return RECEIVETIMEOUT;
    }

    public void setRECEIVETIMEOUT(String RECEIVETIMEOUT) {
        this.RECEIVETIMEOUT = RECEIVETIMEOUT;
    }

    public String getCARDTIMEOUT() {
        return CARDTIMEOUT;
    }

    public void setCARDTIMEOUT(String CARDTIMEOUT) {
        this.CARDTIMEOUT = CARDTIMEOUT;
    }

    public String getSERVICEFIRSTTIME() {
        return SERVICEFIRSTTIME;
    }

    public void setSERVICEFIRSTTIME(String SERVICEFIRSTTIME) {
        this.SERVICEFIRSTTIME = SERVICEFIRSTTIME;
    }

    public String getISACTIVATED() {
        return ISACTIVATED;
    }

    public void setISACTIVATED(String ISACTIVATED) {
        this.ISACTIVATED = ISACTIVATED;
    }

    public String getAMEX_PRINTEXPIRY() {
        return AMEX_PRINTEXPIRY;
    }

    public void setAMEX_PRINTEXPIRY(String AMEX_PRINTEXPIRY) {
        this.AMEX_PRINTEXPIRY = AMEX_PRINTEXPIRY;
    }

    public String getJCB_VOIDENABLE() {
        return JCB_VOIDENABLE;
    }

    public void setJCB_VOIDENABLE(String JCB_VOIDENABLE) {
        this.JCB_VOIDENABLE = JCB_VOIDENABLE;
    }

    public String getPASSWRD() {
        return PASSWRD;
    }

    public void setPASSWRD(String PASSWRD) {
        this.PASSWRD = PASSWRD;
    }

    public String getTIPENABLED() {
        return TIPENABLED;
    }

    public void setTIPENABLED(String TIPENABLED) {
        this.TIPENABLED = TIPENABLED;
    }

    public String getMASTER_ISSUERACTIVATED() {
        return MASTER_ISSUERACTIVATED;
    }

    public void setMASTER_ISSUERACTIVATED(String MASTER_ISSUERACTIVATED) {
        this.MASTER_ISSUERACTIVATED = MASTER_ISSUERACTIVATED;
    }

    public String getUPI_PRINTEXPIRY() {
        return UPI_PRINTEXPIRY;
    }

    public void setUPI_PRINTEXPIRY(String UPI_PRINTEXPIRY) {
        this.UPI_PRINTEXPIRY = UPI_PRINTEXPIRY;
    }

    public String getMASTER_BANKFUNDED() {
        return MASTER_BANKFUNDED;
    }

    public void setMASTER_BANKFUNDED(String MASTER_BANKFUNDED) {
        this.MASTER_BANKFUNDED = MASTER_BANKFUNDED;
    }

    public String getOFFLINEPIN() {
        return OFFLINEPIN;
    }

    public void setOFFLINEPIN(String OFFLINEPIN) {
        this.OFFLINEPIN = OFFLINEPIN;
    }

    public String getPOSSERIALNO() {
        return POSSERIALNO;
    }

    public void setPOSSERIALNO(String POSSERIALNO) {
        this.POSSERIALNO = POSSERIALNO;
    }

    public String getPAYPAK_COMPLETIONENABLE() {
        return PAYPAK_COMPLETIONENABLE;
    }

    public void setPAYPAK_COMPLETIONENABLE(String PAYPAK_COMPLETIONENABLE) {
        this.PAYPAK_COMPLETIONENABLE = PAYPAK_COMPLETIONENABLE;
    }

    public String getTOPUPPIN() {
        return TOPUPPIN;
    }

    public void setTOPUPPIN(String TOPUPPIN) {
        this.TOPUPPIN = TOPUPPIN;
    }

    public String getPAYPAK_REFUNDENABLE() {
        return PAYPAK_REFUNDENABLE;
    }

    public void setPAYPAK_REFUNDENABLE(String PAYPAK_REFUNDENABLE) {
        this.PAYPAK_REFUNDENABLE = PAYPAK_REFUNDENABLE;
    }

    public String getTERMINALGATEWAY() {
        return TERMINALGATEWAY;
    }

    public void setTERMINALGATEWAY(String TERMINALGATEWAY) {
        this.TERMINALGATEWAY = TERMINALGATEWAY;
    }

    public String getUPI_REFUNDENABLE() {
        return UPI_REFUNDENABLE;
    }

    public void setUPI_REFUNDENABLE(String UPI_REFUNDENABLE) {
        this.UPI_REFUNDENABLE = UPI_REFUNDENABLE;
    }

    public String getAMEX_MOD10() {
        return AMEX_MOD10;
    }

    public void setAMEX_MOD10(String AMEX_MOD10) {
        this.AMEX_MOD10 = AMEX_MOD10;
    }

    public String getVISA_COMPLETIONENABLE() {
        return VISA_COMPLETIONENABLE;
    }

    public void setVISA_COMPLETIONENABLE(String VISA_COMPLETIONENABLE) {
        this.VISA_COMPLETIONENABLE = VISA_COMPLETIONENABLE;
    }

    public String getPAYPAK_ADVCASHENABLE() {
        return PAYPAK_ADVCASHENABLE;
    }

    public void setPAYPAK_ADVCASHENABLE(String PAYPAK_ADVCASHENABLE) {
        this.PAYPAK_ADVCASHENABLE = PAYPAK_ADVCASHENABLE;
    }

    public String getHELPTOUCHMSG() {
        return HELPTOUCHMSG;
    }

    public void setHELPTOUCHMSG(String HELPTOUCHMSG) {
        this.HELPTOUCHMSG = HELPTOUCHMSG;
    }

    public String getTERMINALWIFIGATEWAY() {
        return TERMINALWIFIGATEWAY;
    }

    public void setTERMINALWIFIGATEWAY(String TERMINALWIFIGATEWAY) {
        this.TERMINALWIFIGATEWAY = TERMINALWIFIGATEWAY;
    }

    public String getUPI_ISSNAME() {
        return UPI_ISSNAME;
    }

    public void setUPI_ISSNAME(String UPI_ISSNAME) {
        this.UPI_ISSNAME = UPI_ISSNAME;
    }

    public String getEODPRINTDETAILS() {
        return EODPRINTDETAILS;
    }

    public void setEODPRINTDETAILS(String EODPRINTDETAILS) {
        this.EODPRINTDETAILS = EODPRINTDETAILS;
    }

    public String getPAYPAK_VOIDENABLE() {
        return PAYPAK_VOIDENABLE;
    }

    public void setPAYPAK_VOIDENABLE(String PAYPAK_VOIDENABLE) {
        this.PAYPAK_VOIDENABLE = PAYPAK_VOIDENABLE;
    }

    public String getTUPSECONDARYPORT() {
        return TUPSECONDARYPORT;
    }

    public void setTUPSECONDARYPORT(String TUPSECONDARYPORT) {
        this.TUPSECONDARYPORT = TUPSECONDARYPORT;
    }

    public String getRSPTIMEOUT() {
        return RSPTIMEOUT;
    }

    public void setRSPTIMEOUT(String RSPTIMEOUT) {
        this.RSPTIMEOUT = RSPTIMEOUT;
    }

    public String getPAYPAK_ISSUERACTIVATED() {
        return PAYPAK_ISSUERACTIVATED;
    }

    public void setPAYPAK_ISSUERACTIVATED(String PAYPAK_ISSUERACTIVATED) {
        this.PAYPAK_ISSUERACTIVATED = PAYPAK_ISSUERACTIVATED;
    }

    public String getPRIMARYIP() {
        return PRIMARYIP;
    }

    public void setPRIMARYIP(String PRIMARYIP) {
        this.PRIMARYIP = PRIMARYIP;
    }

    public String getVISA_ISSNUM() {
        return VISA_ISSNUM;
    }

    public void setVISA_ISSNUM(String VISA_ISSNUM) {
        this.VISA_ISSNUM = VISA_ISSNUM;
    }

    public String getCASHBACKENABLED() {
        return CASHBACKENABLED;
    }

    public void setCASHBACKENABLED(String CASHBACKENABLED) {
        this.CASHBACKENABLED = CASHBACKENABLED;
    }

    public String getPANLENGTHMAX() {
        return PANLENGTHMAX;
    }

    public void setPANLENGTHMAX(String PANLENGTHMAX) {
        this.PANLENGTHMAX = PANLENGTHMAX;
    }

    public String getTERMINALWIFIIP() {
        return TERMINALWIFIIP;
    }

    public void setTERMINALWIFIIP(String TERMINALWIFIIP) {
        this.TERMINALWIFIIP = TERMINALWIFIIP;
    }

    public String getPSTNSETTLNUMSEC() {
        return PSTNSETTLNUMSEC;
    }

    public void setPSTNSETTLNUMSEC(String PSTNSETTLNUMSEC) {
        this.PSTNSETTLNUMSEC = PSTNSETTLNUMSEC;
    }

    public String getJCB_ADJUSTENABLE() {
        return JCB_ADJUSTENABLE;
    }

    public void setJCB_ADJUSTENABLE(String JCB_ADJUSTENABLE) {
        this.JCB_ADJUSTENABLE = JCB_ADJUSTENABLE;
    }

    public String getAMEX_ISSNAME() {
        return AMEX_ISSNAME;
    }

    public void setAMEX_ISSNAME(String AMEX_ISSNAME) {
        this.AMEX_ISSNAME = AMEX_ISSNAME;
    }

    public String getALLOWFALLBACK() {
        return ALLOWFALLBACK;
    }

    public void setALLOWFALLBACK(String ALLOWFALLBACK) {
        this.ALLOWFALLBACK = ALLOWFALLBACK;
    }

    public String getVISA_ISSNAME() {
        return VISA_ISSNAME;
    }

    public void setVISA_ISSNAME(String VISA_ISSNAME) {
        this.VISA_ISSNAME = VISA_ISSNAME;
    }

    public String getMASTER_FORCEDLBPENABLE() {
        return MASTER_FORCEDLBPENABLE;
    }

    public void setMASTER_FORCEDLBPENABLE(String MASTER_FORCEDLBPENABLE) {
        this.MASTER_FORCEDLBPENABLE = MASTER_FORCEDLBPENABLE;
    }

    public String getMASTER_MOD10() {
        return MASTER_MOD10;
    }

    public void setMASTER_MOD10(String MASTER_MOD10) {
        this.MASTER_MOD10 = MASTER_MOD10;
    }

    public String getJCB_ISSNUM() {
        return JCB_ISSNUM;
    }

    public void setJCB_ISSNUM(String JCB_ISSNUM) {
        this.JCB_ISSNUM = JCB_ISSNUM;
    }

    public String getISLOCALHOST() {
        return ISLOCALHOST;
    }

    public void setISLOCALHOST(String ISLOCALHOST) {
        this.ISLOCALHOST = ISLOCALHOST;
    }

    public String getVCSETHTIMEOUT() {
        return VCSETHTIMEOUT;
    }

    public void setVCSETHTIMEOUT(String VCSETHTIMEOUT) {
        this.VCSETHTIMEOUT = VCSETHTIMEOUT;
    }

    public String getWIFISSID() {
        return WIFISSID;
    }

    public void setWIFISSID(String WIFISSID) {
        this.WIFISSID = WIFISSID;
    }

    public String getTERMINALWIFIDNS2() {
        return TERMINALWIFIDNS2;
    }

    public void setTERMINALWIFIDNS2(String TERMINALWIFIDNS2) {
        this.TERMINALWIFIDNS2 = TERMINALWIFIDNS2;
    }

    public String getPSTNPREFIX() {
        return PSTNPREFIX;
    }

    public void setPSTNPREFIX(String PSTNPREFIX) {
        this.PSTNPREFIX = PSTNPREFIX;
    }

    public String getSECONDARYMEDIUM() {
        return SECONDARYMEDIUM;
    }

    public void setSECONDARYMEDIUM(String SECONDARYMEDIUM) {
        this.SECONDARYMEDIUM = SECONDARYMEDIUM;
    }

    public String getTERMINALWIFIDNS1() {
        return TERMINALWIFIDNS1;
    }

    public void setTERMINALWIFIDNS1(String TERMINALWIFIDNS1) {
        this.TERMINALWIFIDNS1 = TERMINALWIFIDNS1;
    }

    public String getPAYPAK_ISSNUM() {
        return PAYPAK_ISSNUM;
    }

    public void setPAYPAK_ISSNUM(String PAYPAK_ISSNUM) {
        this.PAYPAK_ISSNUM = PAYPAK_ISSNUM;
    }

    public String getAMEX_SCHEME() {
        return AMEX_SCHEME;
    }

    public void setAMEX_SCHEME(String AMEX_SCHEME) {
        this.AMEX_SCHEME = AMEX_SCHEME;
    }

    public String getTHOUSANDSEPARATOR() {
        return THOUSANDSEPARATOR;
    }

    public void setTHOUSANDSEPARATOR(String THOUSANDSEPARATOR) {
        this.THOUSANDSEPARATOR = THOUSANDSEPARATOR;
    }

    public String getAGENTCODE() {
        return AGENTCODE;
    }

    public void setAGENTCODE(String AGENTCODE) {
        this.AGENTCODE = AGENTCODE;
    }

    public String getPRINTBANKRECON() {
        return PRINTBANKRECON;
    }

    public void setPRINTBANKRECON(String PRINTBANKRECON) {
        this.PRINTBANKRECON = PRINTBANKRECON;
    }

    public String getVISA_FORCEDLBPENABLE() {
        return VISA_FORCEDLBPENABLE;
    }

    public void setVISA_FORCEDLBPENABLE(String VISA_FORCEDLBPENABLE) {
        this.VISA_FORCEDLBPENABLE = VISA_FORCEDLBPENABLE;
    }

    public String getUPI_CHECKEXPIRY() {
        return UPI_CHECKEXPIRY;
    }

    public void setUPI_CHECKEXPIRY(String UPI_CHECKEXPIRY) {
        this.UPI_CHECKEXPIRY = UPI_CHECKEXPIRY;
    }

    public String getPRINTREVERSALRCPT() {
        return PRINTREVERSALRCPT;
    }

    public void setPRINTREVERSALRCPT(String PRINTREVERSALRCPT) {
        this.PRINTREVERSALRCPT = PRINTREVERSALRCPT;
    }

    public String getINVOICENUMBER() {
        return INVOICENUMBER;
    }

    public void setINVOICENUMBER(String INVOICENUMBER) {
        this.INVOICENUMBER = INVOICENUMBER;
    }

    public String getBOOTFIRSTTIME() {
        return BOOTFIRSTTIME;
    }

    public void setBOOTFIRSTTIME(String BOOTFIRSTTIME) {
        this.BOOTFIRSTTIME = BOOTFIRSTTIME;
    }

    public String getTERMINALDHCP() {
        return TERMINALDHCP;
    }

    public void setTERMINALDHCP(String TERMINALDHCP) {
        this.TERMINALDHCP = TERMINALDHCP;
    }

    public String getPREAUTHMAXCOMPLETIONDAYS() {
        return PREAUTHMAXCOMPLETIONDAYS;
    }

    public void setPREAUTHMAXCOMPLETIONDAYS(String PREAUTHMAXCOMPLETIONDAYS) {
        this.PREAUTHMAXCOMPLETIONDAYS = PREAUTHMAXCOMPLETIONDAYS;
    }

    public String getCOMPLETIONTHERSHOLD() {
        return COMPLETIONTHERSHOLD;
    }

    public void setCOMPLETIONTHERSHOLD(String COMPLETIONTHERSHOLD) {
        this.COMPLETIONTHERSHOLD = COMPLETIONTHERSHOLD;
    }

    public String getMASTER_REFUNDENABLE() {
        return MASTER_REFUNDENABLE;
    }

    public void setMASTER_REFUNDENABLE(String MASTER_REFUNDENABLE) {
        this.MASTER_REFUNDENABLE = MASTER_REFUNDENABLE;
    }

    public String getUPI_FORCEDLBPENABLE() {
        return UPI_FORCEDLBPENABLE;
    }

    public void setUPI_FORCEDLBPENABLE(String UPI_FORCEDLBPENABLE) {
        this.UPI_FORCEDLBPENABLE = UPI_FORCEDLBPENABLE;
    }

    public String getMASTER_MANUALENTRY() {
        return MASTER_MANUALENTRY;
    }

    public void setMASTER_MANUALENTRY(String MASTER_MANUALENTRY) {
        this.MASTER_MANUALENTRY = MASTER_MANUALENTRY;
    }

    public String getPRINTEMVLOGS() {
        return PRINTEMVLOGS;
    }

    public void setPRINTEMVLOGS(String PRINTEMVLOGS) {
        this.PRINTEMVLOGS = PRINTEMVLOGS;
    }

    public String getJCB_REFUNDENABLE() {
        return JCB_REFUNDENABLE;
    }

    public void setJCB_REFUNDENABLE(String JCB_REFUNDENABLE) {
        this.JCB_REFUNDENABLE = JCB_REFUNDENABLE;
    }

    public String getUPI_ISSUERACTIVATED() {
        return UPI_ISSUERACTIVATED;
    }

    public void setUPI_ISSUERACTIVATED(String UPI_ISSUERACTIVATED) {
        this.UPI_ISSUERACTIVATED = UPI_ISSUERACTIVATED;
    }

    public String getPASSWORDSUPERUSER() {
        return PASSWORDSUPERUSER;
    }

    public void setPASSWORDSUPERUSER(String PASSWORDSUPERUSER) {
        this.PASSWORDSUPERUSER = PASSWORDSUPERUSER;
    }

    public String getVISA_ISSUERACTIVATED() {
        return VISA_ISSUERACTIVATED;
    }

    public void setVISA_ISSUERACTIVATED(String VISA_ISSUERACTIVATED) {
        this.VISA_ISSUERACTIVATED = VISA_ISSUERACTIVATED;
    }

    public String getUPI_PREAUTHENABLE() {
        return UPI_PREAUTHENABLE;
    }

    public void setUPI_PREAUTHENABLE(String UPI_PREAUTHENABLE) {
        this.UPI_PREAUTHENABLE = UPI_PREAUTHENABLE;
    }

    public String getPAYPAK_CHECKEXPIRY() {
        return PAYPAK_CHECKEXPIRY;
    }

    public void setPAYPAK_CHECKEXPIRY(String PAYPAK_CHECKEXPIRY) {
        this.PAYPAK_CHECKEXPIRY = PAYPAK_CHECKEXPIRY;
    }

    public String getISSTATUSDEBUGENABLE() {
        return ISSTATUSDEBUGENABLE;
    }

    public void setISSTATUSDEBUGENABLE(String ISSTATUSDEBUGENABLE) {
        this.ISSTATUSDEBUGENABLE = ISSTATUSDEBUGENABLE;
    }

    public String getTPKGISKE() {
        return TPKGISKE;
    }

    public void setTPKGISKE(String TPKGISKE) {
        this.TPKGISKE = TPKGISKE;
    }

    public String getMASTER_COMPLETIONENABLE() {
        return MASTER_COMPLETIONENABLE;
    }

    public void setMASTER_COMPLETIONENABLE(String MASTER_COMPLETIONENABLE) {
        this.MASTER_COMPLETIONENABLE = MASTER_COMPLETIONENABLE;
    }

    public String getLANGUAGEDEFAULT() {
        return LANGUAGEDEFAULT;
    }

    public void setLANGUAGEDEFAULT(String LANGUAGEDEFAULT) {
        this.LANGUAGEDEFAULT = LANGUAGEDEFAULT;
    }

    public String getUPI_MANUALENTRY() {
        return UPI_MANUALENTRY;
    }

    public void setUPI_MANUALENTRY(String UPI_MANUALENTRY) {
        this.UPI_MANUALENTRY = UPI_MANUALENTRY;
    }

    public String getVISA_LAST4DSMART() {
        return VISA_LAST4DSMART;
    }

    public void setVISA_LAST4DSMART(String VISA_LAST4DSMART) {
        this.VISA_LAST4DSMART = VISA_LAST4DSMART;
    }

    public String getAMEX_ADVCASHENABLE() {
        return AMEX_ADVCASHENABLE;
    }

    public void setAMEX_ADVCASHENABLE(String AMEX_ADVCASHENABLE) {
        this.AMEX_ADVCASHENABLE = AMEX_ADVCASHENABLE;
    }

    public String getPSTNSETTLNUMPRI() {
        return PSTNSETTLNUMPRI;
    }

    public void setPSTNSETTLNUMPRI(String PSTNSETTLNUMPRI) {
        this.PSTNSETTLNUMPRI = PSTNSETTLNUMPRI;
    }

    public String getVISA_PREAUTHENABLE() {
        return VISA_PREAUTHENABLE;
    }

    public void setVISA_PREAUTHENABLE(String VISA_PREAUTHENABLE) {
        this.VISA_PREAUTHENABLE = VISA_PREAUTHENABLE;
    }

    public String getTIPTHERSHOLD() {
        return TIPTHERSHOLD;
    }

    public void setTIPTHERSHOLD(String TIPTHERSHOLD) {
        this.TIPTHERSHOLD = TIPTHERSHOLD;
    }

    public String getMASTER_SCHEME() {
        return MASTER_SCHEME;
    }

    public void setMASTER_SCHEME(String MASTER_SCHEME) {
        this.MASTER_SCHEME = MASTER_SCHEME;
    }

    public String getUPI_ISSNUM() {
        return UPI_ISSNUM;
    }

    public void setUPI_ISSNUM(String UPI_ISSNUM) {
        this.UPI_ISSNUM = UPI_ISSNUM;
    }

    public String getPASSWORDUSER() {
        return PASSWORDUSER;
    }

    public void setPASSWORDUSER(String PASSWORDUSER) {
        this.PASSWORDUSER = PASSWORDUSER;
    }

    public String getAMEX_REFUNDENABLE() {
        return AMEX_REFUNDENABLE;
    }

    public void setAMEX_REFUNDENABLE(String AMEX_REFUNDENABLE) {
        this.AMEX_REFUNDENABLE = AMEX_REFUNDENABLE;
    }

    public String getPREAUTHCLEARCOUNT() {
        return PREAUTHCLEARCOUNT;
    }

    public void setPREAUTHCLEARCOUNT(String PREAUTHCLEARCOUNT) {
        this.PREAUTHCLEARCOUNT = PREAUTHCLEARCOUNT;
    }

    public String getLANGUAGE() {
        return LANGUAGE;
    }

    public void setLANGUAGE(String LANGUAGE) {
        this.LANGUAGE = LANGUAGE;
    }

    public String getRECEIPTFOOTER1() {
        return RECEIPTFOOTER1;
    }

    public void setRECEIPTFOOTER1(String RECEIPTFOOTER1) {
        this.RECEIPTFOOTER1 = RECEIPTFOOTER1;
    }

    public String getRECEIPTFOOTER2() {
        return RECEIPTFOOTER2;
    }

    public void setRECEIPTFOOTER2(String RECEIPTFOOTER2) {
        this.RECEIPTFOOTER2 = RECEIPTFOOTER2;
    }

    public String getAMEX_ISSUERACTIVATED() {
        return AMEX_ISSUERACTIVATED;
    }

    public void setAMEX_ISSUERACTIVATED(String AMEX_ISSUERACTIVATED) {
        this.AMEX_ISSUERACTIVATED = AMEX_ISSUERACTIVATED;
    }

    public String getJCB_CHECKPIN() {
        return JCB_CHECKPIN;
    }

    public void setJCB_CHECKPIN(String JCB_CHECKPIN) {
        this.JCB_CHECKPIN = JCB_CHECKPIN;
    }

    public String getCLIENTTYPE() {
        return CLIENTTYPE;
    }

    public void setCLIENTTYPE(String CLIENTTYPE) {
        this.CLIENTTYPE = CLIENTTYPE;
    }

    public String getUPI_MOD10() {
        return UPI_MOD10;
    }

    public void setUPI_MOD10(String UPI_MOD10) {
        this.UPI_MOD10 = UPI_MOD10;
    }

    public String getPAYPAK_SCHEME() {
        return PAYPAK_SCHEME;
    }

    public void setPAYPAK_SCHEME(String PAYPAK_SCHEME) {
        this.PAYPAK_SCHEME = PAYPAK_SCHEME;
    }

    public String getUPI_ACQUIRER() {
        return UPI_ACQUIRER;
    }

    public void setUPI_ACQUIRER(String UPI_ACQUIRER) {
        this.UPI_ACQUIRER = UPI_ACQUIRER;
    }

    public String getUPI_CHSIGN() {
        return UPI_CHSIGN;
    }

    public void setUPI_CHSIGN(String UPI_CHSIGN) {
        this.UPI_CHSIGN = UPI_CHSIGN;
    }

    public String getMASTER_CHECKPIN() {
        return MASTER_CHECKPIN;
    }

    public void setMASTER_CHECKPIN(String MASTER_CHECKPIN) {
        this.MASTER_CHECKPIN = MASTER_CHECKPIN;
    }

    public String getVISA_ADJUSTENABLE() {
        return VISA_ADJUSTENABLE;
    }

    public void setVISA_ADJUSTENABLE(String VISA_ADJUSTENABLE) {
        this.VISA_ADJUSTENABLE = VISA_ADJUSTENABLE;
    }

    public String getUPI_LAST4DMAG() {
        return UPI_LAST4DMAG;
    }

    public void setUPI_LAST4DMAG(String UPI_LAST4DMAG) {
        this.UPI_LAST4DMAG = UPI_LAST4DMAG;
    }

    public String getAMEX_FORCEDLBPENABLE() {
        return AMEX_FORCEDLBPENABLE;
    }

    public void setAMEX_FORCEDLBPENABLE(String AMEX_FORCEDLBPENABLE) {
        this.AMEX_FORCEDLBPENABLE = AMEX_FORCEDLBPENABLE;
    }

    public String getPAYPAK_PRINTEXPIRY() {
        return PAYPAK_PRINTEXPIRY;
    }

    public void setPAYPAK_PRINTEXPIRY(String PAYPAK_PRINTEXPIRY) {
        this.PAYPAK_PRINTEXPIRY = PAYPAK_PRINTEXPIRY;
    }

    public String getBTMACADDRESS() {
        return BTMACADDRESS;
    }

    public void setBTMACADDRESS(String BTMACADDRESS) {
        this.BTMACADDRESS = BTMACADDRESS;
    }

    public String getVISA_REFUNDENABLE() {
        return VISA_REFUNDENABLE;
    }

    public void setVISA_REFUNDENABLE(String VISA_REFUNDENABLE) {
        this.VISA_REFUNDENABLE = VISA_REFUNDENABLE;
    }

    public String getSECONDARYIP() {
        return SECONDARYIP;
    }

    public void setSECONDARYIP(String SECONDARYIP) {
        this.SECONDARYIP = SECONDARYIP;
    }

    public String getJCB_CHECKEXPIRY() {
        return JCB_CHECKEXPIRY;
    }

    public void setJCB_CHECKEXPIRY(String JCB_CHECKEXPIRY) {
        this.JCB_CHECKEXPIRY = JCB_CHECKEXPIRY;
    }

    public String getISSSLENABLE() {
        return ISSSLENABLE;
    }

    public void setISSSLENABLE(String ISSSLENABLE) {
        this.ISSSLENABLE = ISSSLENABLE;
    }

    public String getENABLEAUTOPINENTER() {
        return ENABLEAUTOPINENTER;
    }

    public void setENABLEAUTOPINENTER(String ENABLEAUTOPINENTER) {
        this.ENABLEAUTOPINENTER = ENABLEAUTOPINENTER;
    }

    public String getLANGUAGEACQNUM() {
        return LANGUAGEACQNUM;
    }

    public void setLANGUAGEACQNUM(String LANGUAGEACQNUM) {
        this.LANGUAGEACQNUM = LANGUAGEACQNUM;
    }

    public String getPAYPAK_ADJUSTENABLE() {
        return PAYPAK_ADJUSTENABLE;
    }

    public void setPAYPAK_ADJUSTENABLE(String PAYPAK_ADJUSTENABLE) {
        this.PAYPAK_ADJUSTENABLE = PAYPAK_ADJUSTENABLE;
    }

    public String getJCB_ADVCASHENABLE() {
        return JCB_ADVCASHENABLE;
    }

    public void setJCB_ADVCASHENABLE(String JCB_ADVCASHENABLE) {
        this.JCB_ADVCASHENABLE = JCB_ADVCASHENABLE;
    }

    public String getTIPNUMADJUST() {
        return TIPNUMADJUST;
    }

    public void setTIPNUMADJUST(String TIPNUMADJUST) {
        this.TIPNUMADJUST = TIPNUMADJUST;
    }

    public String getTERMFORCEDLBPMAGENABLE() {
        return TERMFORCEDLBPMAGENABLE;
    }

    public void setTERMFORCEDLBPMAGENABLE(String TERMFORCEDLBPMAGENABLE) {
        this.TERMFORCEDLBPMAGENABLE = TERMFORCEDLBPMAGENABLE;
    }

    public String getVISA_ADVCASHENABLE() {
        return VISA_ADVCASHENABLE;
    }

    public void setVISA_ADVCASHENABLE(String VISA_ADVCASHENABLE) {
        this.VISA_ADVCASHENABLE = VISA_ADVCASHENABLE;
    }

    public String getCTLSENABLED() {
        return CTLSENABLED;
    }

    public void setCTLSENABLED(String CTLSENABLED) {
        this.CTLSENABLED = CTLSENABLED;
    }

    public String getUPI_PINBYPASS() {
        return UPI_PINBYPASS;
    }

    public void setUPI_PINBYPASS(String UPI_PINBYPASS) {
        this.UPI_PINBYPASS = UPI_PINBYPASS;
    }

    public String getPAYPAK_BANKFUNDED() {
        return PAYPAK_BANKFUNDED;
    }

    public void setPAYPAK_BANKFUNDED(String PAYPAK_BANKFUNDED) {
        this.PAYPAK_BANKFUNDED = PAYPAK_BANKFUNDED;
    }

    public String getJCB_ACQUIRER() {
        return JCB_ACQUIRER;
    }

    public void setJCB_ACQUIRER(String JCB_ACQUIRER) {
        this.JCB_ACQUIRER = JCB_ACQUIRER;
    }

    public String getAMEX_COMPLETIONENABLE() {
        return AMEX_COMPLETIONENABLE;
    }

    public void setAMEX_COMPLETIONENABLE(String AMEX_COMPLETIONENABLE) {
        this.AMEX_COMPLETIONENABLE = AMEX_COMPLETIONENABLE;
    }

    public String getF49ENABLE() {
        return F49ENABLE;
    }

    public void setF49ENABLE(String f49ENABLE) {
        F49ENABLE = f49ENABLE;
    }

    public String getTOPUPENABLED() {
        return TOPUPENABLED;
    }

    public void setTOPUPENABLED(String TOPUPENABLED) {
        this.TOPUPENABLED = TOPUPENABLED;
    }

    public String getUPI_CHSERVCODE() {
        return UPI_CHSERVCODE;
    }

    public void setUPI_CHSERVCODE(String UPI_CHSERVCODE) {
        this.UPI_CHSERVCODE = UPI_CHSERVCODE;
    }

    public String getUPDATEME() {
        return UPDATEME;
    }

    public void setUPDATEME(String UPDATEME) {
        this.UPDATEME = UPDATEME;
    }

    public String getAMEX_PREAUTHENABLE() {
        return AMEX_PREAUTHENABLE;
    }

    public void setAMEX_PREAUTHENABLE(String AMEX_PREAUTHENABLE) {
        this.AMEX_PREAUTHENABLE = AMEX_PREAUTHENABLE;
    }

    public String getEMVLOGSENABLED() {
        return EMVLOGSENABLED;
    }

    public void setEMVLOGSENABLED(String EMVLOGSENABLED) {
        this.EMVLOGSENABLED = EMVLOGSENABLED;
    }

    public String getTMSIP() {
        return TMSIP;
    }

    public void setTMSIP(String TMSIP) {
        this.TMSIP = TMSIP;
    }

    public String getVISA_CHSIGN() {
        return VISA_CHSIGN;
    }

    public void setVISA_CHSIGN(String VISA_CHSIGN) {
        this.VISA_CHSIGN = VISA_CHSIGN;
    }

    public String getAMEX_CHECKEXPIRY() {
        return AMEX_CHECKEXPIRY;
    }

    public void setAMEX_CHECKEXPIRY(String AMEX_CHECKEXPIRY) {
        this.AMEX_CHECKEXPIRY = AMEX_CHECKEXPIRY;
    }

    public String getMERCHANTID() {
        return MERCHANTID;
    }

    public void setMERCHANTID(String MERCHANTID) {
        this.MERCHANTID = MERCHANTID;
    }

    public String getMASTER_VOIDENABLE() {
        return MASTER_VOIDENABLE;
    }

    public void setMASTER_VOIDENABLE(String MASTER_VOIDENABLE) {
        this.MASTER_VOIDENABLE = MASTER_VOIDENABLE;
    }

    public String getVENDORCODE() {
        return VENDORCODE;
    }

    public void setVENDORCODE(String VENDORCODE) {
        this.VENDORCODE = VENDORCODE;
    }

    public String getRECEIPTPRINTED() {
        return RECEIPTPRINTED;
    }

    public void setRECEIPTPRINTED(String RECEIPTPRINTED) {
        this.RECEIPTPRINTED = RECEIPTPRINTED;
    }

    public String getJCB_CHSIGN() {
        return JCB_CHSIGN;
    }

    public void setJCB_CHSIGN(String JCB_CHSIGN) {
        this.JCB_CHSIGN = JCB_CHSIGN;
    }

    public String getVISA_VOIDENABLE() {
        return VISA_VOIDENABLE;
    }

    public void setVISA_VOIDENABLE(String VISA_VOIDENABLE) {
        this.VISA_VOIDENABLE = VISA_VOIDENABLE;
    }

    public String getMASTER_ISSNAME() {
        return MASTER_ISSNAME;
    }

    public void setMASTER_ISSNAME(String MASTER_ISSNAME) {
        this.MASTER_ISSNAME = MASTER_ISSNAME;
    }

    public String getReponsemessage() {
        return Reponsemessage;
    }

    public void setReponsemessage(String reponsemessage) {
        Reponsemessage = reponsemessage;
    }

    public String getHIDECARDHOLDERNAME() {
        return HIDECARDHOLDERNAME;
    }

    public void setHIDECARDHOLDERNAME(String HIDECARDHOLDERNAME) {
        this.HIDECARDHOLDERNAME = HIDECARDHOLDERNAME;
    }

    public String getMERCHANTNAME() {
        return MERCHANTNAME;
    }

    public void setMERCHANTNAME(String MERCHANTNAME) {
        this.MERCHANTNAME = MERCHANTNAME;
    }

    public String getJCB_MOD10() {
        return JCB_MOD10;
    }

    public void setJCB_MOD10(String JCB_MOD10) {
        this.JCB_MOD10 = JCB_MOD10;
    }

    public String getPRINTDECLINEDRCPT() {
        return PRINTDECLINEDRCPT;
    }

    public void setPRINTDECLINEDRCPT(String PRINTDECLINEDRCPT) {
        this.PRINTDECLINEDRCPT = PRINTDECLINEDRCPT;
    }

    public String getUPI_LAST4DSMART() {
        return UPI_LAST4DSMART;
    }

    public void setUPI_LAST4DSMART(String UPI_LAST4DSMART) {
        this.UPI_LAST4DSMART = UPI_LAST4DSMART;
    }

    public String getADVCASHENABLE() {
        return ADVCASHENABLE;
    }

    public void setADVCASHENABLE(String ADVCASHENABLE) {
        this.ADVCASHENABLE = ADVCASHENABLE;
    }

    public String getAMEX_LAST4DMAG() {
        return AMEX_LAST4DMAG;
    }

    public void setAMEX_LAST4DMAG(String AMEX_LAST4DMAG) {
        this.AMEX_LAST4DMAG = AMEX_LAST4DMAG;
    }

    public String getUPI_VOIDENABLE() {
        return UPI_VOIDENABLE;
    }

    public void setUPI_VOIDENABLE(String UPI_VOIDENABLE) {
        this.UPI_VOIDENABLE = UPI_VOIDENABLE;
    }

    public String getPREAUTHBATCHMAXCOUNT() {
        return PREAUTHBATCHMAXCOUNT;
    }

    public void setPREAUTHBATCHMAXCOUNT(String PREAUTHBATCHMAXCOUNT) {
        this.PREAUTHBATCHMAXCOUNT = PREAUTHBATCHMAXCOUNT;
    }

    public String getADVICEENABLED() {
        return ADVICEENABLED;
    }

    public void setADVICEENABLED(String ADVICEENABLED) {
        this.ADVICEENABLED = ADVICEENABLED;
    }

    public String getTERMFORCEDLBPMANUALENABLE() {
        return TERMFORCEDLBPMANUALENABLE;
    }

    public void setTERMFORCEDLBPMANUALENABLE(String TERMFORCEDLBPMANUALENABLE) {
        this.TERMFORCEDLBPMANUALENABLE = TERMFORCEDLBPMANUALENABLE;
    }

    public String getMASTER_ADVCASHENABLE() {
        return MASTER_ADVCASHENABLE;
    }

    public void setMASTER_ADVCASHENABLE(String MASTER_ADVCASHENABLE) {
        this.MASTER_ADVCASHENABLE = MASTER_ADVCASHENABLE;
    }

    public String getPAYPAK_PREAUTHENABLE() {
        return PAYPAK_PREAUTHENABLE;
    }

    public void setPAYPAK_PREAUTHENABLE(String PAYPAK_PREAUTHENABLE) {
        this.PAYPAK_PREAUTHENABLE = PAYPAK_PREAUTHENABLE;
    }

    public String getAMEX_VOIDENABLE() {
        return AMEX_VOIDENABLE;
    }

    public void setAMEX_VOIDENABLE(String AMEX_VOIDENABLE) {
        this.AMEX_VOIDENABLE = AMEX_VOIDENABLE;
    }

    public String getPAYPAK_PINBYPASS() {
        return PAYPAK_PINBYPASS;
    }

    public void setPAYPAK_PINBYPASS(String PAYPAK_PINBYPASS) {
        this.PAYPAK_PINBYPASS = PAYPAK_PINBYPASS;
    }

    public String getAMEX_BANKFUNDED() {
        return AMEX_BANKFUNDED;
    }

    public void setAMEX_BANKFUNDED(String AMEX_BANKFUNDED) {
        this.AMEX_BANKFUNDED = AMEX_BANKFUNDED;
    }

    public String getMERCHANTADDRESS3() {
        return MERCHANTADDRESS3;
    }

    public void setMERCHANTADDRESS3(String MERCHANTADDRESS3) {
        this.MERCHANTADDRESS3 = MERCHANTADDRESS3;
    }

    public String getMERCHANTADDRESS2() {
        return MERCHANTADDRESS2;
    }

    public void setMERCHANTADDRESS2(String MERCHANTADDRESS2) {
        this.MERCHANTADDRESS2 = MERCHANTADDRESS2;
    }

    public String getMERCHANTADDRESS1() {
        return MERCHANTADDRESS1;
    }

    public void setMERCHANTADDRESS1(String MERCHANTADDRESS1) {
        this.MERCHANTADDRESS1 = MERCHANTADDRESS1;
    }

    public String getJCB_PREAUTHENABLE() {
        return JCB_PREAUTHENABLE;
    }

    public void setJCB_PREAUTHENABLE(String JCB_PREAUTHENABLE) {
        this.JCB_PREAUTHENABLE = JCB_PREAUTHENABLE;
    }

    public String getUPI_CHECKPIN() {
        return UPI_CHECKPIN;
    }

    public void setUPI_CHECKPIN(String UPI_CHECKPIN) {
        this.UPI_CHECKPIN = UPI_CHECKPIN;
    }

    public String getUSERID() {
        return USERID;
    }

    public void setUSERID(String USERID) {
        this.USERID = USERID;
    }

    public String getTERMINALMANUALENTRY() {
        return TERMINALMANUALENTRY;
    }

    public void setTERMINALMANUALENTRY(String TERMINALMANUALENTRY) {
        this.TERMINALMANUALENTRY = TERMINALMANUALENTRY;
    }

    public String getECRENABLED() {
        return ECRENABLED;
    }

    public void setECRENABLED(String ECRENABLED) {
        this.ECRENABLED = ECRENABLED;
    }

    public String getVISA_CHSERVCODE() {
        return VISA_CHSERVCODE;
    }

    public void setVISA_CHSERVCODE(String VISA_CHSERVCODE) {
        this.VISA_CHSERVCODE = VISA_CHSERVCODE;
    }

    public String getMERCHANTADDRESS4() {
        return MERCHANTADDRESS4;
    }

    public void setMERCHANTADDRESS4(String MERCHANTADDRESS4) {
        this.MERCHANTADDRESS4 = MERCHANTADDRESS4;
    }

    public String getVISA_CHECKEXPIRY() {
        return VISA_CHECKEXPIRY;
    }

    public void setVISA_CHECKEXPIRY(String VISA_CHECKEXPIRY) {
        this.VISA_CHECKEXPIRY = VISA_CHECKEXPIRY;
    }

    public String getPINTIMEOUT() {
        return PINTIMEOUT;
    }

    public void setPINTIMEOUT(String PINTIMEOUT) {
        this.PINTIMEOUT = PINTIMEOUT;
    }

    public String getPREDIALENABLED() {
        return PREDIALENABLED;
    }

    public void setPREDIALENABLED(String PREDIALENABLED) {
        this.PREDIALENABLED = PREDIALENABLED;
    }

    public String getPRINTSERIALNUM() {
        return PRINTSERIALNUM;
    }

    public void setPRINTSERIALNUM(String PRINTSERIALNUM) {
        this.PRINTSERIALNUM = PRINTSERIALNUM;
    }

    public String getPANLENGTHMIN() {
        return PANLENGTHMIN;
    }

    public void setPANLENGTHMIN(String PANLENGTHMIN) {
        this.PANLENGTHMIN = PANLENGTHMIN;
    }

    public String getPOWERDOWNTIME() {
        return POWERDOWNTIME;
    }

    public void setPOWERDOWNTIME(String POWERDOWNTIME) {
        this.POWERDOWNTIME = POWERDOWNTIME;
    }

    public String getSETTLERANGE() {
        return SETTLERANGE;
    }

    public void setSETTLERANGE(String SETTLERANGE) {
        this.SETTLERANGE = SETTLERANGE;
    }

    public String getMASTER_CHECKEXPIRY() {
        return MASTER_CHECKEXPIRY;
    }

    public void setMASTER_CHECKEXPIRY(String MASTER_CHECKEXPIRY) {
        this.MASTER_CHECKEXPIRY = MASTER_CHECKEXPIRY;
    }

    public String getAMEX_ADJUSTENABLE() {
        return AMEX_ADJUSTENABLE;
    }

    public void setAMEX_ADJUSTENABLE(String AMEX_ADJUSTENABLE) {
        this.AMEX_ADJUSTENABLE = AMEX_ADJUSTENABLE;
    }

    public String getPAYPAK_MANUALENTRY() {
        return PAYPAK_MANUALENTRY;
    }

    public void setPAYPAK_MANUALENTRY(String PAYPAK_MANUALENTRY) {
        this.PAYPAK_MANUALENTRY = PAYPAK_MANUALENTRY;
    }

    public String getJCB_ISSUERACTIVATED() {
        return JCB_ISSUERACTIVATED;
    }

    public void setJCB_ISSUERACTIVATED(String JCB_ISSUERACTIVATED) {
        this.JCB_ISSUERACTIVATED = JCB_ISSUERACTIVATED;
    }

    public String getSRCADDTPDU() {
        return SRCADDTPDU;
    }

    public void setSRCADDTPDU(String SRCADDTPDU) {
        this.SRCADDTPDU = SRCADDTPDU;
    }

    public String getINITTERM() {
        return INITTERM;
    }

    public void setINITTERM(String INITTERM) {
        this.INITTERM = INITTERM;
    }

    public String getPRIMARYPORT() {
        return PRIMARYPORT;
    }

    public void setPRIMARYPORT(String PRIMARYPORT) {
        this.PRIMARYPORT = PRIMARYPORT;
    }

    public String getISTPDUENABLE() {
        return ISTPDUENABLE;
    }

    public void setISTPDUENABLE(String ISTPDUENABLE) {
        this.ISTPDUENABLE = ISTPDUENABLE;
    }

    public String getVISA_ACQUIRER() {
        return VISA_ACQUIRER;
    }

    public void setVISA_ACQUIRER(String VISA_ACQUIRER) {
        this.VISA_ACQUIRER = VISA_ACQUIRER;
    }

    public String getAMEX_ACQUIRER() {
        return AMEX_ACQUIRER;
    }

    public void setAMEX_ACQUIRER(String AMEX_ACQUIRER) {
        this.AMEX_ACQUIRER = AMEX_ACQUIRER;
    }

    public String getAMEX_CHECKPIN() {
        return AMEX_CHECKPIN;
    }

    public void setAMEX_CHECKPIN(String AMEX_CHECKPIN) {
        this.AMEX_CHECKPIN = AMEX_CHECKPIN;
    }

    public String getBATCHMAXCOUNT() {
        return BATCHMAXCOUNT;
    }

    public void setBATCHMAXCOUNT(String BATCHMAXCOUNT) {
        this.BATCHMAXCOUNT = BATCHMAXCOUNT;
    }

    public String getPRIMARYMEDIUM() {
        return PRIMARYMEDIUM;
    }

    public void setPRIMARYMEDIUM(String PRIMARYMEDIUM) {
        this.PRIMARYMEDIUM = PRIMARYMEDIUM;
    }

    public String getVISA_CHECKPIN() {
        return VISA_CHECKPIN;
    }

    public void setVISA_CHECKPIN(String VISA_CHECKPIN) {
        this.VISA_CHECKPIN = VISA_CHECKPIN;
    }

    public String getMASTER_ISSNUM() {
        return MASTER_ISSNUM;
    }

    public void setMASTER_ISSNUM(String MASTER_ISSNUM) {
        this.MASTER_ISSNUM = MASTER_ISSNUM;
    }

    public String getPASSWORDSYSTEM() {
        return PASSWORDSYSTEM;
    }

    public void setPASSWORDSYSTEM(String PASSWORDSYSTEM) {
        this.PASSWORDSYSTEM = PASSWORDSYSTEM;
    }

    public String getJCB_ISSNAME() {
        return JCB_ISSNAME;
    }

    public void setJCB_ISSNAME(String JCB_ISSNAME) {
        this.JCB_ISSNAME = JCB_ISSNAME;
    }

    public String getTMSPORT() {
        return TMSPORT;
    }

    public void setTMSPORT(String TMSPORT) {
        this.TMSPORT = TMSPORT;
    }

    public String getINTERPACKETTIMEOUT() {
        return INTERPACKETTIMEOUT;
    }

    public void setINTERPACKETTIMEOUT(String INTERPACKETTIMEOUT) {
        this.INTERPACKETTIMEOUT = INTERPACKETTIMEOUT;
    }

    public String getPRINTCARDREMRCPT() {
        return PRINTCARDREMRCPT;
    }

    public void setPRINTCARDREMRCPT(String PRINTCARDREMRCPT) {
        this.PRINTCARDREMRCPT = PRINTCARDREMRCPT;
    }

    public String getAMEX_PINBYPASS() {
        return AMEX_PINBYPASS;
    }

    public void setAMEX_PINBYPASS(String AMEX_PINBYPASS) {
        this.AMEX_PINBYPASS = AMEX_PINBYPASS;
    }

    public String getTERMINALWIFINETMASK() {
        return TERMINALWIFINETMASK;
    }

    public void setTERMINALWIFINETMASK(String TERMINALWIFINETMASK) {
        this.TERMINALWIFINETMASK = TERMINALWIFINETMASK;
    }

    public String getJCB_LAST4DSMART() {
        return JCB_LAST4DSMART;
    }

    public void setJCB_LAST4DSMART(String JCB_LAST4DSMART) {
        this.JCB_LAST4DSMART = JCB_LAST4DSMART;
    }

    public String getAMEX_CHSIGN() {
        return AMEX_CHSIGN;
    }

    public void setAMEX_CHSIGN(String AMEX_CHSIGN) {
        this.AMEX_CHSIGN = AMEX_CHSIGN;
    }

    public String getPAYPAK_CHSERVCODE() {
        return PAYPAK_CHSERVCODE;
    }

    public void setPAYPAK_CHSERVCODE(String PAYPAK_CHSERVCODE) {
        this.PAYPAK_CHSERVCODE = PAYPAK_CHSERVCODE;
    }

    public String getTERMINALTOTCURRENCIES() {
        return TERMINALTOTCURRENCIES;
    }

    public void setTERMINALTOTCURRENCIES(String TERMINALTOTCURRENCIES) {
        this.TERMINALTOTCURRENCIES = TERMINALTOTCURRENCIES;
    }

    public String getISDEBUGENABLE() {
        return ISDEBUGENABLE;
    }

    public void setISDEBUGENABLE(String ISDEBUGENABLE) {
        this.ISDEBUGENABLE = ISDEBUGENABLE;
    }

    public String getPAYPAK_LAST4DMAG() {
        return PAYPAK_LAST4DMAG;
    }

    public void setPAYPAK_LAST4DMAG(String PAYPAK_LAST4DMAG) {
        this.PAYPAK_LAST4DMAG = PAYPAK_LAST4DMAG;
    }

    public String getPSTNINITNUMBER() {
        return PSTNINITNUMBER;
    }

    public void setPSTNINITNUMBER(String PSTNINITNUMBER) {
        this.PSTNINITNUMBER = PSTNINITNUMBER;
    }

    public String getJCB_COMPLETIONENABLE() {
        return JCB_COMPLETIONENABLE;
    }

    public void setJCB_COMPLETIONENABLE(String JCB_COMPLETIONENABLE) {
        this.JCB_COMPLETIONENABLE = JCB_COMPLETIONENABLE;
    }

    public String getAMOUNTENTRYTIMEOUT() {
        return AMOUNTENTRYTIMEOUT;
    }

    public void setAMOUNTENTRYTIMEOUT(String AMOUNTENTRYTIMEOUT) {
        this.AMOUNTENTRYTIMEOUT = AMOUNTENTRYTIMEOUT;
    }

    public String getWIFIPASSWORD() {
        return WIFIPASSWORD;
    }

    public void setWIFIPASSWORD(String WIFIPASSWORD) {
        this.WIFIPASSWORD = WIFIPASSWORD;
    }

    public String getTERMFORCEDLBPENABLE() {
        return TERMFORCEDLBPENABLE;
    }

    public void setTERMFORCEDLBPENABLE(String TERMFORCEDLBPENABLE) {
        this.TERMFORCEDLBPENABLE = TERMFORCEDLBPENABLE;
    }

    public String getHELPALFAMSG() {
        return HELPALFAMSG;
    }

    public void setHELPALFAMSG(String HELPALFAMSG) {
        this.HELPALFAMSG = HELPALFAMSG;
    }

    public String getUPI_BANKFUNDED() {
        return UPI_BANKFUNDED;
    }

    public void setUPI_BANKFUNDED(String UPI_BANKFUNDED) {
        this.UPI_BANKFUNDED = UPI_BANKFUNDED;
    }

    public String getTERMINALCURRENCY2() {
        return TERMINALCURRENCY2;
    }

    public void setTERMINALCURRENCY2(String TERMINALCURRENCY2) {
        this.TERMINALCURRENCY2 = TERMINALCURRENCY2;
    }

    public String getTERMINALCURRENCY1() {
        return TERMINALCURRENCY1;
    }

    public void setTERMINALCURRENCY1(String TERMINALCURRENCY1) {
        this.TERMINALCURRENCY1 = TERMINALCURRENCY1;
    }

    public String getTERMINALAPN() {
        return TERMINALAPN;
    }

    public void setTERMINALAPN(String TERMINALAPN) {
        this.TERMINALAPN = TERMINALAPN;
    }

    public String getJCB_PINBYPASS() {
        return JCB_PINBYPASS;
    }

    public void setJCB_PINBYPASS(String JCB_PINBYPASS) {
        this.JCB_PINBYPASS = JCB_PINBYPASS;
    }

    public String getMASTER_PRINTEXPIRY() {
        return MASTER_PRINTEXPIRY;
    }

    public void setMASTER_PRINTEXPIRY(String MASTER_PRINTEXPIRY) {
        this.MASTER_PRINTEXPIRY = MASTER_PRINTEXPIRY;
    }

    // 0th entry in db is ubl configuration initial Single configuration
//    public static ProfileInit insertInitialConfig() {
//        return new ProfileInit(0, "200000005000508", "12345603");
//    }

    // 1st entry in db is UPI configuration initial Single configuration
   /* public static TerminalConfig insertUPIConfig(){
        return new TerminalConfig(1,"200000005000510","42017890",
                "UPI TEST HOST", "202.61.40.171","7006","444","N");// initial UPI config
    }*/

//    public static List<ProfileInit> terminalConfigList(){
//        List<ProfileInit> terminalConfigs = new ArrayList<>();
//       // terminalConfigs.add(0,insertInitialConfig());
//        //terminalConfigs.add(1,insertUPIConfig());
//        return terminalConfigs;
//    }
}
