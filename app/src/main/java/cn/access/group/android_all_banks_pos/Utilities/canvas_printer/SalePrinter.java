package cn.access.group.android_all_banks_pos.Utilities.canvas_printer;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import cn.access.group.android_all_banks_pos.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;

import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.ECR;
import cn.access.group.android_all_banks_pos.Utilities.TransPrinter;



/**
 * Created by Simon on 2019/2/22.
 */

public class SalePrinter extends TransPrinter {
    static final String TAG = "SalePrinter";
    protected List<PrinterItem> printerItems = null;

    public SalePrinter(Context context) {
        super(context);

    }

    public void initializeData(Bundle extraItems ){
        super.initializeData( extraItems);
        Utility.DEBUG_LOG(TAG, "initializeData");
        try {
            printerItems = new ArrayList<>();
            PrinterItem.LOGO.title.sValue = "v_logo.jpg";
            PrinterItem.LOGO.title.style = PrinterDefine.PStyle_align_left;

            PrinterItem.TRANS_TYPE.value.sValue = "SALE";

//            if( null != hostInformation ){
//                // set in super
//            }
//
//            if (null != require) {
//                PrinterItem.AMOUNT.value.sValue = Utility.getReadableAmount(require.getValue(ISO8583.ATTRIBUTE.Amount));
//            }

            printerItems.add(PrinterItem.LOGO);
            printerItems.add(PrinterItem.TITLE);
            printerItems.add(PrinterItem.SUBTITLE);
            printerItems.add(PrinterItem.HOST);

            printerItems.add(PrinterItem.MERCHANT_NAME);
            printerItems.add(PrinterItem.MERCHANT_ID);
            printerItems.add(PrinterItem.TERMINAL_ID);

            printerItems.add(PrinterItem.TRANS_TYPE);

            printerItems.add(PrinterItem.LINE);

            printerItems.add(PrinterItem.CARD_NO_CARD_SCHEME);
            printerItems.add(PrinterItem.AMOUNT);

            PrinterItem.FLEXIBLE_1.copy(PrinterItem.AMOUNT);
            // Set transaction amount on receipt and make it readable
//            PrinterItem.FLEXIBLE_1.value.sValue = Utility.getReadableAmount(TransactionParams.getInstance().getTransactionAmount());
//            PrinterItem.FLEXIBLE_1.title.sValue += " (USD)";
            printerItems.add(PrinterItem.FLEXIBLE_1);

            printerItems.add(PrinterItem.DATE_TIME);

            //PrinterItem.QRCODE_1.value.sValue = context.getResources().getString(R.string.prn_qrcode2);

          //  PrinterItem.BARCODE_1.value.sValue = context.getResources().getString(R.string.prn_barcode);

            printerItems.add(PrinterItem.FEED);
            printerItems.add(PrinterItem.BARCODE_1);
            printerItems.add(PrinterItem.FEED);
            printerItems.add(PrinterItem.FEED);
            printerItems.add(PrinterItem.QRCODE_1);
            printerItems.add(PrinterItem.LINE);

            printerItems.add(PrinterItem.COMMENT_1);
            printerItems.add(PrinterItem.COMMENT_2);
            printerItems.add(PrinterItem.COMMENT_3);

           // ECR.ecrWrite(tnxType, DataFormatterUtil.formattedDate(transactionDetail.getTxnDate()),DataFormatterUtil.formattedTime(transactionDetail.getTxnTime()),DataFormatterUtil.maskCardNo(transactionDetail.getCardNo()),transactionDetail.getCardType(),transactionDetail.getCardHolderName(),transactionDetail.getAmount(),transactionDetail.getRrn(),transactionDetail.getAuthorizationIdentificationResponseCode(),"00","Approved");



        }catch ( Exception e){
            Utility.DEBUG_LOG(TAG,"Exception :" + e.getMessage());
            for (StackTraceElement m:e.getStackTrace()
                 ) {
                Utility.DEBUG_LOG(TAG,"Exception :" + m );

            }
        }
    }

}
