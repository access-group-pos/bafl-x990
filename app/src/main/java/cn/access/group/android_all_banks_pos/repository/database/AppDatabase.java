package cn.access.group.android_all_banks_pos.repository.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;


import cn.access.group.android_all_banks_pos.repository.dao.CardRangesDao;
import cn.access.group.android_all_banks_pos.repository.dao.IssuerDao;
import cn.access.group.android_all_banks_pos.repository.dao.ProfileInitDao;
import cn.access.group.android_all_banks_pos.repository.dao.ReversalDao;
import cn.access.group.android_all_banks_pos.repository.model.CardRanges;
import cn.access.group.android_all_banks_pos.repository.model.Issuer;
import cn.access.group.android_all_banks_pos.repository.model.ProfileInit;
import cn.access.group.android_all_banks_pos.repository.model.Reversal;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.repository.dao.TerminalConfigDao;
import cn.access.group.android_all_banks_pos.repository.dao.TransactionDetailDao;
import cn.access.group.android_all_banks_pos.repository.Converters;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;

/**
 * on 8/29/2019.
 * Singleton class to access database.
 */
@Database(entities = {TransactionDetail.class, TerminalConfig.class, CardRanges.class, Issuer.class, Reversal.class, ProfileInit.class}, version =20,exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract TransactionDetailDao transactionDetailDao();
    public abstract TerminalConfigDao terminalConfigDao();
    public abstract CardRangesDao cardRangesDao();
    public abstract IssuerDao issuerDao();
    public abstract ReversalDao reversalDao();
    public abstract ProfileInitDao profileInitDao();


    //Migrations represents history of change to the database and tables

//    static final Migration MIGRATION_2_3 = new Migration(2,3) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE TransactionDetail "
//                    + " ADD COLUMN status TEXT");
//
//        }
//    };
//    static final Migration MIGRATION_3_4 = new Migration(3,4) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE TransactionDetail "
//                    + " ADD COLUMN messageType TEXT");
//
//        }
//    }; static final Migration MIGRATION_4_5 = new Migration(4,5) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE TransactionDetail "
//                    + " ADD COLUMN acquiringInstitutionIdentificationCode TEXT");
//
//        }
//    }; static final Migration MIGRATION_5_6 = new Migration(5,6) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE TransactionDetail "
//                    + " ADD COLUMN forwardingInstitutionIdentificationCode TEXT");
//
//        }
//    };
//    static final Migration MIGRATION_6_7 = new Migration(6,7) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE TransactionDetail "
//                    + " ADD COLUMN processingCode TEXT");
//
//        }
//    };
//    static final Migration MIGRATION_7_8 = new Migration(7,8) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE TransactionDetail "
//                    + " ADD COLUMN cardType TEXT");
//
//        }
//    };
//
//
//    static final Migration MIGRATION_8_9 = new Migration(8,9) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE TransactionDetail "
//                    + " ADD COLUMN authorizationIdentificationResponseCode TEXT");
//
//        }
//    };
//
//
//    static final Migration MIGRATION_9_10 = new Migration(9,10) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE TransactionDetail "
//                    + " ADD COLUMN tag55 TEXT");
//
//        }
//    };
//
//
//
//    static final Migration MIGRATION_13_14 = new Migration(13,14) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("DROP TABLE TransactionDetail");
//
//        }
//    };
//    static final Migration MIGRATION_14_15 = new Migration(14,15) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("CREATE TABLE IF NOT EXISTS 'TransactionDetail' ( 'mId' TEXT,'tId' TEXT ,'stan' TEXT ,'invoiceNo' TEXT ,'rrn' TEXT,'txnDate' TEXT,'txnTime' TEXT ,'amount' TEXT ," +
//                    "'cardNo' TEXT, 'cardScheme' TEXT,'batchNo' TEXT,'track2' TEXT,'status' TEXT,'messageType' TEXT," +
//                    "'acquiringInstitutionIdentificationCode' TEXT ,'forwardingInstitutionIdentificationCode' TEXT ," +
//                    "'processingCode' TEXT,'cardType' TEXT,'authorizationIdentificationResponseCode' TEXT,'tag55' TEXT)");
//
//        }
//    };


    static final Migration MIGRATION_16_17 = new Migration(16,17) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE TerminalConfig ADD COLUMN settleCounter INTEGER  NOT NULL DEFAULT 0 ");
        }
    };
    static final Migration MIGRATION_17_18 = new Migration(17,18) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE TerminalConfig ADD COLUMN settleRange INTEGER  NOT NULL DEFAULT 3");
        }
    };
    static final Migration MIGRATION_18_19 = new Migration(18,19) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE TerminalConfig ADD COLUMN tipNumAdjust INTEGER  NOT NULL DEFAULT 2");
        }
    };
//    static final Migration MIGRATION_19_20 = new Migration(19,20) {
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE 'Reversal' ADD COLUMN 'migration' TEXT  NOT NULL DEFAULT ''");
//        }
//    };
    static final Migration MIGRATION_19_20 = new Migration(19,20) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE  'TransactionDetail' ADD COLUMN 'ecrRefNo' TEXT");
            database.execSQL("ALTER TABLE 'Reversal' ADD COLUMN 'ecrRefNo' TEXT ");
            database.execSQL("ALTER TABLE  'TransactionDetail' ADD COLUMN 'completion' INTEGER DEFAULT 0 NOT NULL");
            database.execSQL("ALTER TABLE 'Reversal' ADD COLUMN 'completion' INTEGER DEFAULT 0 NOT NULL");
            database.execSQL("ALTER TABLE 'TerminalConfig' ADD COLUMN 'ecrType' TEXT");
        }
    };


    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = buildDatabase(context);
        }
        return INSTANCE;
    }



    private static AppDatabase buildDatabase(Context context) {
        return Room.databaseBuilder(context,
                AppDatabase.class,
                "transaction-database")
                .fallbackToDestructiveMigration()

                .addMigrations(MIGRATION_19_20)
                .allowMainThreadQueries()
                .build();
              /*  .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
                            @Override
                            public void run() {
                                getAppDatabase(context).terminalConfigDao().insertConfig(TerminalConfig.insertInitialConfig());//insert initial Config
                                Utility.DEBUG_LOG("DataBase","Initial Config set");
                            }
                        });
                    }
                })*/
//                .addMigrations(MIGRATION_16_17)
//                .addMigrations(MIGRATION_17_18)
//                .addMigrations(MIGRATION_18_19)

    }

    private static AppDatabase buildSaveDaatbase(Context context) {
        return Room.databaseBuilder(context,
                AppDatabase.class,
                "transaction-database")
                /*  .addCallback(new Callback() {
                      @Override
                      public void onCreate(@NonNull SupportSQLiteDatabase db) {
                          super.onCreate(db);
                          Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
                              @Override
                              public void run() {
                                  getAppDatabase(context).terminalConfigDao().insertConfig(TerminalConfig.insertInitialConfig());//insert initial Config
                                  Utility.DEBUG_LOG("DataBase","Initial Config set");
                              }
                          });
                      }
                  })*/
                .build();
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

}
