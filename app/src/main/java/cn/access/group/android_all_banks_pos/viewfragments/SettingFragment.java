package cn.access.group.android_all_banks_pos.viewfragments;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;

import android.text.InputType;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.system_service.aidl.ISystemManager;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.Utilities.GetRequest;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.repository.Repository;

import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.usecase.DeleteReversal;
import cn.access.group.android_all_banks_pos.usecase.EmvSetAidRid;






import static cn.access.group.android_all_banks_pos.Utilities.Constants.ACQUIRER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AMEX_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AUTO_SETTLEMENT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AUTO_SETTLETIME;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.BATCH_NO;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CHECK_EXPIRY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.COM_ETHERNET;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.COM_GPRS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CURRENCY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.EMV_LOGS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ENABLE_LOGS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FALLBACK_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FOOTER_LINE_1;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FOOTER_LINE_2;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_1;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_2;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_3;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_4;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HIGH_BIN;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ISSUER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LAST_4DIGIT_ICC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LAST_4DIGIT_MAG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.LOW_BIN;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANAGER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANUAL_ENTRY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANUAL_ENTRY_ISSUER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MARKET_SEG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_NAME;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MOD10;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MSGHEADER_LENHEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.NII;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PIN_BYPASS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PRI_MEDIUM;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalTxnUpload;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_IP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_MEDIUM;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_PORT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SSL_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SUPER_USER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SYSTEM_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TERMINAL_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_ENABLED;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_THRESHOLD;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TMS_IP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TMS_PORT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TOPUP_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.USER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.hostIP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.hostPort;

/**
 * A simple {@link Fragment} subclass.
 *
 * @author muhammad.humayun
 * on 9/19/2019.
 */
public class SettingFragment extends Fragment {

    @BindView(R.id.deleteReversalCard)
    Button deleteReversalCard;
    Unbinder unbinder;
    String newText;
    private WeakReference<Context> mContext;
    boolean Validated=false;
    @BindView(R.id.registration)
    Button registration;
    @BindView(R.id.complaint)
    Button complaint;
    @BindView(R.id.tmsUpdate)
    Button tmsUpdate;
    @BindView(R.id.profileDownload)
    Button profileDownload;
    IEMV iemv;
    DeleteReversal deleteReversal;
    ISystemManager iSystemManager;
    public SettingFragment() {
        // Required empty public constructor
    }
    @SuppressLint("ValidFragment")
    public SettingFragment(ISystemManager iSystemManager) {
        this.iSystemManager = iSystemManager;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        unbinder = ButterKnife.bind(this, view);
        // hostInformation = MultiHostsConfig.get(DashboardContainer.index);
//        terminalIdNumber.setText(TERMINAL_ID);
//        merchantIdNumber.setText(MERCHANT_ID);
//        merchantName.setText(MERCHANT_NAME);
//        hostIpNumber.setText(hostIP);
//        hostPortNumber.setText(String.valueOf(Constants.hostPort));
       // niiNumber.setText(NII);
        //showTipText();
        //setListener();

        return view;
    }
    public boolean isSettlementNeeded() {
        try {
            if( ((DashboardContainer)mContext.get()).getAllBatchTransactions().size() !=  0) {
                return SharedPref.read("isSettleRequiured", "").equals("Y");
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }
//    private void showTipText() {
//        if (SharedPref.read("TIP_ENABLED","").equalsIgnoreCase("Y")){
//            tipEnableText="Tip Enabled";
//            tipText.setText(tipEnableText);
//            tipToggle.setChecked(true);
//        }
//        else if (SharedPref.read("TIP_ENABLED","").equalsIgnoreCase("N")){
//            tipEnableText="Tip Disabled";
//            tipText.setText(tipEnableText);
//            tipToggle.setChecked(false);
//        }
//    }

//    private void setListener() {
//        tipToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                String title = "";
//                if (isChecked){
//                    SharedPref.write("TIP_ENABLED","Y");
//                    TIP_ENABLED = SharedPref.read("TIP_ENABLED","");
//                    showTipText();
//                    Validated = true;
//                    title = "Tip Enabled";
//                }else {
//                    SharedPref.write("TIP_ENABLED","N");
//                    TIP_ENABLED = SharedPref.read("TIP_ENABLED","");
//                    showTipText();
//                    Validated = true;
//                    title = "Tip Disabled";
//                }
//                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
//                Repository repository = new Repository(AppDatabase.getAppDatabase(mContext));
//                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
//                repository.updateTerminalConfiguration(TerminalConfig);
//                DialogUtil.successDialog(mContext, title);
//            }
//        });
//    }
//
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.deleteReversalCard,
           R.id.reset_aid_rid,  R.id.profileDownload,R.id.registration,R.id.complaint,R.id.tmsUpdate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            case R.id.terminal_id_number_ll:
//                changeInformation("Terminal ID","Change Terminal Id","tid",8,InputType.TYPE_CLASS_NUMBER);
//                break;
//            case R.id.merchant_id_number_ll:
//                changeInformation("Merchant ID","Change Merchant Id","mid",15,InputType.TYPE_CLASS_NUMBER);
//                break;
//            case R.id.merchant_name_ll:
//                changeInformation("Merchant Name","Change Merchant Name","mname",30,InputType.TYPE_CLASS_TEXT);
//                break;
//            case R.id.host_ip_number_ll:
//                changeInformation("Host IP","Change Host Ip","hip",15,InputType.TYPE_CLASS_TEXT);
//                break;
//            case R.id.host_port_number_ll:
//                changeInformation("Host Port","Change Host Port","hp",4,InputType.TYPE_CLASS_NUMBER);
//                break;
//            case R.id.nii_number_ll:
//                changeInformation("NII","Change NII Number","nii",3,InputType.TYPE_CLASS_NUMBER);
//                break;
            case R.id.reset_aid_rid:
                EmvSetAidRid emvSetAidRid = new EmvSetAidRid(iemv, mContext.get());
                emvSetAidRid.setAID(3);
                emvSetAidRid.setRID(3);
                emvSetAidRid.setAID(1);
                emvSetAidRid.setRID(1);
                break;
            case R.id.deleteReversalCard:
                Utility.DEBUG_LOG("Shivam","Lalwani");
                if(isSettlementNeeded()){
                    DialogUtil.infoDialog(mContext.get(),"Must Settle First!");
                }
                else{
                    deleteReversal = new DeleteReversal(mContext.get());}
                break;

            case R.id.registration:
                if (PortalTxnUpload.equals("Y"))
                    DashboardContainer.switchFragmentWithBackStack(new RegistrationFragment(), "registration");
                else
                {
                    Toast.makeText(mContext.get(),"Not Allowed", Toast.LENGTH_SHORT).show();
                    DashboardContainer.backStack();
                }
                break;

            case R.id.complaint:
                if (PortalTxnUpload.equals("Y"))
                    DashboardContainer.switchFragmentWithBackStack(new ComplaintFragment(), "complaint ");
                else
                {
                    Toast.makeText(mContext.get(),"Not Allowed", Toast.LENGTH_SHORT).show();
                    DashboardContainer.backStack();
                }

                break;
            case R.id.tmsUpdate:
                try {
                    if(isSettlementNeeded() || ((DashboardContainer)mContext.get()).getAllBatchTransactions().size() != 0){
                        DialogUtil.infoDialog(mContext.get(), "Must Settle First!");
                    }else {
                        DashboardContainer.switchFragmentWithBackStack(new UpdateFragment(iSystemManager), "tms update");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                break;
            case R.id.profileDownload:
                try {

                    if (isSettlementNeeded() || ((DashboardContainer)mContext.get()).getAllBatchTransactions().size() != 0) {
                        DialogUtil.infoDialog(mContext.get(), "Must Settle First!");
                    } else {
                        GetRequest.sendGetRequest(mContext.get(), iSystemManager);
                        MainApplication.showProgressDialog(mContext.get(),"Please wait");
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 5s = 5000ms
                                MainApplication.hideProgressDialog();
                                DashboardContainer.backStack();;
                            }
                        }, 5000);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


//                try {
//                    systemManager.reboot();
//                } catch (RemoteException e) {
//                    e.printStackTrace();
//                }

                break;
        }
    }
    private final ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            IDeviceService idevice = IDeviceService.Stub.asInterface(service);
            try {
                iemv = idevice.getEMV();
                //Toast.makeText(mContext.get(), "bind service success", Toast.LENGTH_SHORT).show();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Intent intent = new Intent();
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        boolean isSucc = context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        mContext = new WeakReference<>(context);

    }
    @Override
    public void onDetach() {
        super.onDetach();
        if (getActivity() != null) {
            getActivity().unbindService(conn);
        }
    }

    /**
     * dialog to change any of the data and rewrite to persistence
     * @param title the dialog title
     * @param content the msg to display
     * @param type the data field to be changed
     */
    private void changeInformation(String title,String content,String type, int keyboardLength,int KeyboardType){
        DialogUtil.inputDialogForSystemSettingFragment(mContext.get(),title,content, sweetAlertDialog -> {
            EditText text= Objects.requireNonNull(sweetAlertDialog.getWindow()).getDecorView().findViewById(R.id.my_edit_text_1);
            text.setInputType(InputType.TYPE_CLASS_TEXT);
            newText =   text.getText().toString();
            switch (type){
//                case "tid":
//                    if(newText.length()!=8){
//                        DialogUtil.errorDialog(mContext.get(),"Error!","Length must be 8!");
//                        sweetAlertDialog.dismissWithAnimation(); Validated = false;
//                    }
//                    else {
//                        SharedPref.write("TERMINAL_ID", newText);
//                        TERMINAL_ID = SharedPref.read("TERMINAL_ID", "");
//                        terminalIdNumber.setText(TERMINAL_ID);Validated = true;
//                    }
//                    // hostInformation.terminalID=TERMINAL_ID;
//                    break;
//                case "mid":
//                    if(newText.length()!=15){
//                        DialogUtil.errorDialog(mContext.get(),"Error!","Length must be 15, add spaces if needed!");
//                        sweetAlertDialog.dismissWithAnimation();Validated = false;
//                    }
//                    else {
//                        SharedPref.write("MERCHANT_ID", newText);
//                        MERCHANT_ID = SharedPref.read("MERCHANT_ID", "");
//                        merchantIdNumber.setText(MERCHANT_ID);Validated=true;
//                    }
//                    //  hostInformation.merchantID=MERCHANT_ID;
//                    break;
//                case "mname":
//                    if(newText.equals("")){
//                        DialogUtil.errorDialog(mContext.get(),"Error!","cannot be empty!");
//                        sweetAlertDialog.dismissWithAnimation();Validated = false;
//                    }
//                    else {
//                        SharedPref.write("merchantName", newText);
//                        MERCHANT_NAME = SharedPref.read("merchantName", "");
//                        merchantName.setText(MERCHANT_NAME);Validated = true;
//                    }
                    // hostInformation.merchantName=MERCHANT_NAME;
//                    break;
//                case "hip":
//                    if(!Patterns.IP_ADDRESS.matcher(newText).matches()){
//                        DialogUtil.errorDialog(mContext.get(),"Error!","invalid ip!");
//                        sweetAlertDialog.dismissWithAnimation();Validated = false;
//                    }
//                    else {
//                        SharedPref.write("hostIP", newText);
//                        hostIP = SharedPref.read("hostIP", "");
//                        hostIpNumber.setText(hostIP);Validated = true;
//                    }
//                    //  hostInformation.hostAddr=hostIP;
//                    break;
//                case "hp":
//                    if(newText.length()!=4){
//                        DialogUtil.errorDialog(mContext.get(),"Error!","Length must be 4!");
//                        sweetAlertDialog.dismissWithAnimation();Validated = false;
//                    }
//                    else {
//                        int port = Integer.valueOf(newText);
//                        SharedPref.write("hostPort", port);
//                        hostPort = SharedPref.read("hostPort");
//                        hostPortNumber.setText(String.valueOf(hostPort));Validated = true;
//                    }
//                    // hostInformation.hostPort=hostPort;
//                    break;
//                case "nii":
//                    if(newText.length()!=3){
//                        DialogUtil.errorDialog(mContext.get(),"Error!","Length must be 4!");
//                        sweetAlertDialog.dismissWithAnimation();Validated = false;
//                    }
//                    else {
//                        SharedPref.write("NII", newText);
//                        NII = SharedPref.read("NII", "");
//                        niiNumber.setText(NII);Validated = true;
//                    }
//                    // hostInformation.nii=NII;
//                    break;
                case "tip":
                    break;
            }
            //  MultiHostsConfig.update(DashboardContainer.index, hostInformation);
            if(Validated) {

                //TerminalConfig TerminalConfig = new TerminalConfig(0, MERCHANT_ID, TERMINAL_ID, MERCHANT_NAME,  NII, TIP_ENABLED,  COM_ETHERNET, COM_GPRS,ENABLE_LOGS,MARKET_SEG,TIP_THRESHOLD,FALLBACK_ENABLE, MSGHEADER_LENHEX,SSL_ENABLE,TOPUP_ENABLE,MANUAL_ENTRY,AUTO_SETTLEMENT,EMV_LOGS,AUTO_SETTLETIME,AMEX_ID,PIN_BYPASS,MOD10,CHECK_EXPIRY,MANUAL_ENTRY_ISSUER,LAST_4DIGIT_MAG,LAST_4DIGIT_ICC,ACQUIRER,LOW_BIN,HIGH_BIN,ISSUER,CURRENCY,USER_PASSWORD_CHANGE,MANAGER_PASSWORD_CHANGE,SYSTEM_PASSWORD_CHANGE,SUPER_USER_PASSWORD_CHANGE,HEADER_LINE_1,HEADER_LINE_2,HEADER_LINE_3,HEADER_LINE_4,FOOTER_LINE_1,FOOTER_LINE_2, PRI_MEDIUM, SEC_MEDIUM, TMS_IP, TMS_PORT, BATCH_NO ,hostIP, String.valueOf(hostPort), SEC_IP,  String.valueOf(SEC_PORT),"000000030000");
                Repository repository = new Repository(mContext.get());
                TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
                repository.updateTerminalConfiguration(TerminalConfig);
                sweetAlertDialog.dismissWithAnimation();
                DialogUtil.successDialog(mContext.get(), title + " Changed!");
            }
        }, KeyboardType,null,keyboardLength);

    }


//    public List<TransactionDetail> getAllBatchTransactions() throws ExecutionException, InterruptedException {
//        return new GetTransactionsAsyncTaskAllTransactions().execute().get();
//    }
//
//    @SuppressLint("StaticFieldLeak")
//    private class GetTransactionsAsyncTaskAllTransactions extends AsyncTask<Void, Void, List<TransactionDetail>> {
//        @Override
//        protected List<TransactionDetail> doInBackground(Void... voids) {
//            return appDatabase.transactionDetailDao().getAllTransactions();
//        }
//    }
}
