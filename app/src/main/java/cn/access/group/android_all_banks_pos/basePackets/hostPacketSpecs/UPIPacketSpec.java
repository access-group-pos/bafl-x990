package cn.access.group.android_all_banks_pos.basePackets.hostPacketSpecs;

import android.util.Log;
import android.util.SparseArray;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import java.util.Locale;

import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;
import cn.access.group.android_all_banks_pos.repository.model.Reversal;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

import cn.access.group.android_all_banks_pos.Utilities.Constants;

import cn.access.group.android_all_banks_pos.Utilities.Constants.*;

import static android.content.ContentValues.TAG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.BATCH_NO;


/**
 * on 11/23/2019.
 */
public class UPIPacketSpec {
    private SparseArray<String> data8583_Packet = new SparseArray<>();
    //Constants cn = new Constants();
    // private String host;
    public SparseArray<String> generateSalePacket(){
        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.SALE_MESSAGE_TYPE);
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.SALE_PROCESSING_CODE);
        data8583_Packet.put(ISO8583u.F_STAN_11,  DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);

        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        String dateFromResponse = DataFormatterUtil.systemDate();
        String timeFromResponse = DataFormatterUtil.systemTime();
        data8583_Packet.put(ISO8583u.F_TransmissionDateAndTime_07, dateFromResponse+timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxTime_12, timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxDate_13, dateFromResponse);
        data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE);
        data8583_Packet.put(ISO8583u.F_AcquiringInstitution_19, Constants.ACQUIRING_INSTITUTION); //022
        data8583_Packet.put(ISO8583u.F_AcquiringInstitutionIdentificationCode_32, Constants.ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
        data8583_Packet.put(ISO8583u.F_ForwardingInstitutionIdentificationCode_33, Constants.FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
        String rnn = DataFormatterUtil.systemYear() + DataFormatterUtil.systemDateAndTime();
        data8583_Packet.put(ISO8583u.F_RRN_37, rnn);
        data8583_Packet.put(ISO8583u.F_CardAcceptorNameLocation_43, Constants.CARD_ACCEPTOR_NAME_LOCATION);
        data8583_Packet.put(ISO8583u.F_TransactionCurrencyCode_49 , Constants.TRANSACTION_CURRENCY_CODE );
        data8583_Packet.put(ISO8583u.F_ReservedNational_60 , "00000600030000000000001" );
        return data8583_Packet;
    }

    public SparseArray<String> generateRefundPacket(){
        data8583_Packet.put(ISO8583u.F_MessageType_00, "0200");
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, "200000");
        data8583_Packet.put(ISO8583u.F_STAN_11,  DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);

        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        String dateFromResponse = DataFormatterUtil.systemDate();
        String timeFromResponse = DataFormatterUtil.systemTime();
        data8583_Packet.put(ISO8583u.F_TransmissionDateAndTime_07, dateFromResponse+timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxTime_12, timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxDate_13, dateFromResponse);
        data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE);
        data8583_Packet.put(ISO8583u.F_AcquiringInstitution_19, Constants.ACQUIRING_INSTITUTION); //022
        data8583_Packet.put(ISO8583u.F_AcquiringInstitutionIdentificationCode_32, Constants.ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
        data8583_Packet.put(ISO8583u.F_ForwardingInstitutionIdentificationCode_33, Constants.FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
        String rnn = DataFormatterUtil.systemYear() + DataFormatterUtil.systemDateAndTime();
        data8583_Packet.put(ISO8583u.F_RRN_37, rnn);
        data8583_Packet.put(ISO8583u.F_CardAcceptorNameLocation_43, Constants.CARD_ACCEPTOR_NAME_LOCATION);
        data8583_Packet.put(ISO8583u.F_TransactionCurrencyCode_49 , Constants.TRANSACTION_CURRENCY_CODE );
        data8583_Packet.put(ISO8583u.F_ReservedNational_60 , "00000600030000000000001" );
        return data8583_Packet;
    }

    public SparseArray<String> generateSaleIPPPacket(){
        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.SALE_MESSAGE_TYPE);
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.SALEIPP_PROCESSING_CODE);
        data8583_Packet.put(ISO8583u.F_STAN_11,  DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);

        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        String dateFromResponse = DataFormatterUtil.systemDate();
        String timeFromResponse = DataFormatterUtil.systemTime();
        data8583_Packet.put(ISO8583u.F_TransmissionDateAndTime_07, dateFromResponse+timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxTime_12, timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxDate_13, dateFromResponse);
        data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE);
        data8583_Packet.put(ISO8583u.F_AcquiringInstitution_19, Constants.ACQUIRING_INSTITUTION); //022
        data8583_Packet.put(ISO8583u.F_AcquiringInstitutionIdentificationCode_32, Constants.ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
        data8583_Packet.put(ISO8583u.F_ForwardingInstitutionIdentificationCode_33, Constants.FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
        String rnn = DataFormatterUtil.systemYear() + DataFormatterUtil.systemDateAndTime();
        data8583_Packet.put(ISO8583u.F_RRN_37, rnn);
        data8583_Packet.put(ISO8583u.F_CardAcceptorNameLocation_43, Constants.CARD_ACCEPTOR_NAME_LOCATION);
        data8583_Packet.put(ISO8583u.F_TransactionCurrencyCode_49 , Constants.TRANSACTION_CURRENCY_CODE );
        data8583_Packet.put(ISO8583u.F_ReservedNational_60 , "00000600030000000000001" );
        return data8583_Packet;
    }

    public SparseArray<String> generateCompletionPacket(){
        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.SALE_MESSAGE_TYPE);
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.SALE_PROCESSING_CODE);
        data8583_Packet.put(ISO8583u.F_STAN_11,  DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);

        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        String dateFromResponse = DataFormatterUtil.systemDate();
        String timeFromResponse = DataFormatterUtil.systemTime();
        data8583_Packet.put(ISO8583u.F_TransmissionDateAndTime_07, dateFromResponse+timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxTime_12, timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxDate_13, dateFromResponse);
        data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE);
        data8583_Packet.put(ISO8583u.F_AcquiringInstitution_19, Constants.ACQUIRING_INSTITUTION); //022
        data8583_Packet.put(ISO8583u.F_AcquiringInstitutionIdentificationCode_32, Constants.ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
        data8583_Packet.put(ISO8583u.F_ForwardingInstitutionIdentificationCode_33, Constants.FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
        String rnn = DataFormatterUtil.systemYear() + DataFormatterUtil.systemDateAndTime();
        data8583_Packet.put(ISO8583u.F_RRN_37, rnn);
        data8583_Packet.put(ISO8583u.F_CardAcceptorNameLocation_43, Constants.CARD_ACCEPTOR_NAME_LOCATION);
        data8583_Packet.put(ISO8583u.F_TransactionCurrencyCode_49 , Constants.TRANSACTION_CURRENCY_CODE );
        data8583_Packet.put(ISO8583u.F_ReservedNational_60 , "00000600030000000000001" );
        return data8583_Packet;
    }



    public SparseArray<String> generateCashOutPacket(){
        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.CASH_OUT_MESSAGE_TYPE);
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.CASH_OUT_PROCESSING_CODE);
        data8583_Packet.put(ISO8583u.F_STAN_11,  DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);

        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        String dateFromResponse = DataFormatterUtil.systemDate();
        String timeFromResponse = DataFormatterUtil.systemTime();
        data8583_Packet.put(ISO8583u.F_TransmissionDateAndTime_07, dateFromResponse+timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxTime_12, timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxDate_13, dateFromResponse);
        data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE);
        data8583_Packet.put(ISO8583u.F_AcquiringInstitution_19, Constants.ACQUIRING_INSTITUTION); //022
        data8583_Packet.put(ISO8583u.F_AcquiringInstitutionIdentificationCode_32, Constants.ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
        data8583_Packet.put(ISO8583u.F_ForwardingInstitutionIdentificationCode_33, Constants.FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
        String rnn = DataFormatterUtil.systemYear() + DataFormatterUtil.systemDateAndTime();
        data8583_Packet.put(ISO8583u.F_RRN_37, rnn);
        data8583_Packet.put(ISO8583u.F_CardAcceptorNameLocation_43, Constants.CARD_ACCEPTOR_NAME_LOCATION);
        data8583_Packet.put(ISO8583u.F_TransactionCurrencyCode_49 , Constants.TRANSACTION_CURRENCY_CODE );
        data8583_Packet.put(ISO8583u.F_ServiceCode_61,Constants.FIELD_61);
        data8583_Packet.put(ISO8583u.F_ReservedNational_60 , "00000600030000000000001" );
        return data8583_Packet;
    }
    public SparseArray<String> generateCashAdvPacket(){
        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.CASH_ADV_MESSAGE_TYPE);
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.CASH_ADV_PROCESSING_CODE);
        data8583_Packet.put(ISO8583u.F_STAN_11,  DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);

        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        String dateFromResponse = DataFormatterUtil.systemDate();
        String timeFromResponse = DataFormatterUtil.systemTime();
        data8583_Packet.put(ISO8583u.F_TransmissionDateAndTime_07, dateFromResponse+timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxTime_12, timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxDate_13, dateFromResponse);
        data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE);
        data8583_Packet.put(ISO8583u.F_AcquiringInstitution_19, Constants.ACQUIRING_INSTITUTION); //022
        data8583_Packet.put(ISO8583u.F_AcquiringInstitutionIdentificationCode_32, Constants.ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
        data8583_Packet.put(ISO8583u.F_ForwardingInstitutionIdentificationCode_33, Constants.FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
        String rnn = DataFormatterUtil.systemYear() + DataFormatterUtil.systemDateAndTime();
        data8583_Packet.put(ISO8583u.F_RRN_37, rnn);
        data8583_Packet.put(ISO8583u.F_CardAcceptorNameLocation_43, Constants.CARD_ACCEPTOR_NAME_LOCATION);
        data8583_Packet.put(ISO8583u.F_TransactionCurrencyCode_49 , Constants.TRANSACTION_CURRENCY_CODE );
        data8583_Packet.put(ISO8583u.F_ReservedNational_60 , "00000600030000000000001" );
        return data8583_Packet;
    }
    public SparseArray<String> generatePreAuthPacket(){
        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.PREAUTH_MESSAGE_TYPE);
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.PRE_AUTH_PROCESSING_CODE);
        data8583_Packet.put(ISO8583u.F_STAN_11,  DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);

        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        String dateFromResponse = DataFormatterUtil.systemDate();
        String timeFromResponse = DataFormatterUtil.systemTime();
        data8583_Packet.put(ISO8583u.F_TransmissionDateAndTime_07, dateFromResponse+timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxTime_12, timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxDate_13, dateFromResponse);
        data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE);
        data8583_Packet.put(ISO8583u.F_AcquiringInstitution_19, Constants.ACQUIRING_INSTITUTION); //022
        data8583_Packet.put(ISO8583u.F_AcquiringInstitutionIdentificationCode_32, Constants.ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
        data8583_Packet.put(ISO8583u.F_ForwardingInstitutionIdentificationCode_33, Constants.FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
        String rnn = DataFormatterUtil.systemYear() + DataFormatterUtil.systemDateAndTime();
        data8583_Packet.put(ISO8583u.F_RRN_37, rnn);
        data8583_Packet.put(ISO8583u.F_CardAcceptorNameLocation_43, Constants.CARD_ACCEPTOR_NAME_LOCATION);
        data8583_Packet.put(ISO8583u.F_TransactionCurrencyCode_49 , Constants.TRANSACTION_CURRENCY_CODE );
        data8583_Packet.put(ISO8583u.F_ReservedNational_60 , "00000600030000000000001" );
        data8583_Packet.put(ISO8583u.F_ServiceCode_61 ,Constants.FIELD_61 );
        return data8583_Packet;
    }
    public SparseArray<String> generateAuthPacket(){
        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.PREAUTH_MESSAGE_TYPE);
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, "000000");
        data8583_Packet.put(ISO8583u.F_STAN_11,  DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);

        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );
        String dateFromResponse = DataFormatterUtil.systemDate();
        String timeFromResponse = DataFormatterUtil.systemTime();
        data8583_Packet.put(ISO8583u.F_TransmissionDateAndTime_07, dateFromResponse+timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxTime_12, timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxDate_13, dateFromResponse);
        data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE);
        data8583_Packet.put(ISO8583u.F_AcquiringInstitution_19, Constants.ACQUIRING_INSTITUTION); //022
        data8583_Packet.put(ISO8583u.F_AcquiringInstitutionIdentificationCode_32, Constants.ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
        data8583_Packet.put(ISO8583u.F_ForwardingInstitutionIdentificationCode_33, Constants.FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
        String rnn = DataFormatterUtil.systemYear() + DataFormatterUtil.systemDateAndTime();
        data8583_Packet.put(ISO8583u.F_RRN_37, rnn);
        data8583_Packet.put(ISO8583u.F_CardAcceptorNameLocation_43, Constants.CARD_ACCEPTOR_NAME_LOCATION);
        data8583_Packet.put(ISO8583u.F_TransactionCurrencyCode_49 , Constants.TRANSACTION_CURRENCY_CODE );
        data8583_Packet.put(ISO8583u.F_ReservedNational_60 , "00000600030000000000001" );
        data8583_Packet.put(ISO8583u.F_ServiceCode_61 ,Constants.FIELD_61 );
        return data8583_Packet;
    }
    public SparseArray<String> generateOrbitRedeemPacket(){
        String procCode = "000070";
        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.ORBIT_REDEEM_MESSAGE_TYPE);
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, procCode);
        data8583_Packet.put(ISO8583u.F_STAN_11,  DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
         data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );//String.format( Locale.getDefault(),"%06d",SharedPref.read("batch_no")));
        String dateFromResponse = DataFormatterUtil.systemDate();
        String timeFromResponse = DataFormatterUtil.systemTime();
        data8583_Packet.put(ISO8583u.F_TransmissionDateAndTime_07, dateFromResponse+timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxTime_12, timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxDate_13, dateFromResponse);
        data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE);
        data8583_Packet.put(ISO8583u.F_AcquiringInstitution_19, Constants.ACQUIRING_INSTITUTION); //022
        data8583_Packet.put(ISO8583u.F_AcquiringInstitutionIdentificationCode_32, Constants.ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
        data8583_Packet.put(ISO8583u.F_ForwardingInstitutionIdentificationCode_33, Constants.FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
        String rnn = DataFormatterUtil.systemYear() + DataFormatterUtil.systemDateAndTime();
        data8583_Packet.put(ISO8583u.F_RRN_37, rnn);
        data8583_Packet.put(ISO8583u.F_CardAcceptorNameLocation_43, Constants.CARD_ACCEPTOR_NAME_LOCATION);
        data8583_Packet.put(ISO8583u.F_TransactionCurrencyCode_49 , Constants.TRANSACTION_CURRENCY_CODE );
        data8583_Packet.put(ISO8583u.F_ReservedNational_60 , "00000600030000000000001" );
        data8583_Packet.put(ISO8583u.F_ServiceCode_61 ,Constants.FIELD_61 );
        return data8583_Packet;
    }
    public SparseArray<String> generateOrbitInquiryPacket(){
        String processCode = "310070";
        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.SALE_MESSAGE_TYPE);
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, processCode);
        data8583_Packet.put(ISO8583u.F_STAN_11,  DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);   // 02 mag, 05 smart, 07 ctls; 1 pin
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
         data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );//String.format( Locale.getDefault(),"%06d",SharedPref.read("batch_no")));
        String dateFromResponse = DataFormatterUtil.systemDate();
        String timeFromResponse = DataFormatterUtil.systemTime();
        data8583_Packet.put(ISO8583u.F_TransmissionDateAndTime_07, dateFromResponse+timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxTime_12, timeFromResponse);
        data8583_Packet.put(ISO8583u.F_TxDate_13, dateFromResponse);
        data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE);
        data8583_Packet.put(ISO8583u.F_AcquiringInstitution_19, Constants.ACQUIRING_INSTITUTION); //022
        data8583_Packet.put(ISO8583u.F_AcquiringInstitutionIdentificationCode_32, Constants.ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
        data8583_Packet.put(ISO8583u.F_ForwardingInstitutionIdentificationCode_33, Constants.FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
        String rnn = DataFormatterUtil.systemYear() + DataFormatterUtil.systemDateAndTime();
        data8583_Packet.put(ISO8583u.F_RRN_37, rnn);
        data8583_Packet.put(ISO8583u.F_CardAcceptorNameLocation_43, Constants.CARD_ACCEPTOR_NAME_LOCATION);
        data8583_Packet.put(ISO8583u.F_TransactionCurrencyCode_49 , Constants.TRANSACTION_CURRENCY_CODE );
        data8583_Packet.put(ISO8583u.F_ReservedNational_60 , "00000600030000000000001" );
        data8583_Packet.put(ISO8583u.F_ServiceCode_61 ,Constants.FIELD_61 );
        return data8583_Packet;
    }
    public SparseArray<String> generateUpiVoidPacket(TransactionDetail transactionToVoid){
        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.SALE_MESSAGE_TYPE);// todo from sale to revsersal
        data8583_Packet.put(ISO8583u.F_AccountNumber_02,transactionToVoid.getCardNo());
        data8583_Packet.put(ISO8583u.F_AmountOfTransactions_04,String.format(Locale.getDefault(),"%012d", (int)(Double.parseDouble(transactionToVoid.getAmount())*100)));
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, Constants.VOID_PROCESSING_CODE);
        data8583_Packet.put(ISO8583u.F_TransmissionDateAndTime_07, DataFormatterUtil.systemDateAndTime());
        data8583_Packet.put(ISO8583u.F_STAN_11, transactionToVoid.getStan());
        data8583_Packet.put(ISO8583u.F_TxTime_12, DataFormatterUtil.systemTime());
        data8583_Packet.put(ISO8583u.F_TxDate_13, DataFormatterUtil.systemDate());
        if ( transactionToVoid.getCardexpiry() != null) {
            String exp = transactionToVoid.getCardexpiry();
            String month = exp.substring(0,2);
            String year = exp.substring(3,5);
            Utility.DEBUG_LOG(TAG,"void exp month:"+ month);
            Utility.DEBUG_LOG(TAG,"void exp year:"+ year);
            Utility.DEBUG_LOG(TAG,"void exp:" + year + "" + month);
            String expiry = year + month;
            data8583_Packet.put(ISO8583u.F_DateOfExpired_14, expiry);
        }
        switch (transactionToVoid.getCardType()) {
            case "magnetic":
                data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE);
                data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);
                break;
            case "chip":
                data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE_CHIP);
                data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE_CHIP);
                data8583_Packet.put(ISO8583u.F_ApplicationPANSequenceNumber_23, "001");
                data8583_Packet.put(ISO8583u.F_AuthorizationIdentificationResponseCode_38, transactionToVoid.getAuthorizationIdentificationResponseCode());

                break;
            case "ctls":
                data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE_CTLS);
                data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE_CTLS);   // 02 mag, 05 smart, 07 ctls; 1 pin

                data8583_Packet.put(ISO8583u.F_ApplicationPANSequenceNumber_23, "001");

                //data8583_Packet.put(ISO8583u.F_PointOfServiceCaptureCode_26, Constants.POS_CONDITION_CODE_CTLS);
                //data8583_Packet.put(ISO8583u.F_AuthorizationIdentificationResponseCode_38,transactionToVoid.getAuthorizationIdentificationResponseCode());

                break;
        }


        data8583_Packet.put(ISO8583u.F_AcquiringInstitution_19, Constants.ACQUIRING_INSTITUTION);
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        data8583_Packet.put(ISO8583u.F_AcquiringInstitutionIdentificationCode_32, Constants.ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
        data8583_Packet.put(ISO8583u.F_ForwardingInstitutionIdentificationCode_33, Constants.FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
        data8583_Packet.put(ISO8583u.F_Track_2_Data_35,transactionToVoid.getTrack2()); ///?????
        data8583_Packet.put(ISO8583u.F_RRN_37,transactionToVoid.getRrn());
        data8583_Packet.put(ISO8583u.F_TID_41, Constants.TERMINAL_ID);
        data8583_Packet.put(ISO8583u.F_MerchantID_42, Constants.MERCHANT_ID);
        data8583_Packet.put(ISO8583u.F_CardAcceptorNameLocation_43, Constants.CARD_ACCEPTOR_NAME_LOCATION);
        data8583_Packet.put( ISO8583u.F_TransactionCurrencyCode_49 , Constants.TRANSACTION_CURRENCY_CODE );
        if (Constants.TIP_ENABLED.equals("Y") && (transactionToVoid.getTxnType().equals("SALE") ||transactionToVoid.getTxnType().equals("ADJUST"))){
            data8583_Packet.put(ISO8583u.F_BalancAmount_54, String.format(Locale.getDefault(),"%012d", (int)(Double.parseDouble(transactionToVoid.getTipAmount())*100)));
        }

        data8583_Packet.put( ISO8583u.F_ReservedNational_60 , Constants.RESERVED_NATIONAL );
        data8583_Packet.put( ISO8583u.F_ServiceCode_61 ,"01");
         data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO );// String.format( Locale.getDefault(),"%06d",SharedPref.read("batch_no")));

       // Utility.DEBUG_LOG(TAG,transactionToVoid.toString());
        String Field90 = DataFormatterUtil.generateField90(transactionToVoid);
        data8583_Packet.put( ISO8583u.F_OriginalDataElements_90 , Field90 );
        //0200 MESSAGE TYPE 00
        // 000016  STAN  11
        // 1005185331  DATE AND TIME 07
        // 00035370586  32
        // 00047740586  33 ??
        return  data8583_Packet;
    }


    public  SparseArray<String> generateSettlementPacket(String processingCode,String field63){
        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.SETTLEMENT_MESSAGE_TYPE);
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, processingCode);
        data8583_Packet.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII); //maybe 333
        data8583_Packet.put(ISO8583u.F_TID_41, Constants.TERMINAL_ID);
        data8583_Packet.put(ISO8583u.F_MerchantID_42, Constants.MERCHANT_ID);
        data8583_Packet.put( ISO8583u.F_ReservedNational_60 , Constants.RESERVED_NATIONAL );
        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO);//String.format( Locale.getDefault(),"%06d",SharedPref.read("batch_no")));
        data8583_Packet.put( ISO8583u.F_SettlementData_63 , field63 );
        return  data8583_Packet;
    }

    public SparseArray<String> generateReversalPacket( Reversal transactionToReverse){
        data8583_Packet.put(ISO8583u.F_MessageType_00, Constants.REVERSAL_MESSAGE_TYPE);
        data8583_Packet.put(ISO8583u.F_AccountNumber_02,transactionToReverse.getCardNo());
        data8583_Packet.put(ISO8583u.F_AmountOfTransactions_04,transactionToReverse.getAmount());
        data8583_Packet.put(ISO8583u.F_ProcessingCode_03, transactionToReverse.getProcessingCode());
        data8583_Packet.put(ISO8583u.F_TransmissionDateAndTime_07, transactionToReverse.getTxnDate()+transactionToReverse.getTxnTime());
        data8583_Packet.put(ISO8583u.F_STAN_11, transactionToReverse.getStan());
        data8583_Packet.put(ISO8583u.F_TxTime_12,transactionToReverse.getTxnTime());
        data8583_Packet.put(ISO8583u.F_TxDate_13, transactionToReverse.getTxnDate());
        switch (transactionToReverse.getCardType()) {
            case "magnetic":
                data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE);
                data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);
                data8583_Packet.put(ISO8583u.F_Track_2_Data_35, transactionToReverse.getTrack2());//....not in chip

                break;
            case "chip":
                data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE_CHIP);
                data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE_CHIP);
                data8583_Packet.put(ISO8583u.F_ApplicationPANSequenceNumber_23, "001");
                data8583_Packet.put(ISO8583u.F_PointOfServiceCaptureCode_26, Constants.POS_CONDITION_CODE_CHIP);
                data8583_Packet.put(ISO8583u.F_55, "present");
                break;
            case "ctls":
                data8583_Packet.put(ISO8583u.F_MerchantCategoryCode_18, Constants.MERCHANT_CATEGORY_CODE_CTLS);
                data8583_Packet.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE_CTLS);
                data8583_Packet.put(ISO8583u.F_PointOfServiceCaptureCode_26, Constants.POS_CONDITION_CODE_CTLS);
                data8583_Packet.put(ISO8583u.F_55, "present");
                break;
        }
        data8583_Packet.put(ISO8583u.F_AcquiringInstitution_19, Constants.ACQUIRING_INSTITUTION);
        data8583_Packet.put(ISO8583u.F_NII_24, Constants.NII);
        data8583_Packet.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
        data8583_Packet.put(ISO8583u.F_AcquiringInstitutionIdentificationCode_32, Constants.ACQUIRING_INSTITUTION_IDENTIFICATION_CODE);
        data8583_Packet.put(ISO8583u.F_ForwardingInstitutionIdentificationCode_33, Constants.FORWARDING_INSTITUTION_IDENTIFICATION_CODE);
        data8583_Packet.put(ISO8583u.F_RRN_37,transactionToReverse.getRrn());
        data8583_Packet.put(ISO8583u.F_TID_41, Constants.TERMINAL_ID);
        data8583_Packet.put(ISO8583u.F_MerchantID_42, Constants.MERCHANT_ID);
        data8583_Packet.put(ISO8583u.F_CardAcceptorNameLocation_43, Constants.CARD_ACCEPTOR_NAME_LOCATION);
        data8583_Packet.put( ISO8583u.F_TransactionCurrencyCode_49 , Constants.TRANSACTION_CURRENCY_CODE);
        data8583_Packet.put( ISO8583u.F_ReservedNational_60 , Constants.RESERVED_NATIONAL);
        data8583_Packet.put( ISO8583u.F_KeyExchange_62 , Constants.INVOICE_NO);//String.format( Locale.getDefault(),"%06d",SharedPref.read("batch_no")));
        String Field90 = DataFormatterUtil.generateFieldReversal90(transactionToReverse);
        data8583_Packet.put( ISO8583u.F_OriginalDataElements_90 , Field90 );
        //0200 MESSAGE TYPE 00
        // 000016  STAN  11
        // 1005185331  DATE AND TIME 07
        // 00035370586  32
        // 00047740586  33 ??
        return data8583_Packet;
    }



}
