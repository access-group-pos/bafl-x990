
package cn.access.group.android_all_banks_pos.viewfragments;



        import android.annotation.SuppressLint;
        import android.app.Activity;
        import android.app.Dialog;
        import android.content.ComponentName;
        import android.content.Context;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.content.ServiceConnection;
        import android.content.res.AssetManager;
        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.os.CountDownTimer;
        import android.os.Handler;
        import android.os.IBinder;
        import android.os.Looper;
        import android.os.Message;
        import android.os.RemoteException;
        import android.support.annotation.NonNull;
        import android.support.annotation.Nullable;
        import android.support.constraint.ConstraintLayout;
        import android.support.v4.app.Fragment;
        import android.support.v7.app.AlertDialog;
        import android.text.Editable;
        import android.text.InputType;
        import android.text.TextWatcher;
        import android.util.Log;
        import android.view.KeyEvent;
        import android.view.LayoutInflater;

        import android.view.View;
        import android.view.ViewGroup;
        import android.view.animation.Animation;
        import android.view.animation.AnimationUtils;
        import android.view.inputmethod.EditorInfo;
        import android.view.inputmethod.InputMethodManager;
        import android.widget.AdapterView;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.ListView;
        import android.widget.TableLayout;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.vfi.smartpos.deviceservice.aidl.IBeeper;
        import com.vfi.smartpos.deviceservice.aidl.IDeviceInfo;
        import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
        import com.vfi.smartpos.deviceservice.aidl.IEMV;
        import com.vfi.smartpos.deviceservice.aidl.IPinpad;
        import com.vfi.smartpos.deviceservice.aidl.IPrinter;
        import com.vfi.smartpos.deviceservice.aidl.IRFCardReader;
        import com.vfi.smartpos.deviceservice.aidl.ISerialPort;

        import java.lang.ref.WeakReference;
        import java.text.DecimalFormat;
        import java.util.List;
        import java.util.Locale;
        import java.util.Objects;
        import java.util.concurrent.Delayed;
        import java.util.concurrent.ExecutionException;

        import butterknife.BindView;
        import butterknife.ButterKnife;
        import butterknife.OnClick;
        import butterknife.Unbinder;
        import cn.access.group.android_all_banks_pos.DashboardContainer;
        import cn.access.group.android_all_banks_pos.R;
        import cn.access.group.android_all_banks_pos.Utilities.Constants;
        import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
        import cn.access.group.android_all_banks_pos.Utilities.DecimalDigitsInputFilter;
        import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
        import cn.access.group.android_all_banks_pos.Utilities.ECR;
        import cn.access.group.android_all_banks_pos.Utilities.FourDigitCardFormatWatcher;
        import cn.access.group.android_all_banks_pos.Utilities.NumberTextWatcherForThousand;
        import cn.access.group.android_all_banks_pos.Utilities.PostRequest;
        import cn.access.group.android_all_banks_pos.Utilities.ReceiptPrinter;
        import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
        import cn.access.group.android_all_banks_pos.Utilities.TransPrinter;
        import cn.access.group.android_all_banks_pos.Utilities.Utility;
        import cn.access.group.android_all_banks_pos.Utilities.Validation;
        import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrintRecpSale;
        import cn.access.group.android_all_banks_pos.adapter.aidListAdapter;
        import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
        import cn.access.group.android_all_banks_pos.contracts.PaymentContract;
        import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
        import cn.access.group.android_all_banks_pos.presenter.PaymentAmountPresenter;
        import cn.access.group.android_all_banks_pos.repository.Repository;
        import cn.access.group.android_all_banks_pos.repository.dao.TransactionDetailDao;
        import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
        import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;
        import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
        import cn.pedant.SweetAlert.SweetAlertDialog;
        import io.reactivex.Completable;
        import io.reactivex.android.schedulers.AndroidSchedulers;
        import io.reactivex.schedulers.Schedulers;

/**
 * @author muhammad.humayun
 * on 8/20/2019.
 * this is a view call which is reponsible for starting the hardware service and invoking the presenter of this view
 */
@SuppressLint("ValidFragment")
public class TipAdjustFragment extends Fragment {

    @BindView(R.id.headingAdjustTip)
    TextView headingAdjustTip;

    @BindView(R.id.textLayoutAdjustTip)
    LinearLayout textLayoutAdjustTip;

    @BindView(R.id.textScreenAdjustTip)
    TextView textScreenAdjustTip;

    @BindView(R.id.amountFieldAdjustTip)
    TextView amountFieldAdjustTip;

    @BindView(R.id.keyboard_layout_Adjust_tip)
    LinearLayout keyboard_layout_Adjust_tip;

    @BindView(R.id.tipAdjustList)
    TableLayout tipAdjustList;

    @BindView(R.id.tipAdjustCardNo)
    TextView tipAdjustCardNo;

    @BindView(R.id.tipAdjustInvoiceNo)
    TextView tipAdjustInvoiceNo;

    @BindView(R.id.tipAdjustTxnType)
    TextView tipAdjustTxnType;

    @BindView(R.id.tipAdjustAmount)
    TextView tipAdjustAmount;

    @BindView(R.id.tipAdjustSaleAmount)
    TextView tipAdjustSaleAmount;

    @BindView(R.id.tipAdjustTipAmount)
    TextView tipAdjustTipAmount;


    @BindView(R.id.tipAdjustAuthID)
    TextView tipAdjustAuthID;

    @BindView(R.id.tipAdjustButton)
    Button tipAdjustButton;

    @BindView(R.id.tipAdjustCancelButton)
    Button tipAdjustCancelButton;




    String transactionAmount;
    TransactionDetail td;
    Unbinder unbinder;
    IBeeper iBeeper;
    IPrinter printer;
    private ReceiptPrinter receiptPrinter;
    public static IDeviceService idevice;
    Utility utility;
    boolean isSucc;
    AssetManager assetManager;
    ISerialPort serialPort;
    private static final String TAG = "EMVDemo";

    Intent intent = new Intent();

    Dialog alertDialog;


    String buttonClicked;
    public String current = "";
    int count = 0;
    public boolean bool;

    MyKeyListener myKeyListener;
    private WeakReference<Context> mContext;
    public boolean isFallback;
    String tipAmount = "";
    String invoice = "";
    TransPrinter transPrinter;
    PostRequest postRequest =  new PostRequest();
//    Repository repository;

    @SuppressLint("ValidFragment")
    public TipAdjustFragment(TransactionDetail td , String invoice) {
        this.invoice = invoice;
        this.td = td;
        receiptPrinter = new ReceiptPrinter(handler);
    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Utility.DEBUG_LOG(TAG,"+ FragmentTipAdjust:onCreateView() +");
        //Repository repo = new Repository(AppDatabase.getAppDatabase(Context));
        View rootView = inflater.inflate(R.layout.fragment_tip_adjust, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        tipAdjustCardNo.setText(DataFormatterUtil.maskCardNo(td.getCardNo()));
        tipAdjustInvoiceNo.setText(td.getInvoiceNo());
        tipAdjustTxnType.setText(td.getTxnType());
        tipAdjustAuthID.setText(td.getAuthorizationIdentificationResponseCode());
        tipAdjustAmount.setText((new DecimalFormat("0.00").format(Double.parseDouble(td.getAmount()))));


        if(Constants.TIP_ENABLED.equals("Y")){
            tipAdjustTipAmount.setText(td.getTipAmount());
            tipAdjustSaleAmount.setText((new DecimalFormat("0.00").format(Double.parseDouble(td.getTransactionAmount()))));
        }



        //keyboard_layout_Adjust_tip.setVisibility(View.VISIBLE);
        myKeyListener = new MyKeyListener(amountFieldAdjustTip);
        rootView.findViewById(R.id.key1).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key2).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key3).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key4).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key5).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key6).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key7).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key8).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key9).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key0).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.keyclr).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key_delete).setOnClickListener(myKeyListener);
        rootView.findViewById(R.id.key_confirm).setOnClickListener(myKeyListener);
        return rootView;
    }


    @OnClick({R.id.tipAdjustButton,R.id.tipAdjustCancelButton})
    @Nullable

    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tipAdjustButton:
                tipAdjustList.setVisibility(View.GONE);
                tipAdjustCancelButton.setVisibility(View.GONE);
                tipAdjustButton.setVisibility(View.GONE);
                keyboard_layout_Adjust_tip.setVisibility(View.VISIBLE);
                textLayoutAdjustTip.setVisibility(View.VISIBLE);
                headingAdjustTip.setText("Enter Tip Amount");
                break;

            case R.id.tipAdjustCancelButton:
                DashboardContainer.backStack();
                break;
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        intent.setAction("com.vfi.smartpos.device_service");
        intent.setPackage("com.vfi.smartpos.deviceservice");
        isSucc = context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        assetManager = getResources().getAssets();
        alertDialog=new Dialog(context);
        mContext = new WeakReference<>(context);
        Utility.DEBUG_LOG(TAG,"PaymentAmountFragment:onAttach()");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (getActivity() != null)
            getActivity().unbindService(conn);
    }


    // connect service -- start
    /**
     * \Brief connect the VFI-Service and set some devices
     */
    private final ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            new CountDownTimer(01, 01) {
                public void onFinish() {
                    idevice = IDeviceService.Stub.asInterface(service);
                    try {
                        Utility.DEBUG_LOG("onServiceConnected","onServiceConnected--Control here..");
                        Utility.DEBUG_LOG("terminal serial", Constants.TERMINAL_SERIAL );
                        iBeeper = idevice.getBeeper();
                        printer = idevice.getPrinter();
                        TransPrinter.initialize(printer);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                }

                public void onTick(long millisUntilFinished) {
                    // millisUntilFinished    The amount of time until finished.
                }
            }.start();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    /**
     * this method animates the view, and executes the purchase function of the presenter which takes amount as parameter
     */

    // mmasood: countdown timer in sales screen


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        buttonClicked = "";
        // conn = null;
        unbinder.unbind();

    }



    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            Utility.DEBUG_LOG(TAG,"+ PaymentFragment:Handler:handleMessage()+ ");
            Utility.DEBUG_LOG(TAG,"before getData");

            String str = msg.getData().toString();
            Utility.DEBUG_LOG(TAG,"Message:" + str);
            super.handleMessage(msg);
//            Toast.makeText(getContext(), msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();
        }
    };

    class MyKeyListener implements View.OnClickListener{
        StringBuilder stringBuilder = new StringBuilder("");
        TextView selectedAmount;

        public MyKeyListener(TextView selectedAmount) {
            this.selectedAmount = selectedAmount;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.key0:
                    if (stringBuilder.length() >= 0 & stringBuilder.length() < 6)
                    {
                        stringBuilder.append("0");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key1:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("1");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key2:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("2");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key3:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("3");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key4:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("4");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key5:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("5");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key6:
                    if (stringBuilder.length() < 6){
                        stringBuilder.append("6");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key7:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("7");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key8:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("8");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key9:
                    if (stringBuilder.length() < 6) {
                        stringBuilder.append("9");
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.keyclr:
                    if (stringBuilder.length() > 0) {
                        stringBuilder.delete(0,stringBuilder.length());
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key_delete:
                    if (stringBuilder.length() > 0) {
                        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                        try {
                           if(iBeeper!=null) {
                                iBeeper.startBeep(100);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case R.id.key_confirm:
                    DecimalFormat df= new DecimalFormat("0.00");
                    tipAmount = amountFieldAdjustTip.getText().toString();
                    double tipamount = Double.parseDouble(tipAmount);
                    double txnAmount = Double.parseDouble(td.getTransactionAmount());

                    Double thresholdAmount = 0.00;
                    Double total = 0.00;
                    total = Double.valueOf(new DecimalFormat("0.00").format(txnAmount + tipamount));

                    Utility.DEBUG_LOG(TAG, "transactionamount Float:" + txnAmount);
                    Utility.DEBUG_LOG(TAG, "tipamount Float:" + tipamount);
                    //Utility.DEBUG_LOG(TAG, "percentage Float:" + new DecimalFormat("0.00").format((2.5 * txnAmount)) / 100.00));
                    thresholdAmount = Double.valueOf(new DecimalFormat("0.00").format((Double.parseDouble(Constants.TIP_THRESHOLD) * txnAmount) / 100.00));
                    Utility.DEBUG_LOG(TAG, "thresholdAmount Float:" + new DecimalFormat("0.00").format((Double.parseDouble(Constants.TIP_THRESHOLD) * txnAmount) / 100.00));

                    //Utility.DEBUG_LOG(TAG,"percentage amount Float:" +new DecimalFormat("0.00").format(2.5 * Double.parseDouble(transactionAmount)));
                    if (Double.parseDouble(tipAmount) > thresholdAmount) {
                        Utility.DEBUG_LOG(TAG, "we are in if condition Float:" + txnAmount);
                        // Toast.makeText(mContext,"Tip amount is too high",Toast.LENGTH_SHORT).show();
                        DialogUtil.errorDialog(mContext.get(), "Tip amount is too high", "Please re-enter!");
                        DashboardContainer.backStack();
                    } else {
                        utility = new Utility();
                      //  repository =  new Repository(appDatabase);

                        try {
                            if(Utility.checkAdjust(mContext.get(),td)) {
                                int adjustCounter = td.getTipNumAdjustCounter();
                                adjustCounter++;
                                td.setTipNumAdjustCounter(adjustCounter);
                                Utility.DEBUG_LOG(TAG, "TIPNUMADJUSTCOUNTER:" + adjustCounter);
                                final AlertDialog.Builder alert = new AlertDialog.Builder(mContext.get());

                                LinearLayout lila1 = new LinearLayout(mContext.get());
                                lila1.setOrientation(1); //1 is for vertical orientation
                                lila1.setPadding(10, 10, 10, 10);
                                final TextView input = new TextView(mContext.get());
                                final TextView input1 = new TextView(mContext.get());
                                final TextView input2 = new TextView(mContext.get());

                                input.setPadding(0, 0, 0, 10);
                                input1.setPadding(0, 0, 0, 10);
                                input2.setPadding(0, 0, 0, 10);
                                input.setGravity(1);
                                input1.setGravity(1);
                                input2.setGravity(1);

                                input.setText("Amount: " + txnAmount);
                                input1.setText("Tip Amount: " + tipAmount);
                                input2.setText("Total AMount: " + String.valueOf(total));

                                lila1.addView(input);
                                lila1.addView(input1);
                                lila1.addView(input2);
                                alert.setView(lila1);

                                alert.setIcon(R.drawable.coin);
                                alert.setTitle("Total Amount");

                                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        DecimalFormat df = new DecimalFormat("0.00");
                                        tipAmount = amountFieldAdjustTip.getText().toString();
                                        double tipamount = Double.parseDouble(tipAmount);
                                        double txnAmount = Double.parseDouble(td.getTransactionAmount());
                                        Utility.DEBUG_LOG("adjustTipAmount", String.valueOf(tipamount));
                                        Utility.DEBUG_LOG("adjusttxnAmount", String.valueOf(txnAmount));
                                        double totalAmount = tipamount + txnAmount;
                                        Utility.DEBUG_LOG("adjusttotalAmount", String.valueOf(totalAmount));
                                        if(td.getTxnType().equals("COMPLETION")){
                                            td.setCancelTxnType("COMPLETION");
                                            td.setProcessingCode("000000");
                                        }else {
                                            td.setProcessingCode("020000");
                                        }
                                        //    td.setTransactionAmount(String.valueOf(txnAmount));
                                        td.setTipAmount(tipAmount);
                                        td.setAmount(String.valueOf(df.format(totalAmount)));
                                        td.setCancelTxnType(td.getTxnType());
                                        td.setTxnType("ADJUST");
                                        td.setIsAdvice(true);

                                        new Repository(mContext.get()).updateTransactionTip( tipAmount, invoice, td.getTxnType(), td.getAmount(), td.getIsAdvice(),td.getTipNumAdjustCounter(),td.getProcessingCode());
// SHIVAM UPDATE HERE FOR TEXT PRINTER
//                                        transPrinter = new PrintRecpSale(mContext);
//                                        transPrinter.initializeData(td, "", false, "MERCAHNT COPY");
//                                        transPrinter.print();

                                        receiptPrinter.printSaleReceipt(printer,assetManager,td,td.getAidCode(),"MERCHANT COPY",td.getTxnType(),"","",false);
// SHIVAM UPDATE HERE FOR TEXT PRINTER

                                        postRequest.makeJsonObjReq(mContext.get(), td);

                                        final Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                //receiptPrinter.printSaleReceipt(printer,assetManager,td,td.getAidCode(),"MERCAHNT COPY",td.getTxnType(),"","",false);
                                                Utility.DEBUG_LOG("transaction", "found");
                                                // SHIVAM UPDATE HERE FOR TEXT PRINTER
                                                DialogUtil.confirmDialogCustomer(mContext.get(), "CUSTOMER COPY", "Print Customer Copy", "", sweetAlertDialog1 -> {
//                                                    transPrinter = new PrintRecpSale(mContext);
//                                                    transPrinter.initializeData(td, "", false, "CUSTOMER COPY");
//                                                    transPrinter.print();

                                                    receiptPrinter.printSaleReceipt(printer, assetManager, td, td.getAidCode(), "CUSTOMER COPY", td.getTxnType(),"","",false);
// SHIVAM UPDATE HERE FOR TEXT PRINTER

                                                    // receiptPrinter.printSaleReceipt(printer, assetManager, td, td.getAidCode(), "Customer Copy", td.getTxnType(),"","",false);
                                                    sweetAlertDialog1.dismissWithAnimation();
                                                    DashboardContainer.backStack();                                                });
                                                Utility.DEBUG_LOG("Handler", "Running Handler");
                                            }
                                        }, 1000);
                                        // Toast.makeText(mContext.get(), , Toast.LENGTH_SHORT).show();
                                    }
                                });
                                alert.setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                dialog.cancel();
                                                DashboardContainer.backStack();
                                            }
                                        });

                                alert.show();
                            }
                            else{
                                DialogUtil.errorDialog(mContext.get(),"Error!","Adjust Limit Exceeded");
                                DashboardContainer.backStack();
                                Utility.DEBUG_LOG(TAG, "Adjust counter ");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    break;
            }
            if (stringBuilder.length() > 0) {
                double num = Double.parseDouble(stringBuilder.toString());
                selectedAmount.setText(big2(num / 100));
            } else {
                selectedAmount.setText("0.00");
            }
        }
        private String big2(double d) {
            DecimalFormat format = new DecimalFormat("0.00");
            return format.format(d);
        }
    }

    public void setTransactionAmount(String transactionAmount)
    {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionAmount()
    {
        return transactionAmount;
    }

    public boolean getIsFallback()
    {
        return isFallback;
    }

//    public List<TransactionDetail> getAllTransactions() throws ExecutionException, InterruptedException {
//        return new GetTransactionsAsyncTaskAdjust().execute().get();
//    }
//    @SuppressLint("StaticFieldLeak")
//    private class GetTransactionsAsyncTaskAdjust extends AsyncTask<Void, Void,List<TransactionDetail>>
//    {
//        @Override
//        protected List<TransactionDetail> doInBackground(Void... voids) {
//            return appDatabase.transactionDetailDao().getAllTransactions();
//        }
//    }





}


