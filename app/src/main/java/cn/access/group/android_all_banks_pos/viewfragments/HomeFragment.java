package cn.access.group.android_all_banks_pos.viewfragments;


import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.vfi.smartpos.deviceservice.aidl.IBeeper;
import com.vfi.smartpos.deviceservice.aidl.IDeviceInfo;
import com.vfi.smartpos.deviceservice.aidl.IDeviceService;
import com.vfi.smartpos.deviceservice.aidl.IEMV;
import com.vfi.smartpos.deviceservice.aidl.IPinpad;
import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.IRFCardReader;
import com.vfi.smartpos.deviceservice.aidl.ISerialPort;
import com.vfi.smartpos.deviceservice.aidl.IUsbSerialPort;
import com.vfi.smartpos.deviceservice.aidl.PrinterListener;
import com.vfi.smartpos.system_service.aidl.ISystemManager;
import com.vfi.smartpos.system_service.aidl.settings.ISettingsManager;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Auto_Settle;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.Utilities.ECR;
import cn.access.group.android_all_banks_pos.Utilities.ServiceHelper;
import cn.access.group.android_all_banks_pos.Utilities.SharedPref;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANAGER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MARKET_SEG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SUPER_USER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SYSTEM_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.USER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.autoSettleTask;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ecrType;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.onBackward;
import static cn.access.group.android_all_banks_pos.viewfragments.PaymentAmountFragment.idevice;
import static java.lang.Thread.sleep;


/**
 * @author muhammad.humayun
 * on 8/19/2019.
 */
public class HomeFragment extends Fragment {
    public static int index = 0; //0=ubl ,1=upi
    public static Boolean autoSettleTaskStop = false;
    private final int[] advImageIdList = new int[]{R.mipmap.ad0, R.mipmap.ad1, R.mipmap.ad2, R.mipmap.ad3};
    public String TAG = "HomeFrag";
    Unbinder unbinder;
    String txnType = "";
    CardView saleCard;
    CardView voidCard;
    CardView saleIppCard;
    CardView cashAdvCard;
    CardView refundCard;
    CardView orbitInquiryCard;
    CardView orbitRedeemCard;
    CardView cashOutCard;
    CardView preAuthCard;
    CardView Auth;
    CardView completionCard;
    CardView settlementCard;
    CardView adjustCard;
    CardView reportCard;
    CardView settingsCard;
    CardView saleAdjustCard;
    CardView auth;
    ImageView ecrMcdonald;
    private WeakReference<Context> mcontext;
    View view;
    IEMV iemv;
    IPinpad ipinpad;
    IBeeper iBeeper;
    IPrinter printer;
    ISerialPort serialPort;
    IRFCardReader irfCardReader;
    IUsbSerialPort iUsbSerialPort;
    Intent intent = new Intent();
    PaymentAmountFragment paymentAmountFragment;
    ServiceHelper.OnServiceConnectedListener onServiceConnectedListener;
    String s = null;
    boolean isSucc = false;
    String ECRTxn = "null";
    IDeviceInfo ideviceInfo;
    Repository repository;
    IPrinter iPrinter;
    ISystemManager systemManager = null;
    ISettingsManager settingsManager = null;
    private static final String ACTION = "com.vfi.smartpos.system_service";
    private static final String PACKAGE = "com.vfi.smartpos.system_service";
    private static final String CLASSNAME = "com.vfi.smartpos.system_service.SystemService";
    public static final String ACTION_X9SERVICE = "com.vfi.smartpos.device_service";
    public static final String PACKAGE_X9SERVICE = "com.vfi.smartpos.deviceservice";
    public static final String CLASSNAME_X9SERVICE = "com.verifone.smartpos.service.VerifoneDeviceService";
    Bundle bundle = new Bundle();


    private final ServiceConnection conn = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {


            new CountDownTimer(500, 500) {
                public void onFinish() {
                    idevice = IDeviceService.Stub.asInterface(service);

                    try {
                        Utility.DEBUG_LOG("onServiceConnected", "onServiceConnected--Control here..");
                        serialPort = idevice.getSerialPort("usb-rs232");
                        ideviceInfo = idevice.getDeviceInfo();
                        printer = idevice.getPrinter();
                        Utility.DEBUG_LOG("Terminal Serial No", String.valueOf(ideviceInfo.getSerialNo()));
                        Constants.TERMINAL_SERIAL = ideviceInfo.getSerialNo();
                        Utility.DEBUG_LOG("Total Ram", String.valueOf(ideviceInfo.getRamTotal()));
                        Utility.DEBUG_LOG("Available Ram", String.valueOf(ideviceInfo.getRamAvailable()));
                        Utility.DEBUG_LOG("Battery Level", String.valueOf(ideviceInfo.getBatteryLevel()));
                        Utility.DEBUG_LOG("Battery Temperature", String.valueOf(ideviceInfo.getBatteryTemperature()));
                        Utility.DEBUG_LOG("Data Usage Level", String.valueOf(ideviceInfo.getMobileDataUsageTotal()));
                        Utility.DEBUG_LOG("K21 version", String.valueOf(ideviceInfo.getK21Version()));

                        bundle.putBoolean("HOMEKEY", false);
                        bundle.putBoolean("STATUSBARKEY", false);
                        ideviceInfo.setSystemFunction(bundle);


                        //iemv = idevice.getEMV();
                        //ipinpad = idevice.getPinpad(1);
                        //iBeeper = idevice.getBeeper();
                        //printer = idevice.getPrinter();
                        //irfCardReader = idevice.getRFCardReader();
//                       onServiceConnectedListener.onConnected();
                        if (Constants.isEcrEnable.equals("Y"))
                            ECR.initECR(serialPort);

                        isSucc = true;
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    //paymentAmountPresenter = new PaymentAmountFragment(PaymentAmountFragment.this, PaymentAmountFragment.this, handler, isSucc, assetManager, iemv, ipinpad, iBeeper, printer, appDatabase,transactionAmount,"sale",mContext,serialPort);

//                    Toast.makeText(getContext(), "bind service success", Toast.LENGTH_SHORT).show();
                }

                public void onTick(long millisUntilFinished) {
                    // millisUntilFinished    The amount of time until finished.
                }
            }.start();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            idevice = null;
        }
    };
    private ServiceConnection conn1 = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Utility.DEBUG_LOG("systemSetting", "system service bind success");
            systemManager = ISystemManager.Stub.asInterface(iBinder);
            try {
                settingsManager = systemManager.getSettingsManager();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Utility.DEBUG_LOG("systemSetting", "system service disconnected.");
            systemManager = null;
        }
    };


    private ViewPager advViewPaper;
    private List<ImageView> advImages;
    private List<View> advDotsView;
    private int advPosition;
    private final Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message message) {
            // This is where you do your work in the UI thread.
            // Your worker tells you in the message what to do.
            advViewPaper.setCurrentItem(advPosition);
        }
    };
    private int advLastPosition = 0;
    private AdvViewPagerAdapter advAdapter;
    private ScheduledExecutorService scheduledExecutorService;

    public HomeFragment() {
        // Required empty public constructor
    }

    PrinterListener printerListener = new PrinterListener.Stub() {

        @Override
        public void onFinish() throws RemoteException {
            Utility.DEBUG_LOG(TAG, "fininsh");
        }

        @Override
        public void onError(int error) throws RemoteException {
            Utility.DEBUG_LOG(TAG, String.valueOf(error));
        }
    };


    public void callPaymentFragment() {
        Utility.DEBUG_LOG("HomeFragment:", "+ callPaymentFragment() +");
        DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(), "payment fragment");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_home_new, container, false);
        unbinder = ButterKnife.bind(this, view);
        initBannerView();
        SharedPref.write("reportType", "null");
        Utility.DEBUG_LOG(TAG, "HomeFragment onCreateView: List report " + SharedPref.read("ListReport", ""));
        // Log.d(TAG,"isPlugged: " +isPlugged);
        if (Constants.isEcrEnable.equals("Y")) {
            try {
                Utility.DEBUG_LOG(TAG, "ECR CHECK");
                ECR.cancelMsg();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }


//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                String PATH = "/sdcard/Download/jsonfile.json";
//                File file = new File(PATH);
//                Utility.DEBUG_LOG("TAG", "file path " + file);
//                if (file.exists()) {
//
//                    file.delete();
//                    try {
//                        systemManager.reboot();
//                    } catch (RemoteException e) {
//                        e.printStackTrace();
//                    }
//
//                    Utility.DEBUG_LOG(TAG, "file found and deleted");
//                    // send the request to profile download...
//
//                } else {
//                    Utility.DEBUG_LOG(TAG, "no such file found");
//                }
//            }
//        }, 5000);


        saleCard = view.findViewById(R.id.pointOfSalesCard);
        voidCard = view.findViewById(R.id.voidCard);
        saleIppCard = view.findViewById(R.id.saleIppCard);
        cashAdvCard = view.findViewById(R.id.cashAdvCard);
        orbitRedeemCard = view.findViewById(R.id.orbitRedeemCard);
        refundCard = view.findViewById(R.id.refundCard);
        orbitInquiryCard = view.findViewById(R.id.orbitInquiryCard);
        cashOutCard = view.findViewById(R.id.cashOutCard);
        preAuthCard = view.findViewById(R.id.preAuthCard);
        Auth = view.findViewById(R.id.Auth);
        completionCard = view.findViewById(R.id.completionCard);
        settlementCard = view.findViewById(R.id.settlementCard);
        adjustCard = view.findViewById(R.id.adjustCard);
        reportCard = view.findViewById(R.id.reportCard);
        settingsCard = view.findViewById(R.id.settingsCard);
        saleAdjustCard = view.findViewById(R.id.saleAdjustCard);
        ecrMcdonald = view.findViewById(R.id.ecrMcdonald);
        if (ecrType.equals("U")) {
            ecrMcdonald.setVisibility(View.GONE);
        }
        if (MARKET_SEG.equals("g") || MARKET_SEG.equals("G")) {
            //+ taha 02-04-2021 make only retail type
            saleCard.setVisibility(View.VISIBLE);
            preAuthCard.setVisibility(View.VISIBLE);
            completionCard.setVisibility(View.VISIBLE);
            voidCard.setVisibility(View.VISIBLE);
            refundCard.setVisibility(View.VISIBLE);
            orbitInquiryCard.setVisibility(View.VISIBLE);
            orbitRedeemCard.setVisibility(View.VISIBLE);
            cashOutCard.setVisibility(View.VISIBLE);
            saleIppCard.setVisibility(View.VISIBLE);
            cashAdvCard.setVisibility(View.VISIBLE);
            settlementCard.setVisibility(View.VISIBLE);
            adjustCard.setVisibility(View.VISIBLE);
            saleAdjustCard.setVisibility(View.VISIBLE);
            //Sale Adjust missing
            //+ taha 02-04-2021 remove sale adjust in general
            Auth.setVisibility(View.VISIBLE);
            reportCard.setVisibility(View.VISIBLE);
            settingsCard.setVisibility(View.VISIBLE);
            Constants.TIP_ENABLED = "Y";
        } else if (MARKET_SEG.equals("o") || MARKET_SEG.equals("O")) {

            preAuthCard.setVisibility(View.VISIBLE);
            completionCard.setVisibility(View.VISIBLE);
            saleCard.setVisibility(View.VISIBLE);
            orbitInquiryCard.setVisibility(View.VISIBLE);
            orbitRedeemCard.setVisibility(View.VISIBLE);
            cashOutCard.setVisibility(View.VISIBLE);
            voidCard.setVisibility(View.VISIBLE);
            adjustCard.setVisibility(View.VISIBLE);
            reportCard.setVisibility(View.VISIBLE);
            settingsCard.setVisibility(View.VISIBLE);
            settlementCard.setVisibility(View.VISIBLE);
            refundCard.setVisibility(View.GONE);
            saleIppCard.setVisibility(View.GONE);
            saleAdjustCard.setVisibility(View.GONE);
            Auth.setVisibility(View.GONE);
            cashAdvCard.setVisibility(View.GONE);

           // Constants.TIP_ENABLED = "N";

        } else if (MARKET_SEG.equals("r") || MARKET_SEG.equals("R")) {
            //+ taha 02-04-2021 make only retail type
            saleCard.setVisibility(View.VISIBLE);  //OK
            preAuthCard.setVisibility(View.GONE);
            completionCard.setVisibility(View.GONE);
            voidCard.setVisibility(View.VISIBLE);  //OK
            refundCard.setVisibility(View.GONE);
            orbitInquiryCard.setVisibility(View.VISIBLE);  //OK
            orbitRedeemCard.setVisibility(View.VISIBLE);  //OK
            cashOutCard.setVisibility(View.VISIBLE);
            saleIppCard.setVisibility(View.GONE);
            cashAdvCard.setVisibility(View.GONE);
            settlementCard.setVisibility(View.VISIBLE);    //OK
            adjustCard.setVisibility(View.GONE);
            saleAdjustCard.setVisibility(View.GONE);
            //Sale Adjust missing
            //+ taha 02-04-2021 remove sale adjust in general
            Auth.setVisibility(View.GONE);
            reportCard.setVisibility(View.VISIBLE);     //OK
            settingsCard.setVisibility(View.VISIBLE);   //OK
            Constants.TIP_ENABLED = "N";

        } else if (MARKET_SEG.equals("t") || MARKET_SEG.equals("T")) {
            saleCard.setVisibility(View.VISIBLE);
            orbitInquiryCard.setVisibility(View.VISIBLE);
            orbitRedeemCard.setVisibility(View.VISIBLE);
            cashOutCard.setVisibility(View.VISIBLE);
            voidCard.setVisibility(View.VISIBLE);
            adjustCard.setVisibility(View.VISIBLE);
            settlementCard.setVisibility(View.VISIBLE);
            reportCard.setVisibility(View.VISIBLE);
            settingsCard.setVisibility(View.VISIBLE);
            refundCard.setVisibility(View.GONE);
            saleIppCard.setVisibility(View.GONE);
            cashAdvCard.setVisibility(View.GONE);
            preAuthCard.setVisibility(View.GONE);
            completionCard.setVisibility(View.GONE);
            Auth.setVisibility(View.GONE);
            saleAdjustCard.setVisibility(View.GONE);
            Constants.TIP_ENABLED = "Y";

        } else if (MARKET_SEG.equals("i") || MARKET_SEG.equals("I")) {
            saleCard.setVisibility(View.VISIBLE);
            saleIppCard.setVisibility(View.VISIBLE);
            orbitInquiryCard.setVisibility(View.VISIBLE);
            orbitRedeemCard.setVisibility(View.VISIBLE);
            cashOutCard.setVisibility(View.VISIBLE);
            voidCard.setVisibility(View.VISIBLE);
            settlementCard.setVisibility(View.VISIBLE);
            reportCard.setVisibility(View.VISIBLE);
            settingsCard.setVisibility(View.VISIBLE);

            Auth.setVisibility(View.GONE);
            saleAdjustCard.setVisibility(View.GONE);
            refundCard.setVisibility(View.GONE);
            cashAdvCard.setVisibility(View.GONE);
            adjustCard.setVisibility(View.GONE);
            preAuthCard.setVisibility(View.GONE);
            completionCard.setVisibility(View.GONE);
            Constants.TIP_ENABLED = "N";

        } else if (MARKET_SEG.equals("w") || MARKET_SEG.equals("W")) {
            saleCard.setVisibility(View.VISIBLE);
            orbitInquiryCard.setVisibility(View.VISIBLE);
            orbitRedeemCard.setVisibility(View.VISIBLE);
            cashOutCard.setVisibility(View.VISIBLE);
            voidCard.setVisibility(View.VISIBLE);
            reportCard.setVisibility(View.VISIBLE);
            settingsCard.setVisibility(View.VISIBLE);

            settlementCard.setVisibility(View.GONE);
            Auth.setVisibility(View.GONE);
            saleAdjustCard.setVisibility(View.GONE);
            refundCard.setVisibility(View.GONE);
            saleIppCard.setVisibility(View.GONE);
            cashAdvCard.setVisibility(View.GONE);
            adjustCard.setVisibility(View.GONE);
            preAuthCard.setVisibility(View.GONE);
            completionCard.setVisibility(View.GONE);
            Constants.TIP_ENABLED = "N";

        } else if (MARKET_SEG.equals("J") || MARKET_SEG.equals("j")) {
            saleCard.setVisibility(View.VISIBLE);
            orbitInquiryCard.setVisibility(View.VISIBLE);
            orbitRedeemCard.setVisibility(View.VISIBLE);
            cashOutCard.setVisibility(View.VISIBLE);
            //Sale Ajust
            saleAdjustCard.setVisibility(View.VISIBLE);
            settlementCard.setVisibility(View.VISIBLE);
            voidCard.setVisibility(View.VISIBLE);
            reportCard.setVisibility(View.VISIBLE);
            settingsCard.setVisibility(View.VISIBLE);


            Auth.setVisibility(View.GONE);
            refundCard.setVisibility(View.GONE);
            saleIppCard.setVisibility(View.GONE);
            cashAdvCard.setVisibility(View.GONE);
            adjustCard.setVisibility(View.GONE);
            preAuthCard.setVisibility(View.GONE);
            completionCard.setVisibility(View.GONE);
            Constants.TIP_ENABLED = "N";

        } else if (MARKET_SEG.equals("F") || MARKET_SEG.equals("f")) {
            saleCard.setVisibility(View.VISIBLE);
            orbitInquiryCard.setVisibility(View.VISIBLE);
            orbitRedeemCard.setVisibility(View.VISIBLE);
            cashOutCard.setVisibility(View.VISIBLE);
            refundCard.setVisibility(View.VISIBLE);
            settlementCard.setVisibility(View.VISIBLE);
            voidCard.setVisibility(View.VISIBLE);
            reportCard.setVisibility(View.VISIBLE);
            settingsCard.setVisibility(View.VISIBLE);

            //settlementCard.setVisibility(View.GONE);
            Auth.setVisibility(View.GONE);
            saleAdjustCard.setVisibility(View.GONE);
            saleIppCard.setVisibility(View.GONE);
            cashAdvCard.setVisibility(View.GONE);
            adjustCard.setVisibility(View.GONE);
            preAuthCard.setVisibility(View.GONE);
            completionCard.setVisibility(View.GONE);
            Constants.TIP_ENABLED = "N";

        } else if (MARKET_SEG.equals("A") || MARKET_SEG.equals("a")) {

            //Auth Missing
            Auth.setVisibility(View.VISIBLE);
            orbitInquiryCard.setVisibility(View.VISIBLE);
            orbitRedeemCard.setVisibility(View.VISIBLE);
            cashOutCard.setVisibility(View.VISIBLE);
            reportCard.setVisibility(View.VISIBLE);
            settingsCard.setVisibility(View.VISIBLE);

            saleAdjustCard.setVisibility(View.GONE);
            refundCard.setVisibility(View.GONE);
            settlementCard.setVisibility(View.GONE);
            voidCard.setVisibility(View.GONE);
            saleCard.setVisibility(View.GONE);
            settlementCard.setVisibility(View.GONE);
            saleIppCard.setVisibility(View.GONE);
            cashAdvCard.setVisibility(View.GONE);
            adjustCard.setVisibility(View.GONE);
            preAuthCard.setVisibility(View.GONE);
            completionCard.setVisibility(View.GONE);
            Constants.TIP_ENABLED = "N";

        } else if (MARKET_SEG.equals("C") || MARKET_SEG.equals("c")) {

            cashAdvCard.setVisibility(View.VISIBLE);
            orbitInquiryCard.setVisibility(View.VISIBLE);
            orbitRedeemCard.setVisibility(View.VISIBLE);
            cashOutCard.setVisibility(View.VISIBLE);
            voidCard.setVisibility(View.VISIBLE);
            settlementCard.setVisibility(View.VISIBLE);
            reportCard.setVisibility(View.VISIBLE);
            settingsCard.setVisibility(View.VISIBLE);


            refundCard.setVisibility(View.GONE);
            saleCard.setVisibility(View.GONE);
            settlementCard.setVisibility(View.GONE);
            saleIppCard.setVisibility(View.GONE);
            adjustCard.setVisibility(View.GONE);
            preAuthCard.setVisibility(View.GONE);
            completionCard.setVisibility(View.GONE);
            Constants.TIP_ENABLED = "N";

        }
       else if (ecrType.equals("M")) {
            saleCard.setVisibility(View.GONE);
            preAuthCard.setVisibility(View.GONE);
            completionCard.setVisibility(View.GONE);
            voidCard.setVisibility(View.GONE);
            refundCard.setVisibility(View.GONE);
            orbitInquiryCard.setVisibility(View.GONE);
            orbitRedeemCard.setVisibility(View.GONE);
            cashOutCard.setVisibility(View.GONE);
            saleIppCard.setVisibility(View.GONE);
            cashAdvCard.setVisibility(View.GONE);
            settlementCard.setVisibility(View.GONE);
            adjustCard.setVisibility(View.GONE);
            saleAdjustCard.setVisibility(View.GONE);
            //Sale Adjust missing
            //+ taha 02-04-2021 remove sale adjust in general
            Auth.setVisibility(View.GONE);
            reportCard.setVisibility(View.GONE);
            settingsCard.setVisibility(View.GONE);
            ecrMcdonald.setVisibility(View.VISIBLE);

            if (MARKET_SEG.equals("S") || MARKET_SEG.equals("s")) {
                saleCard.setVisibility(View.GONE);
                preAuthCard.setVisibility(View.GONE);
                completionCard.setVisibility(View.GONE);
                voidCard.setVisibility(View.GONE);
                refundCard.setVisibility(View.GONE);
                orbitInquiryCard.setVisibility(View.GONE);
                orbitRedeemCard.setVisibility(View.GONE);
                cashOutCard.setVisibility(View.GONE);
                saleIppCard.setVisibility(View.GONE);
                cashAdvCard.setVisibility(View.GONE);
                settlementCard.setVisibility(View.VISIBLE);
                adjustCard.setVisibility(View.GONE);
                saleAdjustCard.setVisibility(View.GONE);
                //Sale Adjust missing
                //+ taha 02-04-2021 remove sale adjust in general
                Auth.setVisibility(View.GONE);
                reportCard.setVisibility(View.VISIBLE);
                settingsCard.setVisibility(View.VISIBLE);
                ecrMcdonald.setVisibility(View.GONE);
            }
        }

            Repository repository = new Repository(mcontext.get());
            TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
            repository.updateTerminalConfiguration(TerminalConfig);
            return view;

        }


        private void initBannerView () {
            //广告的显示
            advImages = new ArrayList<ImageView>();
            for (int i = 0; i < advImageIdList.length; i++) {
                ImageView imageView = new ImageView(mcontext.get());
                imageView.setBackgroundResource(advImageIdList[i]);
                advImages.add(imageView);
            }
            advDotsView = new ArrayList<View>();
            advDotsView.add(view.findViewById(R.id.dot_0));
            advDotsView.add(view.findViewById(R.id.dot_1));
            advDotsView.add(view.findViewById(R.id.dot_2));
            advDotsView.add(view.findViewById(R.id.dot_3));

            advAdapter = new AdvViewPagerAdapter();
            advViewPaper = view.findViewById(R.id.advertisement);
            advViewPaper.setAdapter(advAdapter);
            advViewPaper.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    advDotsView.get(position).setBackgroundResource(R.drawable.dot_focused);
                    advDotsView.get(advLastPosition).setBackgroundResource(R.drawable.dot_normal);

                    advLastPosition = position;
                    advPosition = position;
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                @Override
                public void onPageScrollStateChanged(int arg0) {
                }
            });
        }

        @Override
        public void onViewCreated (@NonNull View view, @Nullable Bundle savedInstanceState){
            super.onViewCreated(view, savedInstanceState);
            try {
                String PATH = "/sdcard/Download/jsonfile.json";
                File file = new File(PATH);
                Utility.DEBUG_LOG("TAG", "file path " + file);
                if (file.exists()) {
                    file.delete();
                    Utility.DEBUG_LOG(TAG, "file found and deleted");
                    repository = new Repository(mcontext.get());
                    repository.getTerminalConfiguration();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onAttach (Context context){
            super.onAttach(context);
            mcontext = new WeakReference<>(context);
            Utility.DEBUG_LOG(TAG, "onAttach");
            intent.setAction("com.vfi.smartpos.device_service");
            intent.setPackage("com.vfi.smartpos.deviceservice");
            context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
            Intent intent1 = new Intent();
            intent1.setAction(ACTION_X9SERVICE);
            intent1.setPackage(PACKAGE);
            intent1.setClassName(PACKAGE, CLASSNAME);
            context.bindService(intent1, conn1, Context.BIND_AUTO_CREATE);
        }

        @Override
        public void onResume () {
            super.onResume();
            Utility.DEBUG_LOG("onResume", "working");
            //ECR.ecrRead(serialPort);
            scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
            scheduledExecutorService.scheduleWithFixedDelay(new AdvViewPageTask(), 4, 3, TimeUnit.SECONDS);
            if (Constants.isEcrEnable.equals("Y"))
                scheduledExecutorService.scheduleWithFixedDelay(new ECRMessage(), 1, 1, TimeUnit.SECONDS);

            Utility.DEBUG_LOG(TAG + "SL", Constants.AUTO_SETTLEMENT);
            if (Constants.AUTO_SETTLEMENT.equals("Y")) {
                scheduledExecutorService.scheduleWithFixedDelay(new AutoSettlement(), 10, 10, TimeUnit.SECONDS);
            }
            Utility.DEBUG_LOG("onAttach", "Shivam");
            if (Utility.isPosLowOnMemory(mcontext.get(), systemManager)) {
                DialogUtil.restartDialogForMaintenance_NoButton(mcontext.get(), systemManager, "Please Wait...", sweetAlertDialog1 -> {
                });

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Utility.DEBUG_LOG(TAG, "+ run +");
                        for (int i = 5; i > 0; i--) {
                            Utility.DEBUG_LOG(TAG, "Maintenance Restart in " + i + " seconds...");
                            //                        MainApplication.showProgressDialog(mcontext,"Maintenance Restart in " + i + " seconds...");
                            try {
                                sleep(1 * 1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        Utility.DEBUG_LOG("Restart", "Before reboot");
                        try {
                            systemManager.reboot();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

//            MainApplication.showProgressDialog(mcontext,"Maintenannew Thread(new Runnable() {
////                @Override
////                public void run() {
////                    Utility.DEBUG_LOG(TAG,"+ run +");
////                    for ( int i=10; i > 0 ; i--)
////                    {
////                        Utility.DEBUG_LOG(TAG,"Maintenance Restart in " + i + " seconds...");
//////                        MainApplication.showProgressDialog(mcontext,"Maintenance Restart in " + i + " seconds...");
////                        try {
////                            sleep(1*1000);
////                        } catch (InterruptedException e) {
////                            e.printStackTrace();
////                        }
//////                        MainApplication.hideProgressDialog();
////                    }
////                    Utility.DEBUG_LOG("Restart","Before reboot");
////                    try {
////                        systemManager.reboot();
////                    } catch (RemoteException e) {
////                        e.printStackTrace();
////                    }
////                }
////            }).start();ce cycle: restart required; please wait...");
//


//            DialogUtil.confirmDialogSingleButton(mcontext, "Maintenance cycle [restart]", sweetAlertDialog1 -> {
//                sweetAlertDialog1.dismissWithAnimation();
//                Utility.DEBUG_LOG(TAG,"Before reboot");
//                try {
//                    systemManager.reboot();
////                    systemManager.restartApplication(mcontext.getPackageName());
//                } catch (RemoteException e) {
//                    e.printStackTrace();
//                }
//            });
            }
        }

        @Override
        public void onDestroyView () {
            super.onDestroyView();

            if (scheduledExecutorService != null) {
                scheduledExecutorService.shutdown();
                scheduledExecutorService = null;
            }

            unbinder.unbind();
            // unbinder = null;
            Utility.DEBUG_LOG(TAG, "onDestroyView");
        }

        @Override
        public void onDetach () {
            super.onDetach();
            if (getActivity() != null) {
                getActivity().unbindService(conn);
                getActivity().unbindService(conn1);
            }

        }

        @Override
        public void onStop () {
            super.onStop();

        }
        ;

        //@OnClick({R.id.creditCardCard, R.id.qrCodeCard, R.id.settingsCard, R.id.pointOfSalesCard, R.id.loyaltyCard, R.id.salesCard, R.id.discountCard})

        public boolean isSettlementNeeded () {
            try {
                if (((DashboardContainer) mcontext.get()).getAllBatchTransactions().size() != 0) {
                    return SharedPref.read("isSettleRequiured", "").equals("Y");
                }

            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return false;
        }

        @OnClick({R.id.pointOfSalesCard, R.id.settingsCard, R.id.preAuthCard, R.id.adjustCard, R.id.voidCard, R.id.settlementCard, R.id.reportCard, R.id.orbitRedeemCard, R.id.orbitInquiryCard, R.id.cashOutCard, R.id.cashAdvCard, R.id.completionCard, R.id.refundCard, R.id.manualEntryCard, R.id.saleIppCard, R.id.saleAdjustCard, R.id.Auth})
        @Nullable

        public void onViewClicked (View view){
            Utility.DEBUG_LOG(TAG, "onBackward:" + onBackward);
            switch (view.getId()) {

                case R.id.adjustCard:
                    try {
                        if (printer.getStatus() != 240) {
                            if (Constants.TIP_ENABLED.equals("Y")) {
                                if (isSettlementNeeded()) {
                                    DialogUtil.infoDialog(mcontext.get(), "Must Settle First!");
                                } else {
                                    DashboardContainer.switchFragmentWithBackStack(new AdjustFragment(), "Tip Adjust fragment");
                                    txnType = "ADJUST";
                                    SharedPref.write("button_fragment", txnType);
                                }
                            } else {
                                DialogUtil.infoDialog(mcontext.get(), "Tip is not enabled");
                            }
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }

                    break;

                case R.id.saleAdjustCard:
                    try {
                        if (printer.getStatus() != 240) {
                            if (isSettlementNeeded()) {
                                DialogUtil.infoDialog(mcontext.get(), "Must Settle First!");
                            } else {
                                DashboardContainer.switchFragmentWithBackStack(new SaleAdjustFragment(), "payment fragment");
                                txnType = "ADJUST";
                                SharedPref.write("button_fragment", txnType);
                            }
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }

                    break;

                case R.id.saleIppCard:
                    try {
                        if (printer.getStatus() != 240) {
                            if (isSettlementNeeded()) {
                                DialogUtil.infoDialog(mcontext.get(), "Must Settle First!");
                            } else {
                                DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(), "payment fragment");
                                txnType = "SALEIPP";
                                SharedPref.write("button_fragment", txnType);
                            }
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }

                    break;
                case R.id.preAuthCard:
                    try {
                        if (printer.getStatus() != 240) {
                            if (isSettlementNeeded()) {
                                DialogUtil.infoDialog(mcontext.get(), "Must Settle First!");
                            } else {
                                DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(), "payment fragment");
                                txnType = "PRE AUTH";
                                SharedPref.write("button_fragment", txnType);
                            }
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }
                    break;
                case R.id.Auth:
                    try {
                        if (printer.getStatus() != 240) {
                            if (isSettlementNeeded()) {
                                DialogUtil.infoDialog(mcontext.get(), "Must Settle First!");
                            } else {
                                DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(), "payment fragment");
                                txnType = "AUTH";
                                SharedPref.write("button_fragment", txnType);
                            }
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }
                    break;

                case R.id.settlementCard:
                    try {
                        if (printer.getStatus() != 240) {
                            DialogUtil.showKey(Objects.requireNonNull(getActivity()));
                            DialogUtil.inputDialog(mcontext.get(), "Input Password", "Enter User Password", new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    EditText text = Objects.requireNonNull(sweetAlertDialog.getWindow()).getDecorView().findViewById(R.id.my_edit_text_1);
                                    String password = text.getText().toString();
                                    if (password.contentEquals(MANAGER_PASSWORD_CHANGE)) {
                                        //show confirmation
                                        sweetAlertDialog.dismissWithAnimation();
                                        Utility.DEBUG_LOG(TAG, "before new SettlementFragment");
                                        DashboardContainer.switchFragmentWithBackStack(new SettlementFragment(systemManager), "settlement fragment");
                                        //                            DashboardContainer.switchFragmentWithBackStack(new SettlementFragment(), "settlement fragment");
                                    } else {
                                        DialogUtil.errorDialog(mcontext.get(), "Error!", "Wrong Password");
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                }
                            }, InputType.TYPE_CLASS_NUMBER, PasswordTransformationMethod.getInstance());
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }

                    break;

                case R.id.voidCard:
                    try {
                        if (printer.getStatus() != 240) {
                            if (isSettlementNeeded()) {
                                DialogUtil.infoDialog(mcontext.get(), "Must Settle First!");
                            } else {
                                DashboardContainer.switchFragmentWithBackStack(new VoidTransactionFragment(), "void fragment");
                            }
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }
                    break;


                case R.id.settingsCard:
                    // add button listener


                    // custom dialog
                    final Dialog dialog = new Dialog(mcontext.get());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.setting_alert);
                    Button dialogButton = dialog.findViewById(R.id.dialogButtonOK);
                    Button dialogButton1 = dialog.findViewById(R.id.dialogButtonOK1);
                    Button dialogButton2 = dialog.findViewById(R.id.dialogButtonOK2);
                    // if button is clicked, close the custom dialog
                    dialogButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            DialogUtil.showKey(Objects.requireNonNull(getActivity()));

                            DialogUtil.inputDialog(mcontext.get(), "Input Password", "Enter System Password", new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    EditText text = sweetAlertDialog.getWindow().getDecorView().findViewById(R.id.my_edit_text_1);
                                    String password = text.getText().toString();
                                    if (password.contentEquals(SYSTEM_PASSWORD_CHANGE)) {
                                        //show confirmation
                                        sweetAlertDialog.dismissWithAnimation();
                                        DashboardContainer.switchFragmentWithBackStack(new SystemSettingFragment(), "setting fragment");
                                    } else {
                                        DialogUtil.errorDialog(mcontext.get(), "Error!", "Wrong Password");
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                }
                            }, InputType.TYPE_CLASS_NUMBER, PasswordTransformationMethod.getInstance());
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                    dialogButton1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DialogUtil.showKey(getActivity());
                            DialogUtil.inputDialog(mcontext.get(), "Input Password", "Enter User Password", new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    EditText text = sweetAlertDialog.getWindow().getDecorView().findViewById(R.id.my_edit_text_1);
                                    String password = text.getText().toString();
                                    if (password.contentEquals(USER_PASSWORD_CHANGE)) {
                                        //show confirmation
                                        sweetAlertDialog.dismissWithAnimation();
                                        DashboardContainer.switchFragmentWithBackStack(new SettingFragment(systemManager), "setting fragment");
                                    } else {
                                        DialogUtil.errorDialog(mcontext.get(), "Error!", "Wrong Password");
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                }
                            }, InputType.TYPE_CLASS_NUMBER, PasswordTransformationMethod.getInstance());
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                    dialogButton2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DialogUtil.showKey(Objects.requireNonNull(getActivity()));
                            DialogUtil.inputDialog(mcontext.get(), "Input Password", "Enter Super User Password", new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    EditText text = sweetAlertDialog.getWindow().getDecorView().findViewById(R.id.my_edit_text_1);
                                    String password = text.getText().toString();
                                    if (password.contentEquals(SUPER_USER_PASSWORD_CHANGE)) {
                                        //show confirmation
                                        sweetAlertDialog.dismissWithAnimation();
                                        DashboardContainer.switchFragmentWithBackStack(new SuperUserSettingFragment(), "setting fragment");
                                    } else {
                                        DialogUtil.errorDialog(mcontext.get(), "Error!", "Wrong Password");
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                }
                            }, InputType.TYPE_CLASS_NUMBER, PasswordTransformationMethod.getInstance());
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                    break;
//
//                DialogUtil.inputDialog(getContext(),"Input Password","Enter User Password", new SweetAlertDialog.OnSweetClickListener() {
//                @Override
//                public void onClick(SweetAlertDialog sweetAlertDialog) {
//                     EditText text= sweetAlertDialog.getWindow().getDecorView().findViewById(R.id.my_edit_text_1);
//                    String password =text.getText().toString();
//                    if(password.contentEquals("3737")){
//                        //show confirmation
//                        sweetAlertDialog.dismissWithAnimation();
//                        DashboardContainer.switchFragmentWithBackStack(new SettingFragment(), "setting fragment");
//                    }
//                }
//            },InputType.TYPE_CLASS_NUMBER,PasswordTransformationMethod.getInstance());
//                break;
                case R.id.pointOfSalesCard:
                    //masood commented this on 10-9-2020
//                DashboardContainer.switchFragmentWithBackStack(new PointOfSalesFragment(), "point of sales fragment");
                    try {
                        if (printer.getStatus() != 240) {
                            if (isSettlementNeeded()) {
                                DialogUtil.infoDialog(mcontext.get(), "Must Settle First!");
                            } else {
                                DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(), "payment fragment");
                                txnType = "SALE";
                                SharedPref.write("button_fragment", txnType);

                            }
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }
                    break;
                case R.id.completionCard:
                    //masood commented this on 10-9-2020
                    try {
                        if (printer.getStatus() != 240) {
                            //                DashboardContainer.switchFragmentWithBackStack(new PointOfSalesFragment(), "point of sales fragment");
                            if (isSettlementNeeded()) {
                                DialogUtil.infoDialog(mcontext.get(), "Must Settle First!");
                            } else {
                                DashboardContainer.switchFragmentWithBackStack(new CompletionAuthFragment(), "completion fragment");
                                txnType = "COMPLETION";
                                SharedPref.write("button_fragment", txnType);
                            }
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }
                    break;
                case R.id.refundCard:
                    //masood commented this on 10-9-2020
                    try {
                        if (printer.getStatus() != 240) {
                            DialogUtil.showKey(Objects.requireNonNull(getActivity()));
                            DialogUtil.inputDialog(mcontext.get(), "Input Password", "Enter User Password", new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    EditText text = sweetAlertDialog.getWindow().getDecorView().findViewById(R.id.my_edit_text_1);
                                    String password = text.getText().toString();
                                    txnType = "REFUND";
                                    SharedPref.write("button_fragment", txnType);
                                    // DialogUtil.hideKeyboard(Objects.requireNonNull(getActivity()));
                                    if (password.contentEquals(USER_PASSWORD_CHANGE)) {
                                        //show confirmation
                                        sweetAlertDialog.dismissWithAnimation();


                                        DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(), "payment fragment");

                                    } else {
                                        DialogUtil.errorDialog(mcontext.get(), "Error!", "Wrong Password");
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                }
                            }, InputType.TYPE_CLASS_NUMBER, PasswordTransformationMethod.getInstance());
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }

                    break;

                case R.id.orbitRedeemCard:
                    try {
                        if (printer.getStatus() != 240) {
                            if (isSettlementNeeded()) {
                                DialogUtil.infoDialog(mcontext.get(), "Must Settle First!");
                            } else {
                                DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(), "orbit redeem fragment");
                                txnType = "REDEEM";
                                SharedPref.write("button_fragment", txnType);
                            }
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }
                    break;
                case R.id.orbitInquiryCard:
                    try {
                        if (printer.getStatus() != 240) {
                            if (isSettlementNeeded()) {
                                DialogUtil.infoDialog(mcontext.get(), "Must Settle First!");
                            } else {
                                DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(), "orbit Inquiry fragment");
                                txnType = "ORBIT INQUIRY";
                                SharedPref.write("button_fragment", txnType);
                            }
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }
                    break;
                case R.id.cashOutCard:
                    try {
                        if (printer.getStatus() != 240) {
                            if (isSettlementNeeded()) {
                                DialogUtil.infoDialog(mcontext.get(), "Must Settle First!");
                            } else {
                                DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(), "cash out fragment");
                                txnType = "CASH OUT";
                                SharedPref.write("button_fragment", txnType);
                            }
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }
                    break;
                case R.id.cashAdvCard:
                    try {
                        if (printer.getStatus() != 240) {
                            if (isSettlementNeeded()) {
                                DialogUtil.infoDialog(mcontext.get(), "Must Settle First!");
                            } else {
                                DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(), "cash adv fragment");
                                txnType = "CASH ADV";
                                SharedPref.write("button_fragment", txnType);
                            }
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }
                    break;

                case R.id.reportCard:


                    DashboardContainer.switchFragmentWithBackStack(new TransactionDetailViewFragment(), "transaction detail fragment");
                    txnType = "REPORTS";
                    SharedPref.write("button_fragment", txnType);


                /*
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);*/
                    break;

                case R.id.manualEntryCard:
                    try {
                        if (printer.getStatus() != 240) {
                            DashboardContainer.switchFragmentWithBackStack(new PaymentAmountFragment(), "Manual Entry Fragment");
                            txnType = "MANUAL ENTRY";
                            SharedPref.write("button_fragment", txnType);
                        } else {
                            DialogUtil.errorDialog(mcontext.get(), "Error", "Paper Out!");
                        }
                    } catch (RemoteException | NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        DashboardContainer.backStack();
                        e.printStackTrace();
                    }
                    break;
            }
        }

        private class AdvViewPagerAdapter extends PagerAdapter {


            @Override
            public int getCount() {
                if (Constants.isEcrEnable.equals("Y"))
                    ECR.ECRRespParser(); // will move from here
                if (autoSettleTask && autoSettleTaskStop) {
                    Utility.DEBUG_LOG(TAG, "performSettleauto");
                    Auto_Settle as = new Auto_Settle(); // will move from here
                    Auto_Settle.performSettle();
                }
                return advImages.size();
            }

            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public void destroyItem(ViewGroup view, int position, Object object) {
                view.removeView(advImages.get(position));

            }

            @Override
            public Object instantiateItem(ViewGroup view, int position) {
                view.addView(advImages.get(position));
                return advImages.get(position);
            }

        }

        /**
         * 图片轮播任务
         */


        private class AdvViewPageTask implements Runnable {
            @Override
            public void run() {
                advPosition = (advPosition + 1) % advImageIdList.length;
                mHandler.sendEmptyMessage(0);
            }
        }

        private class ECRMessage implements Runnable {
            @Override
            public void run() {
                if (isSucc == true) {
                    try {
                        ECR.ecrRead(serialPort, mcontext.get());
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                }
            }
        }


//    private Handler mHandler = new Handler() {
//        public void handleMessage(android.os.Message msg) {
//            advViewPaper.setCurrentItem(advPosition);
//        }
//    };

        private class AutoSettlement implements Runnable {
            @Override
            public void run() {
                Utility.DEBUG_LOG(TAG, "Auto Settlement time check");
                String currentTime = new SimpleDateFormat("HHmm", Locale.getDefault()).format(new Date());
                String currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
                Utility.DEBUG_LOG(TAG, "Time:" + currentTime);
                Utility.DEBUG_LOG(TAG, "Date:" + currentDate);
                Utility.DEBUG_LOG(TAG, "Last_Date:" + Constants.last_settle_time);
                Utility.DEBUG_LOG(TAG, "Auto Settle time:" + Constants.AUTO_SETTLETIME);


                int last_settle_time = Integer.parseInt(Constants.AUTO_SETTLETIME);
                int current_time = Integer.parseInt(currentTime);
                Date current_date = null;
                Date last_settle = null;
                try {
                    current_date = new SimpleDateFormat("dd/MM/yyyy").parse(currentDate);
                    last_settle = new SimpleDateFormat("dd/MM/yyyy").parse(Constants.last_settle_time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                ;

                if (current_date.after(last_settle) && current_time > last_settle_time) {
                    Utility.DEBUG_LOG(TAG, "Performing settle auto:");
                    Constants.last_settle_time = currentDate;
                    autoSettleTask = true;
                    autoSettleTaskStop = true;
                }
            }
        }


    }
