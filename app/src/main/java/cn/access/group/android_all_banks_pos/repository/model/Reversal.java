package cn.access.group.android_all_banks_pos.repository.model;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.util.SparseArray;
import android.util.Log;

import java.io.Serializable;

import cn.access.group.android_all_banks_pos.repository.Converters;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

/**
 * on 8/29/2019.
 * this is an entity class which represents transaction detail table in room database
 */
@Entity(tableName = "Reversal")
public class Reversal extends TransactionDetail implements Serializable {
    private static final String TAG = "EMVDemoReversal";
    public void copy(TransactionDetail transactionDetail)
    {
        this.setAmount(transactionDetail.getAmount());
        this.setRrn(transactionDetail.getRrn());
        this.setInvoiceNo(transactionDetail.getInvoiceNo());
        this.setAcquiringInstitutionIdentificationCode(transactionDetail.getAcquiringInstitutionIdentificationCode());
        this.setAuthorizationIdentificationResponseCode(transactionDetail.getAuthorizationIdentificationResponseCode());
        this.setBatchNo(transactionDetail.getBatchNo());
        this.setCancelTxnType(transactionDetail.getCancelTxnType());
        this.setCardexpiry(transactionDetail.getCardexpiry());
        this.setCardHolderName(transactionDetail.getCardHolderName());
        this.setCardNo(transactionDetail.getCardNo());
        this.setCardScheme(transactionDetail.getCardScheme());
        this.setCardType(transactionDetail.getCardType());
        this.setForwardingInstitutionIdentificationCode(transactionDetail.getForwardingInstitutionIdentificationCode());
        this.setMessageType(transactionDetail.getMessageType());
        this.setMId(transactionDetail.getMId());
        this.setProcessingCode(transactionDetail.getProcessingCode());
        this.setStan(transactionDetail.getStan());
        this.setStatus(transactionDetail.getStatus());
        this.setTId(transactionDetail.getTId());
        this.setTag55(transactionDetail.getTag55());
        this.setTipAmount(transactionDetail.getTipAmount());
        this.setTrack2(transactionDetail.getTrack2());
        this.setTxnDate(transactionDetail.getTxnDate());
        this.setTxnTime(transactionDetail.getTxnTime());
        this.setTxnType(transactionDetail.getTxnType());
        //+ taha 20-01-2021 new columns in DB
        this.setPosEntryMode(transactionDetail.getPosEntryMode());
        this.setPanSequenceNumber(transactionDetail.getPanSequenceNumber());
        this.setTipNumAdjustCounter(transactionDetail.getTipNumAdjustCounter());
//        insertTransactionReversal(appDatabase,this);
    }
}
