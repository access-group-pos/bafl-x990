package cn.access.group.android_all_banks_pos.usecase;

import android.annotation.SuppressLint;
import android.util.SparseArray;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.caseA.ISO8583u;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.BATCH_NO;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.INVOICE_NO;

//import static cn.access.group.android_all_banks_pos.Utilities.Constants.BATCH_NO;

/**
 * on 10/15/2019.
 */
public class ReconcileTransaction {
    //private static final String BATCH_NO = ;
    private List<TransactionDetail> transactionToReconcile;
    private List<SparseArray<String>> data8583 = new ArrayList<>();


    @SuppressLint("CheckResult")
    public ReconcileTransaction(AppDatabase appDatabase){
        appDatabase.transactionDetailDao().findByStatusList("approved")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(transactionDetailList -> {
                    transactionToReconcile=transactionDetailList;
                });

    }


    public List<SparseArray<String>> createReconcileTransaction(){

        SparseArray<String> data8583_u_purchase = new SparseArray<>();
        for(int i=0;i<transactionToReconcile.size();i++) {
            data8583_u_purchase.put(ISO8583u.F_MessageType_00, Constants.BATCH_UPLOAD_MESSAGE_TYPE);
            data8583_u_purchase.put(ISO8583u.F_AccountNumber_02, transactionToReconcile.get(i).getCardNo());
            data8583_u_purchase.put(ISO8583u.F_ProcessingCode_03,  transactionToReconcile.get(i).getProcessingCode());
            String   amount = String.format(Locale.getDefault(),"%012d", (int)(Double.parseDouble(transactionToReconcile.get(i).getAmount())*100));
            data8583_u_purchase.put(ISO8583u.F_AmountOfTransactions_04, amount);
            data8583_u_purchase.put(ISO8583u.F_STAN_11, DataFormatterUtil.getNewStan());
            data8583_u_purchase.put(ISO8583u.F_TxTime_12,  transactionToReconcile.get(i).getTxnTime());
            data8583_u_purchase.put(ISO8583u.F_TxDate_13,  transactionToReconcile.get(i).getTxnDate());
           // data8583_u_purchase.put(ISO8583u.F_DateOfExpired_14, transactionToReconcile.get(i).c);
            //14
            if(transactionToReconcile.get(i).getCardType().equals("Chip")){
                data8583_u_purchase.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE_CHIP);
            }
            else {
                data8583_u_purchase.put(ISO8583u.F_POSEntryMode_22, Constants.POS_ENTRY_MODE);
            }
            data8583_u_purchase.put(ISO8583u.F_NII_24, Constants.NII);
            data8583_u_purchase.put(ISO8583u.F_POSConditionCode_25, Constants.POS_CONDITION_CODE);
            data8583_u_purchase.put(ISO8583u.F_RRN_37,  transactionToReconcile.get(i).getRrn());
            data8583_u_purchase.put(ISO8583u.F_TID_41, Constants.TERMINAL_ID);
            data8583_u_purchase.put(ISO8583u.F_MerchantID_42, Constants.MERCHANT_ID);
            data8583_u_purchase.put(ISO8583u.F_ReservedNational_60, Constants.RESERVED_NATIONAL);
            //Constants cn = new Constants();
            data8583_u_purchase.put(ISO8583u.F_KeyExchange_62, INVOICE_NO);//String.format( Locale.getDefault(),"%06d", SharedPref.read("batch_no")));
            if(Constants.HOST_INDEX_FROM_DB==0){
                data8583_u_purchase.put(ISO8583u.F_AuthorizationIdentificationResponseCode_38,transactionToReconcile.get(i).getAuthorizationIdentificationResponseCode());
                data8583_u_purchase.put(ISO8583u.F_KeyExchange_62,transactionToReconcile.get(i).getInvoiceNo());
                ////////////////////////////////////////////////////////msgtype+transstan............................+12spaces...... 22total length
                data8583_u_purchase.put(ISO8583u.F_ReservedNational_60, "0200"+transactionToReconcile.get(i).getStan()+"            ");
            }
            data8583.add(i, data8583_u_purchase);

        }
        return data8583;
    }

    public int reconcilesize(){
        int a =transactionToReconcile.size();
        return a;
    }
}
