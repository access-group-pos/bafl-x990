package cn.access.group.android_all_banks_pos.Utilities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.vfi.smartpos.system_service.aidl.ISystemManager;

import org.json.JSONObject;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.TerminalConfig;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.AMEX_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AUTO_SETTLEMENT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.AUTO_SETTLETIME;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.CURRENCY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.EMV_LOGS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ENABLE_LOGS;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FALLBACK_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FOOTER_LINE_1;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.FOOTER_LINE_2;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_1;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_2;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_3;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.HEADER_LINE_4;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANAGER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MANUAL_ENTRY;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MARKET_SEG;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MERCHANT_NAME;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.MSGHEADER_LENHEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.NII;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PRI_MEDIUM;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalPassword;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalTimeout;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalTxnUpload;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUrl;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUserName;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_IP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_MEDIUM;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SEC_PORT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SETTLECOUNTER;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SETTLERANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SSL_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SUPER_USER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.SYSTEM_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TERMINAL_ID;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TERMINAL_SERIAL;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIPNUMADJUST;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_ENABLED;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TIP_THRESHOLD;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TMS_IP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TMS_PORT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.TOPUP_ENABLE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.USER_PASSWORD_CHANGE;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_AMEX;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_JCB;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_MC;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_PAYPAK;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.VEPS_LIMIT_UPI;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.ecrType;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.hostIP;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.hostPort;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.isEcrEnable;

public class GetRequest {

    public static String TAG = "GetRequest";

    public static void sendGetRequest(Context context, ISystemManager iSystemManager) {
        // appDatabase = AppDatabase.getAppDatabase(context);
        //get working now
        //let's try post and send some data to server
        Utility.DEBUG_LOG(TAG, "+ sendGetRequest +");
        String currentTime = new SimpleDateFormat("HHmmss", Locale.getDefault()).format(new Date());
        String currentDate = new SimpleDateFormat("MMddyy", Locale.getDefault()).format(new Date());
        String POSDateTime = currentTime + currentDate;

        RequestQueue queue = Volley.newRequestQueue(context);
        //TerminalConfig terminalConfig= new TerminalConfig();

//        + Constants.TERMINAL_SERIAL +"&Model=X990&APPNAME=BAF&SerialNumber=IWDF4376";
        String url = PortalUrl + "/baflserviceprofile?MID="+MERCHANT_ID+"&TID=" + TERMINAL_SERIAL + "&Model=X990&APPNAME=BAF&SerialNumber=" + TERMINAL_SERIAL + "&PosDateTime=" + POSDateTime;
//       String url="";
        Utility.DEBUG_LOG(TAG, "url: " + url);
        //String url = "https://api.npoint.io/d6402c99cbec47629dd5";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    // public ProfileInit profileInit;

                    @Override
                    public void onResponse(String response) {
                        Utility.DEBUG_LOG(TAG, "+ onResponse +");
                        // Utility.DEBUG_LOG("Data1", response);
                        try {

                            Map<String, String> runtimeMap = new HashMap<String, String>();

//                    runtimeMap.put("CVM_LIMIT_VISA",CVM_LIMIT_VISA );
//                    runtimeMap.put("CVM_LIMIT_MC",CVM_LIMIT_MC );
//                    runtimeMap.put("CVM_LIMIT_UPI",CVM_LIMIT_UPI );
//                    runtimeMap.put("CVM_LIMIT_PAYPAK",CVM_LIMIT_PAYPAK );
//                    runtimeMap.put("CVM_LIMIT_JCB",CVM_LIMIT_JCB );
//                    runtimeMap.put("CVM_LIMIT_AMEX",CVM_LIMIT_AMEX );
//
//                    runtimeMap.put("SALE_OFFLINE_LIMIT",SALE_OFFLINE_LIMIT );
//                    runtimeMap.put("SALE_MIN_AMOUNT_LIMIT",SALE_MIN_AMOUNT_LIMIT );
//                    runtimeMap.put("SALE_MAX_AMOUNT_LIMIT",SALE_MAX_AMOUNT_LIMIT );
//
//                    runtimeMap.put("VEPS_LIMIT",VEPS_LIMIT );
//                    runtimeMap.put("VEPS_LIMIT_MC",VEPS_LIMIT_MC );
//                    runtimeMap.put("VEPS_LIMIT_AMEX",VEPS_LIMIT_AMEX );
//                    runtimeMap.put("VEPS_LIMIT_JCB",VEPS_LIMIT_JCB );
//                    runtimeMap.put("VEPS_LIMIT_PAYPAK",VEPS_LIMIT_PAYPAK );
//                    runtimeMap.put("VEPS_LIMIT_UPI",VEPS_LIMIT_UPI );

                            JSONObject jsonObject = new JSONObject(response);
                            Utility.DEBUG_LOG(TAG, "response:" + response.toString());
                            if(jsonObject.getString("Reponsemessage").equals("Not available")) {
                                Toast.makeText(context, "Profile: " + jsonObject.getString("Reponsemessage"), Toast.LENGTH_LONG).show();
                            }
                            else{
                                Toast.makeText(context, "Profile: Downloaded Successfully" , Toast.LENGTH_LONG).show();
                            }
//                    for (Map.Entry<String, String> entry : runtimeMap.entrySet()) {
//                        String key = entry.getKey();
//                        String value = entry.getValue();
//                        if ( jsonObject.getString(key) != null )
//                        {
//                            Utility.DEBUG_LOG(TAG,"Fetch " + key + " from response");
//                            value = jsonObject.getString(key);
//                            entry.setValue(value);
//                        }
//                    }


                            MERCHANT_ID = jsonObject.getString("MERCHANTID");
                            TERMINAL_ID = jsonObject.getString("TERMINALID");
                            MERCHANT_NAME = jsonObject.getString("MERCHANTNAME");
                            NII = jsonObject.getString("DESTADDTPDU");
                            TIP_ENABLED = jsonObject.getString("TIPENABLED");
                           // TIP_ENABLED="N";
//                                    COM_ETHERNET, COM_GPRS,
                            ENABLE_LOGS =jsonObject.getString("EMVLOGSENABLED");
                            MARKET_SEG= jsonObject.getString("MARKETSEGMENT");
                            TIP_THRESHOLD = jsonObject.getString("TIPTHERSHOLD");
                            FALLBACK_ENABLE = jsonObject.getString("ALLOWFALLBACK");
                            MSGHEADER_LENHEX=jsonObject.getString("MSGHEADERLENHEX");
                            SSL_ENABLE = jsonObject.getString("ISSSLENABLE");
                            TOPUP_ENABLE = jsonObject.getString("TOPUPENABLED");
                            MANUAL_ENTRY =  jsonObject.getString("TERMINALMANUALENTRY");
                            AUTO_SETTLEMENT = jsonObject.getString("AUTOSETTLE");
                            EMV_LOGS = jsonObject.getString("EMVLOGSENABLED");
                            AUTO_SETTLETIME = jsonObject.getString("AUTOSETTLETIME");
                            AMEX_ID = jsonObject.getString("AMEXID");
//                                    , PIN_BYPASS, MOD10, CHECK_EXPIRY, MANUAL_ENTRY_ISSUER, LAST_4DIGIT_MAG, LAST_4DIGIT_ICC, ACQUIRER, LOW_BIN, HIGH_BIN, ISSUER,
                            CURRENCY = jsonObject.getString("TERMINALCURRENCY1");
                            USER_PASSWORD_CHANGE = jsonObject.getString("PASSWORDUSER");
                            MANAGER_PASSWORD_CHANGE = jsonObject.getString("PASSWORDMANAGEMENT");
                            SYSTEM_PASSWORD_CHANGE = jsonObject.getString("PASSWORDSYSTEM");
                            SUPER_USER_PASSWORD_CHANGE = jsonObject.getString("PASSWORDSUPERUSER");
                            Utility.DEBUG_LOG(TAG,"SYSTEM_PASSWORD_CHANGE :"+jsonObject.getString("PASSWORDSYSTEM"));
                            HEADER_LINE_1 = jsonObject.getString("MERCHANTADDRESS1");
                            HEADER_LINE_2 = jsonObject.getString("MERCHANTADDRESS2");
                            HEADER_LINE_3 = jsonObject.getString("MERCHANTADDRESS3");
                            HEADER_LINE_4 = jsonObject.getString("MERCHANTADDRESS4");
                            FOOTER_LINE_1 = jsonObject.getString("RECEIPTFOOTER1");
                            FOOTER_LINE_2 = jsonObject.getString("RECEIPTFOOTER2");
                            PRI_MEDIUM = jsonObject.getString("PRIMARYMEDIUM");
                            SEC_MEDIUM  =  jsonObject.getString("SECONDARYMEDIUM");
                            TMS_IP= jsonObject.getString("TMSIP");
                            TMS_PORT=  jsonObject.getString("TMSPORT");
//                          BATCH_NO,
                            hostIP = jsonObject.getString("PRIMARYIP");
                            hostPort = Integer.parseInt(jsonObject.getString("PRIMARYPORT"));
                            SEC_IP = jsonObject.getString("SECONDARYIP");
                            SEC_PORT = Integer.parseInt(jsonObject.getString("SECONDARYPORT"));
                            VEPS_LIMIT=jsonObject.getString("VEPSLIMIT");
                            VEPS_LIMIT_MC=jsonObject.getString("MasterQPSLimit".toUpperCase());
                            VEPS_LIMIT_UPI=jsonObject.getString("UpiQPSLimit".toUpperCase());
                            VEPS_LIMIT_PAYPAK=jsonObject.getString("PayPakQPSLimit".toUpperCase());
                            VEPS_LIMIT_JCB=jsonObject.getString("JcbQPSLimit".toUpperCase());
                            VEPS_LIMIT_AMEX=jsonObject.getString("AmexQPSLimit".toUpperCase());
//                                    CVM_LIMIT_VISA, CVM_LIMIT_MC, CVM_LIMIT_UPI, CVM_LIMIT_PAYPAK, CVM_LIMIT_JCB, CVM_LIMIT_AMEX,
//                                    SALE_OFFLINE_LIMIT,
//                                    SALE_MIN_AMOUNT_LIMIT,
//                                    SALE_MAX_AMOUNT_LIMIT,
                            PortalUrl = jsonObject.getString("PORTALURL");
                            PortalUserName = jsonObject.getString("PORTALUSERNAME");
                            PortalPassword = jsonObject.getString("PORTALPASSWORD");
                            PortalTimeout = jsonObject.getString("PORTALTIMEOUT");
                            PortalTxnUpload = jsonObject.getString("PORTALTXNUPLOAD");
                            SETTLECOUNTER = jsonObject.getInt("SETTLECOUNTER");
                            SETTLERANGE = jsonObject.getInt("SETTLERANGE");
                            TIPNUMADJUST = jsonObject.getInt("TIPNUMADJUST");
                            isEcrEnable = jsonObject.getString("ECRENABLED");
                            Utility.DEBUG_LOG("ECR ENABLED",jsonObject.getString("ECRENABLED"));
                            ecrType= jsonObject.getString("ECRTYPE");



//
//                            runtimeMap.get("CVM_LIMIT_VISA"),
//                            runtimeMap.get("CVM_LIMIT_MC"),
//                            runtimeMap.get("CVM_LIMIT_UPI"),
//                            runtimeMap.get("CVM_LIMIT_PAYPAK"),
//                            runtimeMap.get("CVM_LIMIT_JCB"),
//                            runtimeMap.get("CVM_LIMIT_AMEX"),
//
//                            runtimeMap.get("SALE_OFFLINE_LIMIT"),
//                            runtimeMap.get("SALE_MIN_AMOUNT_LIMIT"),
//                            runtimeMap.get("SALE_MAX_AMOUNT_LIMIT"),
//
//                            runtimeMap.get("VEPS_LIMIT"),
//                            runtimeMap.get("VEPS_LIMIT_MC"),
//                            runtimeMap.get("VEPS_LIMIT_UPI"),
//                            runtimeMap.get("VEPS_LIMIT_PAYPAK"),
//                            runtimeMap.get("VEPS_LIMIT_JCB"),
//                            runtimeMap.get("VEPS_LIMIT_AMEX")


//                            USER_PASSWORD_CHANGE = jsonObject.getString("PASSWORDUSER");
//                            MANAGER_PASSWORD_CHANGE = jsonObject.getString("PASSWORDMANAGEMENT");
//                            SYSTEM_PASSWORD_CHANGE = jsonObject.getString("PASSWORDSYSTEM");
//                            SUPER_USER_PASSWORD_CHANGE = jsonObject.getString("PASSWORDSUPERUSER");
                            Repository repository = new Repository(context);
                            TerminalConfig TerminalConfig = repository.getTerminalConfigurationFromUi();
                            repository.updateTerminalConfiguration(TerminalConfig);


//                    profileInit.setSTAN(jsonObject.getString("STAN"));
//                    profileInit.setAMEX_ISSNUM(jsonObject.getString("AMEX_ISSNUM"));
//                    profileInit.setUPI_ADJUSTENABLE(jsonObject.getString("UPI_ADJUSTENABLE"));
//                    profileInit.setMASTER_ADJUSTENABLE(jsonObject.getString("MASTER_ADJUSTENABLE"));
//                    profileInit.setAMEX_MANUALENTRY(jsonObject.getString("AMEX_MANUALENTRY"));
//                    profileInit.setISRECEIPTPRINTED(jsonObject.getString("ISRECEIPTPRINTED"));
//                    profileInit.setTUPPRIMARYIP(jsonObject.getString("TUPPRIMARYIP"));
//                    profileInit.setCONNPROFILESCREATED(jsonObject.getString("CONNPROFILESCREATED"));
//                    profileInit.setISEXTERNALPPCONN(jsonObject.getString("ISEXTERNALPPCONN"));
//                    profileInit.setVISA_LAST4DMAG(jsonObject.getString("VISA_LAST4DMAG"));
//                    profileInit.setPAYPAK_FORCEDLBPENABLE(jsonObject.getString("PAYPAK_FORCEDLBPENABLE"));
//                    profileInit.setPSTNNUMSEC(jsonObject.getString("PSTNNUMSEC"));
//                    profileInit.setVISA_BANKFUNDED(jsonObject.getString("VISA_BANKFUNDED"));
//                    profileInit.setVISA_PRINTEXPIRY(jsonObject.getString("VISA_PRINTEXPIRY"));
//                    profileInit.setMASTER_CHSERVCODE(jsonObject.getString("MASTER_CHSERVCODE"));
//                    profileInit.setPAYPAK_CHECKPIN(jsonObject.getString("PAYPAK_CHECKPIN"));
//                    profileInit.setHELPDESKNUMBER(jsonObject.getString("HELPDESKNUMBER"));
//                    profileInit.setACQNAME(jsonObject.getString("ACQNAME"));
//                    profileInit.setAUDITPRINTDETAILS(jsonObject.getString("AUDITPRINTDETAILS"));
//                    profileInit.setTUPSECONDARYIP(jsonObject.getString("TUPSECONDARYIP"));
//                    profileInit.setPASSWORDMANAGEMENT(jsonObject.getString("PASSWORDMANAGEMENT"));
//                    profileInit.setVISA_MOD10(jsonObject.getString("VISA_MOD10"));
//                    profileInit.setSETGROUP(jsonObject.getString("SETGROUP"));
//                    profileInit.setMASTER_CHSIGN(jsonObject.getString("MASTER_CHSIGN"));
//                    profileInit.setPAYPAK_ISSNAME(jsonObject.getString("PAYPAK_ISSNAME"));
//                    profileInit.setMSGHEADERLENHEX(jsonObject.getString("MSGHEADERLENHEX"));
//                    profileInit.setUPI_SCHEME(jsonObject.getString("UPI_SCHEME"));
//                    profileInit.setSECONDARYPORT(jsonObject.getString("SECONDARYPORT"));
//                    profileInit.setVISA_PINBYPASS(jsonObject.getString("VISA_PINBYPASS"));
//                    profileInit.setDESTADDTPDU(jsonObject.getString("DESTADDTPDU"));
//                    profileInit.setFLASH(jsonObject.getString("FLASH"));
//                    profileInit.setJCB_FORCEDLBPENABLE(jsonObject.getString("JCB_FORCEDLBPENABLE"));
//                    profileInit.setAMEX_CHSERVCODE(jsonObject.getString("AMEX_CHSERVCODE"));
//                    profileInit.setPAYPAK_CHSIGN(jsonObject.getString("PAYPAK_CHSIGN"));
//                    profileInit.setCOUNTRYCODE(jsonObject.getString("COUNTRYCODE"));
//                    profileInit.setPSTNTIMEOUT(jsonObject.getString("PSTNTIMEOUT"));
//                    profileInit.setSAVEDEODPRINTDETAILS(jsonObject.getString("SAVEDEODPRINTDETAILS"));
//                    profileInit.setPRTPOWERFAILURE(jsonObject.getString("PRTPOWERFAILURE"));
//                    profileInit.setPASSWORDRETRYON(jsonObject.getString("PASSWORDRETRYON"));
//                    profileInit.setJCB_LAST4DMAG(jsonObject.getString("JCB_LAST4DMAG"));
//                    profileInit.setCLOCK(jsonObject.getString("CLOCK"));
//                    profileInit.setTERMINALIP(jsonObject.getString("TERMINALIP"));
//                    profileInit.setPSTNNUMPRI(jsonObject.getString("PSTNNUMPRI"));
//                    profileInit.setBATCHNUMBER(jsonObject.getString("BATCHNUMBER"));
//                    profileInit.setMASTER_PINBYPASS(jsonObject.getString("MASTER_PINBYPASS"));
//                    profileInit.setPAYPAK_LAST4DSMART(jsonObject.getString("PAYPAK_LAST4DSMART"));
//                    profileInit.setTERMINALNETMASK(jsonObject.getString("TERMINALNETMASK"));
//                    profileInit.setDECIMALSEPARATOR(jsonObject.getString("DECIMALSEPARATOR"));
//                    profileInit.setPSTNINTERCHARTO(jsonObject.getString("PSTNINTERCHARTO"));
//                    profileInit.setSETTLECOUNTER(jsonObject.getString("SETTLECOUNTER"));
//                    profileInit.setACQNUM(jsonObject.getString("ACQNUM"));
//                    profileInit.setMASTER_PREAUTHENABLE(jsonObject.getString("MASTER_PREAUTHENABLE"));
//                    profileInit.setMARKETSEGMENT(jsonObject.getString("MARKETSEGMENT"));
//                    profileInit.setAUTOSETTLE(jsonObject.getString("AUTOSETTLE"));
//                    profileInit.setTERMINALDNS2(jsonObject.getString("TERMINALDNS2"));
//                    profileInit.setTERMINALDNS1(jsonObject.getString("TERMINALDNS1"));
//                    profileInit.setJCB_CHSERVCODE(jsonObject.getString("JCB_CHSERVCODE"));
//                    profileInit.setBULKDOWNLOADPENDING(jsonObject.getString("BULKDOWNLOADPENDING"));
//                    profileInit.setTUPPRIMARYPORT(jsonObject.getString("TUPPRIMARYPORT"));
//                    profileInit.setTERMINALCOUNTRYCODE(jsonObject.getString("TERMINALCOUNTRYCODE"));
//                    profileInit.setVISA_SCHEME(jsonObject.getString("VISA_SCHEME"));
//                    profileInit.setMASTER_LAST4DMAG(jsonObject.getString("MASTER_LAST4DMAG"));
//                    profileInit.setJCB_MANUALENTRY(jsonObject.getString("JCB_MANUALENTRY"));
//                    profileInit.setSTAN(jsonObject.getString("STAN"));
//                    profileInit.setPAYPAK_ACQUIRER(jsonObject.getString("PAYPAK_ACQUIRER"));
//                    profileInit.setPAYPAK_MOD10(jsonObject.getString("PAYPAK_MOD10"));
//                    profileInit.setISSIMSWDEBUGENABLE(jsonObject.getString("ISSIMSWDEBUGENABLE"));
//                    profileInit.setJCB_SCHEME(jsonObject.getString("JCB_SCHEME"));
//                    profileInit.setPRINTBARCODE(jsonObject.getString("PRINTBARCODE"));
//                    profileInit.setTERMINALID(jsonObject.getString("TERMINALID"));
//                    profileInit.setAMEXID(jsonObject.getString("AMEXID"));
//                    profileInit.setJCB_PRINTEXPIRY(jsonObject.getString("JCB_PRINTEXPIRY"));
//                    profileInit.setCLIENTNAME(jsonObject.getString("CLIENTNAME"));
//                    profileInit.setMASTER_LAST4DSMART(jsonObject.getString("MASTER_LAST4DSMART"));
//                    profileInit.setUPI_ADVCASHENABLE(jsonObject.getString("UPI_ADVCASHENABLE"));
//                    profileInit.setTUPSSLENABLE(jsonObject.getString("TUPSSLENABLE"));
//                    profileInit.setVISA_MANUALENTRY(jsonObject.getString("VISA_MANUALENTRY"));
//                    profileInit.setUPI_COMPLETIONENABLE(jsonObject.getString("UPI_COMPLETIONENABLE"));
//                    profileInit.setMASTER_ACQUIRER(jsonObject.getString("MASTER_ACQUIRER"));
//                    profileInit.setJCB_BANKFUNDED(jsonObject.getString("JCB_BANKFUNDED"));
//                    profileInit.setTERMINALWIFIDHCP(jsonObject.getString("TERMINALWIFIDHCP"));
//                    profileInit.setAUTOSETTLETIME(jsonObject.getString("AUTOSETTLETIME"));
//                    profileInit.setAMEX_LAST4DSMART(jsonObject.getString("AMEX_LAST4DSMART"));
//                    profileInit.setEMVCOUNTER(jsonObject.getString("EMVCOUNTER"));
//                    profileInit.setRECEIVETIMEOUT(jsonObject.getString("RECEIVETIMEOUT"));
//                    profileInit.setCARDTIMEOUT(jsonObject.getString("CARDTIMEOUT"));
//                    profileInit.setSERVICEFIRSTTIME(jsonObject.getString("SERVICEFIRSTTIME"));
//                    profileInit.setISACTIVATED(jsonObject.getString("ISACTIVATED"));
//                    profileInit.setAMEX_PRINTEXPIRY(jsonObject.getString("AMEX_PRINTEXPIRY"));
//                    profileInit.setJCB_VOIDENABLE(jsonObject.getString("JCB_VOIDENABLE"));
//                    profileInit.setPASSWRD(jsonObject.getString("PASSWRD"));
//                    profileInit.setTIPENABLED(jsonObject.getString("TIPENABLED"));
//                    profileInit.setMASTER_ISSUERACTIVATED(jsonObject.getString("MASTER_ISSUERACTIVATED"));
//                    profileInit.setUPI_PRINTEXPIRY(jsonObject.getString("UPI_PRINTEXPIRY"));
//                    profileInit.setMASTER_BANKFUNDED(jsonObject.getString("MASTER_BANKFUNDED"));
//                    profileInit.setOFFLINEPIN(jsonObject.getString("OFFLINEPIN"));
//                    profileInit.setPOSSERIALNO(jsonObject.getString("POSSERIALNO"));
//                    profileInit.setPAYPAK_COMPLETIONENABLE(jsonObject.getString("PAYPAK_COMPLETIONENABLE"));
//                    profileInit.setTOPUPPIN(jsonObject.getString("TOPUPPIN"));
//                    profileInit.setPAYPAK_REFUNDENABLE(jsonObject.getString("PAYPAK_REFUNDENABLE"));
//                    profileInit.setTERMINALGATEWAY(jsonObject.getString("TERMINALGATEWAY"));
//                    profileInit.setUPI_REFUNDENABLE(jsonObject.getString("UPI_REFUNDENABLE"));
//                    profileInit.setAMEX_MOD10(jsonObject.getString("AMEX_MOD10"));
//                    profileInit.setVISA_COMPLETIONENABLE(jsonObject.getString("VISA_COMPLETIONENABLE"));
//                    profileInit.setPAYPAK_ADVCASHENABLE(jsonObject.getString("PAYPAK_ADVCASHENABLE"));
//                    profileInit.setHELPTOUCHMSG(jsonObject.getString("HELPTOUCHMSG"));
//                    profileInit.setTERMINALWIFIGATEWAY(jsonObject.getString("TERMINALWIFIGATEWAY"));
//                    profileInit.setUPI_ISSNAME(jsonObject.getString("UPI_ISSNAME"));
//                    profileInit.setEODPRINTDETAILS(jsonObject.getString("EODPRINTDETAILS"));
//                    profileInit.setPAYPAK_VOIDENABLE(jsonObject.getString("PAYPAK_VOIDENABLE"));
//                    profileInit.setTUPSECONDARYPORT(jsonObject.getString("TUPSECONDARYPORT"));
//                    profileInit.setRSPTIMEOUT(jsonObject.getString("RSPTIMEOUT"));
//                    profileInit.setPAYPAK_ISSUERACTIVATED(jsonObject.getString("PAYPAK_ISSUERACTIVATED"));
//                    profileInit.setPRIMARYIP(jsonObject.getString("PRIMARYIP"));
//                    profileInit.setVISA_ISSNUM(jsonObject.getString("VISA_ISSNUM"));
//                    profileInit.setCASHBACKENABLED(jsonObject.getString("CASHBACKENABLED"));
//                    profileInit.setPANLENGTHMAX(jsonObject.getString("PANLENGTHMAX"));
//                    profileInit.setTERMINALWIFIIP(jsonObject.getString("TERMINALWIFIIP"));
//                    profileInit.setPSTNSETTLNUMSEC(jsonObject.getString("PSTNSETTLNUMSEC"));
//                    profileInit.setJCB_ADJUSTENABLE(jsonObject.getString("JCB_ADJUSTENABLE"));
//                    profileInit.setAMEX_ISSNAME(jsonObject.getString("AMEX_ISSNAME"));
//                    profileInit.setALLOWFALLBACK(jsonObject.getString("ALLOWFALLBACK"));
//                    profileInit.setVISA_ISSNAME(jsonObject.getString("VISA_ISSNAME"));
//                    profileInit.setMASTER_FORCEDLBPENABLE(jsonObject.getString("MASTER_FORCEDLBPENABLE"));
//                    profileInit.setMASTER_MOD10(jsonObject.getString("MASTER_MOD10"));
//                    profileInit.setJCB_ISSNUM(jsonObject.getString("JCB_ISSNUM"));
//                    profileInit.setISLOCALHOST(jsonObject.getString("ISLOCALHOST"));
//                    profileInit.setVCSETHTIMEOUT(jsonObject.getString("VCSETHTIMEOUT"));
//                    profileInit.setWIFISSID(jsonObject.getString("WIFISSID"));
//                    profileInit.setTERMINALWIFIDNS2(jsonObject.getString("TERMINALWIFIDNS2"));
//                    profileInit.setPSTNPREFIX(jsonObject.getString("PSTNPREFIX"));
//                    profileInit.setSECONDARYMEDIUM(jsonObject.getString("SECONDARYMEDIUM"));
//                    profileInit.setTERMINALWIFIDNS1(jsonObject.getString("TERMINALWIFIDNS1"));
//                    profileInit.setPAYPAK_ISSNUM(jsonObject.getString("PAYPAK_ISSNUM"));
//                    profileInit.setAMEX_SCHEME(jsonObject.getString("AMEX_SCHEME"));
//                    profileInit.setTHOUSANDSEPARATOR(jsonObject.getString("THOUSANDSEPARATOR"));
//                    profileInit.setAGENTCODE(jsonObject.getString("AGENTCODE"));
//                    profileInit.setPRINTBANKRECON(jsonObject.getString("PRINTBANKRECON"));
//                    profileInit.setVISA_FORCEDLBPENABLE(jsonObject.getString("VISA_FORCEDLBPENABLE"));
//                    profileInit.setUPI_CHECKEXPIRY(jsonObject.getString("UPI_CHECKEXPIRY"));
//                    profileInit.setPRINTREVERSALRCPT(jsonObject.getString("PRINTREVERSALRCPT"));
//                    profileInit.setINVOICENUMBER(jsonObject.getString("INVOICENUMBER"));
//                    profileInit.setBOOTFIRSTTIME(jsonObject.getString("BOOTFIRSTTIME"));
//                    profileInit.setTERMINALDHCP(jsonObject.getString("TERMINALDHCP"));
//                    profileInit.setPREAUTHMAXCOMPLETIONDAYS(jsonObject.getString("PREAUTHMAXCOMPLETIONDAYS"));
//                    profileInit.setCOMPLETIONTHERSHOLD(jsonObject.getString("COMPLETIONTHERSHOLD"));
//                    profileInit.setMASTER_REFUNDENABLE(jsonObject.getString("MASTER_REFUNDENABLE"));
//                    profileInit.setUPI_FORCEDLBPENABLE(jsonObject.getString("UPI_FORCEDLBPENABLE"));
//                    profileInit.setMASTER_MANUALENTRY(jsonObject.getString("MASTER_MANUALENTRY"));
//                    profileInit.setPRINTEMVLOGS(jsonObject.getString("PRINTEMVLOGS"));
//                    profileInit.setJCB_REFUNDENABLE(jsonObject.getString("JCB_REFUNDENABLE"));
//                    profileInit.setUPI_ISSUERACTIVATED(jsonObject.getString("UPI_ISSUERACTIVATED"));
//                    profileInit.setPASSWORDSUPERUSER(jsonObject.getString("PASSWORDSUPERUSER"));
//                    profileInit.setVISA_ISSUERACTIVATED(jsonObject.getString("VISA_ISSUERACTIVATED"));
//                    profileInit.setUPI_PREAUTHENABLE(jsonObject.getString("UPI_PREAUTHENABLE"));
//                    profileInit.setPAYPAK_CHECKEXPIRY(jsonObject.getString("PAYPAK_CHECKEXPIRY"));
//                    profileInit.setISSTATUSDEBUGENABLE(jsonObject.getString("ISSTATUSDEBUGENABLE"));
//                    profileInit.setTPKGISKE(jsonObject.getString("TPKGISKE"));
//                    profileInit.setMASTER_COMPLETIONENABLE(jsonObject.getString("MASTER_COMPLETIONENABLE"));
//                    profileInit.setLANGUAGEDEFAULT(jsonObject.getString("LANGUAGEDEFAULT"));
//                    profileInit.setUPI_MANUALENTRY(jsonObject.getString("UPI_MANUALENTRY"));
//                    profileInit.setVISA_LAST4DSMART(jsonObject.getString("VISA_LAST4DSMART"));
//                    profileInit.setAMEX_ADVCASHENABLE(jsonObject.getString("AMEX_ADVCASHENABLE"));
//                    profileInit.setPSTNSETTLNUMPRI(jsonObject.getString("PSTNSETTLNUMPRI"));
//                    profileInit.setVISA_PREAUTHENABLE(jsonObject.getString("VISA_PREAUTHENABLE"));
//                    profileInit.setTIPTHERSHOLD(jsonObject.getString("TIPTHERSHOLD"));
//                    profileInit.setMASTER_SCHEME(jsonObject.getString("MASTER_SCHEME"));
//                    profileInit.setUPI_ISSNUM(jsonObject.getString("UPI_ISSNUM"));
//                    profileInit.setPASSWORDUSER(jsonObject.getString("PASSWORDUSER"));
//                    profileInit.setAMEX_REFUNDENABLE(jsonObject.getString("AMEX_REFUNDENABLE"));
//                    profileInit.setPREAUTHCLEARCOUNT(jsonObject.getString("PREAUTHCLEARCOUNT"));
//                    profileInit.setLANGUAGE(jsonObject.getString("LANGUAGE"));
//                    profileInit.setRECEIPTFOOTER1(jsonObject.getString("RECEIPTFOOTER1"));
//                    profileInit.setRECEIPTFOOTER2(jsonObject.getString("RECEIPTFOOTER2"));
//                    profileInit.setAMEX_ISSUERACTIVATED(jsonObject.getString("AMEX_ISSUERACTIVATED"));
//                    profileInit.setJCB_CHECKPIN(jsonObject.getString("JCB_CHECKPIN"));
//                    profileInit.setCLIENTTYPE(jsonObject.getString("CLIENTTYPE"));
//                    profileInit.setUPI_MOD10(jsonObject.getString("UPI_MOD10"));
//                    profileInit.setPAYPAK_SCHEME(jsonObject.getString("PAYPAK_SCHEME"));
//                    profileInit.setUPI_ACQUIRER(jsonObject.getString("UPI_ACQUIRER"));
//                    profileInit.setUPI_CHSIGN(jsonObject.getString("UPI_CHSIGN"));
//                    profileInit.setMASTER_CHECKPIN(jsonObject.getString("MASTER_CHECKPIN"));
//                    profileInit.setVISA_ADJUSTENABLE(jsonObject.getString("VISA_ADJUSTENABLE"));
//                    profileInit.setUPI_LAST4DMAG(jsonObject.getString("UPI_LAST4DMAG"));
//                    profileInit.setAMEX_FORCEDLBPENABLE(jsonObject.getString("AMEX_FORCEDLBPENABLE"));
//                    profileInit.setPAYPAK_PRINTEXPIRY(jsonObject.getString("PAYPAK_PRINTEXPIRY"));
//                    profileInit.setBTMACADDRESS(jsonObject.getString("BTMACADDRESS"));
//                    profileInit.setVISA_REFUNDENABLE(jsonObject.getString("VISA_REFUNDENABLE"));
//                    profileInit.setSECONDARYIP(jsonObject.getString("SECONDARYIP"));
//                    profileInit.setJCB_CHECKEXPIRY(jsonObject.getString("JCB_CHECKEXPIRY"));
//                    profileInit.setISSSLENABLE(jsonObject.getString("ISSSLENABLE"));
//                    profileInit.setENABLEAUTOPINENTER(jsonObject.getString("ENABLEAUTOPINENTER"));
//                    profileInit.setLANGUAGEACQNUM(jsonObject.getString("LANGUAGEACQNUM"));
//                    profileInit.setPAYPAK_ADJUSTENABLE(jsonObject.getString("PAYPAK_ADJUSTENABLE"));
//                    profileInit.setJCB_ADVCASHENABLE(jsonObject.getString("JCB_ADVCASHENABLE"));
//                    profileInit.setTIPNUMADJUST(jsonObject.getString("TIPNUMADJUST"));
//                    profileInit.setTERMFORCEDLBPMAGENABLE(jsonObject.getString("TERMFORCEDLBPMAGENABLE"));
//                    profileInit.setVISA_ADVCASHENABLE(jsonObject.getString("VISA_ADVCASHENABLE"));
//                    profileInit.setCTLSENABLED(jsonObject.getString("CTLSENABLED"));
//                    profileInit.setUPI_PINBYPASS(jsonObject.getString("UPI_PINBYPASS"));
//                    profileInit.setPAYPAK_BANKFUNDED(jsonObject.getString("PAYPAK_BANKFUNDED"));
//                    profileInit.setJCB_ACQUIRER(jsonObject.getString("JCB_ACQUIRER"));
//                    profileInit.setAMEX_COMPLETIONENABLE(jsonObject.getString("AMEX_COMPLETIONENABLE"));
//                    profileInit.setF49ENABLE(jsonObject.getString("F49ENABLE"));
//                    profileInit.setTOPUPENABLED(jsonObject.getString("TOPUPENABLED"));
//                    profileInit.setUPI_CHSERVCODE(jsonObject.getString("UPI_CHSERVCODE"));
//                    profileInit.setUPDATEME(jsonObject.getString("UPDATEME"));
//                    profileInit.setAMEX_PREAUTHENABLE(jsonObject.getString("AMEX_PREAUTHENABLE"));
//                    profileInit.setEMVLOGSENABLED(jsonObject.getString("EMVLOGSENABLED"));
//                    profileInit.setTMSIP(jsonObject.getString("TMSIP"));
//                    profileInit.setVISA_CHSIGN(jsonObject.getString("VISA_CHSIGN"));
//                    profileInit.setAMEX_CHECKEXPIRY(jsonObject.getString("AMEX_CHECKEXPIRY"));
//                    profileInit.setMERCHANTID(jsonObject.getString("MERCHANTID"));
//                    profileInit.setMASTER_VOIDENABLE(jsonObject.getString("MASTER_VOIDENABLE"));
//                    profileInit.setVENDORCODE(jsonObject.getString("VENDORCODE"));
//                    profileInit.setRECEIPTPRINTED(jsonObject.getString("RECEIPTPRINTED"));
//                    profileInit.setJCB_CHSIGN(jsonObject.getString("JCB_CHSIGN"));
//                    profileInit.setVISA_VOIDENABLE(jsonObject.getString("VISA_VOIDENABLE"));
//                    profileInit.setMASTER_ISSNAME(jsonObject.getString("MASTER_ISSNAME"));
//                    profileInit.setReponsemessage(jsonObject.getString("Reponsemessage"));
//                    profileInit.setHIDECARDHOLDERNAME(jsonObject.getString("HIDECARDHOLDERNAME"));
//                    profileInit.setMERCHANTNAME(jsonObject.getString("MERCHANTNAME"));
//                    profileInit.setJCB_MOD10(jsonObject.getString("JCB_MOD10"));
//                    profileInit.setPRINTDECLINEDRCPT(jsonObject.getString("PRINTDECLINEDRCPT"));
//                    profileInit.setUPI_LAST4DSMART(jsonObject.getString("UPI_LAST4DSMART"));
//                    profileInit.setADVCASHENABLE(jsonObject.getString("ADVCASHENABLE"));
//                    profileInit.setAMEX_LAST4DMAG(jsonObject.getString("AMEX_LAST4DMAG"));
//                    profileInit.setUPI_VOIDENABLE(jsonObject.getString("UPI_VOIDENABLE"));
//                    profileInit.setPREAUTHBATCHMAXCOUNT(jsonObject.getString("PREAUTHBATCHMAXCOUNT"));
//                    profileInit.setADVICEENABLED(jsonObject.getString("ADVICEENABLED"));
//                    profileInit.setTERMFORCEDLBPMANUALENABLE(jsonObject.getString("TERMFORCEDLBPMANUALENABLE"));
//                    profileInit.setMASTER_ADVCASHENABLE(jsonObject.getString("MASTER_ADVCASHENABLE"));
//                    profileInit.setPAYPAK_PREAUTHENABLE(jsonObject.getString("PAYPAK_PREAUTHENABLE"));
//                    profileInit.setAMEX_VOIDENABLE(jsonObject.getString("AMEX_VOIDENABLE"));
//                    profileInit.setPAYPAK_PINBYPASS(jsonObject.getString("PAYPAK_PINBYPASS"));
//                    profileInit.setAMEX_BANKFUNDED(jsonObject.getString("AMEX_BANKFUNDED"));
//                    profileInit.setMERCHANTADDRESS3(jsonObject.getString("MERCHANTADDRESS3"));
//                    profileInit.setMERCHANTADDRESS2(jsonObject.getString("MERCHANTADDRESS2"));
//                    profileInit.setMERCHANTADDRESS1(jsonObject.getString("MERCHANTADDRESS1"));
//                    profileInit.setJCB_PREAUTHENABLE(jsonObject.getString("JCB_PREAUTHENABLE"));
//                    profileInit.setUPI_CHECKPIN(jsonObject.getString("UPI_CHECKPIN"));
//                    profileInit.setUSERID(jsonObject.getString("USERID"));
//                    profileInit.setTERMINALMANUALENTRY(jsonObject.getString("TERMINALMANUALENTRY"));
//                    profileInit.setECRENABLED(jsonObject.getString("ECRENABLED"));
//                    profileInit.setVISA_CHSERVCODE(jsonObject.getString("VISA_CHSERVCODE"));
//                    profileInit.setMERCHANTADDRESS4(jsonObject.getString("MERCHANTADDRESS4"));
//                    profileInit.setVISA_CHECKEXPIRY(jsonObject.getString("VISA_CHECKEXPIRY"));
//                    profileInit.setPINTIMEOUT(jsonObject.getString("PINTIMEOUT"));
//                    profileInit.setPREDIALENABLED(jsonObject.getString("PREDIALENABLED"));
//                    profileInit.setPRINTSERIALNUM(jsonObject.getString("PRINTSERIALNUM"));
//                    profileInit.setPANLENGTHMIN(jsonObject.getString("PANLENGTHMIN"));
//                    profileInit.setPOWERDOWNTIME(jsonObject.getString("POWERDOWNTIME"));
//                    profileInit.setSETTLERANGE(jsonObject.getString("SETTLERANGE"));
//                    profileInit.setMASTER_CHECKEXPIRY(jsonObject.getString("MASTER_CHECKEXPIRY"));
//                    profileInit.setAMEX_ADJUSTENABLE(jsonObject.getString("AMEX_ADJUSTENABLE"));
//                    profileInit.setPAYPAK_MANUALENTRY(jsonObject.getString("PAYPAK_MANUALENTRY"));
//                    profileInit.setJCB_ISSUERACTIVATED(jsonObject.getString("JCB_ISSUERACTIVATED"));
//                    profileInit.setSRCADDTPDU(jsonObject.getString("SRCADDTPDU"));
//                    profileInit.setINITTERM(jsonObject.getString("INITTERM"));
//                    profileInit.setPRIMARYPORT(jsonObject.getString("PRIMARYPORT"));
//                    profileInit.setISTPDUENABLE(jsonObject.getString("ISTPDUENABLE"));
//                    profileInit.setVISA_ACQUIRER(jsonObject.getString("VISA_ACQUIRER"));
//                    profileInit.setAMEX_ACQUIRER(jsonObject.getString("AMEX_ACQUIRER"));
//                    profileInit.setAMEX_CHECKPIN(jsonObject.getString("AMEX_CHECKPIN"));
//                    profileInit.setBATCHMAXCOUNT(jsonObject.getString("BATCHMAXCOUNT"));
//                    profileInit.setPRIMARYMEDIUM(jsonObject.getString("PRIMARYMEDIUM"));
//                    profileInit.setVISA_CHECKPIN(jsonObject.getString("VISA_CHECKPIN"));
//                    profileInit.setMASTER_ISSNUM(jsonObject.getString("MASTER_ISSNUM"));
//                    profileInit.setPASSWORDSYSTEM(jsonObject.getString("PASSWORDSYSTEM"));
//                    profileInit.setJCB_ISSNAME(jsonObject.getString("JCB_ISSNAME"));
//                    profileInit.setTMSPORT(jsonObject.getString("TMSPORT"));
//                    profileInit.setINTERPACKETTIMEOUT(jsonObject.getString("INTERPACKETTIMEOUT"));
//                    profileInit.setPRINTCARDREMRCPT(jsonObject.getString("PRINTCARDREMRCPT"));
//                    profileInit.setAMEX_PINBYPASS(jsonObject.getString("AMEX_PINBYPASS"));
//                    profileInit.setTERMINALWIFINETMASK(jsonObject.getString("TERMINALWIFINETMASK"));
//                    profileInit.setJCB_LAST4DSMART(jsonObject.getString("JCB_LAST4DSMART"));
//                    profileInit.setAMEX_CHSIGN(jsonObject.getString("AMEX_CHSIGN"));
//                    profileInit.setPAYPAK_CHSERVCODE(jsonObject.getString("PAYPAK_CHSERVCODE"));
//                    profileInit.setTERMINALTOTCURRENCIES(jsonObject.getString("TERMINALTOTCURRENCIES"));
//                    profileInit.setISDEBUGENABLE(jsonObject.getString("ISDEBUGENABLE"));
//                    profileInit.setPAYPAK_LAST4DMAG(jsonObject.getString("PAYPAK_LAST4DMAG"));
//                    profileInit.setPSTNINITNUMBER(jsonObject.getString("PSTNINITNUMBER"));
//                    profileInit.setJCB_COMPLETIONENABLE(jsonObject.getString("JCB_COMPLETIONENABLE"));
//                    profileInit.setAMOUNTENTRYTIMEOUT(jsonObject.getString("AMOUNTENTRYTIMEOUT"));
//                    profileInit.setWIFIPASSWORD(jsonObject.getString("WIFIPASSWORD"));
//                    profileInit.setTERMFORCEDLBPENABLE(jsonObject.getString("TERMFORCEDLBPENABLE"));
//                    profileInit.setHELPALFAMSG(jsonObject.getString("HELPALFAMSG"));
//                    profileInit.setUPI_BANKFUNDED(jsonObject.getString("UPI_BANKFUNDED"));
//                    profileInit.setTERMINALCURRENCY2(jsonObject.getString("TERMINALCURRENCY2"));
//                    profileInit.setTERMINALCURRENCY1(jsonObject.getString("TERMINALCURRENCY1"));
//                    profileInit.setTERMINALAPN(jsonObject.getString("TERMINALAPN"));
//                    profileInit.setJCB_PINBYPASS(jsonObject.getString("JCB_PINBYPASS"));
//                    profileInit.setMASTER_PRINTEXPIRY(jsonObject.getString("MASTER_PRINTEXPIRY"));
//
//                    insertTransaction(appDatabase,profileInit);
//                    Utility.DEBUG_LOG("SL",profileInit.getSTAN());
//                    insertTransaction(appDatabase,profileInit);
                            // iSystemManager.reboot();
                            ProfileAck.makeJsonObjReq(context, iSystemManager);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Utility.DEBUG_LOG("RTSG", "Failed to Parse Json");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context, "Error! :" + error.toString(), Toast.LENGTH_LONG).show();
                Utility.DEBUG_LOG("RTSG", "Failed to Parse Json " + error);
            }
        }) {
            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        ;
        new NukeSSLCerts().nuke();
        queue.add(stringRequest);
    }

    @SuppressLint("CheckResult")
    private static void insertTransaction(final AppDatabase db, TerminalConfig transactionDetail) {
        Completable.fromAction(() -> db.terminalConfigDao().insertConfig(transactionDetail))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                        }// completed with success
                        , throwable -> {
                        }// there was an error
                );
    }

    public static class NukeSSLCerts {
        protected static final String TAG = "NukeSSLCerts";

        public static void nuke() {
            try {
                TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            public X509Certificate[] getAcceptedIssuers() {
                                X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                                return myTrustedAnchors;
                            }

                            @Override
                            public void checkClientTrusted(X509Certificate[] certs, String authType) {
                            }

                            @Override
                            public void checkServerTrusted(X509Certificate[] certs, String authType) {
                            }
                        }
                };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String arg0, SSLSession arg1) {
                        return true;
                    }
                });
            } catch (Exception e) {
            }
        }
    }


}
