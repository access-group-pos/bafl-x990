package cn.access.group.android_all_banks_pos.Utilities.canvas_printer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.vfi.smartpos.deviceservice.aidl.IPrinter;
import com.vfi.smartpos.deviceservice.aidl.PrinterConfig;
import com.vfi.smartpos.deviceservice.aidl.PrinterListener;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import cn.access.group.android_all_banks_pos.DashboardContainer;
import cn.access.group.android_all_banks_pos.R;
import cn.access.group.android_all_banks_pos.Utilities.Constants;
import cn.access.group.android_all_banks_pos.Utilities.DataFormatterUtil;
import cn.access.group.android_all_banks_pos.Utilities.DialogUtil;
import cn.access.group.android_all_banks_pos.Utilities.ServiceHelper;
import cn.access.group.android_all_banks_pos.Utilities.Utility;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.CountedTransactionItem;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;
import cn.access.group.android_all_banks_pos.viewfragments.HomeFragment;
import cn.access.group.android_all_banks_pos.viewfragments.TransactionDetailViewFragment;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication.hideProgressDialog;
import static cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication.showProgressDialog;
import static cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication.successPrintingHide;
import static cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication.successPrintingShow;
import static java.lang.Thread.sleep;

/**
 * Created by Simon on 2019/2/1.
 */

/**
 * Class {@code PrinterCanvas} is for calling PrinterEx to draw PrinterItem on canvas
 * and the canvas can be set to printer or activity
 */

public class PrinterCanvas {

    static final String TAG = "PrinterCanvas";
    public enum PrinterState {PS_NA, PS_Printing, PS_Finish, PS_PaperOut};

    public static IPrinter iPrinter;
    public static Context context;
    List<TransactionDetail> transactionDetailList;
    List<CountedTransactionItem> countedTransactionItemList;

    public boolean printFinished = false;
    public PrinterState printerState = PrinterState.PS_NA;
    // for print
    public static PrinterEx printerEx;

    protected List<PrinterItem> printerItems = null;
    int offsetX = 0;
    int fontsize = 1;
    int offsetX1 = 0;
    int fontsize1 = 1;
    private Bitmap bitmap;

    public PrinterCanvas(Context context, List<TransactionDetail> transactionDetailList, List<CountedTransactionItem> countedTransactionItemList) {
        this.context = context;
        Utility.DEBUG_LOG("CAnvas1", String.valueOf(transactionDetailList.size()));
        this.transactionDetailList = transactionDetailList;
        this.countedTransactionItemList = countedTransactionItemList;
    }

//    public void initializeData( List<TransactionDetail> transactionDetailList, List<CountedTransactionItem> countedTransactionItemList){
//        Utility.DEBUG_LOG(TAG, "initializeData");
//
//        this.transactionDetailList = transactionDetailList;
//        this.countedTransactionItemList = countedTransactionItemList;
//        Utility.DEBUG_LOG("CAnvas", String.valueOf(transactionDetailList.size()));
//        for (PrinterItem p: PrinterItem.values()
//        ) {
//            p.restore();
//        }
//
//
//    }


    public PrinterCanvas(Context context) {
        this.context = context;
        // appDatabase = AppDatabase.getAppDatabase(context);
    }

//    public PrinterCanvas(Context context) {
//
//    }

    public void initializeData(Bundle extraitems) {
        Utility.DEBUG_LOG(TAG, "initializeData");
        for (PrinterItem p : PrinterItem.values()
        ) {
            p.restore();
        }
    }


    private void initializeDefaltRecp() {
        printerItems = new ArrayList<>();

        for (PrinterItem p : PrinterItem.values()
        ) {
            printerItems.add(p);
        }
    }

    public static void initialize(IPrinter iprinter1) {
        PrinterCanvas.iPrinter = iprinter1;

        Utility.DEBUG_LOG("Printer Canvas", String.valueOf(PrinterCanvas.iPrinter));
    }
//    public void initialize(IPrinter iprinter1, List<TransactionDetail> transactionDetailList, List<CountedTransactionItem> countedTransactionItemList) {
//        PrinterCanvas.iPrinter = iprinter1;
//        Utility.DEBUG_LOG("CAnvas1", String.valueOf(transactionDetailList.size()));
//        this.transactionDetailList = transactionDetailList;
//        this.countedTransactionItemList = countedTransactionItemList;
//        Utility.DEBUG_LOG("Printer Canvas", String.valueOf(PrinterCanvas.iPrinter));
//    }


    public Bundle getParameterFromBitmap(PrinterElement printerElement, Bitmap bitmap) {
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        Bundle fmtImage = new Bundle();
        fmtImage.putInt("width", width);  // bigger then actual, will print the actual
        fmtImage.putInt("height", height); // bigger then actual, will print the actual

        if ((printerElement.style & PrinterDefine.PStyle_align_left) == PrinterDefine.PStyle_align_left) {
            //
            fmtImage.putInt("offset", 0);
        } else if ((printerElement.style & PrinterDefine.PStyle_align_center) == PrinterDefine.PStyle_align_center) {
            //
            fmtImage.putInt("offset", (384 - width) / 2);
        }
        if ((printerElement.style & PrinterDefine.PStyle_align_right) == PrinterDefine.PStyle_align_right) {
            //
            fmtImage.putInt("offset", (384 - width));
        }

        return fmtImage;
    }

    public Bitmap convertBitmap(PrinterItemType printerItemType, PrinterElement printerElement) {
        try {
            Utility.DEBUG_LOG(TAG, "Try convert bitmap:" + printerElement.sValue);
//            Bitmap bitmap;
            if (printerItemType == PrinterItemType.LOGO_ASSETS) {
                InputStream is = context.getAssets().open(printerElement.sValue);
                bitmap = BitmapFactory.decodeStream(is);
//                                bitmap.getPixels();
            } else {
                bitmap = Bitmap.createBitmap(384, 300, Bitmap.Config.ARGB_8888);
                bitmap.eraseColor(Color.WHITE);
            }

            int height = bitmap.getHeight();
            int width = bitmap.getWidth();


//        int colorThreshold=0xFFa0a0a0;    //  0xFF------ for revert
//        int colorThreshold=0xFF606060;    //  0xFF------ for revert, 60 much white
//                        int colorThreshold=0xFF404040;    //  0xFF------ for revert, 40 more white
            int colorThreshold = 0;
            if ((printerElement.style & PrinterDefine.PStyle_image_revert) == PrinterDefine.PStyle_image_revert) {
                colorThreshold |= 0xFF000000;
            }
            if ((printerElement.style & PrinterDefine.PStyle_image_contrast_light) == PrinterDefine.PStyle_image_contrast_light) {
                colorThreshold |= 0x00a0a0a0;
            } else if ((printerElement.style & PrinterDefine.PStyle_image_contrast_normal) == PrinterDefine.PStyle_image_contrast_normal) {
                colorThreshold |= 0x00808080;
            } else if ((printerElement.style & PrinterDefine.PStyle_image_contrast_heavy) == PrinterDefine.PStyle_image_contrast_heavy) {
                colorThreshold |= 0x00404040;
            }

//                            printerEx.addImage(fmtImage, bitmap);
//                            printerEx.feedPixel( null, 8);

            // convert bitmap -- start
            int r, g, b;
            int r_t, g_t, b_t;
            r_t = ((colorThreshold & 0x00FF0000) >> 16);
            g_t = ((colorThreshold & 0x0000FF00) >> 8);
            b_t = ((colorThreshold & 0x000000FF));
            Utility.DEBUG_LOG(TAG, "Color Threshold:" + r_t + ", " + g_t + ", " + b_t);

            int pixels[] = new int[width * height];
            int pixels2[] = new int[width * height];
            bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
            int n = 0;
            for (int i = 0; i < height; i++) {
//                                String line ="Pixel:";
                for (int j = 0; j < width; j++) {
                    int pixel = pixels[n];
//                                    if( j < 64 ){
//                                        line += Integer.toHexString(pixel);
////                                        line += ",";
//                                    }
                    r = ((pixel & 0x00FF0000) >> 16);
                    g = ((pixel & 0x0000FF00) >> 8);
                    b = ((pixel & 0x000000FF));
                    if ((colorThreshold & 0x00FFFFFF) > 0) {
                        // convert color
                        if (r > r_t) {
                            r = 0xFF;
                        }
                        if (g > g_t) {
                            g = 0xFF;
                        }
                        if (b > b_t) {
                            b = 0xFF;
                        }

                        int c = (r < g) ? r : g;
                        c = (c < b) ? c : b;

                        r = c;
                        g = c;
                        b = c;

//                                    pixels[n] = (0xFF000000 + (r << 16) + (g << 8) + b );
                        pixels[n] = (0xFF000000 + (c << 16) + (c << 8) + c);
                        pixels2[n] = pixels[n];
                    }

                    if ((colorThreshold & 0xFF000000) == 0xFF000000) {
                        // revert
                        if ((r + g + b) < 600) {
                            r = 0xFF;
                            g = 0xFF;
                            b = 0xFF;
                        } else {
                            r = 0;
                            g = 0;
                            b = 0;
//                                            r = 0xFF - r;
//                                            g = 0xFF - g;
//                                            b = 0xFF - b;
                        }
                        pixels[n] = (0xFF000000 + (r << 16) + (g << 8) + b);
                    }

//                                    if( j < 64 ){
//                                        line += "-";
//                                        line += Integer.toHexString(pixels[n]);
//                                        line += ",";
//                                    }

                    n++;
                }
//                                Utility.DEBUG_LOG("PIXEL", line);
            }
//                            bitmap = Bitmap.createBitmap(pixels2, 0, width, width, height, Bitmap.Config.ARGB_8888);
//                            // convert bitmap -- end
//                            printerEx.addImage(fmtImage, bitmap);
//                            printerEx.feedPixel( null, 8);

            bitmap = Bitmap.createBitmap(pixels, 0, width, width, height, Bitmap.Config.ARGB_8888);
            // convert bitmap -- end
            return bitmap;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void print() {
        print(printerListenerDef);
        print(printerListenerDef,transactionDetailList);
        Utility.DEBUG_LOG("CAnvas2", String.valueOf(transactionDetailList.size()));

    }

    public boolean printBitmapChunkWrapper(PrinterListener printerListener, Bundle fmtImage, Bundle format, boolean isEnd) throws RemoteException
    {
        Utility.DEBUG_LOG(TAG,"+ printBitmapChunkWrapper +");
        boolean rv = false;
        int t = printerEx.getOffsetY() + 50;
        Utility.DEBUG_LOG(TAG,"printerEx.getOffsetY():"+printerEx.getOffsetY());
        Utility.DEBUG_LOG(TAG,"printerEx.getMaxHeight():"+printerEx.getMaxHeight());
        Utility.DEBUG_LOG(TAG,"printerEx.getOffsetY() + 50:"+ t);
        if ( t > printerEx.getMaxHeight() )
        {
            //+ taha 26-03-2021 wait till previous printing is finished
            while(true)
            {
                Utility.DEBUG_LOG(TAG,"printerState 1:"+printerState );
                try { sleep(1*100); }
                catch (Exception e) { Utility.DEBUG_LOG(TAG,"exception:"+e.getMessage().toString()); }
                if ( printerState == PrinterState.PS_Finish || printerState == PrinterState.PS_NA || printerState == PrinterState.PS_PaperOut)
                {
                    Utility.DEBUG_LOG(TAG,"printerState [NA or finish]: "+printerState );
                    try { sleep(1*200); }
                    catch (Exception e) { Utility.DEBUG_LOG(TAG,"exception:"+e.getMessage().toString()); }

                    printFinished = false;
                    printerState = PrinterState.PS_NA;
                    break;
                }
            }

            Utility.DEBUG_LOG(TAG,"before printBitmapChunk 1");
            rv = this.printBitmapChunk(printerListener,fmtImage,format,false);
            //+ taha 26-03-2021 wait till print finished
            while(true)
            {
                Utility.DEBUG_LOG(TAG,"printerState 2a:"+printerState );
//                Utility.DEBUG_LOG(TAG,"printFinished 2a:"+printFinished );
                try { sleep(1*100); }
                catch (Exception e) { Utility.DEBUG_LOG(TAG,"exception:"+e.getMessage().toString()); }
                Utility.DEBUG_LOG(TAG,"printerState 2b:"+printerState );
//                Utility.DEBUG_LOG(TAG,"printFinished 2b:"+printFinished );
//                if ( printFinished )
                if ( printerState == PrinterState.PS_Finish || printerState == PrinterState.PS_NA || printerState == PrinterState.PS_PaperOut)
                {
                    Utility.DEBUG_LOG(TAG,"printerState 2 [NA or finish]: "+printerState );
                    try { sleep(1*10); }
                    catch (Exception e) { Utility.DEBUG_LOG(TAG,"exception:"+e.getMessage().toString()); }
                    printFinished = false;
                    printerState = PrinterState.PS_NA;
                    break;
                }
            }
            printerEx = new PrinterEx();
            Utility.DEBUG_LOG(TAG,"printerEx.getOffsetY 1:" + printerEx.getOffsetY() );
        }
        return rv;
    }

//    public boolean printBitmapChunkWrapper(PrinterListener printerListener, Bundle fmtImage, Bundle format, boolean isEnd) throws RemoteException
//    {
//        Utility.DEBUG_LOG(TAG,"+ printBitmapChunkWrapper +");
//        boolean rv = false;
//        int t = printerEx.getOffsetY() + 50;
//        Utility.DEBUG_LOG(TAG,"printerEx.getOffsetY():"+printerEx.getOffsetY());
//        Utility.DEBUG_LOG(TAG,"printerEx.getMaxHeight():"+printerEx.getMaxHeight());
//        Utility.DEBUG_LOG(TAG,"printerEx.getOffsetY() + 50:"+ t);
//        if ( t > printerEx.getMaxHeight() )
//        {
//            Utility.DEBUG_LOG(TAG,"Start printing process...");
//            //+ taha 26-03-2021 wait till previous printing is finished
//            while(true)
//            {
//                Utility.DEBUG_LOG(TAG,"printerState 1:"+printerState );
//                try { sleep(1*100); }
//                catch (Exception e) { Utility.DEBUG_LOG(TAG,"exception:"+e.getMessage().toString()); }
//                if ( printerState == PrinterState.PS_Finish || printerState == PrinterState.PS_NA )
//                {
//                    Utility.DEBUG_LOG(TAG,"printerState [NA or finish]: "+printerState );
//                    try { sleep(1*200); }
//                    catch (Exception e) { Utility.DEBUG_LOG(TAG,"exception:"+e.getMessage().toString()); }
//                    printFinished = false;
//                    printerState = PrinterState.PS_NA;
//                    break;
//                }
//            }
//            new Thread(new Runnable()
//            {
//                @Override
//                public void run()
//                {
//                    Utility.DEBUG_LOG(TAG,"+ run +");
//
//                    Utility.DEBUG_LOG(TAG,"before printBitmapChunk 1");
//                    try {
//                        Utility.DEBUG_LOG(TAG,"Before printBitmapChunk");
////                        rv = this.printBitmapChunk(printerListener,fmtImage,format,false);
//                        printBitmapChunk(printerListener,fmtImage,format,false);
//                    } catch (RemoteException e) {
//                        e.printStackTrace();
//                    }
//                    //+ taha 26-03-2021 wait till print finished
//                    while(true)
//                    {
//                        Utility.DEBUG_LOG(TAG,"printerState 2a:"+printerState );
////                Utility.DEBUG_LOG(TAG,"printFinished 2a:"+printFinished );
//                        try { sleep(1*100); }
//                        catch (Exception e) { Utility.DEBUG_LOG(TAG,"exception:"+e.getMessage().toString()); }
//                        Utility.DEBUG_LOG(TAG,"printerState 2b:"+printerState );
////                Utility.DEBUG_LOG(TAG,"printFinished 2b:"+printFinished );
////                if ( printFinished )
//                        if ( printerState == PrinterState.PS_Finish || printerState == PrinterState.PS_NA )
//                        {
//                            Utility.DEBUG_LOG(TAG,"printerState 2 [NA or finish]: "+printerState );
//                            try { sleep(1*10); }
//                            catch (Exception e) { Utility.DEBUG_LOG(TAG,"exception:"+e.getMessage().toString()); }
//                            printFinished = false;
//                            printerState = PrinterState.PS_NA;
//                            break;
//                        }
//                    }
//                    printerEx = new PrinterEx();
//                    Utility.DEBUG_LOG(TAG,"printerEx.getOffsetY 1:" + printerEx.getOffsetY() );
//                }
//            }
//            ).start();
//         }
//        return rv;
//    }

    public boolean printBitmapChunk(PrinterListener printerListener, Bundle fmtImage, Bundle format, boolean isEnd) throws RemoteException
    {
        boolean rv = false;
        Utility.DEBUG_LOG(TAG,"+ printBitmapChunk +");
        Utility.DEBUG_LOG(TAG,"isEnd:"+isEnd);
        Utility.DEBUG_LOG(TAG,"iPrinter:"+iPrinter);
        if( iPrinter != null ){
            if ( isEnd )
            {
                format.putInt(PrinterConfig.addText.FontSize.BundleName, 0);
                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                iPrinter.addText(format, "    ");

                // add printerEx to iPrinter
                fmtImage.putInt("offset", 0);
                fmtImage.putInt("width", 384);  // bigger then actual, will print the actual
                fmtImage.putInt("height", printerEx.getHeight(true)); // bigger then actual, will print the actual
                Utility.DEBUG_LOG(TAG,"width:"+384);
                Utility.DEBUG_LOG(TAG,"height:"+printerEx.getHeight(true));
                Utility.DEBUG_LOG(TAG,"Before printerEx.getData");
                iPrinter.addImage(fmtImage, printerEx.getData(true));
                // printerEx -- end

                format.putInt(PrinterConfig.addText.FontSize.BundleName, 0);
                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                iPrinter.addText(format, "---------X-----------X----------");

                iPrinter.feedLine(3);

                // start print here
                iPrinter.startPrint(printerListener);

//            paperPrinting = true;
            }
            else
            {
                // add printerEx to iPrinter
                fmtImage.putInt("offset", 0);
                fmtImage.putInt("width", 384);  // bigger then actual, will print the actual
                fmtImage.putInt("height", printerEx.getHeight(true)); // bigger then actual, will print the actual
                Utility.DEBUG_LOG(TAG,"width:"+384);
                Utility.DEBUG_LOG(TAG,"height:"+printerEx.getHeight(true));
                Utility.DEBUG_LOG(TAG,"Before printerEx.getData");
                iPrinter.addImage(fmtImage, printerEx.getData(true));
                // printerEx -- end
                // start print here
                iPrinter.startPrint(printerListener);
            }

//            try
//            {
//                sleep(1*500);
//            }
//            catch (Exception e)
//            {
//                Utility.DEBUG_LOG(TAG,"exception:"+e.getMessage().toString());
//            }
            printerState = PrinterState.PS_Printing;
        }
        return rv;
    }

    public boolean print(PrinterListener printerListener,List<TransactionDetail> transactionDetailList) {
        Utility.DEBUG_LOG(TAG, "print: "+ String.valueOf(transactionDetailList.size()));
        Utility.DEBUG_LOG(TAG, "print() 1");

        if (null == printerItems) {
            initializeDefaltRecp();
        }
        boolean paperPrinting = false;
        printerEx = new PrinterEx();
        int fontSizeTemp = 0;
        int fontSizeTotal = 0;

        Bundle format = new Bundle();

        // bundle formate for AddTextInLine
        Bundle fmtAddTextInLine = new Bundle();

        Bundle fmtImage = new Bundle();
        try {
            byte[] bufferLogo = null;
            for (PrinterItem printerItem : printerItems )
            {
                format.putInt(PrinterConfig.addText.FontSize.BundleName, 1);
                format.putInt(PrinterConfig.addText.Alignment.BundleName, 2);
                format.putString("fontStyle", PrinterDefine.Font_default);
                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);

                switch (printerItem.type) {
                    case LOGO_STORAGE:
                    case LOGO_ASSETS:


                        bitmap = convertBitmap(printerItem.type, printerItem.title);
                        if (null != bitmap) {
                            fmtImage = getParameterFromBitmap(printerItem.title, bitmap);

                            printerEx.addImage(fmtImage, bitmap);
                        }

                        if (printerItem.value.sValue != null) {
                            if (printerItem.value.sValue.length() > 0) {
                                int w = 0;
                                int h = 0;
                                if (null != bitmap) {
                                    w = bitmap.getWidth();
                                    h = bitmap.getHeight();
                                }
                                bitmap = convertBitmap(printerItem.type, printerItem.value);
                                if (null != bitmap) {
                                    fmtImage = getParameterFromBitmap(printerItem.value, bitmap);

                                    if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0
                                            && (printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                                        // try print 2 logo in one line
                                        if ((w + bitmap.getWidth()) < PrinterEx.MAX_WIDTH) {
                                            // print 2 logo in one line
                                            printerEx.feedPixel(null, 0 - h - 2);
                                        } else {

                                        }
                                    }
                                    printerEx.addImage(fmtImage, bitmap);
                                }
                            }

                        }
                        break;


                    case STRING:
                        if (printerItem.title.sValue.length() > 0) {
                            // font size
                            fontsize = printerItem.title.size;
                            if (fontsize > 0) {
                                format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                            }
                            if (printerItem.title.fontFile.length() > 0) {
                                format.putString("fontStyle", printerItem.title.fontFile);
                            }
                            // aligment
                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                            if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                            } else if ((printerItem.title.style & PrinterDefine.PStyle_align_center) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                            } else if ((printerItem.title.style & PrinterDefine.PStyle_align_right) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                            }
                            if (printerItem.isForceMultiLines) {
                                format.putBoolean("newline", true);
                            } else if (null != printerItem.value.sValue) {
                                if (printerItem.value.sValue.length() > 0) {
                                    format.putBoolean("newline", false);
                                }
                            }
                            offsetX = printerEx.addText(format, printerItem.title.sValue, printerItem.printerMode);
                            if (null != printerItem.title.secValue) {

                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                offsetX = printerEx.addText(format, printerItem.title.secValue, printerItem.printerMode);
                            }
                        }


                        if (null != printerItem.value.sValue) {
                            if (printerItem.value.sValue.length() > 0) {
                                // font size
                                fontsize = printerItem.value.size;
                                Utility.DEBUG_LOG("fontsize", String.valueOf(fontsize));
                                if (fontsize > 0) {
                                    format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                }
                                if (printerItem.value.fontFile.length() > 0) {
                                    format.putString("fontStyle", printerItem.value.fontFile);
                                }
                                // aligment
                                format.putBoolean("newline", true);
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                if ((printerItem.value.style & PrinterDefine.PStyle_align_left) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                } else if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                }
                                printerEx.addText(format, printerItem.value.sValue, printerItem.printerMode);
                            }
                        }
                        break;

                    case DETAIL_AMOUNT:
                        Utility.DEBUG_LOG("DETAIL", String.valueOf(transactionDetailList.size()));
                        for (int i = 0; i < transactionDetailList.size(); i++) {
                            if(!transactionDetailList.get(i).getTxnType().equals("PRE AUTH")) {
                                Utility.DEBUG_LOG(TAG, "BEAT 3");

                                Utility.DEBUG_LOG(TAG, "before printBitmapChunkWrapper 1");
                                printBitmapChunkWrapper(printerListener, fmtImage, format, false);
                                // for card info
                                if (printerItem.title.sValue.length() > 0) {
                                    // font size
                                    fontsize = printerItem.title.size;
                                    if (fontsize > 0) {
                                        format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                    }
                                    if (printerItem.title.fontFile.length() > 0) {
                                        format.putString("fontStyle", printerItem.title.fontFile);
                                    }
                                    // aligment
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                    if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0) {
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                    } else if ((printerItem.title.style & PrinterDefine.PStyle_align_center) != 0) {
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                    } else if ((printerItem.title.style & PrinterDefine.PStyle_align_right) != 0) {
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                    }
                                    if (printerItem.isForceMultiLines) {
                                        format.putBoolean("newline", true);
                                    } else if (null != printerItem.value.sValue) {
                                        if (printerItem.value.sValue.length() > 0) {
                                            format.putBoolean("newline", false);
                                        }
                                    }
                                    offsetX = printerEx.addText(format, DataFormatterUtil.formattedDate(transactionDetailList.get(i).getTxnDate()), printerItem.printerMode);
                                    if (null != printerItem.title.secValue) {

                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                        offsetX = printerEx.addText(format, " ", printerItem.printerMode);
                                    }
                                }
                                //for card info
                                Utility.DEBUG_LOG(TAG, "before printBitmapChunkWrapper 1a");
                                printBitmapChunkWrapper(printerListener, fmtImage, format, false);
                                if (null != printerItem.value.sValue) {
                                    if (printerItem.value.sValue.length() > 0) {
                                        // font size
                                        fontsize = printerItem.value.size;
                                        Utility.DEBUG_LOG("fontsize", String.valueOf(fontsize));
                                        if (fontsize > 0) {
                                            format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                        }
                                        if (printerItem.value.fontFile.length() > 0) {
                                            format.putString("fontStyle", printerItem.value.fontFile);
                                        }
                                        // aligment
                                        format.putBoolean("newline", true);
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                        if ((printerItem.value.style & PrinterDefine.PStyle_align_left) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                        } else if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                        } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                        }
                                        printerEx.addText(format, DataFormatterUtil.formattedTime(transactionDetailList.get(i).getTxnTime()), printerItem.printerMode);
                                    }
                                }
                                // for card info
                                Utility.DEBUG_LOG(TAG, "before printBitmapChunkWrapper 1b");
                                printBitmapChunkWrapper(printerListener, fmtImage, format, false);
                                if (printerItem.title.sValue.length() > 0) {
                                    // font size
                                    fontsize = printerItem.title.size;
                                    if (fontsize > 0) {
                                        format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                    }
                                    if (printerItem.title.fontFile.length() > 0) {
                                        format.putString("fontStyle", printerItem.title.fontFile);
                                    }
                                    // aligment
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                    if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0) {
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                    } else if ((printerItem.title.style & PrinterDefine.PStyle_align_center) != 0) {
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                    } else if ((printerItem.title.style & PrinterDefine.PStyle_align_right) != 0) {
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                    }
                                    if (printerItem.isForceMultiLines) {
                                        format.putBoolean("newline", true);
                                    } else if (null != printerItem.value.sValue) {
                                        if (printerItem.value.sValue.length() > 0) {
                                            format.putBoolean("newline", false);
                                        }
                                    }
                                    offsetX = printerEx.addText(format, transactionDetailList.get(i).getInvoiceNo(), printerItem.printerMode);
                                    if (null != printerItem.title.secValue) {

                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                        offsetX = printerEx.addText(format, transactionDetailList.get(i).getTxnType(), printerItem.printerMode);
                                    }
                                }
                                //for card info
                                Utility.DEBUG_LOG(TAG, "before printBitmapChunkWrapper 1c");
                                printBitmapChunkWrapper(printerListener, fmtImage, format, false);
                                if (null != printerItem.value.sValue) {
                                    if (printerItem.value.sValue.length() > 0) {
                                        // font size
                                        fontsize = printerItem.value.size;
                                        Utility.DEBUG_LOG("fontsize", String.valueOf(fontsize));
                                        if (fontsize > 0) {
                                            format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                        }
                                        if (printerItem.value.fontFile.length() > 0) {
                                            format.putString("fontStyle", printerItem.value.fontFile);
                                        }
                                        // aligment
                                        format.putBoolean("newline", true);
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                        if ((printerItem.value.style & PrinterDefine.PStyle_align_left) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                        } else if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                        } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                        }
                                        printerEx.addText(format, transactionDetailList.get(i).getAuthorizationIdentificationResponseCode(), printerItem.printerMode);
                                    }
                                }
                                // for card info
                                Utility.DEBUG_LOG(TAG, "before printBitmapChunkWrapper 1d");
                                printBitmapChunkWrapper(printerListener, fmtImage, format, false);
                                if (printerItem.title.sValue.length() > 0) {
                                    // font size
                                    fontsize = printerItem.title.size;
                                    if (fontsize > 0) {
                                        format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                    }
                                    if (printerItem.title.fontFile.length() > 0) {
                                        format.putString("fontStyle", printerItem.title.fontFile);
                                    }
                                    // aligment
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                    if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0) {
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                    } else if ((printerItem.title.style & PrinterDefine.PStyle_align_center) != 0) {
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                    } else if ((printerItem.title.style & PrinterDefine.PStyle_align_right) != 0) {
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                    }
                                    if (printerItem.isForceMultiLines) {
                                        format.putBoolean("newline", true);
                                    } else if (null != printerItem.value.sValue) {
                                        if (printerItem.value.sValue.length() > 0) {
                                            format.putBoolean("newline", false);
                                        }
                                    }
                                    offsetX = printerEx.addText(format, DataFormatterUtil.maskCardNo(transactionDetailList.get(i).getCardNo()), printerItem.printerMode);
                                    if (null != printerItem.title.secValue) {

                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                        if (transactionDetailList.get(i).getCardType().equals("Contactless")) {
                                            offsetX = printerEx.addText(format, "CTLS", printerItem.printerMode);
                                        } else {
                                            offsetX = printerEx.addText(format, transactionDetailList.get(i).getCardType(), printerItem.printerMode);
                                        }
                                    }
                                }
                                //for card info
                                Utility.DEBUG_LOG(TAG, "before printBitmapChunkWrapper 1e");
                                printBitmapChunkWrapper(printerListener, fmtImage, format, false);
                                if (null != printerItem.value.sValue) {
                                    if (printerItem.value.sValue.length() > 0) {
                                        // font size
                                        fontsize = printerItem.value.size;
                                        Utility.DEBUG_LOG("fontsize", String.valueOf(fontsize));
                                        if (fontsize > 0) {
                                            format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                        }
                                        if (printerItem.value.fontFile.length() > 0) {
                                            format.putString("fontStyle", printerItem.value.fontFile);
                                        }
                                        // aligment
                                        format.putBoolean("newline", true);
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                        if ((printerItem.value.style & PrinterDefine.PStyle_align_left) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                        } else if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                        } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                        }
                                        printerEx.addText(format, transactionDetailList.get(i).getCardScheme2(), printerItem.printerMode);
                                    }
                                }
                                // for amount
                                Utility.DEBUG_LOG(TAG, "before printBitmapChunkWrapper 1f");
                                printBitmapChunkWrapper(printerListener, fmtImage, format, false);
                                if (printerItem.title.sValue.length() > 0) {
                                    // font size
                                    fontsize = printerItem.title.size;
                                    if (fontsize > 0) {
                                        format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                    }
                                    if (printerItem.title.fontFile.length() > 0) {
                                        format.putString("fontStyle", printerItem.title.fontFile);
                                    }
                                    // aligment
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                    if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0) {
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                    } else if ((printerItem.title.style & PrinterDefine.PStyle_align_center) != 0) {
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                    } else if ((printerItem.title.style & PrinterDefine.PStyle_align_right) != 0) {
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                    }
                                    if (printerItem.isForceMultiLines) {
                                        format.putBoolean("newline", true);
                                    } else if (null != printerItem.value.sValue) {
                                        if (printerItem.value.sValue.length() > 0) {
                                            format.putBoolean("newline", false);
                                        }
                                    }
                                    offsetX = printerEx.addText(format, "AMOUNT", printerItem.printerMode);
                                    if (null != printerItem.title.secValue) {

                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                        offsetX = printerEx.addText(format, "PKR", printerItem.printerMode);
                                    }
                                }
                                // for amount
                                Utility.DEBUG_LOG(TAG, "before printBitmapChunkWrapper 1g");
                                printBitmapChunkWrapper(printerListener, fmtImage, format, false);
                                if (null != printerItem.value.sValue) {
                                    if (printerItem.value.sValue.length() > 0) {
                                        // font size
                                        fontsize = printerItem.value.size;
                                        Utility.DEBUG_LOG("fontsize", String.valueOf(fontsize));
                                        if (fontsize > 0) {
                                            format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                        }
                                        if (printerItem.value.fontFile.length() > 0) {
                                            format.putString("fontStyle", printerItem.value.fontFile);
                                        }
                                        // aligment
                                        format.putBoolean("newline", true);
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                        if ((printerItem.value.style & PrinterDefine.PStyle_align_left) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                        } else if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                        } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                        }
                                        if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")) {
                                            Utility.DEBUG_LOG("TIP", "IS ENABLED");
                                            if (!transactionDetailList.get(i).getTransactionAmount().equals("0.00")) {
                                                printerEx.addText(format, transactionDetailList.get(i).getTransactionAmount(), printerItem.printerMode);
                                            } else {
                                                printerEx.addText(format, transactionDetailList.get(i).getAmount(), printerItem.printerMode);
                                            }
                                        } else {
                                            printerEx.addText(format, transactionDetailList.get(i).getAmount(), printerItem.printerMode);
//                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);

                                            format.putBoolean("newline", true);
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                            printerEx.addText(format, "-------------", printerItem.printerMode);
                                        }
                                    }
                                }

                                //for tip
                                Utility.DEBUG_LOG(TAG, "before printBitmapChunkWrapper 1h");
                                printBitmapChunkWrapper(printerListener, fmtImage, format, false);
                                if (Constants.TIP_ENABLED.equalsIgnoreCase("Y")) {
                                    Utility.DEBUG_LOG("TIP", "IS ENABLED");
                                    // for amount
                                    if (printerItem.title.sValue.length() > 0) {
                                        // font size
                                        fontsize = printerItem.title.size;
                                        if (fontsize > 0) {
                                            format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                        }
                                        if (printerItem.title.fontFile.length() > 0) {
                                            format.putString("fontStyle", printerItem.title.fontFile);
                                        }
                                        // aligment
                                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                        if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                        } else if ((printerItem.title.style & PrinterDefine.PStyle_align_center) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                        } else if ((printerItem.title.style & PrinterDefine.PStyle_align_right) != 0) {
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                        }
                                        if (printerItem.isForceMultiLines) {
                                            format.putBoolean("newline", true);
                                        } else if (null != printerItem.value.sValue) {
                                            if (printerItem.value.sValue.length() > 0) {
                                                format.putBoolean("newline", false);
                                            }
                                        }
                                        offsetX = printerEx.addText(format, "TIP AMOUNT", printerItem.printerMode);
                                        if (null != printerItem.title.secValue) {

                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                            offsetX = printerEx.addText(format, "PKR", printerItem.printerMode);
                                        }
                                    }
                                    // for amount
                                    if (null != printerItem.value.sValue) {
                                        if (printerItem.value.sValue.length() > 0) {
                                            // font size
                                            fontsize = printerItem.value.size;
                                            Utility.DEBUG_LOG("fontsize", String.valueOf(fontsize));
                                            if (fontsize > 0) {
                                                format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                            }
                                            if (printerItem.value.fontFile.length() > 0) {
                                                format.putString("fontStyle", printerItem.value.fontFile);
                                            }
                                            // aligment
                                            format.putBoolean("newline", true);
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                            if ((printerItem.value.style & PrinterDefine.PStyle_align_left) != 0) {
                                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                            } else if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                            } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                            }
                                            printerEx.addText(format, transactionDetailList.get(i).getTipAmount(), printerItem.printerMode);
//                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);

                                            format.putBoolean("newline", true);
                                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                            printerEx.addText(format, "-------------", printerItem.printerMode);
                                        }
                                    }
                                }
                                Utility.DEBUG_LOG(TAG, "BEAT 4");
                            }
                        }
                        break;


                    case THREE_COLUMN:
                        offsetX = 0;
                        fontsize = 1;
                        if (printerItem.title.sValue.length() > 0) {
                            // font size
                            fontsize = printerItem.title.size;
                            if (fontsize > 0) {
                                format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                            }
                            if (printerItem.title.fontFile.length() > 0) {
                                format.putString("fontStyle", printerItem.title.fontFile);
                            }
                            // aligment
                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                            if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                            } else if ((printerItem.title.style & PrinterDefine.PStyle_align_center) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                            } else if ((printerItem.title.style & PrinterDefine.PStyle_align_right) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                            }
                            if (printerItem.isForceMultiLines) {
                                format.putBoolean("newline", false);
                            } else if (null != printerItem.value1.sValue) {
                                if (printerItem.value1.sValue.length() > 0) {
                                    format.putBoolean("newline", false);
                                }
                            }
                            offsetX = printerEx.addText(format, printerItem.title.sValue, printerItem.printerMode);
                            if (null != printerItem.title.secValue) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                offsetX = printerEx.addText(format, printerItem.title.secValue, printerItem.printerMode);
                            }

//                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
//                            offsetX = printerEx.addText(format, printerItem.value1.sValue, printerItem.printerMode);
//                            Utility.DEBUG_LOG("fontPrinter",printerItem.value1.sValue + " " + printerItem.value.sValue + " " + printerItem.title.sValue );

                        }
//                        if (printerItem.value1.sValue.length() > 0) {
//                            // font size
//                            fontsize = printerItem.value1.size;
//                            if (fontsize > 0) {
//                                format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
//                            }
//                            if (printerItem.value1.fontFile.length() > 0) {
//                                format.putString("fontStyle", printerItem.value1.fontFile);
//                            }
//                            // aligment
//                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
//                            if ((printerItem.value1.style & PrinterDefine.PStyle_align_left) != 0) {
//                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
//                            } else if ((printerItem.value1.style & PrinterDefine.PStyle_align_center) != 0) {
//                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
//                            } else if ((printerItem.value1.style & PrinterDefine.PStyle_align_right) != 0) {
//                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
//                            }
//                            if (printerItem.isForceMultiLines) {
//                                format.putBoolean("newline", false);
//                            } else if (null != printerItem.value.sValue) {
//                                if (printerItem.value.sValue.length() > 0) {
//                                    format.putBoolean("newline", false);
//                                }
//                            }
//                            offsetX = printerEx.addText(format, printerItem.value1.sValue, printerItem.printerMode);
//                        }


                        if (null != printerItem.value.sValue) {
                            if (printerItem.value.sValue.length() > 0) {
                                // font size
                                fontsize = printerItem.value.size;
                                if (fontsize > 0) {
                                    format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                }
                                if (printerItem.value.fontFile.length() > 0) {
                                    format.putString("fontStyle", printerItem.value.fontFile);
                                }
                                // aligment
                                format.putBoolean("newline", false);
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                if ((printerItem.value.style & PrinterDefine.PStyle_align_left) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                } else if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                }

                                printerEx.addText(format, printerItem.value.sValue, printerItem.printerMode);
                            }
                        }
                        break;


                    case LINE:
                        printerEx.addLine(null, printerItem.title.size);
                        break;
                    case FEED:
                        printerEx.feedPixel(null, printerItem.title.size);
                        break;
                    case QRCODE:
                        Bundle qrCodeFormat = new Bundle();
                        boolean needScrollBack = false;
                        if (printerItem.title.sValue.length() > 0 && printerItem.value.sValue.length() > 0) {
                            printerItem.title.style = PrinterDefine.PStyle_align_left;
                            if (printerItem.title.size > 180) {
                                printerItem.title.size = 180;
                            }
                            printerItem.value.style = PrinterDefine.PStyle_align_right;
                            if (printerItem.value.size > 180) {
                                printerItem.value.size = 180;
                            }

                            needScrollBack = true;
                        }
                        PrinterElement qrcode = printerItem.title;
                        if (qrcode.sValue.length() > 0) {
                            if ((qrcode.style & PrinterDefine.PStyle_align_center) != 0) {
                                qrCodeFormat.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                            } else if ((qrcode.style & PrinterDefine.PStyle_align_right) != 0) {
                                qrCodeFormat.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                            }
                            // size
                            qrCodeFormat.putInt(PrinterConfig.addQrCode.Height.BundleName, qrcode.size);
                            printerEx.addQrCode(qrCodeFormat, qrcode.sValue);
                        }

                        qrcode = printerItem.value;
                        if (qrcode.sValue.length() > 0) {
                            if ((qrcode.style & PrinterDefine.PStyle_align_center) != 0) {
                                qrCodeFormat.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                            } else if ((qrcode.style & PrinterDefine.PStyle_align_right) != 0) {
                                qrCodeFormat.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                            }
                            // size
                            qrCodeFormat.putInt(PrinterConfig.addQrCode.Height.BundleName, qrcode.size);
                            if (needScrollBack) {
                                printerEx.scrollBack();
                            }
                            printerEx.addQrCode(qrCodeFormat, qrcode.sValue);
                        }

                        break;
                    case BARCODE:
                        Bundle barcodeFormat = new Bundle();
                        if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                            barcodeFormat.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                        } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                            barcodeFormat.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                        }
                        barcodeFormat.putInt(PrinterConfig.addBarCode.Height.BundleName, printerItem.value.size);
                        printerEx.addBarCode(barcodeFormat, printerItem.value.sValue);
                        break;
//                    //case IMG_BCD:
//                        // title
//                        addString(printerItem.title);
//
//                        // image
//                        if (null != printerItem.value.sValue) {
//                            Utility.DEBUG_LOG(TAG, "add IMG_BCD, size" + printerItem.value.sValue.length());
//                            if (printerItem.value.sValue.length() > 0) {
//                                printerEx.addImage(null, (Utility.hexStr2Byte(printerItem.value.sValue)));
//                            }
//                        }
//                        break;
                }
                Utility.DEBUG_LOG(TAG,"before printBitmapChunkWrapper 2");

                printBitmapChunkWrapper(printerListener,fmtImage,format,false);
            }

            //
            //
            if( true ) {
                Utility.DEBUG_LOG(TAG,"Before printBitmapChunk 1e");
//                // print to paper
                this.printBitmapChunk(printerListener,fmtImage,format,true);
                paperPrinting = false;

            }

            //successPrintingShow(context,"Print Finished");
          //  DialogUtil.successDialog(context,"Print successfull");
            DashboardContainer.backStack();;

            //hideProgressDialog();
            Utility.DEBUG_LOG(TAG, "hideProgressDialog :");

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (Exception e) {
            //hideProgressDialog();
            Utility.DEBUG_LOG(TAG, "Exception :" + e.getMessage());
            for (StackTraceElement m : e.getStackTrace()
            ) {
                Utility.DEBUG_LOG(TAG, "Exception :" + m);

            }

        }
//        Intent intent = new Intent( context, PrintCanvasActivity.class);
//        context.startActivity(intent);

        return paperPrinting;

    }

    public int printInternal(Bundle format,Bundle fmtImage,PrinterItem printerItem, PrinterListener printerListener) throws RemoteException
    {
        Utility.DEBUG_LOG(TAG,"+ printInternal +");

        format.putInt(PrinterConfig.addText.FontSize.BundleName, 1);
        format.putInt(PrinterConfig.addText.Alignment.BundleName, 2);
        format.putString("fontStyle", PrinterDefine.Font_default);
        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);

        switch (printerItem.type) {
            case LOGO_STORAGE:
            case LOGO_ASSETS:

                bitmap = convertBitmap(printerItem.type, printerItem.title);
                if (null != bitmap) {
                    fmtImage = getParameterFromBitmap(printerItem.title, bitmap);
                    printerEx.addImage(fmtImage, bitmap);
                }

                if (printerItem.value.sValue != null) {
                    if (printerItem.value.sValue.length() > 0) {
                        int w = 0;
                        int h = 0;
                        if (null != bitmap) {
                            w = bitmap.getWidth();
                            h = bitmap.getHeight();
                        }
                        bitmap = convertBitmap(printerItem.type, printerItem.value);
                        if (null != bitmap) {
                            fmtImage = getParameterFromBitmap(printerItem.value, bitmap);

                            if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0
                                    && (printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                                // try print 2 logo in one line
                                if ((w + bitmap.getWidth()) < PrinterEx.MAX_WIDTH) {
                                    // print 2 logo in one line
                                    printerEx.feedPixel(null, 0 - h - 2);
                                } else {

                                }
                            }
                            printerEx.addImage(fmtImage, bitmap);
                        }
                    }

                }
                break;


            case STRING:
                if (printerItem.title.sValue.length() > 0) {
                    // font size
                    fontsize = printerItem.title.size;
                    if (fontsize > 0) {
                        format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                    }
                    if (printerItem.title.fontFile.length() > 0) {
                        format.putString("fontStyle", printerItem.title.fontFile);
                    }
                    // aligment
                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                    if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0) {
                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                    } else if ((printerItem.title.style & PrinterDefine.PStyle_align_center) != 0) {
                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                    } else if ((printerItem.title.style & PrinterDefine.PStyle_align_right) != 0) {
                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                    }
                    if (printerItem.isForceMultiLines) {
                        format.putBoolean("newline", true);
                    } else if (null != printerItem.value.sValue) {
                        if (printerItem.value.sValue.length() > 0) {
                            format.putBoolean("newline", false);
                        }
                    }
                    offsetX = printerEx.addText(format, printerItem.title.sValue, printerItem.printerMode);
                    if (null != printerItem.title.secValue) {

                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                        offsetX = printerEx.addText(format, printerItem.title.secValue, printerItem.printerMode);
                    }
                }


                if (null != printerItem.value.sValue) {
                    if (printerItem.value.sValue.length() > 0) {
                        // font size
                        fontsize = printerItem.value.size;
                        Utility.DEBUG_LOG("fontsize", String.valueOf(fontsize));
                        if (fontsize > 0) {
                            format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                        }
                        if (printerItem.value.fontFile.length() > 0) {
                            format.putString("fontStyle", printerItem.value.fontFile);
                        }
                        // aligment
                        format.putBoolean("newline", true);
                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                        if ((printerItem.value.style & PrinterDefine.PStyle_align_left) != 0) {
                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                        } else if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                        } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                        }
                        printerEx.addText(format, printerItem.value.sValue, printerItem.printerMode);
                    }
                }
                break;

            case DETAIL_AMOUNT:
                Utility.DEBUG_LOG("DETAIL", String.valueOf(transactionDetailList.size()));
                for (int i = 0; i < transactionDetailList.size(); i++) {
                    if (!transactionDetailList.get(i).getTxnType().equals("PRE AUTH")) {
                        Utility.DEBUG_LOG(TAG, "BEAT 1");
                        Utility.DEBUG_LOG(TAG, "before printBitmapChunkWrapper 3");
                        printBitmapChunkWrapper(printerListener, fmtImage, format, false);

                        // for card info
                        if (printerItem.title.sValue.length() > 0) {
                            // font size
                            fontsize = printerItem.title.size;
                            if (fontsize > 0) {
                                format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                            }
                            if (printerItem.title.fontFile.length() > 0) {
                                format.putString("fontStyle", printerItem.title.fontFile);
                            }
                            // aligment
                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                            if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                            } else if ((printerItem.title.style & PrinterDefine.PStyle_align_center) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                            } else if ((printerItem.title.style & PrinterDefine.PStyle_align_right) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                            }
                            if (printerItem.isForceMultiLines) {
                                format.putBoolean("newline", true);
                            } else if (null != printerItem.value.sValue) {
                                if (printerItem.value.sValue.length() > 0) {
                                    format.putBoolean("newline", false);
                                }
                            }
                            offsetX = printerEx.addText(format, DataFormatterUtil.formattedDate(transactionDetailList.get(i).getTxnDate()), printerItem.printerMode);
                            if (null != printerItem.title.secValue) {

                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                offsetX = printerEx.addText(format, " ", printerItem.printerMode);
                            }
                        }
                        //for card info
                        if (null != printerItem.value.sValue) {
                            if (printerItem.value.sValue.length() > 0) {
                                // font size
                                fontsize = printerItem.value.size;
                                Utility.DEBUG_LOG("fontsize", String.valueOf(fontsize));
                                if (fontsize > 0) {
                                    format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                }
                                if (printerItem.value.fontFile.length() > 0) {
                                    format.putString("fontStyle", printerItem.value.fontFile);
                                }
                                // aligment
                                format.putBoolean("newline", true);
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                if ((printerItem.value.style & PrinterDefine.PStyle_align_left) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                } else if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                }
                                printerEx.addText(format, DataFormatterUtil.formattedTime(transactionDetailList.get(i).getTxnTime()), printerItem.printerMode);
                            }
                        }
                        // for card info
                        if (printerItem.title.sValue.length() > 0) {
                            // font size
                            fontsize = printerItem.title.size;
                            if (fontsize > 0) {
                                format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                            }
                            if (printerItem.title.fontFile.length() > 0) {
                                format.putString("fontStyle", printerItem.title.fontFile);
                            }
                            // aligment
                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                            if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                            } else if ((printerItem.title.style & PrinterDefine.PStyle_align_center) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                            } else if ((printerItem.title.style & PrinterDefine.PStyle_align_right) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                            }
                            if (printerItem.isForceMultiLines) {
                                format.putBoolean("newline", true);
                            } else if (null != printerItem.value.sValue) {
                                if (printerItem.value.sValue.length() > 0) {
                                    format.putBoolean("newline", false);
                                }
                            }
                            offsetX = printerEx.addText(format, transactionDetailList.get(i).getInvoiceNo(), printerItem.printerMode);
                            if (null != printerItem.title.secValue) {

                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                offsetX = printerEx.addText(format, transactionDetailList.get(i).getTxnType(), printerItem.printerMode);
                            }
                        }
                        //for card info
                        if (null != printerItem.value.sValue) {
                            if (printerItem.value.sValue.length() > 0) {
                                // font size
                                fontsize = printerItem.value.size;
                                Utility.DEBUG_LOG("fontsize", String.valueOf(fontsize));
                                if (fontsize > 0) {
                                    format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                }
                                if (printerItem.value.fontFile.length() > 0) {
                                    format.putString("fontStyle", printerItem.value.fontFile);
                                }
                                // aligment
                                format.putBoolean("newline", true);
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                if ((printerItem.value.style & PrinterDefine.PStyle_align_left) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                } else if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                }
                                printerEx.addText(format, transactionDetailList.get(i).getAuthorizationIdentificationResponseCode(), printerItem.printerMode);
                            }
                        }
                        // for card info
                        if (printerItem.title.sValue.length() > 0) {
                            // font size
                            fontsize = printerItem.title.size;
                            if (fontsize > 0) {
                                format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                            }
                            if (printerItem.title.fontFile.length() > 0) {
                                format.putString("fontStyle", printerItem.title.fontFile);
                            }
                            // aligment
                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                            if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                            } else if ((printerItem.title.style & PrinterDefine.PStyle_align_center) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                            } else if ((printerItem.title.style & PrinterDefine.PStyle_align_right) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                            }
                            if (printerItem.isForceMultiLines) {
                                format.putBoolean("newline", true);
                            } else if (null != printerItem.value.sValue) {
                                if (printerItem.value.sValue.length() > 0) {
                                    format.putBoolean("newline", false);
                                }
                            }
                            offsetX = printerEx.addText(format, transactionDetailList.get(i).getCardNo(), printerItem.printerMode);
                            if (null != printerItem.title.secValue) {

                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                offsetX = printerEx.addText(format, transactionDetailList.get(i).getCardType(), printerItem.printerMode);
                            }
                        }
                        //for card info
                        if (null != printerItem.value.sValue) {
                            if (printerItem.value.sValue.length() > 0) {
                                // font size
                                fontsize = printerItem.value.size;
                                Utility.DEBUG_LOG("fontsize", String.valueOf(fontsize));
                                if (fontsize > 0) {
                                    format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                }
                                if (printerItem.value.fontFile.length() > 0) {
                                    format.putString("fontStyle", printerItem.value.fontFile);
                                }
                                // aligment
                                format.putBoolean("newline", true);
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                if ((printerItem.value.style & PrinterDefine.PStyle_align_left) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                } else if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                }
                                printerEx.addText(format, transactionDetailList.get(i).getCardScheme2(), printerItem.printerMode);
                            }
                        }
                        // for amount
                        if (printerItem.title.sValue.length() > 0) {
                            // font size
                            fontsize = printerItem.title.size;
                            if (fontsize > 0) {
                                format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                            }
                            if (printerItem.title.fontFile.length() > 0) {
                                format.putString("fontStyle", printerItem.title.fontFile);
                            }
                            // aligment
                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                            if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                            } else if ((printerItem.title.style & PrinterDefine.PStyle_align_center) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                            } else if ((printerItem.title.style & PrinterDefine.PStyle_align_right) != 0) {
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                            }
                            if (printerItem.isForceMultiLines) {
                                format.putBoolean("newline", true);
                            } else if (null != printerItem.value.sValue) {
                                if (printerItem.value.sValue.length() > 0) {
                                    format.putBoolean("newline", false);
                                }
                            }
                            offsetX = printerEx.addText(format, "AMOUNT", printerItem.printerMode);
                            if (null != printerItem.title.secValue) {

                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                offsetX = printerEx.addText(format, "PKR", printerItem.printerMode);
                            }
                        }
                        // for amount
                        if (null != printerItem.value.sValue) {
                            if (printerItem.value.sValue.length() > 0) {
                                // font size
                                fontsize = printerItem.value.size;
                                Utility.DEBUG_LOG("fontsize", String.valueOf(fontsize));
                                if (fontsize > 0) {
                                    format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                                }
                                if (printerItem.value.fontFile.length() > 0) {
                                    format.putString("fontStyle", printerItem.value.fontFile);
                                }
                                // aligment
                                format.putBoolean("newline", true);
                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                if ((printerItem.value.style & PrinterDefine.PStyle_align_left) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                                } else if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                                }
                                printerEx.addText(format, transactionDetailList.get(i).getAmount(), printerItem.printerMode);
//                                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                                //printerEx.addLine(format, 0);
                                //printerEx.feedPixel(null, printerItem.value.size);
                                format.putBoolean("newline", true);
                                printerEx.addText(format, "-------------", printerItem.printerMode);
                            }
                        }
                        Utility.DEBUG_LOG(TAG, "BEAT 2");
                    }
                }

                break;


            case THREE_COLUMN:
                offsetX = 0;
                fontsize = 1;
                if (printerItem.title.sValue.length() > 0) {
                    // font size
                    fontsize = printerItem.title.size;
                    if (fontsize > 0) {
                        format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                    }
                    if (printerItem.title.fontFile.length() > 0) {
                        format.putString("fontStyle", printerItem.title.fontFile);
                    }
                    // aligment
                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                    if ((printerItem.title.style & PrinterDefine.PStyle_align_left) != 0) {
                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                    } else if ((printerItem.title.style & PrinterDefine.PStyle_align_center) != 0) {
                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                    } else if ((printerItem.title.style & PrinterDefine.PStyle_align_right) != 0) {
                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                    }
                    if (printerItem.isForceMultiLines) {
                        format.putBoolean("newline", false);
                    } else if (null != printerItem.value1.sValue) {
                        if (printerItem.value1.sValue.length() > 0) {
                            format.putBoolean("newline", false);
                        }
                    }
                    offsetX = printerEx.addText(format, printerItem.title.sValue, printerItem.printerMode);
                    if (null != printerItem.title.secValue) {
                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                        offsetX = printerEx.addText(format, printerItem.title.secValue, printerItem.printerMode);
                    }

//                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
//                            offsetX = printerEx.addText(format, printerItem.value1.sValue, printerItem.printerMode);
//                            Utility.DEBUG_LOG("fontPrinter",printerItem.value1.sValue + " " + printerItem.value.sValue + " " + printerItem.title.sValue );

                }
//                        if (printerItem.value1.sValue.length() > 0) {
//                            // font size
//                            fontsize = printerItem.value1.size;
//                            if (fontsize > 0) {
//                                format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
//                            }
//                            if (printerItem.value1.fontFile.length() > 0) {
//                                format.putString("fontStyle", printerItem.value1.fontFile);
//                            }
//                            // aligment
//                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
//                            if ((printerItem.value1.style & PrinterDefine.PStyle_align_left) != 0) {
//                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
//                            } else if ((printerItem.value1.style & PrinterDefine.PStyle_align_center) != 0) {
//                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
//                            } else if ((printerItem.value1.style & PrinterDefine.PStyle_align_right) != 0) {
//                                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
//                            }
//                            if (printerItem.isForceMultiLines) {
//                                format.putBoolean("newline", false);
//                            } else if (null != printerItem.value.sValue) {
//                                if (printerItem.value.sValue.length() > 0) {
//                                    format.putBoolean("newline", false);
//                                }
//                            }
//                            offsetX = printerEx.addText(format, printerItem.value1.sValue, printerItem.printerMode);
//                        }


                if (null != printerItem.value.sValue) {
                    if (printerItem.value.sValue.length() > 0) {
                        // font size
                        fontsize = printerItem.value.size;
                        if (fontsize > 0) {
                            format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
                        }
                        if (printerItem.value.fontFile.length() > 0) {
                            format.putString("fontStyle", printerItem.value.fontFile);
                        }
                        // aligment
                        format.putBoolean("newline", false);
                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                        if ((printerItem.value.style & PrinterDefine.PStyle_align_left) != 0) {
                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
                        } else if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                        } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                        }

                        printerEx.addText(format, printerItem.value.sValue, printerItem.printerMode);

                    }
                }
                break;


            case LINE:
                printerEx.addLine(null, printerItem.title.size);
                break;
            case FEED:
                printerEx.feedPixel(null, printerItem.title.size);
                break;
            case QRCODE:
                Bundle qrCodeFormat = new Bundle();
                boolean needScrollBack = false;
                if (printerItem.title.sValue.length() > 0 && printerItem.value.sValue.length() > 0) {
                    printerItem.title.style = PrinterDefine.PStyle_align_left;
                    if (printerItem.title.size > 180) {
                        printerItem.title.size = 180;
                    }
                    printerItem.value.style = PrinterDefine.PStyle_align_right;
                    if (printerItem.value.size > 180) {
                        printerItem.value.size = 180;
                    }

                    needScrollBack = true;
                }
                PrinterElement qrcode = printerItem.title;
                if (qrcode.sValue.length() > 0) {
                    if ((qrcode.style & PrinterDefine.PStyle_align_center) != 0) {
                        qrCodeFormat.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                    } else if ((qrcode.style & PrinterDefine.PStyle_align_right) != 0) {
                        qrCodeFormat.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                    }
                    // size
                    qrCodeFormat.putInt(PrinterConfig.addQrCode.Height.BundleName, qrcode.size);
                    printerEx.addQrCode(qrCodeFormat, qrcode.sValue);
                }

                qrcode = printerItem.value;
                if (qrcode.sValue.length() > 0) {
                    if ((qrcode.style & PrinterDefine.PStyle_align_center) != 0) {
                        qrCodeFormat.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                    } else if ((qrcode.style & PrinterDefine.PStyle_align_right) != 0) {
                        qrCodeFormat.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                    }
                    // size
                    qrCodeFormat.putInt(PrinterConfig.addQrCode.Height.BundleName, qrcode.size);
                    if (needScrollBack) {
                        printerEx.scrollBack();
                    }
                    printerEx.addQrCode(qrCodeFormat, qrcode.sValue);
                }

                break;
            case BARCODE:
                Bundle barcodeFormat = new Bundle();
                if ((printerItem.value.style & PrinterDefine.PStyle_align_center) != 0) {
                    barcodeFormat.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
                } else if ((printerItem.value.style & PrinterDefine.PStyle_align_right) != 0) {
                    barcodeFormat.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
                }
                barcodeFormat.putInt(PrinterConfig.addBarCode.Height.BundleName, printerItem.value.size);
                printerEx.addBarCode(barcodeFormat, printerItem.value.sValue);
                break;
//                    //case IMG_BCD:
//                        // title
//                        addString(printerItem.title);
//
//                        // image
//                        if (null != printerItem.value.sValue) {
//                            Utility.DEBUG_LOG(TAG, "add IMG_BCD, size" + printerItem.value.sValue.length());
//                            if (printerItem.value.sValue.length() > 0) {
//                                printerEx.addImage(null, (Utility.hexStr2Byte(printerItem.value.sValue)));
//                            }
//                        }
//                        break;
        }
        return fontsize;
    }

    // return true for paper printing, false for not
    public boolean print(PrinterListener printerListener) {



        Utility.DEBUG_LOG(TAG, "print() 2");
        if (null == printerItems) {
            initializeDefaltRecp();
        }
        int fontSizeTemp = 0;
        int fontSizeTotal = 0;
        boolean paperPrinting = false;
        printerEx = new PrinterEx();

        Bundle format = new Bundle();

        // bundle formate for AddTextInLine
        Bundle fmtAddTextInLine = new Bundle();

        Bundle fmtImage = new Bundle();
        try {
            byte[] bufferLogo = null;



            for (PrinterItem printerItem : printerItems)
            {
                fontSizeTemp = printInternal(format,fmtImage,printerItem,printerListener);
                fontSizeTotal += fontSizeTemp;
                Utility.DEBUG_LOG(TAG,"fontSizeTemp:"+fontSizeTemp);
                Utility.DEBUG_LOG(TAG,"fontSizeTotal:"+fontSizeTotal);
//
//                if (fontSizeTotal > 100 ) {
//                    // print to paper
//                    if (iPrinter != null) {
//                        format.putInt(PrinterConfig.addText.FontSize.BundleName, 0);
//                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
//                        iPrinter.addText(format, "    ");
//
//                        // add printerEx to iPrinter
//                        fmtImage.putInt("offset", 0);
//                        fmtImage.putInt("width", 384);  // bigger then actual, will print the actual
//                        fmtImage.putInt("height", printerEx.getHeight(true)); // bigger then actual, will print the actual
//                        Utility.DEBUG_LOG(TAG,"before getData 2");
//                        iPrinter.addImage(fmtImage, printerEx.getData(true));
//                        // printerEx -- end
//
//                        format.putInt(PrinterConfig.addText.FontSize.BundleName, 2);
//                        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
//                        iPrinter.addText(format, "--------------------");
//
//                        iPrinter.feedLine(2);
//
//                        // start print here
//                        iPrinter.startPrint(printerListener);
//
//                        paperPrinting = true;
//                    }
//
//                }
                Utility.DEBUG_LOG(TAG,"before printBitmapChunkWrapper 4");
                printBitmapChunkWrapper(printerListener,fmtImage,format,false);
            }

            if (true) {
                Utility.DEBUG_LOG(TAG,"Before printBitmapChunk 2e");
                this.printBitmapChunk(printerListener,fmtImage,format,true);
                paperPrinting = true;
            }
//            //
//            if (true) {
//                // print to paper
//                if (iPrinter != null) {
//                    format.putInt(PrinterConfig.addText.FontSize.BundleName, 0);
//                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
//                    iPrinter.addText(format, "    ");
//
//                    // add printerEx to iPrinter
//                    fmtImage.putInt("offset", 0);
//                    fmtImage.putInt("width", 384);  // bigger then actual, will print the actual
//                    fmtImage.putInt("height", printerEx.getHeight(true)); // bigger then actual, will print the actual
//                    Utility.DEBUG_LOG(TAG,"before getData 2");
//                    iPrinter.addImage(fmtImage, printerEx.getData(true));
//                    // printerEx -- end
//
//                    format.putInt(PrinterConfig.addText.FontSize.BundleName, 2);
//                    format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
//                    iPrinter.addText(format, "--------------------");
//
//                    iPrinter.feedLine(2);
//
//                    // start print here
//                    iPrinter.startPrint(printerListener);
//
//                    paperPrinting = true;
//                }
//
//            }

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Utility.DEBUG_LOG(TAG, "Exception :" + e.getMessage());
            for (StackTraceElement m : e.getStackTrace()
            ) {
                Utility.DEBUG_LOG(TAG, "Exception :" + m);

            }

        }
//        Intent intent = new Intent( context, PrintCanvasActivity.class);
//        context.startActivity(intent);

        return paperPrinting;

    }

    public void addString(PrinterElement printerElement) {
        int offsetX = 0;
        int fontsize = 1;
        Bundle format = new Bundle();
        format.putInt(PrinterConfig.addText.FontSize.BundleName, 1);
        format.putInt(PrinterConfig.addText.Alignment.BundleName, 2);
        format.putString("fontStyle", PrinterDefine.Font_default);
        format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);

        if (printerElement.sValue.length() > 0) {
            // font size
            fontsize = printerElement.size;
            if (fontsize > 0) {
                format.putInt(PrinterConfig.addText.FontSize.BundleName, fontsize);
            }
            if (printerElement.fontFile.length() > 0) {
                format.putString("fontStyle", printerElement.fontFile);
            }
            // aligment
            format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
            if ((printerElement.style & PrinterDefine.PStyle_align_left) != 0) {
                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.LEFT);
            } else if ((printerElement.style & PrinterDefine.PStyle_align_center) != 0) {
                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.CENTER);
            } else if ((printerElement.style & PrinterDefine.PStyle_align_right) != 0) {
                format.putInt(PrinterConfig.addText.Alignment.BundleName, PrinterConfig.addText.Alignment.RIGHT);
            }
            try {
                offsetX = printerEx.addText(format, printerElement.sValue, 3);
            } catch (RemoteException e) {
                e.printStackTrace();
            }

        }
    }


    PrinterListener printerListenerDef = new PrinterListener.Stub() {
        @Override
        public void onFinish() throws RemoteException {
            Utility.DEBUG_LOG(TAG, "Printer : Finish");
        }

        @Override
        public void onError(int error) throws RemoteException {
            Utility.DEBUG_LOG(TAG, "Printer error : " + error);
        }
    };

//    public List<TransactionDetail> getAllTransactions() throws ExecutionException, InterruptedException {
//        return new PrinterCanvas.GetTransactionsAsyncTaskCanvas().execute().get();
//    }
//    @SuppressLint("StaticFieldLeak")
//    private class GetTransactionsAsyncTaskCanvas extends AsyncTask<Void, Void,List<TransactionDetail>>
//    {
//        @Override
//        protected List<TransactionDetail> doInBackground(Void... voids) {
//            return appDatabase.transactionDetailDao().getAllTransactionsNotPreAuth();
//
//        }
//    }
}
