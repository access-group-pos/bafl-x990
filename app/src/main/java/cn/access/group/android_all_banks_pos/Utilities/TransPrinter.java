package cn.access.group.android_all_banks_pos.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.RemoteException;
import android.util.Log;

import com.vfi.smartpos.deviceservice.aidl.PrinterListener;

import java.util.List;

import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.HostInformation;
import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrintRecpSettlement;
import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrinterCanvas;
import cn.access.group.android_all_banks_pos.Utilities.canvas_printer.PrinterItem;
import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.contracts.TransactionContract;
import cn.access.group.android_all_banks_pos.repository.model.CountedTransactionItem;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

import static android.app.PendingIntent.getActivity;
import static cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication.hideProgressDialog;

/**
 * Created by Simon on 2019/2/1.
 */

public class TransPrinter extends PrinterCanvas {

    static final String TAG = "TransPrinter";
    PrinterCanvas pc;

    TransactionContract transactionContract = null;
    //    protected ISO8583 iso8583require;
//    protected ISO8583 iso8583response;
    HostInformation hostInformation;
   // TransactionDetail transactionDetail;
    List<TransactionDetail> transactionDetailList;
    List<CountedTransactionItem> countedTransactionItemList;

//    private boolean printFinished = false;
    // for print

    public void setTransactionContract(TransactionContract transactionContract)
    {
        this.transactionContract = transactionContract;
    }

    public TransPrinter(Context context) {
        super(context);
    }

    public void initializeData(List<TransactionDetail> transactionDetailList, List<CountedTransactionItem> countedTransactionItemList) {
        Utility.DEBUG_LOG(TAG, "initializeData");
        Utility.DEBUG_LOG(TAG, "initialize data with size:" + String.valueOf(transactionDetailList.size()));
        // pc = new PrinterCanvas(context,transactionDetailList, countedTransactionItemList);
        this.transactionDetailList = transactionDetailList;
        this.countedTransactionItemList = countedTransactionItemList;
        Utility.DEBUG_LOG(TAG,"Transprinter constructor(): List report "+SharedPref.read("ListReport",""));
        Utility.DEBUG_LOG(TAG, "initializedata with size:" + String.valueOf(transactionDetailList.size()));

        //pc.initializeData(transactionDetailList, countedTransactionItemList);
        for (PrinterItem p : PrinterItem.values()
        ) {
            p.restore();
        }


    }


    public void initializeData(TransactionDetail extraItems, String balInquiry, boolean redeemCheck, String copy) {
        this.hostInformation = hostInformation;

                Utility.DEBUG_LOG(TAG, "initializeData");
//        this.iso8583require = require;
//        this.iso8583response= response;


                for (PrinterItem p : PrinterItem.values()
                ) {
                    p.restore();
                }

                if (null != hostInformation) {
                    if (null != hostInformation.merchantName) {
                        if (hostInformation.merchantName.length() > 0) {
                            PrinterItem.MERCHANT_NAME.value.sValue = hostInformation.merchantName;
                        }
                    }
                    if (null != hostInformation.merchantID) {
                        if (hostInformation.merchantID.length() > 0) {
                            PrinterItem.MERCHANT_ID.value.sValue = hostInformation.merchantID;
                        }
                    }
                    if (null != hostInformation.terminalID) {
                        if (hostInformation.terminalID.length() > 0) {
                            PrinterItem.TERMINAL_ID.value.sValue = hostInformation.terminalID;
                        }

                    }
                    if (null != hostInformation.description) {
                        if (hostInformation.description.length() > 0) {
                            PrinterItem.HOST.value.sValue = hostInformation.description;
                        }

                    }
                }
        //this.transactionDetail = extraItems;

    }

    public void initializeData(List<CountedTransactionItem> cardTransactionItemList) {
        Utility.DEBUG_LOG(TAG, "initializeData");
//        this.iso8583require = require;
//        this.iso8583response= response;
        this.hostInformation = hostInformation;

        for (PrinterItem p : PrinterItem.values()
        ) {
            p.restore();
        }

        if (null != hostInformation) {
            if (null != hostInformation.merchantName) {
                if (hostInformation.merchantName.length() > 0) {
                    PrinterItem.MERCHANT_NAME.value.sValue = hostInformation.merchantName;
                }
            }
            if (null != hostInformation.merchantID) {
                if (hostInformation.merchantID.length() > 0) {
                    PrinterItem.MERCHANT_ID.value.sValue = hostInformation.merchantID;
                }
            }
            if (null != hostInformation.terminalID) {
                if (hostInformation.terminalID.length() > 0) {
                    PrinterItem.TERMINAL_ID.value.sValue = hostInformation.terminalID;
                }

            }
            if (null != hostInformation.description) {
                if (hostInformation.description.length() > 0) {
                    PrinterItem.HOST.value.sValue = hostInformation.description;
                }

            }
        }

//        if (null != require) {
//            PrinterItem.AMOUNT.value.sValue = require.getValue(ISO8583.ATTRIBUTE.Amount);
//            PrinterItem.CARD_NO.value.sValue = Utility.fixCardNoWithMask( require.getValue(ISO8583.ATTRIBUTE.Track2) );
//        }
//
//        if ( null != response) {
//            PrinterItem.DATE_TIME.value.sValue = response.getValue(ISO8583.ATTRIBUTE.Date) + " " + response.getValue(ISO8583.ATTRIBUTE.Time) ;
//        }


    }


    public void print() {
        Utility.DEBUG_LOG(TAG, "print()");
//        if ( transactionContract != null )
//        {
//            Utility.DEBUG_LOG(TAG,"Show printing on screen");
//            transactionContract.showProgress("Printing");
//        }\

        super.print(printerListener);

    }

    public void print(List<TransactionDetail> transactionDetailList) {
        Utility.DEBUG_LOG(TAG, "print(transactionDetailList)");
//        if ( transactionContract != null )
//        {
//            Utility.DEBUG_LOG(TAG,"Show printing on screen");
//            transactionContract.showProgress("Printing");
//        }
        super.print(printerListener, transactionDetailList);

    }


    PrinterListener printerListener = new PrinterListener.Stub() {
        @Override
        public void onFinish() throws RemoteException {
//            if ( transactionContract != null )
//            {
//                Utility.DEBUG_LOG(TAG,"hide printing on screen");
//                transactionContract.hideProgress();
//            }

            Utility.DEBUG_LOG(TAG, "Printer : Finish");
            SharedPref.write("ListReport","N");
//            super.printFinished = true;
            setPrintFinished(true);
            Utility.DEBUG_LOG(TAG, "Printer : Finish2");

//            ((Activity) context).runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                   hideProgressDialog();
//
//                }
//            });
//            ((Activity) context).runOnUiThread(new Runnable() {
//
//                @Override
//                public void run() {
//                    hideProgressDialog();
//                }
//            });



            //transactionContract.transactionSuccessCustomer("customer copy",transactionDetail,"Customer Copy",transactionDetail.getTxnType(),"123",false);

        }

        @Override
        public void onError(int error) throws RemoteException {

            Utility.DEBUG_LOG(TAG, "Printer error : " + error);
            if(error == 240){
                //TODO Assign PS_PaperOut here
                Utility.DEBUG_LOG(TAG,"Transprinter onError: List report "+SharedPref.read("ListReport",""));
                if(SharedPref.read("ListReport","") == "Y" ){
                    setPrintError(true);

//                    ((Activity) context).runOnUiThread(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            DialogUtil.confirmReprintDialog(context, "Reprint", sweetAlertDialog1 -> {
//                                sweetAlertDialog1.dismissWithAnimation();
//                                print(printerListener, transactionDetailList);
//
//                            });
//
//                        }
//                    });
                }
                else {
                    ((Activity) context).runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            DialogUtil.confirmReprintDialog(context, "Reprint", sweetAlertDialog1 -> {
                                sweetAlertDialog1.dismissWithAnimation();
                                print();
                            });

                        }
                    });
                }


            }
            hideProgressDialog();

        }
    };

    public Resources getResources() {
        return context.getResources();
    }

    public boolean isPrintFinished() {
        return super.printFinished;
    }


    public void setPrintFinished(boolean val)
    {
        Utility.DEBUG_LOG(TAG,"+ setPrintFinished + :" + val );
        super.printFinished = val;
        if ( val )
        {
            super.printerState = PrinterState.PS_Finish;
        }
    }

    public void setPrintError(boolean bool){

        Utility.DEBUG_LOG(TAG,"+ setPrintError + :" + bool );
        super.printFinished = bool;
        if ( bool )
        {
            super.printerState = PrinterState.PS_PaperOut;
        }
    }


}
