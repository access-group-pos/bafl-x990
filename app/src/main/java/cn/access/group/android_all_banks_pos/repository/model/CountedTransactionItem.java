package cn.access.group.android_all_banks_pos.repository.model;

public class CountedTransactionItem {
    public String txnType, cardScheme;
    public double totalAmount;
    public double totalTipAmount;

    public Integer count;


    public Double getTotalAmount() {
            return totalAmount;
        }

        public Double getTotaTiplAmount() {
            return totalTipAmount;
        }

    public Integer getCount() {
            return count;
        }

    @Override
    public String toString() {
            return cardScheme;
    }
    public String getTxnType()
    {
        return txnType;
    }

}
