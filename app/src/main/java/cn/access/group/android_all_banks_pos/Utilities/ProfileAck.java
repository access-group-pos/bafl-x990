package cn.access.group.android_all_banks_pos.Utilities;

import android.content.Context;
import android.os.PowerManager;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vfi.smartpos.system_service.aidl.ISystemManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.access.group.android_all_banks_pos.applicationContextHelper.MainApplication;
import cn.access.group.android_all_banks_pos.repository.Repository;
import cn.access.group.android_all_banks_pos.repository.database.AppDatabase;
import cn.access.group.android_all_banks_pos.repository.model.TransactionDetail;

import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalPassword;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUrl;
import static cn.access.group.android_all_banks_pos.Utilities.Constants.PortalUserName;

public class ProfileAck {
    Repository repository;
        public static void makeJsonObjReq(Context context, ISystemManager iSystemManager) {
            RequestQueue queue = Volley.newRequestQueue(context);
            String url = PortalUrl + "/baflserviceprofileack";
//            String url = "";
            Utility.DEBUG_LOG("url",url);
            String currentTime = new SimpleDateFormat("HHmmss", Locale.getDefault()).format(new Date());
            String currentDate = new SimpleDateFormat("MMddyy", Locale.getDefault()).format(new Date());
            String POSDateTime = currentTime + currentDate;
            Map<String, String> postParam = new HashMap<String, String>();
            postParam.put("UserName",PortalUserName);
            postParam.put("PASSWORD",PortalPassword);
            postParam.put("imei","123");
            postParam.put("telco","123");
            postParam.put("latitude","123");
            postParam.put("longitude","123");
            postParam.put("fieldOne","123");
            postParam.put("fieldTwo","123");
            postParam.put("fieldThree","123");
            postParam.put("fieldFour","123");
            postParam.put("fieldFive","123");
            postParam.put("fieldSix","123");
            postParam.put("PosDateTime",POSDateTime);
            postParam.put("Model", "X990");
            postParam.put("SerialNumber",  Constants.TERMINAL_SERIAL);
            postParam.put("TID", Constants.TERMINAL_ID);
            postParam.put("APPNAME", "1.0.5");
            postParam.put("MID", Constants.MERCHANT_ID);
            Utility.DEBUG_LOG("params", String.valueOf(postParam));


            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, new JSONObject(postParam),
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Toast.makeText(context,"Response :" + response.getString("Reponsemessage"), Toast.LENGTH_LONG).show();
//                                repository = new Repository(appDatabase);
//                                // repository.getTerminalConfiguration();
//                                try {
//                                    iSystemManager.reboot();
//                                } catch (RemoteException e) {
//                                    e.printStackTrace();
//                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Utility.DEBUG_LOG("Volley", response.toString());
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Utility.DEBUG_LOG("Volley", "Error: " + error.getMessage());
                    //DialogUtil.errorDialog(context,"Error!", "Connection Failed!");
                    error.printStackTrace();
                }
            }) {

                /**
                 * Passing some request headers
                 * */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };
            new NukeSSLCerts().nuke();

            jsonObjReq.setTag("Volley");
            // Adding request to request queue
            queue.add(jsonObjReq);

            // Cancelling request
//     if (queue!= null) {
//    queue.cancelAll(TAG);
        }


        public static void downloadAcknowledgement(Context context) {
        RequestQueue queue = Volley.newRequestQueue(context);
        MainApplication.hideProgressDialog();

        //String url = "https://125.209.123.231/BAFLPORTAL/baflservicetxn";
        //String url="http://172.191.1.219:9090/BAFL-PORTAL/baflserviceprofileack";
        String url =  PortalUrl + "/baflservicefileack";
//            String url = "";
        Utility.DEBUG_LOG("url3",url);
        String currentTime = new SimpleDateFormat("HHmmss", Locale.getDefault()).format(new Date());
        String currentDate = new SimpleDateFormat("MMddyy", Locale.getDefault()).format(new Date());
        String POSDateTime = currentTime + currentDate;
        Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("imei","124314123");
        postParam.put("telco","13151251");
        postParam.put("latitude","1.909");
        postParam.put("longitude","1.213123");
        postParam.put("fieldOne","signal strength");
        postParam.put("fieldTwo","media interface");
        postParam.put("fieldThree","cpu memory");
        postParam.put("fieldFour","battery");
        postParam.put("fieldFive","OS:ADF:FW");
        postParam.put("fieldSix","APP VERSION");
        postParam.put("Model", "0.00");
        postParam.put("SerialNumber",  Constants.TERMINAL_SERIAL);
        postParam.put("TID", Constants.TERMINAL_ID);
        postParam.put("APPNAME", "1.0.5=7");
        postParam.put("MID", Constants.MERCHANT_ID);
        Utility.DEBUG_LOG("params", String.valueOf(postParam));


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, new JSONObject(postParam),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Toast.makeText(context,"Response :" + response.getString("Reponsemessage"), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Utility.DEBUG_LOG("Volley", response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Utility.DEBUG_LOG("Volley", "Error: " + error.getMessage());
                //DialogUtil.errorDialog(context,"Error!", "Connection Failed!");
                error.printStackTrace();
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        new NukeSSLCerts().nuke();

        jsonObjReq.setTag("Volley");
        // Adding request to request queue
        queue.add(jsonObjReq);

        // Cancelling request
//     if (queue!= null) {
//    queue.cancelAll(TAG);
    }


        public static class NukeSSLCerts {
            protected static final String TAG = "NukeSSLCerts";

            public static void nuke() {
                try {
                    TrustManager[] trustAllCerts = new TrustManager[] {
                            new X509TrustManager() {
                                public X509Certificate[] getAcceptedIssuers() {
                                    X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                                    return myTrustedAnchors;
                                }

                                @Override
                                public void checkClientTrusted(X509Certificate[] certs, String authType) {}

                                @Override
                                public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                            }
                    };

                    SSLContext sc = SSLContext.getInstance("SSL");
                    sc.init(null, trustAllCerts, new SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String arg0, SSLSession arg1) {
                            return true;
                        }
                    });
                } catch (Exception e) {
                }
            }
        }

    }


