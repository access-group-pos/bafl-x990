package cn.access.group.android_all_banks_pos.Utilities;

import java.util.Locale;

import cn.access.group.android_all_banks_pos.BuildConfig;

/**
 * on 10/10/2019.
 */
public  class  Constants {
    public static boolean onBackward = false;
    public static String CHECKSUM = "";

    public static String URL = "";
    public static String hostIP = SharedPref.read("hostIP","");
    public static int hostPort = SharedPref.read("hostPort");
    public static boolean MUST_SETTLE = false;
    public static boolean CUST_COPY = false;
    public static boolean BANK_COPY = false;
    public static String INVOICE_NO;
    public static String TRANSACTION_TYPE_PREAUTH;
    ///public static String BATCH_NO = "";  /// Masood's code updated on 15 Jan 2021 for Batch from setting.
    //public static String BATCH_NO = String.format(Locale.getDefault(),"%06d", SharedPref.read("batch_no"));
    public static String BATCH_NO = String.format(Locale.getDefault(),"%06d", (int)21);
    public static String REVERSAL_MESSAGE_TYPE= "0400";
    public static String SALE_MESSAGE_TYPE= "0200"; //void 0400
    public static String ADVICE_MESSAGE_TYPE= "0220";
    public static String PREAUTH_MESSAGE_TYPE= "0100"; //void 0400
    public static String SETTLEMENT_MESSAGE_TYPE= "0500";
    public static String BATCH_UPLOAD_MESSAGE_TYPE = "0320";
    public static String SALE_PROCESSING_CODE="000000";
    public static String SALEIPP_PROCESSING_CODE="003000";
    public static String CASH_OUT_PROCESSING_CODE="010000";
    public static String CASH_OUT_MESSAGE_TYPE= "0200";
    public static String CASH_ADV_PROCESSING_CODE="010000";
    public static String CASH_ADV_MESSAGE_TYPE= "0200";
    public static String REFUND_MESSAGE_TYPE= "200000";
    public static String COMPLETION_PROCESSING_CODE="010000";
    public static String COMPLETION_MESSAGE_TYPE= "0200";
    public static String PRE_AUTH_PROCESSING_CODE="030000";
    public static String ORBIT_REDEEM_PROCESSING_CODE = "000070";
    public static String ORBIT_REDEEM_MESSAGE_TYPE = "0200";
    public static String ORBIT_INQUIRY_PROCESSING_CODE = "310070";
    public static String ORBIT_INQUIRY_MESSAGE_TYPE = "0100";
    public static String VOID_PROCESSING_CODE="020000";
    public static String VOID_PROCESSING_CODE_REDEEM="020070";
    public static String SETTLEMENT_PROCESSING_CODE="920000";
    public static String SETTLEMENT_PROCESSING_CODE_CLOSE="960000";
    public static String TERMINAL_ID= SharedPref.read("TERMINAL_ID","");
    public static String MERCHANT_ID=SharedPref.read("MERCHANT_ID","");
    public static String MERCHANT_CATEGORY_CODE="7011";
    public static String MERCHANT_CATEGORY_CODE_CHIP="5311";
    public static String MERCHANT_CATEGORY_CODE_CTLS="7011";
    public static String ACQUIRING_INSTITUTION="586";
    public static String POS_ENTRY_MODE="022";
    public static String POS_ENTRY_MODE_CHIP="052";
    public static String POS_ENTRY_MODE_CTLS="072";
    public static String NII=SharedPref.read("NII","");
    public static String TIP_ENABLED=SharedPref.read("TIP_ENABLED","");
    public static String POS_CONDITION_CODE="00";
    public static String POS_CONDITION_CODE_CHIP="06";
    public static String POS_CONDITION_CODE_CTLS="06";
    public static String ACQUIRING_INSTITUTION_IDENTIFICATION_CODE="35370586";
    public static String FORWARDING_INSTITUTION_IDENTIFICATION_CODE="47740586";
    public static String TRANSACTION_CURRENCY_CODE="586";
    public static String RESERVED_NATIONAL="00000600030000000000001";
    public static String CARD_ACCEPTOR_NAME_LOCATION= "AG UPI PAKISTAN          KARACHI     PAK";
    public static String SECURITY_RELATED_CONTROL_INFORMATION ="2600000000000000";
    public static String MERCHANT_NAME=SharedPref.read("merchantName","");
    public static int HOST_INDEX_FROM_DB;//0=UBL,... this determines which host settings are being used
    public static String FIELD_61 = "01";
    public static String TERMINAL_SERIAL = "";
    public static String COM_ETHERNET = "123";
    public static String COM_GPRS = "123";
    public static String ENABLE_LOGS = "123";
    public static String MARKET_SEG = "123";
    public static String MAINTENANCE_THRESHOLD = "0";
    public static String TIP_THRESHOLD = "123";
    public static String FALLBACK_ENABLE = "123";
    public static String MSGHEADER_LENHEX = "123";
    public static String SSL_ENABLE = SharedPref.read("SSL_ENABLE","");
    public static String TOPUP_ENABLE = "123";
    public static String MANUAL_ENTRY = "123";
    public static String AUTO_SETTLEMENT = "Y";
    public static String EMV_LOGS = "123";
    public static String AUTO_SETTLETIME = "123";
    public static String AMEX_ID = "123";
    public static String PIN_BYPASS = "123";
    public static String MOD10 = "123";
    public static String CHECK_EXPIRY = "123";
    public static String MANUAL_ENTRY_ISSUER = "123";
    public static String LAST_4DIGIT_MAG = "123";
    public static String LAST_4DIGIT_ICC = "123";
    public static String ACQUIRER = "123";
    public static String LOW_BIN = "123";
    public static String HIGH_BIN = "123";
    public static String ISSUER = "123";
    public static String CURRENCY = "123";
    public static String USER_PASSWORD_CHANGE = "4848";
    public static String MANAGER_PASSWORD_CHANGE = "4848";
    public static String SYSTEM_PASSWORD_CHANGE = "4848";
    public static String SUPER_USER_PASSWORD_CHANGE = "4848";
    public static String HEADER_LINE_1 = SharedPref.read("HEADER_LINE_1", "");;
    public static String HEADER_LINE_2 = SharedPref.read("HEADER_LINE_2", "");;
    public static String HEADER_LINE_3 = SharedPref.read("HEADER_LINE_3", "");;
    public static String HEADER_LINE_4 = SharedPref.read("HEADER_LINE_4", "");;
    public static String FOOTER_LINE_1 = "123";
    public static String FOOTER_LINE_2 = "123";
    public static String PRI_MEDIUM = "SYSTEM";
    public static String SEC_MEDIUM = "SYSTEM";
    //public static String BATCH_NO = String.format( Locale.getDefault(),"%06d",SharedPref.read("batch_no"));
    public static String TMS_IP = "SYSTEM";
    public static String TMS_PORT = "SYSTEM";
    public static String SEC_IP =  SharedPref.read("SEC_IP","");
    public static int SEC_PORT =  SharedPref.read("SEC_PORT");
//    public static String CVM_LIMIT =  SharedPref.read("CVM_LIMIT","300.00");//TODO: this will no longer be used
    public static String CVM_LIMIT_VISA =  SharedPref.read("CVM_LIMIT_VISA","");
    public static String CVM_LIMIT_MC =  SharedPref.read("CVM_LIMIT_MC","");
    public static String CVM_LIMIT_UPI =  SharedPref.read("CVM_LIMIT_UPI","");
    public static String CVM_LIMIT_PAYPAK =  SharedPref.read("CVM_LIMIT_PAYPAK","");
    public static String CVM_LIMIT_JCB =  SharedPref.read("CVM_LIMIT_JCB","");
    public static String CVM_LIMIT_AMEX =  SharedPref.read("CVM_LIMIT_AMEX","");

    public static String SALE_OFFLINE_LIMIT =  SharedPref.read("SALE_OFFLINE_LIMIT","0.00");
    public static String SALE_MIN_AMOUNT_LIMIT =  SharedPref.read("SALE_MIN_AMOUNT_LIMIT","0.00");
    public static String SALE_MAX_AMOUNT_LIMIT =  SharedPref.read("SALE_MAX_AMOUNT_LIMIT","999999.99");

    public static String VEPS_LIMIT =  SharedPref.read("VEPS_LIMIT","0.00");
    public static String VEPS_LIMIT_MC =  SharedPref.read("VEPS_LIMIT_MC","0.00");
    public static String VEPS_LIMIT_JCB =  SharedPref.read("VEPS_LIMIT_JCB","0.00");
    public static String VEPS_LIMIT_PAYPAK =  SharedPref.read("VEPS_LIMIT_PAYPAK","0.00");
    public static String VEPS_LIMIT_UPI =  SharedPref.read("VEPS_LIMIT_UPI","0.00");
    public static String VEPS_LIMIT_AMEX =  SharedPref.read("VEPS_LIMIT_AMEX","0.00");

    public static String PortalUrl =  SharedPref.read("PortalUrl",""); /// UAT/Live Merchant URL https://103.232.225.125/BAFL-PORTAL
//    public static String PortalUrl =  "https://125.209.123.231/BAFL-PORTAL";  /// TEST Merchant URL
    //public static String PortalUrl =  "http://172.191.1.198:9090/BAFL-PORTAL"; /// TEST Merchant URL
    public static String APP_VERSION_NAME =  BuildConfig.VERSION_NAME;
    public static int APP_VERSION_CODE = BuildConfig.VERSION_CODE;
    public static String PortalUserName =  SharedPref.read("PortalUserName","bafl.service");
    public static String PortalPassword =  SharedPref.read("PortalPassword","bafl.service!");
    public static String PortalTimeout =  SharedPref.read("PortalTimeout","3");
    public static String PortalTxnUpload =  SharedPref.read("PortalTxnUpload","Y");

    public static int  SETTLECOUNTER = SharedPref.read("settleCounter");
    public static int   SETTLERANGE = SharedPref.read("settleRange");
    public static int TIPNUMADJUST = SharedPref.read("tipNumAdjust");
    public static int TIPNUMADJUSTCOUNTER;
    public static String CARD_TYPE_NAME;
    public static int LOWER_RANGE;
    public static int HIGHER_RANGE;
    public static String CARD_LENGTH;
    public static String CARD_ISSUER;
    public static String ISS_NAME;
    public static String ISS_NUM;
    public static String  ISS_ACQUIRER;
    public static String ISS_SCHEME;
    public static String ISS_BANK_FUND;
    public static String  ISS_MOD10;
    public static String ISS_CHECK_EXPIRY;
    public static String ISS_MANUAL_ENTRY;
    public static String ISS_PRINT_EXPIRY;
    public static String ISS_LAST4DIGIT_MAG;
    public static String ISS_LAST4DIGIT_SMART;
    public static String ISS_PIN_BYPASS;
    public static String ISS_CHSERVE_CODE;
    public static String ISS_CHECKPIN;
    public static String ISS_CHSIGN;
    public static String ISS_ISSUERACTIVATED;
    public static String ISS_REFUND_ENABLE;
    public static String ISS_VOID_ENABLE;
    public static String ISS_PREAUTH_ENABLE;
    public static String ISS_COMPLETION_ENABLE;
    public static String ISS_ADJUST_ENABLE;
    public static String ISS_CASHADV_ENABLE;
    public static String ISS_ORBITREDEEM_ENABLE;
    public static String ISS_FORCEDlBP_ENABLE;
    public static Boolean isEcrReq = false;
    public static String ecrAmount = "0.00";
    public static String ecrRefNo = "0.00";
    public static Boolean firstBootUp = true;
    public static String ecrType = SharedPref.read("ecrType","");
    public static String selectedECRMENU = "M";
    public static String isEcrEnable = SharedPref.read("ecrEnabled","");

    public static String CARD_AUTH_METHOD = "SIGNATURE";
    public static String keysType = "P";
    public static String last_settle_time="03/05/2021";
    public static Boolean autoSettleTask = false;
    public static int ecrOfflineCounter = 0;
}
