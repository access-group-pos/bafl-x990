package cn.access.group.android_all_banks_pos.contracts;

/**
 * on 10/7/2019.
 */
public interface VoidContract {
    void askConfirmation();
}
